<?php

namespace app\models;

use app\helpers\MainHelper;
use app\modules\backend\controllers\RegistrationController;
use app\modules\frontend\models\UserGroups;
use dektrium\user\helpers\Password;
use dektrium\user\models\User as BaseUser;
use Yii;

class User extends BaseUser
{

    public $fullName;

    public function getCommentatorAvatar()
    {
        $url = 'http://gravatar.com/avatar/' . $this->profile->gravatar_id;
        return $url;
    }

    public function getCommentatorName()
    {
        return $this->username;

    }

    public function getCommentatorUrl()
    {
        return ['/user/settings/profile', 'id' => $this->id]; // or false, if user does not have a public page
    }

    //set role student
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $auth = Yii::$app->authManager;
        $role = $auth->getRole('student');
        if (!\Yii::$app->authManager->getRolesByUser($this->id)) {
            $auth->assign($role, $this->id);
        }


        return true;
    }

    public function getUserGroups()
    {
        // return $this->hasMany(UserGroups::className(), ['USER_ID' => 'ID']);
    }

    /**
     * get full is isset gavatar
     * @return string
     */
    public function getAvatarImage()
    {
        $url = 'http://gravatar.com/avatar/' . $this->profile->gravatar_id;
        return $url;
    }

    /**
     * Get role user
     * @return int|string
     */
    public function getUserRole()
    {
        $roles = \Yii::$app->authManager->getRolesByUser($this->id);

        $role = '';

        foreach ($roles as $key => $value) {
            $role = $key;
        }

        return $role;
    }

    /**
     * Extend labels of dectrium/user/User model
     * @return array
     */
    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        $newattributeLabels = [
            'userRole' => Yii::t('B-User', 'User role'),
            'fullName' => Yii::t('B-User', 'fullName'),
        ];
        return array_merge($attributeLabels, $newattributeLabels);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentGroups()
    {
        return $this->hasMany(StudentGroup::className(), ['student_id' => 'id']);
    }

    /**
     * Метод создает пользователя в системе
     * Методом afterSave назначается роль
     * @see RegistrationController
     * @param $username
     * @return User|array
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public static function createUserByUserName($username)
    {

        $model = new User();
        $password = Password::generate(8);
        $model->username = $username;
        $model->email = $model->username . "@to.change";
        $model->password = $password;
        $model->auth_key = Yii::$app->security->generateRandomString();
        $model->confirmed_at = time();
        $model->updated_at = time();
        if ($model->save(false)) {
            $model->password = $password; //unhash pass
            return $model;
        } else {
            return $model->errors;
        }

    }

    /**
     * Генерация логина по фамилии
     * Generated random login
     * @return string
     */
    public static function GenerateLogin($surname, $groupId)
    {
        $login = MainHelper::translit($surname) . '-' . $groupId . '-' . MainHelper::generateRandomString(3);

        return $login;
    }


}
