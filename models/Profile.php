<?php

/**
 * This is the model class for table "profile".
 *
 * @property string $surname Фамилия
 * @property string $middlename Отчество
 *
 * @property \app\modules\frontend\models\Awards $awards
 * @property \app\modules\frontend\models\Projects $projects
 * @property \app\modules\frontend\models\Conferences $conferences
 * @property \app\modules\frontend\models\Degree $degrees
 * @property \app\modules\frontend\models\Intellectual $intellectuals
 * @property \app\modules\frontend\models\Internship $internships
 * @property \app\modules\frontend\models\Publication $publications
 * @property \app\modules\frontend\models\Qualification $qualifications
 * @property \app\modules\frontend\models\Reviews $reviews
 * @property \app\modules\frontend\models\SocialActivities $socialActivities
 *
 * @property \app\modules\backend\models\Groups[] $groups
 * @property \app\modules\backend\models\StudentGroup[] $studentGroups
 */

namespace app\models;

use app\modules\backend\models\Groups;
use app\modules\backend\models\StudentGroup;
use app\modules\frontend\models\Awards;
use app\modules\frontend\models\Conferences;
use app\modules\frontend\models\Degree;
use app\modules\frontend\models\Intellectual;
use app\modules\frontend\models\Internship;
use app\modules\frontend\models\Projects;
use app\modules\frontend\models\Publication;
use app\modules\frontend\models\Qualification;
use app\modules\frontend\models\Reviews;
use app\modules\frontend\models\SocialActivities;
use Yii;
use yii\helpers\ArrayHelper;

class Profile extends \dektrium\user\models\Profile
{
    public $photoImg;

    /**
     * Returns avatar url or gravatar
     * @param  int $size
     * @return string
     */
    public function getAvatarUrl($size = 200)
    {

        return $this->photo_url ? Yii::getAlias('@web') . '/' . $this->photo_url : '//gravatar.com/avatar/' . $this->gravatar_id . '?s=' . $size;
    }

    /**
     * Make full name of user, lilke Surname Name Middlename
     * @return string
     */
    public function getFullName()
    {
        return $this->surname . " " . $this->name . " " . $this->middlename;
    }

    /**
     * Extend rules of dectrium/user/Profile model
     * @return array
     */
    public function rules()
    {

        $rules = parent::rules();
        $newRules = [
            'surname' => ['surname', 'string', 'max' => 255],
            'middlename' => ['middlename', 'string', 'max' => 255],
            'photo_url' => ['middlename', 'string', 'max' => 255],
            'photoImg' => ['photoImg', 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],

        ];
        return array_merge($rules, $newRules);

    }

    /**
     * Extend attribute labels of dectrium/user/Profile model
     * @return array
     */
    public function attributeLabels()
    {
        $attributeLabels = parent::attributeLabels();
        $newattributeLabels = [
            'surname' => 'Фамилия',
            'middlename' => 'Отчество',
        ];
        return array_merge($attributeLabels, $newattributeLabels);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentGroups()
    {
        return $this->hasMany(StudentGroup::className(), ['student_id' => 'user_id']);

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Groups::className(), ['id' => 'group_id'])
            ->via('studentGroups');

    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAwards()
    {
        return $this->hasMany(Awards::className(), ['USER_ID' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Projects::className(), ['USER_ID' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConferences()
    {
        return $this->hasMany(Conferences::className(), ['USER_ID' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDegrees()
    {
        return $this->hasMany(Degree::className(), ['USER_ID' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntellectuals()
    {
        return $this->hasMany(Intellectual::className(), ['USER_ID' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInternships()
    {
        return $this->hasMany(Internship::className(), ['USER_ID' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublications()
    {
        return $this->hasMany(Publication::className(), ['USER_ID' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQualifications()
    {
        return $this->hasMany(Qualification::className(), ['USER_ID' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), ['USER_ID' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialActivities()
    {
        return $this->hasMany(SocialActivities::className(), ['USER_ID' => 'user_id']);
    }

    /**
     * @param $role
     * @return null|Profile[]
     */
    public static function findAllByRole($role)
    {
        return static::find()
            ->join('LEFT JOIN', 'auth_assignment', 'auth_assignment.user_id = profile.user_id')
            ->where(['auth_assignment.item_name' => $role])
            ->all();
    }

    /**
     * Метод возвращает название группы и год обучения
     * В которой последний раз учился студент
     * @return array|string
     */
    public function getLastStudentGroup($userGroups)
    {

        $defaultYearEnd = date('Y', '0000'); // любая дефолтная дата
        foreach ($userGroups as $userGroup) {
            $realDateEnd = new \DateTime($userGroup->group->idYear->year_end);
            if ($realDateEnd->format('Y') > $defaultYearEnd) {
                $userLastStudentGroup = $userGroup;
            }
        }
        return isset($userLastStudentGroup) ? $userLastStudentGroup : 'Error';
    }

    /**
     * Метод готовит данные для отображения профиля пользователя
     * @param $id
     * @return array
     */
    public static function getStudentProfile($id)
    {
        $student = self::findOne(['user_id' => $id]); // Получаем профиль
        $studentGroups = StudentGroup::find()->with([
            'group.idYear',
            'group.groupsDisciplines.discipline',
            'group.groupsDisciplines.teacher',
            'student.awards',
            'student.awards.dOCUMENT',
            'student.awards.oRGANIZATION',
            'student.projects',
            'student.conferences',
            'student.conferences.sTATUS',
            'student.conferences.kINDINVOVLVEMENT',
            'student.degrees',
            'student.degrees.dOCUMENT',
            'student.degrees.oRGANIZATION',
            'student.intellectuals',
            'student.intellectuals.oRGANIZATION',
            'student.internships',
            'student.internships.oRGANIZATION',
            'student.publications',
            'student.publications.tYPEEDITION',
            'student.qualifications',
            'student.qualifications.oRGANIZATION',
            'student.reviews',
            'student.reviews.oRGANIZATION',
            'student.socialActivities',
            'student.socialActivities.sTATUS',
            'student.socialActivities.tYPEEVENT',
            'student.socialActivities.tYPEPARTICIPATION',
            'student.socialActivities.lEVEL'
        ])->where(['student_id' => $id])->asArray()->all(); // Группы студента
        ArrayHelper::multisort($studentGroups, 'p_id', SORT_DESC);
        $out = [
            'student' => $student,
            'studentGroups' => $studentGroups
        ];
        return $out;
    }

    /**
     * @return Profile[]|StudentGroup[]|array|\yii\db\ActiveRecord[]
     */
    public static function getStudentsWithGroups(){
        return static::find()
            ->with('groups','groups.idYear')
            ->join('LEFT JOIN', 'auth_assignment', 'auth_assignment.user_id = profile.user_id')
            ->where(['auth_assignment.item_name' => 'student'])
            ->all();
    }

    /**
     * Метод изменяет автоматически созданный профайл пользователя
     * @param $middlename
     * @param $name
     * @param $surname
     * @param $userModel
     * @return array|null|static
     */
    public static function createProfile($middlename, $name, $surname, $userModel)
    {
        $profile = self::findOne(['user_id' => $userModel->id]);
        $profile->middlename = $middlename;
        $profile->name = $name;
        $profile->surname = $surname;
        if ($profile->save()){
            return $profile;
        } else{
            return $profile->errors;
        }

    }
}