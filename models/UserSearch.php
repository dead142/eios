<?php

namespace app\models;

use Yii;
use dektrium\user\models\UserSearch as BaseUserSearch;


class UserSearch extends BaseUserSearch
{
    public $userRole;
    public $fullName;

    /**
     * Extend rules of dectrium/user/UserSearch model
     * @return array
     */
    public function rules()
    {
        $parentRules = parent::rules();
        $rules = [
            [['userRole', 'fullName'], 'safe']

        ];
        return array_merge($parentRules, $rules);
    }

    /**
     * Extend attributeLabels of dectrium/user/UserSearch model
     * @return array
     */
    public function attributeLabels()
    {
        $parentLabels = parent::attributeLabels();
        $labels = [
            'userRole' => Yii::t('B-User', 'User role'),
            'fullName' => Yii::t('B-User', 'User role'),
        ];
        return array_merge($parentLabels, $labels);
    }

    /**
     * Extend search of dectrium/user/UserSearch model
     * @return dataProvider
     */
    public function search($params)
    {
        $dataProvider = parent::search($params);
        $dataProvider->query->joinWith('profile');
        $dataProvider->query->leftJoin('auth_assignment', '`auth_assignment`.`user_id` = `user`.`id`');
        $dataProvider->setSort([
                'attributes' => [
                    'id',
                    'email',
                    'created_at',
                    'last_login_at',
                    'userRole' => [
                        'asc' => [
                            'auth_assignment.item_name' => SORT_ASC,
                        ],
                        'desc' => [
                            'auth_assignment.item_name' => SORT_DESC,
                        ],
                    ],
                    'fullName' => [
                        'asc' => [

                            'profile.surname' => SORT_ASC,
                            'profile.name' => SORT_ASC,
                            'profile.middlename' => SORT_ASC,

                        ],
                        'desc' => [

                            'profile.surname' => SORT_DESC,
                            'profile.name' => SORT_DESC,
                            'profile.middlename' => SORT_DESC,

                        ],

                        'default' => SORT_ASC
                    ],
                ],
            ]
        );

        $dataProvider->query->andFilterWhere(['like', 'auth_assignment.item_name', $this->userRole]);
        $dataProvider->query->andFilterWhere([
                'or',
                ['like', 'profile.surname', $this->fullName],
                ['like', 'profile.name', $this->fullName],
                ['like', 'profile.middlename', $this->fullName]
            ]
        );
        return $dataProvider;
    }
}