<?php

use yii\db\Migration;

/**
 * Class m180607_222014_delete_status_id_field_from_student_group
 */
class m180607_222014_delete_status_id_field_from_student_group extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'FK_student_group_student_status_id',
            'student_group'
        );
        $this->dropIndex(
            'IDX_student_year_status_id',
            'student_group'
        );
        $this->dropColumn('student_group','status_id');




    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180607_222014_delete_status_id_field_from_student_group cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180607_222014_delete_status_id_field_from_student_group cannot be reverted.\n";

        return false;
    }
    */
}
