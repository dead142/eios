<?php

use yii\db\Migration;

/**
 * Class m180604_185526_add_status_column_in_groups_table
 */
class m180604_185526_add_status_column_in_groups_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('groups','active',$this->boolean()->defaultValue(true));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('groups','active');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180604_185526_add_status_column_in_groups_table cannot be reverted.\n";

        return false;
    }
    */
}
