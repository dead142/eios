<?php

use yii\db\Migration;

/**
 * Class m180529_233558_edit_enum_field_in_groups_discipline
 */
class m180529_233558_edit_enum_field_in_groups_discipline extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('groups_discipline','semestr_id',$this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {


        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180529_233558_edit_enum_field_in_groups_discipline cannot be reverted.\n";

        return false;
    }
    */
}
