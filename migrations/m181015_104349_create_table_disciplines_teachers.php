<?php

use yii\db\Migration;

class m181015_104349_create_table_disciplines_teachers extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%disciplines_teachers}}', [
            'id' => $this->primaryKey(),
            'teacher_id' => $this->integer()->notNull(),
            'discipline_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_bt' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('IDX_diciplines_teachers_discip', '{{%disciplines_teachers}}', 'discipline_id');
        $this->createIndex('IDX_diciplines_teachers_teache', '{{%disciplines_teachers}}', 'teacher_id');
        $this->addForeignKey('FK_diciplines_teachers_disciplines_id', '{{%disciplines_teachers}}', 'discipline_id', '{{%disciplines}}', 'id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('FK_diciplines_teachers_profile_user_id', '{{%disciplines_teachers}}', 'teacher_id', '{{%profile}}', 'user_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%disciplines_teachers}}');
    }
}
