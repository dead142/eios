<?php

use yii\db\Migration;

/**
 * Class m181015_111136_delete_teacher_id_discipline_id_from_groups_disciplines
 */
class m181015_111136_delete_discipline_id_from_groups_disciplines extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'FK_groups_discipline_groups_id',
            'groups_discipline'
        );

        $this->dropIndex(
            'IDX_groups_discipline_dicipline_id',
            'groups_discipline'
        );
        $this->dropColumn('groups_discipline','discipline_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181015_111136_delete_teacher_id_discipline_id_from_groups_disciplines cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181015_111136_delete_teacher_id_discipline_id_from_groups_disciplines cannot be reverted.\n";

        return false;
    }
    */
}
