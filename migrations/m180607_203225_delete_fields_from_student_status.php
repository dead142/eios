<?php

use yii\db\Migration;

/**
 * Class m180607_203225_delete_fields_from_student_status
 */
class m180607_203225_delete_fields_from_student_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropIndex(
            'FK_st_status_st_group_id',
            'student_status'
        );

        $this->dropIndex(
            'group_id',
            'student_status'
        );

        $this->dropColumn('student_status','group_id');

        $this->dropIndex(
            'FK_st_status_year_id',
            'student_status'
        );

        $this->dropIndex(
            'year_id',
            'student_status'
        );

        $this->dropColumn('student_status','year_id');

        $this->dropIndex(
            'FK_student_status_profile_user_id',
            'student_status'
        );
        $this->dropColumn('student_status','profile_id');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180607_203225_delete_fields_from_student_status cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180607_203225_delete_fields_from_student_status cannot be reverted.\n";

        return false;
    }
    */
}
