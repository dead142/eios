<?php

use yii\db\Migration;

class m171229_013816_create_table_rating extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%rating}}', [
            'id' => $this->integer(11)->notNull()->append('AUTO_INCREMENT PRIMARY KEY'),
            'url' => $this->string(255)->notNull(),
            'grade' => $this->integer(11)->notNull()->defaultValue('0'),
            'count_vote' => $this->integer(11)->notNull()->defaultValue('0'),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%rating}}');
    }
}
