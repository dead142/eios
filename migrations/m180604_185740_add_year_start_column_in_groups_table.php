<?php

use yii\db\Migration;

/**
 * Class m180604_185740_add_year_start_column_in_groups_table
 */
class m180604_185740_add_year_start_column_in_groups_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('groups','year_start_id',$this->integer(11));

        $this->createIndex(
            'idx-groups-year_id',
            'groups',
            'year_start_id'
        );

        $this->addForeignKey(
            'fk-groups-year_id',
            'groups',
            'year_start_id',
            'year',
            'id',
            'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {


        $this->dropForeignKey(
            'fk-groups-year_id',
            'groups'
        );

        $this->dropIndex(
            'idx-groups-year_id',
            'groups'
        );


        $this->dropColumn('groups','year_start_id');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180604_185740_add_year_start_column_in_groups_table cannot be reverted.\n";

        return false;
    }
    */
}
