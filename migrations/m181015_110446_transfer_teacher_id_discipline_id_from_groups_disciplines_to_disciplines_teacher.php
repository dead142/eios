<?php

use yii\db\Migration;

/**
 * Class m181015_110446_transfer_teacher_id_discipline_id_from_groups_disciplines_to_disciplines_teacher
 */
class m181015_110446_transfer_teacher_id_discipline_id_from_groups_disciplines_to_disciplines_teacher extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $groupsDisciplines = \app\modules\backend\models\GroupsDiscipline::find()->all();
            foreach ($groupsDisciplines as $groupsDiscipline){
                $disciplinesTeacher = new \app\modules\backend\models\DisciplinesTeachers();
                $disciplinesTeacher->discipline_id = $groupsDiscipline->discipline_id;
                $disciplinesTeacher->teacher_id = $groupsDiscipline->teacher_id;
                if ($disciplinesTeacher->save()){
                    $groupsDiscipline->discipline_teacher_id = $disciplinesTeacher->id;
                    $groupsDiscipline->save();
                }
            }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181015_110446_transfer_teacher_id_discipline_id_from_groups_disciplines_to_disciplines_teacher cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181015_110446_transfer_teacher_id_discipline_id_from_groups_disciplines_to_disciplines_teacher cannot be reverted.\n";

        return false;
    }
    */
}
