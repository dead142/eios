<?php

use yii\db\Migration;

/**
 * Миграция создает уникальный индекс для поля p_id таблицы groups
 * Так как у одной группы может быть только один родитель
 * Class m180629_185445_set_p_id_uniq_in_groups
 */
class m180629_185445_set_p_id_uniq_in_groups extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->createIndex('p_id_IDX','groups','p_id',true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        if ($this->dropIndex('p_id_IDX','groups')){
            echo 'Уникальный индекс p_id таблицы groups успешно удален';
            return true;
        } else {
            echo 'Уникальный индекс p_id таблицы groups НЕ удален!';
        }
    }

}
