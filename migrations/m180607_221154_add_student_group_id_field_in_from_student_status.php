<?php

use yii\db\Migration;

/**
 * Class m180607_221154_add_student_group_id_field_in_from_student_status
 */
class m180607_221154_add_student_group_id_field_in_from_student_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('student_status','student_group_id',$this->integer(11));

        $this->createIndex(
            'idx-student_group-student_group_id',
            'student_status',
            'student_group_id'
        );

        $this->addForeignKey(
            'fk-student_group-student_group_id',
            'student_status',
            'student_group_id',
            'student_group',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('student_status','student_group_id');

        $this->dropIndex(
            'idx-student_group-student_group_id',
            'student_status',
            'student_group_id'
        );

        $this->dropForeignKey(
            'fk-groups-year_id',
            'student_status'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180607_221154_add_student_group_id_field_in_from_student_status cannot be reverted.\n";

        return false;
    }
    */
}
