<?php

use yii\db\Migration;

/**
 * Class m181015_112435_delete_teacher_id_from_groups_disciplines
 */
class m181015_112435_delete_teacher_id_from_groups_disciplines extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        try {
            $this->dropForeignKey(
                'FK_groups_discipline_profile_user_id',
                'groups_discipline'
            );
            $this->dropIndex(
                'IDX_groups_discipline_teacher_id',
                'groups_discipline'
            );
        } catch (Exception $exception){

        }


        $this->dropColumn('groups_discipline','teacher_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181015_112435_delete_teacher_id_from_groups_disciplines cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181015_112435_delete_teacher_id_from_groups_disciplines cannot be reverted.\n";

        return false;
    }
    */
}
