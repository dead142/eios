<?php

use yii\db\Migration;

/**
 * Class m181015_105604_add_field_discipline_teacher_id_to_groups_discipline
 */
class m181015_105604_add_field_discipline_teacher_id_to_groups_discipline extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('groups_discipline','discipline_teacher_id',$this->integer(11));

        $this->createIndex(
            'idx-groups_discipline-discipline_teacher_id',
            'groups_discipline',
            'discipline_teacher_id'
        );

        $this->addForeignKey(
            'fk-groups_discipline-discipline_teacher_id',
            'groups_discipline',
            'discipline_teacher_id',
            'disciplines_teachers',
            'id',
            'NO ACTION',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('groups_discipline','discipline_teacher_id');

        $this->dropIndex(
            'idx-groups_discipline-discipline_teacher_id',
            'groups_discipline'

        );

        $this->dropForeignKey(
            'fk-groups_discipline-discipline_teacher_id',
            'groups_discipline'

        );

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181015_105604_add_field_discipline_teacher_id_to_groups_discipline cannot be reverted.\n";

        return false;
    }
    */
}
