-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 19 2017 г., 18:12
-- Версия сервера: 5.7.13
-- Версия PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `isu`
--

-- --------------------------------------------------------

--
-- Структура таблицы `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admins', '1', 1502659542),
('student', '10', 1507581565),
('student', '11', 1507581717),
('student', '12', 1507581718),
('student', '13', 1507581730),
('student', '14', 1507581730),
('student', '15', 1507581813),
('student', '16', 1507581813),
('student', '17', 1507581849),
('student', '18', 1507581920),
('student', '19', 1507581926),
('student', '20', 1507581955),
('student', '21', 1507581979),
('student', '22', 1507582067),
('student', '23', 1507582093),
('student', '24', 1507582121),
('student', '25', 1507582395),
('student', '26', 1507582395),
('student', '27', 1507582395),
('student', '28', 1507582951),
('student', '29', 1507582951),
('student', '3', 1507581317),
('student', '30', 1507582952),
('student', '31', 1507586021),
('student', '32', 1507586022),
('student', '33', 1507586047),
('student', '34', 1507586047),
('student', '35', 1507586143),
('student', '36', 1507586143),
('student', '37', 1507586177),
('student', '38', 1507586177),
('student', '39', 1507586177),
('student', '4', 1507581317),
('student', '40', 1510143004),
('student', '41', 1510143948),
('student', '42', 1510143949),
('student', '43', 1510143950),
('student', '44', 1510143950),
('student', '45', 1510143951),
('student', '46', 1510143952),
('student', '47', 1510143952),
('student', '48', 1510143953),
('student', '49', 1510143953),
('student', '5', 1507581317),
('student', '50', 1510143954),
('student', '51', 1510143955),
('student', '52', 1510143956),
('student', '53', 1510143957),
('student', '54', 1510667167),
('student', '55', 1510673746),
('student', '56', 1510673748),
('student', '57', 1510673960),
('student', '58', 1510674196),
('student', '59', 1510674207),
('student', '6', 1507581453),
('student', '60', 1510674226),
('student', '61', 1510674284),
('student', '62', 1510674405),
('student', '63', 1510674461),
('student', '64', 1510674552),
('student', '65', 1510674598),
('student', '66', 1510674626),
('student', '67', 1510675397),
('student', '68', 1510675423),
('student', '69', 1510675441),
('student', '7', 1507581468),
('student', '70', 1510675475),
('student', '71', 1510675601),
('student', '72', 1510675608),
('student', '73', 1510675619),
('student', '74', 1510675633),
('student', '75', 1510675689),
('student', '76', 1510675710),
('student', '77', 1513677366),
('student', '78', 1513677697),
('student', '8', 1507581468),
('student', '80', 1513678006),
('student', '81', 1513678303),
('student', '82', 1513678304),
('student', '83', 1513678305),
('student', '84', 1513691518),
('student', '85', 1513691787),
('student', '86', 1513691883),
('student', '87', 1513691989),
('student', '89', 1513692078),
('student', '9', 1507581565),
('student', '90', 1513692180),
('student', '91', 1513692181),
('student', '92', 1513692292),
('student', '93', 1513692292),
('student', '94', 1513692502),
('student', '96', 1513692611),
('teacher', '1', 1510662060);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('about', 2, 'See a about', NULL, NULL, 1452878684, 1452878684),
('admins', 1, 'Администратор', NULL, NULL, NULL, 1465202752),
('comments.create', 2, 'Can create own comments', NULL, NULL, 1455776154, 1455776154),
('comments.delete', 2, 'Can delete all comments', NULL, NULL, 1455776155, 1455776155),
('comments.delete.own', 2, 'Can delete own comments', 'comments.its-my-comment', NULL, 1455776155, 1455776155),
('comments.update', 2, 'Can update all comments', NULL, NULL, 1455776155, 1455776155),
('comments.update.own', 2, 'Can update own comments', 'comments.its-my-comment', NULL, 1455776155, 1455776155),
('contact', 2, 'See a contact', NULL, NULL, 1452878233, 1452878233),
('deleteUser', 2, 'Право удалять пользователя', NULL, NULL, 1452870084, 1452870084),
('student', 1, 'Учащийся ВУЗа', NULL, NULL, 1455561753, 1455776042),
('teacher', 1, 'Преподаватель', NULL, NULL, 1452872037, 1465202739),
('updateOwnPost', 2, 'Update own post', 'isAuthor', NULL, 1455608224, 1455616081),
('updatePost', 2, 'Update post', NULL, NULL, 1455608117, 1455616070);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('teacher', 'student'),
('admins', 'teacher'),
('student', 'updateOwnPost'),
('teacher', 'updatePost'),
('updateOwnPost', 'updatePost');

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('comments.its-my-comment', 'O:45:"rmrevin\\yii\\module\\Comments\\rbac\\ItsMyComment":3:{s:4:"name";s:23:"comments.its-my-comment";s:9:"createdAt";i:1455776154;s:9:"updatedAt";i:1455776154;}', 1455776154, 1455776154),
('isAuthor', 'O:19:"app\\rbac\\AuthorRule":3:{s:4:"name";s:8:"isAuthor";s:9:"createdAt";i:1455608224;s:9:"updatedAt";i:1455608224;}', 1455608224, 1455608224);

-- --------------------------------------------------------

--
-- Структура таблицы `awards`
--

CREATE TABLE IF NOT EXISTS `awards` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `ORGANIZATION_ID` int(11) DEFAULT NULL,
  `DOCUMENT_ID` int(11) DEFAULT NULL,
  `ISSUE_DATE` datetime NOT NULL,
  `DESC` text,
  `DATE_CREATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DATE_UPDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `USER_ID` int(11) DEFAULT NULL,
  `FILE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Prizes, awards, diplomas';

-- --------------------------------------------------------

--
-- Структура таблицы `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `message` text,
  `updateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `chat`
--

INSERT INTO `chat` (`id`, `userId`, `message`, `updateDate`) VALUES
(1, 4, 'Проверка', '2016-06-02 08:47:29');

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL,
  `entity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci,
  `deleted` tinyint(1) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `rate` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `conferences`
--

CREATE TABLE IF NOT EXISTS `conferences` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `LOCATION` varchar(255) DEFAULT NULL,
  `DATE_START` datetime DEFAULT NULL,
  `DATE_END` datetime DEFAULT NULL,
  `STATUS_ID` int(11) DEFAULT NULL,
  `KIND_INVOVLVEMENT_ID` int(11) DEFAULT NULL COMMENT 'Type of participation',
  `NAME_REPORT` varchar(50) NOT NULL,
  `NUMBER_NIR` varchar(255) DEFAULT NULL,
  `DATE_CREATE` timestamp NULL DEFAULT NULL,
  `DATE_UPDATE` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `USER_ID` int(11) DEFAULT NULL,
  `FILE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Conferences, schools, symposia, seminars';

-- --------------------------------------------------------

--
-- Структура таблицы `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL,
  `option_name` varchar(255) NOT NULL,
  `option_value` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `config`
--

INSERT INTO `config` (`id`, `option_name`, `option_value`) VALUES
(1, 'Отображение портфолио на главной странице', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `country`
--

INSERT INTO `country` (`ID`, `NAME`) VALUES
(1, 'Россия'),
(2, 'Финляндия'),
(3, 'Норвегия'),
(4, 'Абхазия'),
(5, 'Италия'),
(6, 'Германия'),
(7, 'Швеция'),
(8, 'Китай');

-- --------------------------------------------------------

--
-- Структура таблицы `degree`
--

CREATE TABLE IF NOT EXISTS `degree` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `AREA_ID` int(11) DEFAULT NULL COMMENT 'Area of science',
  `ORGANIZATION_ID` int(11) DEFAULT NULL COMMENT 'The name of the organization',
  `DOCUMENT_ID` int(11) DEFAULT NULL COMMENT 'The document number',
  `ISSUE_DATE` datetime DEFAULT NULL COMMENT 'Date of issue',
  `DOCUMENT_NUMBER` varchar(255) DEFAULT NULL COMMENT 'The document TITLE',
  `DATE_CREATE` timestamp NULL DEFAULT NULL,
  `DATE_UPDATE` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `USER_ID` int(11) DEFAULT NULL,
  `FILE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `disciplines`
--

CREATE TABLE IF NOT EXISTS `disciplines` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `disciplines`
--

INSERT INTO `disciplines` (`id`, `name`, `create_at`, `create_by`) VALUES
(1, 'Дисциплина для 1 семестра 1 ого курса', '2017-11-14 00:11:50', 1),
(2, 'Дисциплина 2 для 1 семестра 1 ого курса', '2017-11-14 00:11:05', 1),
(3, 'Дисциплина 1 для 2 семестра 1 ого курса', '2017-11-14 00:11:21', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `document`
--

INSERT INTO `document` (`ID`, `NAME`) VALUES
(1, 'Dr. habil.'),
(2, 'PhD'),
(3, 'ScD'),
(5, 'Аттестат доцента'),
(6, 'Аттестат о среднем образовании'),
(7, 'Аттестат профессора'),
(8, 'Аттестат старшего научного сотрудника'),
(9, 'Диплом доктора наук'),
(10, 'Диплом кандидата наук'),
(11, 'Диплом о высшем образовании'),
(12, 'Диплом о средне-специальном образовании'),
(13, 'Диплом о средне-техническом образовании'),
(14, 'паспорт');

-- --------------------------------------------------------

--
-- Структура таблицы `formgrade`
--

CREATE TABLE IF NOT EXISTS `formgrade` (
  `id` int(11) NOT NULL,
  `type` enum('current control','intermediate control') NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` datetime DEFAULT NULL,
  `groups_discipline_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `grades`
--

CREATE TABLE IF NOT EXISTS `grades` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `grade` enum('2','3','4','5','0','1','pass','fail','Excellent','Good','Satisfactorily','Unsatisfactorily','Failure to appear','Hospital') DEFAULT NULL,
  `formgrade_id` int(11) NOT NULL,
  `discipline_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `id_year` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `course` enum('1','2','3','4','5') NOT NULL,
  `p_id` int(11) DEFAULT NULL COMMENT 'A pointer to the parent group'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Groups of student';

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `name`, `id_year`, `created_at`, `course`, `p_id`) VALUES
(13, 'Группа №1 (2017/2018)', 10, '2017-12-19 09:12:09', '1', NULL),
(14, 'Группа №0 (2016/2017)', 11, '2017-12-19 09:12:50', '1', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `groups_discipline`
--

CREATE TABLE IF NOT EXISTS `groups_discipline` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `discipline_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `semestr_id` enum('1','2','3','4','5','6','7','8','9','10') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `indexes`
--

CREATE TABLE IF NOT EXISTS `indexes` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `indexes`
--

INSERT INTO `indexes` (`ID`, `NAME`) VALUES
(1, 'Высшая аттестационная комиссия (ВАК)'),
(2, 'Российский индекс научного цитирования (РИНЦ)'),
(3, 'Web of Science'),
(4, 'Scopus');

-- --------------------------------------------------------

--
-- Структура таблицы `intellectual`
--

CREATE TABLE IF NOT EXISTS `intellectual` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `COPYRIGHT` varchar(255) DEFAULT NULL COMMENT 'copyright ',
  `ORGANIZATION_ID` int(11) DEFAULT NULL,
  `DOCUMENT_ID` int(11) DEFAULT NULL,
  `DOCUMENT` varchar(255) DEFAULT NULL,
  `DATE_ISSUE` datetime DEFAULT NULL,
  `DESC` text,
  `DATE_CREATE` timestamp NULL DEFAULT NULL,
  `DATE_UPDATE` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `USER_ID` int(11) DEFAULT NULL,
  `FILE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='The objects of intellectual property';

-- --------------------------------------------------------

--
-- Структура таблицы `internship`
--

CREATE TABLE IF NOT EXISTS `internship` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `DATE_START` datetime DEFAULT NULL,
  `DATE_END` datetime DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `USER_ID` int(11) NOT NULL,
  `COUNTRY_ID` int(11) NOT NULL,
  `FILE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `kind_involvement`
--

CREATE TABLE IF NOT EXISTS `kind_involvement` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `kind_involvement`
--

INSERT INTO `kind_involvement` (`ID`, `NAME`) VALUES
(1, 'Участие с докладом'),
(2, 'Участие без доклада'),
(3, 'Участие с экспонатом'),
(4, 'Участие в конкурсе'),
(5, 'Участие');

-- --------------------------------------------------------

--
-- Структура таблицы `lang`
--

CREATE TABLE IF NOT EXISTS `lang` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `lang`
--

INSERT INTO `lang` (`ID`, `NAME`) VALUES
(1, 'Русский'),
(2, 'Английский'),
(5, 'Финский'),
(6, 'Итальянский'),
(11, 'Итальянский'),
(12, 'Итальянский'),
(13, 'Немецкий'),
(14, 'Инди'),
(15, 'Инди'),
(16, 'Хинди'),
(18, 'Китайский'),
(19, 'русский'),
(20, 'Русский'),
(21, 'Русский'),
(22, 'Русский'),
(23, 'Русский');

-- --------------------------------------------------------

--
-- Структура таблицы `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='The level of the event';

--
-- Дамп данных таблицы `level`
--

INSERT INTO `level` (`ID`, `NAME`) VALUES
(1, 'Кафедральное'),
(2, 'Факультетское'),
(3, 'Вузовское'),
(4, 'Районное'),
(5, 'Городское'),
(6, 'Региональное'),
(7, 'Всероссийское'),
(8, 'Международное'),
(9, 'Национальное');

-- --------------------------------------------------------

--
-- Структура таблицы `materials`
--

CREATE TABLE IF NOT EXISTS `materials` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `FULLTEXT` text NOT NULL,
  `DESC` text NOT NULL,
  `DATE_CREATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DATE_UPDATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `CREATE_BY` int(11) NOT NULL,
  `STATUS` tinyint(1) NOT NULL DEFAULT '1',
  `VERSION` int(11) NOT NULL DEFAULT '1',
  `ORDER` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AVG_ROW_LENGTH=1260;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m140209_132017_init', 1502657667),
('m140403_174025_create_account_table', 1502658506),
('m140504_113157_update_tables', 1502658506),
('m140504_130429_create_token_table', 1502658526),
('m140830_171933_fix_ip_field', 1502658526),
('m140830_172703_change_account_table_name', 1502658539),
('m141222_110026_update_ip_field', 1502658539),
('m141222_135246_alter_username_length', 1502658540),
('m150614_103145_update_social_account_table', 1502658540),
('m150623_212711_fix_username_notnull', 1502658540),
('m151218_234654_add_timezone_to_profile', 1502658540),
('m160929_103127_add_last_login_at_to_user_table', 1502658540),
('m170607_221044_create_config', 1502661028),
('m170813_214611_add_surname_column_to_profile_table', 1502661028),
('m170813_215555_add_surname_column_to_middlename_table', 1502661412),
('m170831_095842_add_photo_url_column_to_profile_table', 1504211449),
('m171112_211628_create_table_student_status', 1510573339);

-- --------------------------------------------------------

--
-- Структура таблицы `organization`
--

CREATE TABLE IF NOT EXISTS `organization` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middlename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo_url` varchar(244) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`, `timezone`, `surname`, `middlename`, `photo_url`) VALUES
(1, 'Андрей', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', 'Pacific/Kiritimati', 'Бережков ', '', 'uploads/user_photo/1-150429805859a9c44ae70142.38108425.jpg'),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'Студент', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', NULL, 'Студентов', 'Студентович', NULL),
(55, 'ИСтудент', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ФСтудент', 'ОСтудент (Группа №3 (2017/2018))', NULL),
(56, 'ИСтудент2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ФСтудент1', 'ОСтуден3 Группа №3 (2017/2018)', NULL),
(57, 'иГруппа №1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'фГруппа №1', 'оГруппа №1', NULL),
(58, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL),
(59, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL),
(60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'тут', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'тут', 'тут', NULL),
(64, 'тут', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'тут', 'тут', NULL),
(65, 'тут', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'тут', 'тут', NULL),
(66, 'тут', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'тут', 'тут', NULL),
(67, 'тут', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'тут', 'тут', NULL),
(68, 'тут', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'тут', 'тут', NULL),
(69, 'тут', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'тут', 'тут', NULL),
(70, 'тут', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'тут', 'тут', NULL),
(71, 'тут', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'тут', 'тут', NULL),
(74, 'тут', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'тут', 'тут', NULL),
(81, 'Петр', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Петров', 'Петрович', NULL),
(82, 'Андрей', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Бережков', 'Вячеславович', NULL),
(83, 'Александр', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Блинов', 'Фридрихович', NULL),
(84, 'Александр', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Петров', 'Игоревич', NULL),
(86, 'Григорий', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Гринорьев', 'Григорьевич', NULL),
(92, 'Тимофей', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Тимофеев', 'Тимофеевич', NULL),
(93, 'Яков', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Яковлев', 'Яковлевич', NULL),
(96, 'Илья', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ильев', 'Ильевич', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `ROLE` enum('Исполнитель','Ответственный исполнитель','Руководитель') NOT NULL,
  `TITLE_COMPETENTION` varchar(255) NOT NULL COMMENT 'The title of the competition',
  `TITLE_SECTION` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL COMMENT 'Code system project contest',
  `WINNER` enum('no','yes') NOT NULL COMMENT 'The winner of the contest',
  `ORGANIZATION_ID` int(11) DEFAULT NULL,
  `DOCUMENT_ID` int(11) DEFAULT NULL,
  `DOCUMENT_NUMBER` varchar(255) DEFAULT NULL COMMENT 'document number',
  `DATE_ISSUE` datetime DEFAULT NULL,
  `DATE_START` datetime DEFAULT NULL,
  `DATE_END` datetime DEFAULT NULL,
  `FUNDING` int(11) DEFAULT NULL COMMENT 'Funding',
  `CURRENCY` enum('USD','RUR','EUR','ANOTHER') DEFAULT NULL COMMENT 'currency unit',
  `DATE_CREATE` timestamp NULL DEFAULT NULL,
  `DATE_UPDATE` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `USER_ID` int(11) DEFAULT NULL,
  `FILE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Projects, grants';

-- --------------------------------------------------------

--
-- Структура таблицы `publication`
--

CREATE TABLE IF NOT EXISTS `publication` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `LANG_ID` int(11) DEFAULT NULL,
  `YEAR` year(4) DEFAULT NULL,
  `COUNTRY_ID` int(11) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `TYPE_EDITION_ID` int(11) DEFAULT NULL,
  `PUBLISHER` varchar(255) DEFAULT NULL,
  `PAGES` varchar(255) DEFAULT NULL,
  `FILE` varchar(255) DEFAULT NULL,
  `DESC` varchar(255) DEFAULT NULL,
  `DATE_CREATE` timestamp NULL DEFAULT NULL,
  `DATE_UPDATE` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `USER_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='monograph, book, article';

-- --------------------------------------------------------

--
-- Структура таблицы `publication_indexes`
--

CREATE TABLE IF NOT EXISTS `publication_indexes` (
  `ID` int(11) NOT NULL,
  `INDEXES_ID` int(11) NOT NULL,
  `PUBLICATION_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `qualification`
--

CREATE TABLE IF NOT EXISTS `qualification` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `LOCATION` varchar(255) DEFAULT NULL,
  `DATE_START` datetime DEFAULT NULL,
  `DATE_END` datetime DEFAULT NULL,
  `ORGANIZATION_ID` int(11) DEFAULT NULL,
  `DOCUMENT_ID` int(11) DEFAULT NULL,
  `DATE_ISSUE` datetime DEFAULT NULL,
  `HOURS` varchar(255) DEFAULT NULL,
  `DATE_CREATE` timestamp NULL DEFAULT NULL,
  `DATE_UPDATE` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `USER_ID` int(11) DEFAULT NULL,
  `FILE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='qualification improvement';

-- --------------------------------------------------------

--
-- Структура таблицы `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `desc` text,
  `create_at` datetime DEFAULT NULL,
  `answer` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) DEFAULT NULL,
  `ORGANIZATION_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `FILE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `semesters`
--

CREATE TABLE IF NOT EXISTS `semesters` (
  `id` int(11) NOT NULL,
  `name` enum('1','2') DEFAULT NULL,
  `year_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `id` char(40) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `session`
--

INSERT INTO `session` (`id`, `expire`, `data`) VALUES
('bf1b5vp85rdm9jt8d69055v0c2', 1511011324, 0x5f5f666c6173687c613a303a7b7d5f5f69647c693a313b);

-- --------------------------------------------------------

--
-- Структура таблицы `social_account`
--

CREATE TABLE IF NOT EXISTS `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `social_activities`
--

CREATE TABLE IF NOT EXISTS `social_activities` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `DATE_START` datetime NOT NULL,
  `DATE_END` datetime NOT NULL,
  `STATUS_ID` int(11) DEFAULT NULL,
  `TYPE_EVENT_ID` int(11) DEFAULT NULL COMMENT 'Type of event',
  `LEVEL_ID` int(11) DEFAULT NULL,
  `TYPE_PARTICIPATION_ID` int(11) DEFAULT NULL COMMENT 'Type of participation',
  `DOCUMENT` varchar(255) DEFAULT NULL COMMENT 'DOCUMENT LINK',
  `DESC` varchar(255) DEFAULT NULL,
  `DATE_CREATE` timestamp NULL DEFAULT NULL,
  `DATE_UPDATE` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `USER_ID` int(11) DEFAULT NULL,
  `FILE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Social, cultural and creative, sports activities';

-- --------------------------------------------------------

--
-- Структура таблицы `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `status`
--

INSERT INTO `status` (`ID`, `NAME`) VALUES
(1, 'Конкурс'),
(2, 'Фестиваль'),
(3, 'Олимпиада'),
(4, 'Игра'),
(5, 'Форум'),
(6, 'Конгресс'),
(7, 'Конференция'),
(8, 'Турнир'),
(9, 'Дискотека'),
(10, 'Концерт'),
(11, 'Круглый стол'),
(12, 'Мастер-класс'),
(13, 'Тренинг'),
(14, 'Чемпионат'),
(15, 'Выступление');

-- --------------------------------------------------------

--
-- Структура таблицы `student_group`
--

CREATE TABLE IF NOT EXISTS `student_group` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `year_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `student_group`
--

INSERT INTO `student_group` (`id`, `student_id`, `group_id`, `status_id`, `year_id`) VALUES
(54, 81, 13, NULL, 10),
(55, 82, 13, NULL, 10),
(56, 83, 13, NULL, 10),
(57, 84, 14, NULL, 11),
(59, 86, 14, NULL, 11),
(64, 92, 13, NULL, 10),
(65, 93, 13, NULL, 10),
(67, 96, 14, NULL, 11);

-- --------------------------------------------------------

--
-- Структура таблицы `student_status`
--

CREATE TABLE IF NOT EXISTS `student_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc` text COLLATE utf8_unicode_ci,
  `year_id` int(11) NOT NULL,
  `semestr` int(11) DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `date_out` datetime DEFAULT NULL,
  `date_in` datetime DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `profile_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `student_status`
--

INSERT INTO `student_status` (`id`, `name`, `desc`, `year_id`, `semestr`, `create_at`, `update_at`, `create_by`, `date_out`, `date_in`, `group_id`, `profile_id`) VALUES
(4, 'active', NULL, 10, 1, '2017-12-19 13:11:43', '2017-12-19 13:11:43', 1, NULL, NULL, 54, 81),
(5, 'active', NULL, 10, 1, '2017-12-19 13:11:44', '2017-12-19 13:11:44', 1, NULL, NULL, 55, 82),
(6, 'active', NULL, 10, 1, '2017-12-19 13:11:45', '2017-12-19 13:11:45', 1, NULL, NULL, 56, 83),
(12, 'active', NULL, 11, 1, '2017-12-19 16:51:58', '2017-12-19 16:51:58', 1, NULL, NULL, 64, 84),
(13, 'active', NULL, 11, 1, '2017-12-19 16:58:03', '2017-12-19 16:58:03', 1, NULL, NULL, 65, 86),
(14, 'active', NULL, 11, 1, '2017-12-19 17:10:11', '2017-12-19 17:10:11', 1, NULL, NULL, 67, 96);

-- --------------------------------------------------------

--
-- Структура таблицы `token`
--

CREATE TABLE IF NOT EXISTS `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `token`
--

INSERT INTO `token` (`user_id`, `code`, `created_at`, `type`) VALUES
(1, 'Q2VeWLP8SFNNfUPmeyNViCIeqH5PnWle', 1502658591, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `type_edition`
--

CREATE TABLE IF NOT EXISTS `type_edition` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `type_edition`
--

INSERT INTO `type_edition` (`ID`, `NAME`) VALUES
(1, 'Монография'),
(2, 'Сборник научных трудов'),
(3, 'Статья');

-- --------------------------------------------------------

--
-- Структура таблицы `type_event`
--

CREATE TABLE IF NOT EXISTS `type_event` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `type_event`
--

INSERT INTO `type_event` (`ID`, `NAME`) VALUES
(1, 'Культурно-массовое'),
(2, 'Социально-общественное'),
(3, 'Спортивное'),
(4, 'Предпринимательское'),
(5, 'Студенческое самоуправление'),
(6, 'Информационное'),
(7, 'Развлекательное');

-- --------------------------------------------------------

--
-- Структура таблицы `type_participation`
--

CREATE TABLE IF NOT EXISTS `type_participation` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `type_participation`
--

INSERT INTO `type_participation` (`ID`, `NAME`) VALUES
(1, 'Главный организатор'),
(2, 'Организатор'),
(3, 'Помощник организатора'),
(4, 'Волонтер'),
(5, 'Участник'),
(6, 'Победитель'),
(7, 'Призер'),
(8, 'Участник');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`) VALUES
(1, 'admin', 'dead0343@gmail.com', '$2y$12$LFfPkO/JGnm4z3fVNxKi9.zcgjoiHh3zxm.5YulH8R5qwDg0VA14q', 'amPgqw_Mm4PqVLyR1yAzgOKMfTYx5dRi', 1, NULL, NULL, '127.0.0.1', 1502658591, 1502658591, 0, 1513677080),
(2, 'dead142', 'dead142@mail.ru', '$2y$12$VbLLlk5lSVSc5CeLDOIDw.EWO9wwwWCE83.7Xvjd2Rkr7ySZt8KuS', 'YcudccCgvSawdW8YN2XNm9bwyal5a3_O', 1502661455, NULL, NULL, '127.0.0.1', 1502661455, 1502661455, 0, NULL),
(54, 'prepod', 'dead1421@mail.ru', '$2y$12$Ae2pNAk4hdD48k9tPywk..TPvtr2zihFP4YxQ.ARxxECKQEzt/k7i', 'Gt982ro-AwJowbUTDnVmgUhZI4luxY0g', 1510667167, NULL, NULL, '127.0.0.1', 1510667167, 1510667167, 0, NULL),
(55, '175115a0b0d5178524', '175115a0b0d5178524@to.change', '$2y$12$eXf1J90rG2fnaI6KZc/grO7F0txtwCEs8cnnf2czJb7mmSmBE7zU2', 'G4GDPY_8PAWvZBU8JDNX7BXj60V7vXvg', 1510673745, NULL, NULL, '127.0.0.1', 1510673746, 1510673746, 0, NULL),
(56, '77935a0b0d533c2f2', '77935a0b0d533c2f2@to.change', '$2y$12$czCNUOp10lf3a2NRC5rB2eu.RAnn0elHdy4knI3AIyqw6j3MeTegq', 'd8_-w7O2ExIvQZWAyAs9qnHRyvI_8Cbi', 1510673747, NULL, NULL, '127.0.0.1', 1510673748, 1510673748, 0, NULL),
(57, '50085a0b0e274953f', '50085a0b0e274953f@to.change', '$2y$12$71Js9XCuFQzYRWmigyWPlOZt/oCmXhItuAcfswlEO8U7zQrdisYCG', '_Itc_bDDDSzWY9wY-8Q1CrY4aNdHe_js', 1510673959, NULL, NULL, '127.0.0.1', 1510673960, 1510673960, 0, NULL),
(58, '294365a0b0f143abd9', '294365a0b0f143abd9@to.change', '$2y$12$ALPRH9Hk5IL5ZvSH3/Iw9uJ2EUNs/MhRG6zDvB/tcO2I0IG2iT7ze', 'vE5-hIsb83cJVMvTP9fCkrJkDaZl1GRB', 1510674196, NULL, NULL, '127.0.0.1', 1510674196, 1510674196, 0, NULL),
(59, '312875a0b0f1ebd5ba', '312875a0b0f1ebd5ba@to.change', '$2y$12$cu4sXvhUDdaFlmUJR8UtDuYmqfRcwF8xQJqteZcWMOaekEGaPQ2/u', 'SfrfwDlEYnY7RwE-6X-CoAn0SRMHfPG0', 1510674206, NULL, NULL, '127.0.0.1', 1510674207, 1510674207, 0, NULL),
(60, '40745a0b0f3257f7a', '40745a0b0f3257f7a@to.change', '$2y$12$7SOFY3dyKbugpybv3sqtWubTV5oAQAisIFBz6obRElwPugWMp/lj.', '_Q6gQpa4GhhAvxpRbjxQk99wZ_Y8Mozi', 1510674226, NULL, NULL, '127.0.0.1', 1510674226, 1510674226, 0, NULL),
(61, '197765a0b0f6b20e4e', '197765a0b0f6b20e4e@to.change', '$2y$12$9VMcO4ERooAe77j7cFH2X.V1I0wPsAX6W5VT/DfoMPB8AwY.w5DUi', 'k6dhbpHZxTAiDy8brGZyZ_XkNQStrHjc', 1510674283, NULL, NULL, '127.0.0.1', 1510674284, 1510674284, 0, NULL),
(62, '313725a0b0fe54f658', '313725a0b0fe54f658@to.change', '$2y$12$wkE4fUwrLkFOjNuqw6G04OTixV0ynmkmF12ODoQjaMq.PqiB370Li', 'UIR3yi07HmIElW_G-sbXXDhNX-pr16Sx', 1510674405, NULL, NULL, '127.0.0.1', 1510674405, 1510674405, 0, NULL),
(63, '59765a0b101bca1af', '59765a0b101bca1af@to.change', '$2y$12$kimCwD2/EyVVBVZehgdqOu0TUjBkdHu1gKLFGXMYFiwZf.y1KchvK', 'DvH31tDEUhZOJkNpckKjZ8VZjhrzyT0Q', 1510674459, NULL, NULL, '127.0.0.1', 1510674461, 1510674461, 0, NULL),
(64, '263555a0b10779410a', '263555a0b10779410a@to.change', '$2y$12$jBF90b492oBOUDmeyUw/HunbLXDMYJLvTGNW31O3uGnF1HMYi44WO', 'HjLRUlokD4Un7cllhbLobSwdwq2dUhf_', 1510674551, NULL, NULL, '127.0.0.1', 1510674552, 1510674552, 0, NULL),
(65, '127065a0b10a4d5860', '127065a0b10a4d5860@to.change', '$2y$12$P3yW7fLSHIGQGPDQywcQcuCm08hA7ecgAzB8HIAO29atcgikdrnz6', 'sozDG4u8T7R_zVdm89vq1lYcznbZbClH', 1510674596, NULL, NULL, '127.0.0.1', 1510674598, 1510674598, 0, NULL),
(66, '23565a0b10c21ebaa', '23565a0b10c21ebaa@to.change', '$2y$12$2itfVTQTO4Etvh9NZWqP/uGq5pRe.XDFhpiglTqwAZvUherWDmKOy', 'JnA_R3w8W3n4Y2IfyUoXKjo1AXKtBZ5s', 1510674626, NULL, NULL, '127.0.0.1', 1510674626, 1510674626, 0, NULL),
(67, '5395a0b13c4cf87b', '5395a0b13c4cf87b@to.change', '$2y$12$j0yRAj/l4/IDlrDoj7x38u4cf4Y4kxuqrqHrvcAYPwy2B08AfzEn2', 'fWoGY6XUanb9o8LDS8LPQH5m4eHoaEJK', 1510675396, NULL, NULL, '127.0.0.1', 1510675397, 1510675397, 0, NULL),
(68, '165955a0b13ddcd050', '165955a0b13ddcd050@to.change', '$2y$12$zvUwQSzxpJMLzLbym1E5lewQOWv3tvb2qFY0VmWbB0zmTwJeBsJfy', 'MaHaN6PQ2SecWwlCZFH_wvetyOhzQtf1', 1510675421, NULL, NULL, '127.0.0.1', 1510675423, 1510675423, 0, NULL),
(69, '54715a0b13f07da07', '54715a0b13f07da07@to.change', '$2y$12$4UVhIqW1F8F4esfZr8UVLezL9QY5R.xO.evrQtet/Z6zhg8uJPo7m', '1zWxrO2tgyeSCDJb8mh5ErjHlxgj6Jsj', 1510675440, NULL, NULL, '127.0.0.1', 1510675441, 1510675441, 0, NULL),
(70, '250575a0b14128b7b9', '250575a0b14128b7b9@to.change', '$2y$12$hAKgIyQTWyzpgeFR5Tm4tuEmMuv3Q87w6zkGFkGzjIp8adhVwKHOu', 'WHD_Bl0Lv0zODxUwT9GFufuoyrM39F3z', 1510675474, NULL, NULL, '127.0.0.1', 1510675475, 1510675475, 0, NULL),
(71, '242275a0b1490f1909', '242275a0b1490f1909@to.change', '$2y$12$3afllJ8i.adkqDxPi3cjyOSy42qSRP/k0dQXVQRcZ3JgRN/WtTWru', 'C9CCCGOtnkjiz3IqcPiICRwBhHK93NeA', 1510675600, NULL, NULL, '127.0.0.1', 1510675601, 1510675601, 0, NULL),
(74, '129425a0b14b04470e', '129425a0b14b04470e@to.change', '$2y$12$wq8sA7QnImWMzgm8NiiByuNChxmA1JBsxq0vJJh6jaGlywm2zNDwe', '5RcVB2vF46t67aegdT5jqb-8tL5tPnCC', 1510675632, NULL, NULL, '127.0.0.1', 1510675633, 1510675633, 0, NULL),
(81, 'petrov13', 'petrov13@to.change', '$2y$12$9WMj9HglXRIsyg3i5CAsoOcxySJwVWM16ntW4m0ocqWKzL7EuI1/e', 'HLhKYI5EJre5qbj2dsIvyjZo9MlC5RXa', 1513678303, NULL, NULL, '127.0.0.1', 1513678303, 1513678303, 0, NULL),
(82, 'berejkov13', 'berejkov13@to.change', '$2y$12$u4iF7OnsxNkgQq0S.y8RxuAg.MCQFSl21mfo26oh.QgWJrvZE4Uxi', 'c5iFZcbM84bevHA3JGf7Pri6PuMMjOsb', 1513678303, NULL, NULL, '127.0.0.1', 1513678304, 1513678304, 0, NULL),
(83, 'blinov13', 'blinov13@to.change', '$2y$12$kHEBaDt.idZh2csH9E54Ze6W8YeL6OgC.LHb2JkzjDcc7vjF6onJi', '6zLYOt7QYZIKvNcvp8s2FKWHKcFeFuoS', 1513678304, NULL, NULL, '127.0.0.1', 1513678305, 1513678305, 0, NULL),
(84, 'petrov14', 'petrov14@to.change', '$2y$12$m1X0.jaNLUX0W7pXHuX4SOosnzR6dEPbkIZKBTGyrRee8WQ1oQnHu', 'lDa0XjxzTM5dzkDkEiDkHb7dV59UdSRK', 1513691517, NULL, NULL, '127.0.0.1', 1513691518, 1513691518, 0, NULL),
(86, 'grinorev14', 'grinorev14@to.change', '$2y$12$btbtKcUZBuBVg4.MswcS3.I/dSVuz4N43mWW6d3FkPfcIgnc3vxXi', 'TtHygj76v0jM7J9rFsaS1Zi5TVHdopze', 1513691882, NULL, NULL, '127.0.0.1', 1513691883, 1513691883, 0, NULL),
(92, 'timofeev13', 'timofeev13@to.change', '$2y$12$qL0H2Kg7hwdH8OQtKQ7P8OWkbjBhNEg6PAOSlukf821LaRjOMWBWe', 'Jgb1LlyyZsZr_GEJBJOl1L0NXFS2SpJc', 1513692291, NULL, NULL, '127.0.0.1', 1513692292, 1513692292, 0, NULL),
(93, 'yakovlev13', 'yakovlev13@to.change', '$2y$12$JMzhoSOkGG2BnUKRKMMJs./VDLDPEk435TiiIb6Vr4Az2zR3la6aW', 'jm-1wIyAtJ3dEjGtKNqw7z6mdytcFVft', 1513692292, NULL, NULL, '127.0.0.1', 1513692292, 1513692292, 0, NULL),
(96, 'ilev14', 'ilev14@to.change', '$2y$12$viEWnUAlzvtYGweBwwa2O.O1ZKzMocDu7hlXd3NSphWss21sN7NbS', 'sEnO4J5ws1HX821xGtfVLwLQSC2Pj4zU', 1513692610, NULL, NULL, '127.0.0.1', 1513692611, 1513692611, 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `year`
--

CREATE TABLE IF NOT EXISTS `year` (
  `id` int(11) NOT NULL,
  `year_start` date NOT NULL,
  `year_end` date NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `year`
--

INSERT INTO `year` (`id`, `year_start`, `year_end`, `name`) VALUES
(10, '2017-09-01', '2018-08-31', '2017/2018'),
(11, '2016-09-01', '2017-08-31', '2016/2017');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `idx-auth_item-type` (`type`),
  ADD KEY `rule_name` (`rule_name`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `awards`
--
ALTER TABLE `awards`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `UK_awards_DOCUMENT_ID` (`DOCUMENT_ID`),
  ADD KEY `UK_awards_ORGANIZATION_ID` (`ORGANIZATION_ID`),
  ADD KEY `IDX_awards_USER_ID` (`USER_ID`);

--
-- Индексы таблицы `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_entity` (`entity`),
  ADD KEY `index_created_by` (`created_by`),
  ADD KEY `index_created_at` (`created_at`);

--
-- Индексы таблицы `conferences`
--
ALTER TABLE `conferences`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_conferences_kind_involvement_ID` (`KIND_INVOVLVEMENT_ID`),
  ADD KEY `UK_conferences` (`STATUS_ID`),
  ADD KEY `IDX_conferences_USER_ID` (`USER_ID`);

--
-- Индексы таблицы `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `degree`
--
ALTER TABLE `degree`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_degree_area_ID` (`AREA_ID`),
  ADD KEY `FK_degree_document_ID` (`DOCUMENT_ID`),
  ADD KEY `ORGANIZATION_ID` (`ORGANIZATION_ID`),
  ADD KEY `DOCUMENT_NUMBER` (`DOCUMENT_NUMBER`),
  ADD KEY `IDX_degree_USER_ID` (`USER_ID`);

--
-- Индексы таблицы `disciplines`
--
ALTER TABLE `disciplines`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `formgrade`
--
ALTER TABLE `formgrade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_formgrade_groups_discipline_id` (`groups_discipline_id`);

--
-- Индексы таблицы `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_grades_student_id` (`student_id`),
  ADD KEY `IDX_grades_formgrade_id` (`formgrade_id`),
  ADD KEY `IDX_grades_discipline_id` (`discipline_id`),
  ADD KEY `IDX_grades_formgrade_id2` (`formgrade_id`),
  ADD KEY `formgrade_id` (`formgrade_id`);

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_groups_id_year` (`id_year`);

--
-- Индексы таблицы `groups_discipline`
--
ALTER TABLE `groups_discipline`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_groups_discipline_group_id` (`group_id`),
  ADD KEY `IDX_groups_discipline_dicipline_id` (`discipline_id`),
  ADD KEY `IDX_groups_discipline_teacher_id` (`teacher_id`);

--
-- Индексы таблицы `indexes`
--
ALTER TABLE `indexes`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `intellectual`
--
ALTER TABLE `intellectual`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_intellectual_document_ID` (`DOCUMENT_ID`),
  ADD KEY `IDX_intellectual_USER_ID` (`USER_ID`),
  ADD KEY `IDX_intellectual_ORGANIZATION_ID` (`ORGANIZATION_ID`);

--
-- Индексы таблицы `internship`
--
ALTER TABLE `internship`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `UK_internship _COUNTRY_ID` (`COUNTRY_ID`),
  ADD KEY `IDX_internship _USER_ID` (`USER_ID`);

--
-- Индексы таблицы `kind_involvement`
--
ALTER TABLE `kind_involvement`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `lang`
--
ALTER TABLE `lang`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `IDX_materials_CREATE_BY` (`CREATE_BY`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_projects_document_ID` (`DOCUMENT_ID`),
  ADD KEY `IDX_projects_ORGANIZATION_ID` (`ORGANIZATION_ID`),
  ADD KEY `IDX_projects_USER_ID` (`USER_ID`);

--
-- Индексы таблицы `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_publication_country_ID` (`COUNTRY_ID`),
  ADD KEY `FK_publication_type_edition_ID` (`TYPE_EDITION_ID`),
  ADD KEY `UK_PUBLICATION` (`LANG_ID`),
  ADD KEY `IDX_publication_USER_ID` (`USER_ID`);

--
-- Индексы таблицы `publication_indexes`
--
ALTER TABLE `publication_indexes`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `INDEXES_ID` (`INDEXES_ID`),
  ADD KEY `PUBLICATION_ID` (`PUBLICATION_ID`);

--
-- Индексы таблицы `qualification`
--
ALTER TABLE `qualification`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_qualification_document_ID` (`DOCUMENT_ID`),
  ADD KEY `IDX_qualification_ORGANIZATION_ID` (`ORGANIZATION_ID`),
  ADD KEY `IDX_qualification_USER_ID` (`USER_ID`);

--
-- Индексы таблицы `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `IDX_reviews_ORGANIZATION_ID` (`ORGANIZATION_ID`),
  ADD KEY `IDX_reviews_USER_ID` (`USER_ID`);

--
-- Индексы таблицы `semesters`
--
ALTER TABLE `semesters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_semesters_year_id` (`year_id`);

--
-- Индексы таблицы `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_unique` (`provider`,`client_id`),
  ADD UNIQUE KEY `account_unique_code` (`code`),
  ADD KEY `fk_user_accountt` (`user_id`);

--
-- Индексы таблицы `social_activities`
--
ALTER TABLE `social_activities`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_social_activities_level_ID` (`LEVEL_ID`),
  ADD KEY `FK_social_activities_type_event_ID` (`TYPE_EVENT_ID`),
  ADD KEY `FK_social_activities_type_participation_ID` (`TYPE_PARTICIPATION_ID`),
  ADD KEY `IDX_social_activities_STSTUS_ID` (`STATUS_ID`),
  ADD KEY `IDX_social_activities_USER_ID` (`USER_ID`);

--
-- Индексы таблицы `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `student_group`
--
ALTER TABLE `student_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_student_year_student_id` (`student_id`),
  ADD KEY `IDX_student_year_year_id` (`group_id`),
  ADD KEY `IDX_student_year_status_id` (`status_id`),
  ADD KEY `IDX_student_group_year_id` (`year_id`);

--
-- Индексы таблицы `student_status`
--
ALTER TABLE `student_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_st_status_st_group_id` (`group_id`),
  ADD KEY `FK_st_status_year_id` (`year_id`),
  ADD KEY `FK_student_status_profile_user_id` (`profile_id`);

--
-- Индексы таблицы `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Индексы таблицы `type_edition`
--
ALTER TABLE `type_edition`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `type_event`
--
ALTER TABLE `type_event`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `type_participation`
--
ALTER TABLE `type_participation`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_username` (`username`),
  ADD UNIQUE KEY `user_unique_email` (`email`);

--
-- Индексы таблицы `year`
--
ALTER TABLE `year`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `area`
--
ALTER TABLE `area`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `awards`
--
ALTER TABLE `awards`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `conferences`
--
ALTER TABLE `conferences`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `country`
--
ALTER TABLE `country`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `degree`
--
ALTER TABLE `degree`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `disciplines`
--
ALTER TABLE `disciplines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `document`
--
ALTER TABLE `document`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `formgrade`
--
ALTER TABLE `formgrade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `groups_discipline`
--
ALTER TABLE `groups_discipline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `indexes`
--
ALTER TABLE `indexes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `intellectual`
--
ALTER TABLE `intellectual`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `internship`
--
ALTER TABLE `internship`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `kind_involvement`
--
ALTER TABLE `kind_involvement`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `lang`
--
ALTER TABLE `lang`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT для таблицы `level`
--
ALTER TABLE `level`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `materials`
--
ALTER TABLE `materials`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `organization`
--
ALTER TABLE `organization`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `projects`
--
ALTER TABLE `projects`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `publication`
--
ALTER TABLE `publication`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `publication_indexes`
--
ALTER TABLE `publication_indexes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `qualification`
--
ALTER TABLE `qualification`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `semesters`
--
ALTER TABLE `semesters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `social_activities`
--
ALTER TABLE `social_activities`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `status`
--
ALTER TABLE `status`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `student_group`
--
ALTER TABLE `student_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT для таблицы `student_status`
--
ALTER TABLE `student_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `type_edition`
--
ALTER TABLE `type_edition`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `type_event`
--
ALTER TABLE `type_event`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `type_participation`
--
ALTER TABLE `type_participation`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT для таблицы `year`
--
ALTER TABLE `year`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `awards`
--
ALTER TABLE `awards`
  ADD CONSTRAINT `FK_awards_document_ID` FOREIGN KEY (`DOCUMENT_ID`) REFERENCES `document` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_awards_organization_ID` FOREIGN KEY (`ORGANIZATION_ID`) REFERENCES `organization` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_awards_user_id` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `conferences`
--
ALTER TABLE `conferences`
  ADD CONSTRAINT `FK_conferences_kind_involvement_ID` FOREIGN KEY (`KIND_INVOVLVEMENT_ID`) REFERENCES `kind_involvement` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_conferences_status_ID` FOREIGN KEY (`STATUS_ID`) REFERENCES `status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_conferences_user_id` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `degree`
--
ALTER TABLE `degree`
  ADD CONSTRAINT `FK_degree_area_ID` FOREIGN KEY (`AREA_ID`) REFERENCES `area` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_degree_document_ID` FOREIGN KEY (`DOCUMENT_ID`) REFERENCES `document` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_degree_organization_ID` FOREIGN KEY (`ORGANIZATION_ID`) REFERENCES `organization` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_degree_user_id` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `formgrade`
--
ALTER TABLE `formgrade`
  ADD CONSTRAINT `FK_formgrade_groups_discipline_id` FOREIGN KEY (`groups_discipline_id`) REFERENCES `groups_discipline` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `FK_grades_groups_discipline_id` FOREIGN KEY (`discipline_id`) REFERENCES `groups_discipline` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_grades_profile_user_id` FOREIGN KEY (`student_id`) REFERENCES `profile` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `grades_ibfk_1` FOREIGN KEY (`formgrade_id`) REFERENCES `formgrade` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `FK_groups_year_id` FOREIGN KEY (`id_year`) REFERENCES `year` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `groups_discipline`
--
ALTER TABLE `groups_discipline`
  ADD CONSTRAINT `FK_groups_discipline_disciplines_id` FOREIGN KEY (`discipline_id`) REFERENCES `disciplines` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_groups_discipline_groups_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_groups_discipline_profile_user_id` FOREIGN KEY (`teacher_id`) REFERENCES `profile` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `intellectual`
--
ALTER TABLE `intellectual`
  ADD CONSTRAINT `FK_intellectual_document_ID` FOREIGN KEY (`DOCUMENT_ID`) REFERENCES `document` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_intellectual_organization_ID` FOREIGN KEY (`ORGANIZATION_ID`) REFERENCES `organization` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_intellectual_user_id` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `internship`
--
ALTER TABLE `internship`
  ADD CONSTRAINT `FK_internship_country_ID` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `country` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_internship_user_id` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `materials`
--
ALTER TABLE `materials`
  ADD CONSTRAINT `FK_materials_user_id` FOREIGN KEY (`CREATE_BY`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `FK_projects_document_ID` FOREIGN KEY (`DOCUMENT_ID`) REFERENCES `document` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_projects_organization_ID` FOREIGN KEY (`ORGANIZATION_ID`) REFERENCES `organization` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_projects_user_id` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `publication`
--
ALTER TABLE `publication`
  ADD CONSTRAINT `FK_publication_country_ID` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `country` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_publication_lang_ID` FOREIGN KEY (`LANG_ID`) REFERENCES `lang` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_publication_type_edition_ID` FOREIGN KEY (`TYPE_EDITION_ID`) REFERENCES `type_edition` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_publication_user_id` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `publication_indexes`
--
ALTER TABLE `publication_indexes`
  ADD CONSTRAINT `publication_indexes_ibfk_2` FOREIGN KEY (`PUBLICATION_ID`) REFERENCES `publication` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `publication_indexes_ibfk_3` FOREIGN KEY (`INDEXES_ID`) REFERENCES `indexes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `qualification`
--
ALTER TABLE `qualification`
  ADD CONSTRAINT `FK_qualification_document_ID` FOREIGN KEY (`DOCUMENT_ID`) REFERENCES `document` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_qualification_organization_ID` FOREIGN KEY (`ORGANIZATION_ID`) REFERENCES `organization` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_qualification_user_id` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `FK_reviews_organization_ID` FOREIGN KEY (`ORGANIZATION_ID`) REFERENCES `organization` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_reviews_user_id` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `semesters`
--
ALTER TABLE `semesters`
  ADD CONSTRAINT `FK_semesters_year_id` FOREIGN KEY (`year_id`) REFERENCES `year` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_accountt` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `social_activities`
--
ALTER TABLE `social_activities`
  ADD CONSTRAINT `FK_social_activities_level_ID` FOREIGN KEY (`LEVEL_ID`) REFERENCES `level` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_social_activities_status_ID` FOREIGN KEY (`STATUS_ID`) REFERENCES `status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_social_activities_type_event_ID` FOREIGN KEY (`TYPE_EVENT_ID`) REFERENCES `type_event` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_social_activities_type_participation_ID` FOREIGN KEY (`TYPE_PARTICIPATION_ID`) REFERENCES `type_participation` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_social_activities_user_id` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `student_group`
--
ALTER TABLE `student_group`
  ADD CONSTRAINT `FK_student_group_groups_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_student_group_profile_user_id` FOREIGN KEY (`student_id`) REFERENCES `profile` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_student_group_student_status_id` FOREIGN KEY (`status_id`) REFERENCES `student_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_student_group_year_id` FOREIGN KEY (`year_id`) REFERENCES `year` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `student_status`
--
ALTER TABLE `student_status`
  ADD CONSTRAINT `FK_st_status_st_group_id` FOREIGN KEY (`group_id`) REFERENCES `student_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_st_status_year_id` FOREIGN KEY (`year_id`) REFERENCES `year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_student_status_profile_user_id` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
