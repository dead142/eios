<?php

namespace app\modules\common\models;

use app\modules\frontend\models\SocialActivities;
use Yii;

/**
 * This is the model class for table "{{%type_participation}}".
 *
 * @property integer $ID
 * @property string $NAME
 *
 * @property SocialActivities[] $socialActivities
 */
class TypeParticipation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%type_participation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('social', 'ID'),
            'NAME' => Yii::t('social', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialActivities()
    {
        return $this->hasMany(SocialActivities::className(), ['TYPE_PARTICIPATION_ID' => 'ID']);
    }
}
