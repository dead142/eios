<?php

namespace app\modules\common\models;

use app\modules\frontend\models\Degree;
use Yii;

/**
 * This is the model class for table "{{%area}}".
 *
 * @property integer $ID
 * @property string $NAME
 *
 * @property Degree[] $degrees
 */
class Area extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%area}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('portfolio', 'ID'),
            'NAME' => Yii::t('portfolio', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDegrees()
    {
        return $this->hasMany(Degree::className(), ['AREA_ID' => 'ID']);
    }

}
