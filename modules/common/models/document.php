<?php

namespace app\modules\common\models;

use app\modules\frontend\models\Awards;
use app\modules\frontend\models\Degree;
use app\modules\frontend\models\Intellectual;
use app\modules\frontend\models\Projects;
use app\modules\frontend\models\Qualification;
use Yii;

/**
 * This is the model class for table "{{%document}}".
 *
 * @property integer $ID
 * @property string $NAME
 *
 * @property Awards $awards
 * @property Degree[] $degrees
 * @property Intellectual[] $intellectuals
 * @property Projects[] $projects
 * @property Qualification[] $qualifications
 */
class document extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%document}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('portfolio', 'ID'),
            'NAME' => Yii::t('portfolio', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAwards()
    {
        return $this->hasOne(Awards::className(), ['DOCUMENT_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDegrees()
    {
        return $this->hasMany(Degree::className(), ['DOCUMENT_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntellectuals()
    {
        return $this->hasMany(Intellectual::className(), ['DOCUMENT_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Projects::className(), ['DOCUMENT_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQualifications()
    {
        return $this->hasMany(Qualification::className(), ['DOCUMENT_ID' => 'ID']);
    }


}
