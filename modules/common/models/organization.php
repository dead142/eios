<?php

namespace app\modules\common\models;

use app\modules\frontend\models\Awards;
use app\modules\frontend\models\Degree;
use app\modules\frontend\models\Intellectual;
use app\modules\frontend\models\Projects;
use app\modules\frontend\models\Qualification;
use Yii;

/**
 * This is the model class for table "{{%organization}}".
 *
 * @property integer $ID
 * @property string $NAME
 *
 * @property Awards $awards
 * @property Degree $degree
 * @property Intellectual[] $intellectuals
 * @property Projects[] $projects
 * @property Qualification[] $qualifications
 */
class organization extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%organization}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          //  [['ID'], 'required'],
            [['ID'], 'integer'],
            [['NAME'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('portfolio', 'ID'),
            'NAME' => Yii::t('portfolio', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAwards()
    {
        return $this->hasOne(Awards::className(), ['ORGANIZATION_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDegree()
    {
        return $this->hasOne(Degree::className(), ['ORGANIZATION_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntellectuals()
    {
        return $this->hasMany(Intellectual::className(), ['ORGANIZATION_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Projects::className(), ['ORGANIZATION_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQualifications()
    {
        return $this->hasMany(Qualification::className(), ['ORGANIZATION_ID' => 'ID']);
    }

}
