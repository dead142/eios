<?php

namespace app\modules\common\models;

use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property integer $id
 * @property string $name
 * @property string $desc
 * @property string $create_at
 * @property string $answer
 *
 * @property Answers[] $answers
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['desc', 'answer'], 'string'],
            [['create_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend/main', 'ID'),
            'name' => Yii::t('frontend/main', 'Question short'),
            'desc' => Yii::t('frontend/main', 'Question'),
            'create_at' => Yii::t('frontend/main', 'Question Create At'),
            'answer' => Yii::t('frontend/main', 'Answer'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(Answers::className(), ['question_id' => 'id']);
    }
}
