<?php

namespace app\modules\common\models;

use app\models\Profile;
use app\modules\backend\models\StudentGroup;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "student_status".
 *
 * @property int $id
 * @property string $name
 * @property string $desc
 * @property int $semestr
 * @property string $create_at
 * @property string $update_at
 * @property int $create_by
 * @property string $date_out
 * @property string $date_in
 * @property int $student_group_id
 *
 * @property StudentGroup $studentGroup
 * @property int $year_id [int(11)]
 * @property int $group_id [int(11)]
 * @property int $profile_id [int(11)]
 */
class StudentStatus extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';

    /**
     * Свойства используются в форме для выбора года, студента, группы
     * @var $yearId integer
     * @var $groupId integer
     * @var $studentId integer
     */
    public $yearId;
    public $groupId;
    public $studentId;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['desc'], 'string'],
            [['semestr', 'create_by', 'student_group_id'], 'integer'],
            [['create_at', 'update_at', 'date_out', 'date_in'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['student_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => StudentGroup::className(), 'targetAttribute' => ['student_group_id' => 'id']],
            [['yearId', 'groupId', 'studentId'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => 'Статус',
            'desc' => 'Описание',
            'semestr' => 'Семестр',
            'create_at' => 'Создано',
            'update_at' => 'Обновлено',
            'create_by' => 'Добавил запись',
            'date_out' => 'Дата отчисления',
            'date_in' => 'Дата начала',
            'student_group_id' => 'Группа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentGroup()
    {
        return $this->hasOne(StudentGroup::className(), ['id' => 'student_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'create_by']);
    }

    /**
     * @return array
     */
    public static function status_list()
    {
        $statuses = StudentStatus::find()->select(['name'])->distinct('name')->all();
        return ArrayHelper::map($statuses, 'name', 'name');
    }

    /**
     * Метод добавлеет статус для студента
     * @param string $name
     * @param string $desc
     * @param int $semestr
     * @param $date_in \DateTime
     * @param int $student_group_id
     * @return StudentStatus|array
     */
    public static function createStudentStatus($name = 'active', $desc, $semestr, $date_in, $student_group_id)
    {
        $model = new StudentStatus();
        $model->name = $name;
        $model->date_in = $date_in;
        $model->desc = $desc;
        $model->semestr = $semestr;
        $model->student_group_id = $student_group_id;
        if ($model->save()) {
            return $model;
        } else {
            return $model->errors;
        }
    }

    /**
     * Метод ищет все активные статусы стулдента и меняет их не неактивные
     * если у студента нет стутуса - то он создается
     * @param $groupId
     * @param null $semestr
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function updateStudentStatusInGroup($groupId, $semestr = null)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $flagTransaction = true;
        $studentGroupsFrom = StudentGroup::findAll(['group_id' => $groupId]);
        $studentGroupsIdsFrom = ArrayHelper::getColumn($studentGroupsFrom, 'id'); // StudentGroups IDs
        //Студенты с статусом active
        $studentsWithActiveStatus = StudentStatus::getStudentStatuses(StudentStatus::STATUS_ACTIVE, $studentGroupsIdsFrom);
        $studentsWithActiveStatusIds = ArrayHelper::getColumn($studentsWithActiveStatus, 'id');
        //Обновляем значения всех студентов группы со статусом active
        $updatedCount = StudentStatus::updateAll(['name' => StudentStatus::STATUS_DISABLED, 'date_out' => date("Y-m-d H:i:s")], ['id' => $studentsWithActiveStatusIds]);
        ($updatedCount >= 0) ? $flagTransaction = $flagTransaction && true : $flagTransaction = $flagTransaction && false;
        foreach ($studentGroupsFrom as $studentGroup) {
            if (empty($studentGroup->studentStatuses)) {
                $model = StudentStatus::createStudentStatus(StudentStatus::STATUS_DISABLED, 'Переведен', $semestr, NULL, $studentGroup->id);
                ($model instanceof StudentStatus) ? $flagTransaction = $flagTransaction && true : $flagTransaction = $flagTransaction && false;
            }
        }
        if ($flagTransaction == true) {
            $transaction->commit();
            return true;
        } else {
            $transaction->rollback();
            return false;
        }
    }

    /**
     * @param null $status
     * @param null $studentGroupId
     * @return \app\modules\backend\models\Groups[]|\app\modules\backend\models\GroupsDiscipline[]|StudentGroup[]|\app\modules\backend\models\Year[]|StudentStatus[]|array|\yii\db\ActiveRecord[]
     */
    public static function getStudentStatuses($status = null, $studentGroupId = null)
    {
        $query = StudentStatus::find();
        is_null($status) ? null : $query->andWhere(['name' => StudentStatus::STATUS_ACTIVE]);
        is_null($studentGroupId) ? null : $query->andWhere(['student_group_id' => $studentGroupId]);
        return $query->all();
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'create_by',
                'updatedByAttribute' => false,
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_at',
                'updatedAtAttribute' => 'update_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
