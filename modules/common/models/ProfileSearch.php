<?php

namespace app\modules\common\models;

use app\models\Profile;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OrganizationSearch represents the model behind the search form about `app\modules\frontend\models\organization`.
 */
class ProfileSearch extends Profile
{

    public $surname;
    public $middlename;
    public $group = NULL;

    public $year;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['name','group','surname','middlename','year'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = Profile::find();
        $query->joinWith(['studentGroups']);
        $query->joinWith(['studentGroups.group.idYear']);
       // $query->orderBy('group_id');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'name',
                'surname' => [
                    'asc' => [
                        'surname' => SORT_ASC,
                    ],
                    'desc' => [
                        'surname' => SORT_DESC,
                    ],
                    'label' => 'surname',
                    'default' => SORT_ASC
                ],
                'middlename' => [
                    'asc' => [
                        'middlename' => SORT_ASC,
                    ],
                    'desc' => [
                        'middlename' => SORT_DESC,
                    ],
                    'label' => 'middlename',
                    'default' => SORT_ASC
                ],
                'group' => [
                    'asc' => [
                        'groups.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'groups.name' => SORT_DESC,
                    ],
                    'label' => 'group',
                    'default' => SORT_DESC
                ],
                'year' => [
                    'asc' => [
                        'year.year_start' => SORT_ASC,
                    ],
                    'desc' => [
                        'year.year_start' => SORT_DESC,
                    ],
                    'label' => 'year',
                    'default' => SORT_ASC
                ],
            ],
        ]);
        $this->load($params);
       // var_dump($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_id' => $this->user_id,
        ]);


        $query->andFilterWhere(['like', 'profile.name', $this->name])
            ->andFilterWhere(['like', 'groups.name', $this->group])
            ->andFilterWhere(['like', 'year.year_start', $this->year])
            ->andFilterWhere(['like', 'profile.surname', $this->surname])
            ->andFilterWhere(['like', 'profile.middlename', $this->middlename])
        ;

        return $dataProvider;
    }
}
