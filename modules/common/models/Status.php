<?php

namespace app\modules\common\models;

use app\modules\frontend\models\Conferences;
use app\modules\frontend\models\SocialActivities;
use app\modules\frontend\models\SocialSearch;
use Yii;

/**
 * This is the model class for table "{{%status}}".
 *
 * @property integer $ID
 * @property string $NAME
 *
 * @property Conferences[] $conferences
 * @property SocialActivities[] $socialActivities
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('portfolio', 'ID'),
            'NAME' => Yii::t('portfolio', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConferences()
    {
        return $this->hasMany(Conferences::className(), ['STATUS_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialActivities()
    {
        return $this->hasMany(SocialActivities::className(), ['STATUS_ID' => 'ID']);
    }
}
