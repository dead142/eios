<?php

namespace app\modules\common\models;

use app\modules\frontend\models\Publication;
use Yii;

/**
 * This is the model class for table "country".
 *
 * @property integer $ID
 * @property string $NAME
 *
 * @property Publication[] $publications
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('portfolio', 'ID'),
            'NAME' => Yii::t('portfolio', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublications()
    {
        return $this->hasMany(Publication::className(), ['COUNTRY_ID' => 'ID']);
    }
}
