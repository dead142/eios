<?php

namespace app\modules\common\models;

use app\controllers\MaterialLockController;
use app\models\User;
use Yii;

/**
 * This is the model class for table "materials".
 *
 * @property integer $ID
 * @property string $NAME
 * @property string $FULLTEXT
 * @property string $DESC
 * @property string $DATE_CREATE
 * @property string $DATE_UPDATE
 * @property integer $CREATE_BY
 * @property integer $STATUS
 * @property integer $VERSION
 * @property integer $ORDER
 *
 * @property User $cREATEBY
 */
class Materials extends MaterialLockController
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'materials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME', 'FULLTEXT', 'DESC'], 'required'],
            [['FULLTEXT', 'DESC'], 'string'],
            [['DATE_CREATE', 'DATE_UPDATE'], 'safe'],
            [['CREATE_BY', 'STATUS','VERSION', 'ORDER'], 'integer'],
            [['NAME'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('materials', 'ID'),
            'NAME' => Yii::t('materials', 'Name'),
            'FULLTEXT' => Yii::t('materials', 'Fulltext'),
            'DESC' => Yii::t('materials', 'Desc'),
            'DATE_CREATE' => Yii::t('materials', 'Date  Create'),
            'DATE_UPDATE' => Yii::t('materials', 'Date  Update'),
            'CREATE_BY' => Yii::t('materials', 'Create  By'),
            'STATUS' => Yii::t('materials', 'Status'),
            'VERSION' => Yii::t('materials', 'Version'),
            'ORDER' => Yii::t('materials', 'Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCREATEBY()
    {
        return $this->hasOne(User::className(), ['id' => 'CREATE_BY']);
    }
}
