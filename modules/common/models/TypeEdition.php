<?php

namespace app\modules\common\models;

use app\modules\frontend\models\Publication;
use Yii;

/**
 * This is the model class for table "type_edition".
 *
 * @property integer $ID
 * @property string $NAME
 *
 * @property Publication[] $publications
 */
class TypeEdition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_edition';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('publication', 'ID'),
            'NAME' => Yii::t('publication', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublications()
    {
        return $this->hasMany(Publication::className(), ['TYPE_EDITION_ID' => 'ID']);
    }
}
