<?php

namespace app\modules\common\models;

use app\modules\frontend\models\SocialActivities;
use Yii;

/**
 * This is the model class for table "{{%level}}".
 *
 * @property integer $ID
 * @property string $NAME
 *
 * @property SocialActivities[] $socialActivities
 */
class Level extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%level}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('portfolio', 'ID'),
            'NAME' => Yii::t('portfolio', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialActivities()
    {
        return $this->hasMany(SocialActivities::className(), ['LEVEL_ID' => 'ID']);
    }
}
