<?php

namespace app\modules\common\models;

use app\modules\frontend\models\PublicationIndexes;
use Yii;

/**
 * This is the model class for table "indexes".
 *
 * @property integer $ID
 * @property string $NAME
 *
 * @property PublicationIndexes[] $publicationIndexes
 */
class Indexes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'indexes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('portfolio', 'ID'),
            'NAME' => Yii::t('portfolio', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicationIndexes()
    {
        return $this->hasMany(PublicationIndexes::className(), ['INDEXES_ID' => 'ID']);
    }
}
