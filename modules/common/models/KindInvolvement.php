<?php

namespace app\modules\common\models;

use app\modules\frontend\models\Conferences;
use Yii;

/**
 * This is the model class for table "kind_involvement".
 *
 * @property integer $ID
 * @property string $NAME
 *
 * @property Conferences[] $conferences
 */
class KindInvolvement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kind_involvement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('portfolio', 'ID'),
            'NAME' => Yii::t('portfolio', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConferences()
    {
        return $this->hasMany(Conferences::className(), ['KIND_INVOVLVEMENT_ID' => 'ID']);
    }
}
