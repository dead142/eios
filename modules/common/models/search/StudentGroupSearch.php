<?php

namespace app\modules\common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\backend\models\StudentGroup;

/**
 * StudentGroupSearch represents the model behind the search form about `app\modules\backend\models\StudentGroup`.
 */
class StudentGroupSearch extends StudentGroup
{
    public $fullStudentName; # Полное имя из таблицы profile
    public $groupName;       # Название группы из таблицы groups

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'student_id', 'group_id', 'year_id'], 'integer'],
            [['fullStudentName', 'groupName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StudentGroup::find();
        $query->joinWith('student');    # таблица profile
        $query->joinWith('group');      # таблица group
        $query->joinWith('year');       # таблица year

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'groupName' => [
                    'asc' => [
                        'groups.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'groups.name' => SORT_DESC,
                    ],
                ],
                'year_id' => [
                    'asc' => [
                        'year.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'year.name' => SORT_DESC,
                    ],
                ],
                'fullStudentName' => [
                    'asc' => [
                        'profile.name' => SORT_ASC,
                        'profile.surname' => SORT_ASC,
                        'profile.middlename' => SORT_ASC,
                    ],
                    'desc' => [
                        'profile.name' => SORT_DESC,
                        'profile.surname' => SORT_DESC,
                        'profile.middlename' => SORT_DESC,
                    ],
                    'default' => SORT_ASC
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            #'student_id' => $this->student->fullName,
            'group_id' => $this->group_id,
            'year_id' => $this->year_id,
        ]);

        $query->andFilterWhere([
                'or',
                ['like', 'profile.surname', $this->fullStudentName],
                ['like', 'profile.name', $this->fullStudentName],
                ['like', 'profile.middlename', $this->fullStudentName]
            ]
        )
            ->andFilterWhere(['like', 'groups.name', $this->groupName]);


        return $dataProvider;
    }
}