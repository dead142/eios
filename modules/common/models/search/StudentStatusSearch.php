<?php

namespace app\modules\common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\common\models\StudentStatus;

/**
 * StudentStatusSearch represents the model behind the search form about `app\modules\common\models\StudentStatus`.
 */
class StudentStatusSearch extends StudentStatus
{
    public $group;
    public $year;
    public $fullName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id',  'semestr', 'create_by'], 'integer'],
            [['name', 'desc', 'create_at', 'update_at', 'date_out', 'date_in',
                'group', 'year', 'fullName'
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StudentStatus::find()
            //->select(['*','groups.name as name'])
            ->joinWith('studentGroup.group')
            ->joinWith('studentGroup.group.idYear')
            ->joinWith('studentGroup.student');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
                'attributes' => [
                    'semestr',
                    'create_at',
                    'group' => [
                        'asc' => [
                            'groups.name' => SORT_ASC,
                        ],
                        'desc' => [
                            'groups.name' => SORT_DESC,
                        ],
                    ],
                    'year' => [
                        'asc' => [
                            'year.name' => SORT_ASC,
                        ],
                        'desc' => [
                            'year.name' => SORT_DESC,
                        ],
                    ],
                    'fullName' => [
                        'asc' => [

                            'profile.surname' => SORT_ASC,
                            'profile.name' => SORT_ASC,
                            'profile.middlename' => SORT_ASC,

                        ],
                        'desc' => [

                            'profile.surname' => SORT_DESC,
                            'profile.name' => SORT_DESC,
                            'profile.middlename' => SORT_DESC,

                        ],
                    ],


                    'default' => SORT_ASC
                ],

            ]
        );
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'student_status.name' => $this->name,
            'semestr' => $this->semestr,
            'create_at' => $this->create_at,
            'update_at' => $this->update_at,
            'create_by' => $this->create_by,
            'date_out' => $this->date_out,
            'date_in' => $this->date_in,
        ]);

        $query->andFilterWhere(['like', 'desc', $this->desc])
           ->andFilterWhere(['like', 'groups.name', $this->group])
            ->andFilterWhere(['like', 'year.name', $this->year])
            ->andFilterWhere([
                    'or',
                    ['like', 'profile.surname', $this->fullName],
                    ['like', 'profile.name', $this->fullName],
                    ['like', 'profile.middlename', $this->fullName]
                ]
            );;

        return $dataProvider;
    }
}
