<?php

namespace app\modules\common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\common\models\Materials;

/**
 * MaterialsSearch represents the model behind the search form about `app\modules\common\models\Materials`.
 */
class MaterialsSearch extends Materials
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'CREATE_BY', 'STATUS', 'ORDER'], 'integer'],
            [['NAME', 'FULLTEXT', 'DESC', 'DATE_CREATE', 'DATE_UPDATE'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Materials::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'DATE_CREATE' => $this->DATE_CREATE,
            'DATE_UPDATE' => $this->DATE_UPDATE,
            'CREATE_BY' => $this->CREATE_BY,
            'STATUS' => $this->STATUS,
            'ORDER' => $this->ORDER,
        ]);

        $query->andFilterWhere(['like', 'NAME', $this->NAME])
            ->andFilterWhere(['like', 'FULLTEXT', $this->FULLTEXT])
            ->andFilterWhere(['like', 'DESC', $this->DESC]);

        return $dataProvider;
    }
}
