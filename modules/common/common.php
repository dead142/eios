<?php

namespace app\modules\common;

class common extends \yii\base\Module
{

   // public $layout = 'backend';
    public $controllerNamespace = 'app\modules\common\controllers';


    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
