<?php

namespace app\modules\common\controllers;

use app\modules\frontend\models\Degree;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\UploadedFile;
use app\modules\common\models\Uploads;


use Yii;



/**
 * AwardsController implements the CRUD actions for Awards model.
 */
class FileController extends Controller {

    public function actionMultiple(){

        $controller =Yii::$app->request->post('controller');
        $files = UploadedFile::getInstancesByName('file');
        $model = new Uploads();
        $model->saveUploads($files,$controller);
        return true;
    }
    public function actionTest(){
        $model = new Uploads();
       $data = $model->getInitialPreview('degree',9);
        VarDumper::dump($data,[10],true);
    }

}
