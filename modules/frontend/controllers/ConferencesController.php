<?php

namespace app\modules\frontend\controllers;

use app\modules\frontend\models\ConferencesSearch;
use Yii;
use app\modules\frontend\models\Conferences;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\HttpException;
use yii\web\UploadedFile;

/**
 * ConferencesController implements the CRUD actions for Conferences model.
 */
class ConferencesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins','teacher','student'],

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Conferences models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new ConferencesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            //send to view
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
    }

    /**
     * Displays a single Conferences model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Conferences model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Conferences();

        if ($model->load(Yii::$app->request->post())) {

            $model->DATE_CREATE = date('Y-m-d h:m:s');
            $model->DATE_UPDATE = date('Y-m-d h:m:s');
            $model->USER_ID = Yii::$app->user->identity->id;
            $file = new \app\modules\frontend\models\File();
            $file->saveFile($model);
            $model->save();

            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Conferences model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $old_file = $model->FILE;
        if (!\Yii::$app->user->can('updatePost', ['degree' => $model])) {
            throw new HttpException(403,'You don\'t have premission');
        }
        if ($model->load(Yii::$app->request->post())) {
            $file = new \app\modules\frontend\models\File();
            if ($old_file){
                @unlink($old_file);
            }
            $model->file = UploadedFile::getInstance($model, 'file');
            $file->saveFile($model);
            $model->save();
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Conferences model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model =$this->findModel($id);
        if (!\Yii::$app->user->can('updatePost', ['degree' => $model])) {
            throw new HttpException(403,'You don\'t have premission');
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Conferences model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Conferences the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Conferences::findOne(['id'=>$id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionDeleteimg($id, $field)
    {

        $img = $this->findModel($id)->$field;
        if($img){
            if (!@unlink($img)) {
                return false;
            }
        }

        $img = $this->findModel($id);
        $img->$field = NULL;
        $img->update();

        return $this->redirect(['update', 'id' => $id]);
    }
}
