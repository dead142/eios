<?php

namespace app\modules\frontend\controllers;

use app\modules\frontend\models\DisciplineTeacher;
use Yii;
use app\modules\frontend\models\GradeTypes;
use app\modules\frontend\models\GradeTypesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

/**
 * GradeTypesController implements the CRUD actions for GradeTypes model.
 */
class GradetypesController extends Controller
{
    private $teacherDisciplines;

    public function init()
    {
        //TODO: Сделать нормальный AR
        $Disciplines = DisciplineTeacher::find()
            ->select('DISCIPLINE_ID')
            ->where(['TEACHER_ID' => Yii::$app->user->id])
            ->all();
        if ($Disciplines)
        {
            foreach ($Disciplines as $discipline)
            {
                $this->teacherDisciplines[] = $discipline->DISCIPLINE_ID;
            }
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins','teacher'],

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all GradeTypes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GradeTypesSearch();

        $queryParams = Yii::$app->request->queryParams;

        if (Yii::$app->request->get('discipline'))
        {
            $queryParams['GradeTypesSearch']['DISCIPLINE_ID'] = Yii::$app->request->get('discipline');
        }

        $dataProvider = $searchModel->search($queryParams, $this->teacherDisciplines, 1, 0);
        $archieveDataProvider = $searchModel->search($queryParams, $this->teacherDisciplines, 0, 1);

//        $dataProvider = new ActiveDataProvider([
//            'query' => GradeTypes::find()->where(['DISCIPLINE_ID' => $this->teacherDisciplines]),
//            'pagination' => [
//                'pageSize' => 20,
//            ],
//        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'archieveDataProvider' => $archieveDataProvider,
        ]);
    }

    /**
     * Displays a single GradeTypes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GradeTypes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GradeTypes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->ID]);
            return $this->redirect(['/frontend/grades/teacher']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'teacherDisciplines' => $this->teacherDisciplines
            ]);
        }
    }

    /**
     * Updates an existing GradeTypes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->ID]);
            return $this->redirect(['/frontend/grades/teacher']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'teacherDisciplines' => $this->teacherDisciplines
            ]);
        }
    }

    /**
     * Deletes an existing GradeTypes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GradeTypes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GradeTypes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GradeTypes::findOne(['id'=>$id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
