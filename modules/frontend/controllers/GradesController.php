<?php

namespace app\modules\frontend\controllers;

use app\modules\frontend\models\DisciplineTeacher;
use app\modules\frontend\models\User;
use Yii;
use app\modules\frontend\models\Discipline;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class GradesController extends \yii\web\Controller
{
    private $userID;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['teacher'],
                        'roles' => ['admins','teacher'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'choosegroup', 'grades'],
                        'roles' => ['student'],
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        $this->userID = Yii::$app->user->id;
    }

    public function actionIndex()
    {
        $user = new User();
        $groupChecked = $user->checkGroup($this->userID, Yii::$app->request->get('group'));
        $discipline = new Discipline();
        $disciplineChecked = $discipline->checkDisciplineGroupBound(Yii::$app->request->get('discipline'), Yii::$app->request->get('group'));
        $disciplineArray = null;

        if ($groupChecked && $disciplineChecked)
        {
            $discipline = new Discipline();
            $disciplineArray = $discipline->getGroupDisciplineInfo(Yii::$app->request->get('group'), Yii::$app->request->get('discipline'));
        }

        return $this->render('index', [
            'groupChecked' => $groupChecked,
            'disciplineChecked' => $disciplineChecked,
            'disciplineArray' => $disciplineArray
        ]);
    }

    public function actionChoosegroup ()
    {
        $user = new User();
        $groups = $user->getAllUserGroups($this->userID);



        if (count($groups) == 1)
        {
           $this->redirect('/frontend/grades/grades?group='.$groups[0]['GROUP_ID']);
           // $this->redirect(Url::toRoute(['/frontend/grades/grades', 'groups' => '$groups[0]["GROUP_ID"]']));
        }

        return $this->render('choosegroup', [
            'groups' => $groups,
        ]);
    }

    public function actionGrades ()
    {
        $user = new User();
        $groupChecked = $user->checkGroup($this->userID, Yii::$app->request->get('group'));

        $grades = null;
        $gradesToView = null;
        $archiveGradesToView = null;
        if ($groupChecked)
        {
            $grades = $user->getUserGroupGrades($this->userID, Yii::$app->request->get('group'), true);

            //TODO: Перенести формирование массива в модель либо намутить маппер
            foreach ($grades as $grade)
            {
                $gradesToView[$grade['DISCIPLINE_ID']]['DISCIPLINE_NAME'] = $grade['DISCIPLINE_NAME'];
                $gradesToView[$grade['DISCIPLINE_ID']]['DISCIPLINE_ID'] = $grade['DISCIPLINE_ID'];
                $gradesToView[$grade['DISCIPLINE_ID']]['GRADES'][$grade['GRADE_TYPE_ID']]
                    = array('GRADE_NAME' => $grade['GRADE_TYPE_NAME'], 'GRADE' => $grade['GRADE']);
            }

            $archiveGrades = $user->getUserGroupGrades($this->userID, Yii::$app->request->get('group'));

            foreach ($archiveGrades as $grade)
            {
                $archiveGradesToView[$grade['DISCIPLINE_ID']]['DISCIPLINE_NAME'] = $grade['DISCIPLINE_NAME'];
                $archiveGradesToView[$grade['DISCIPLINE_ID']]['DISCIPLINE_ID'] = $grade['DISCIPLINE_ID'];
                $archiveGradesToView[$grade['DISCIPLINE_ID']]['GRADES'][$grade['GRADE_TYPE_ID']]
                    = array('GRADE_NAME' => $grade['GRADE_TYPE_NAME'], 'GRADE' => $grade['GRADE']);
            }
        }

        return $this->render('grades', [
            'groupChecked' => $groupChecked,
            'grades' => $gradesToView,
            'archiveGrades' => $archiveGradesToView
        ]);
    }

    public function actionTeacher ()
    {
        $disciplineTeacher = new DisciplineTeacher();
        $disciplinesData = $disciplineTeacher->getTeacherDisciplinesData($this->userID, null, true);
        $archiveDisciplinesData = $disciplineTeacher->getTeacherDisciplinesData($this->userID, null, false);

        return $this->render('teacher', [
            'disciplinesData' => $disciplinesData,
            'archiveDisciplinesData' => $archiveDisciplinesData
        ]);
    }

}
