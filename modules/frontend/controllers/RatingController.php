<?

namespace app\modules\frontend\controllers;

use app\modules\frontend\models\Rating;
use Yii;
use yii\web\NotFoundHttpException;


class RatingController extends \yii\web\Controller
{

    /**
     * Проверем сущестувет ли рейтинг у записи
     * Создаем или обновляем его и редиректим на страницу записи
     * @param $url
     * @param null $id
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    public function actionVote($url, $id = null)
    {
        $grade = Yii::$app->request->post('grade');
        if ($id) {
            $model = $this->findModel($id);
            $model->count_vote++;
            $model->grade += $grade;
            $model->save();
            return $this->redirect($url);
        } else {
            $model = new Rating();
            $model->url = $url;
            $model->grade = $grade;
            $model->count_vote++;
            $model->save();
            return $this->redirect($url);
        }
    }

    /**
     * Finds the Publication model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rating the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rating::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

