<?php

namespace app\modules\frontend\controllers;

use Yii;
use app\modules\frontend\models\Comment;
use dektrium\user\models\Profile;

class CommentController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        $makeFrom = ['Comment' => Yii::$app->request->post()];

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new Comment();
        if ($model->load($makeFrom))
        {
            $model->created_at = date('Y-m-d h:m:s');
            $model->created_by = Yii::$app->user->identity->id;

            return $model->save();
        }
        return null;
    }

    public function actionGet()
    {

        $url = Yii::$app->request->post('entity');

        $comments = Comment::find()->where(['entity' => $url, 'deleted' => 0])->with('pROFILE')->all();
        $result = null;
        if ($comments)
        {
            foreach ($comments as $comment)
            {
                $commentPart = [];
                $commentPart['NAME'] = $comment->pROFILE->name;
                $commentPart['SURNAME'] = $comment->pROFILE->surname;
                $commentPart['MIDDLENAME'] = $comment->pROFILE->middlename;
                $commentPart['TEXT'] = $comment->text;
                $commentPart['CREATED_BY'] = $comment->created_by;
                if ($commentPart['CREATED_BY'] == Yii::$app->user->identity->id)
                {
                    $commentPart['CREATED_BY_ME'] = 1;
                }
                $commentPart['ID'] = $comment->id;
                $commentPart['CREATED_AT'] = $comment->created_at;
                $result[] = $commentPart;
            }
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ($result);
    }

    public function actionDelete()
    {
        $comment_id =  Yii::$app->request->post('comment_id');

        $model = Comment::findOne((['id'=>$comment_id]));
        if ($model)
        {
            if ($model->created_by == Yii::$app->user->identity->id)
            {
                $model->deleted = 1;
                return $model->save();
            }
        }
        return;
    }

}
