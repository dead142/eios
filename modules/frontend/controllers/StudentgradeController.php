<?php

namespace app\modules\frontend\controllers;

use app\modules\frontend\models\Discipline;
use Yii;
use app\modules\frontend\models\StudentGrade;
use app\modules\frontend\models\StudentGradeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\frontend\models\GradeTypes;
use app\modules\frontend\models\DisciplineTeacher;
use app\modules\frontend\models\UserGroups;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

/**
 * StudentGradeController implements the CRUD actions for StudentGrade model.
 */
class StudentgradeController extends Controller
{
    private $userID;

    public function init()
    {
        $this->userID = Yii::$app->user->id;
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins','teacher'],

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentGrade models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StudentGradeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * List of grades for requested discipline (from GET)
     * @return mixed
     * ВНИМАНИЕ! Отрабатывает только в случае, если у преподавателя есть указанная дисциплина и она есть в принципе
     */
    public function actionDisciplinegrades()
    {
        $searchModel = new StudentGradeSearch();

        $disciplineChecked = DisciplineTeacher::find()->where(['DISCIPLINE_ID' => Yii::$app->request->get('discipline'), 'TEACHER_ID' => $this->userID])->with('dISCIPLINE')->one();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->getDisciplineGradeTypes(Yii::$app->request->get('discipline')), 1, 0);
        $archieveDataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->getDisciplineGradeTypes(Yii::$app->request->get('discipline')), 0, 1);

//        $dataProvider = new ActiveDataProvider([
//            'query' => StudentGrade::find()->where(['GRADE_TYPE_ID' => $this->getDisciplineGradeTypes(Yii::$app->request->get('discipline'))]),
//            'pagination' => [
//                'pageSize' => 20,
//            ],
//        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'archieveDataProvider' => $archieveDataProvider,
            'disciplineChecked' => $disciplineChecked
        ]);

    }

    /**
     * Displays a single StudentGrade model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StudentGrade model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * ВНИМАНИЕ! Отрабатывает только если указан предмет в GET и он действительно связан с текущим преподавателем
     */
    public function actionCreate()
    {
        $userGroup = new UserGroups();

        $model = new StudentGrade();

        if (Yii::$app->request->get('discipline'))
        {
            $teacherDisciplines[] = Yii::$app->request->get('discipline');
        }
        else
        {
            $teacherDisciplines = $this->getTeacherDisciplines();
        }

        $discipline = new Discipline();
        $teacherStudents = $discipline->getDisciplineStudents($teacherDisciplines, Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->ID]);
            return $this->redirect(['/frontend/grades/teacher']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'teacherDisciplines' => $teacherDisciplines,
                'teacherStudents' => $teacherStudents
            ]);
        }
    }

    public function actionCreategroup ()
    {
        $group = Yii::$app->request->get('group');
        $students = UserGroups::find()->where(['GROUP_ID' => $group])->joinWith(['pROFILE'])->all();
        usort($students, function ($a, $b) {
            return strcmp($a->pROFILE->surname, $b->pROFILE->surname);
        });
        $gradeID = Yii::$app->request->get('grade');
        $grade_type_id = GradeTypes::findOne(['id' => $gradeID]);

        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            $grades = $post['StudentGrade']['GRADE'];

            foreach ($grades as $user_id => $grade)
            {
                if ($grade)
                {
                    $model = StudentGrade::find()->where(['STUDENT_ID' => $user_id, 'GRADE_TYPE_ID' => $grade_type_id->ID])->one();
                    if ($model)
                    {
                        $model->GRADE = $grade;
                        $model->save();
                    }
                    else
                    {
                        $model = new StudentGrade();
                        $model->STUDENT_ID = $user_id;
                        $model->GRADE_TYPE_ID = $grade_type_id->ID;
                        $model->GRADE = $grade;
                        $model->CREATED_DATE = date('Y-m-d H:i:s');
                        $model->TEACHER_ID = Yii::$app->user->id;

                        $model->save();
                    }
                }
            }

            return $this->redirect(['/frontend/grades/teacher']);
    
        } else {
            $model = new StudentGrade();
            return $this->render('creategroup', [
                'model' => $model,
                'students' => $students,
                'grade' => $grade_type_id
            ]);
        }
    }

    /**
     * Updates an existing StudentGrade model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * ВНИМАНИЕ! Отрабатывает только если оценку проставил текущий преподаватель
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $teacherDisciplines = $this->getTeacherDisciplines();
        $discipline = new Discipline();
        $teacherStudents = $discipline->getDisciplineStudents($teacherDisciplines, Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->ID]);
            return $this->redirect(['/frontend/grades/teacher']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'teacherDisciplines' => $teacherDisciplines,
                'teacherStudents' => $teacherStudents
            ]);
        }
    }

    /**
     * Deletes an existing StudentGrade model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * ВНИМАНИЕ! Отрабатывает только если оценку поставил текущий преподаватель
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StudentGrade model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StudentGrade the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StudentGrade::findOne(['id'=>$id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return array or null
     */
    private function getDisciplineGradeTypes($discipline_id)
    {
        $gradeTypes = GradeTypes::find()->where(['DISCIPLINE_ID' => $discipline_id])->all();
        if ($gradeTypes)
        {
            foreach ($gradeTypes as $gradeType)
            {
                $gradeTypesIDs[] = $gradeType->ID;
            }
        }
        return $gradeTypesIDs;
    }

    /**
     * @return array or null
     */
    private function getTeacherDisciplines()
    {
        $objDisciplines = DisciplineTeacher::find()
            ->where(['TEACHER_ID' => Yii::$app->user->id])
            ->all();
        if ($objDisciplines)
        {
            foreach ($objDisciplines as $objDiscipline)
            {
                $teacherDisciplines[] = $objDiscipline->DISCIPLINE_ID;
            }
        }
        $objDisciplines = Discipline::find()
            ->where(['and', "DATE_BEGIN<=".'"'.date('Y-m-d').'"', "DATE_END>=".'"'.date('Y-m-d').'"'])
            ->andWhere(['ID' => $teacherDisciplines])
            ->all();
        $teacherDisciplines = [];
        if ($objDisciplines)
        {
            foreach ($objDisciplines as $objDiscipline)
            {
                $teacherDisciplines[] = $objDiscipline->ID;
            }
        }
        return $teacherDisciplines;
    }
}
