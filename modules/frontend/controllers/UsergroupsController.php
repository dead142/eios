<?php

namespace app\modules\frontend\controllers;

use Yii;
use app\modules\frontend\models\UserGroups;
use app\modules\frontend\models\User;
use app\modules\frontend\models\UserGroupsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserGroupsController implements the CRUD actions for UserGroups model.
 */
class UsergroupsController extends Controller
{
    public $redirectTo = '/frontend/grades/teacher';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins','teacher'],

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all UserGroups models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserGroupsSearch();

        $queryParams = Yii::$app->request->queryParams;

        $group_id = Yii::$app->request->get('group');
        if ($group_id)
        {
            $queryParams['UserGroupsSearch']['GROUP_ID'] = $group_id;
        }

        $dataProvider = $searchModel->search($queryParams, 1, 0);
        $archieveDataProvider = $searchModel->search($queryParams, 0, 1);



        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'archieveDataProvider' => $archieveDataProvider,
            'group_id' => $group_id
        ]);
    }

    /**
     * Displays a single UserGroups model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserGroups();

        //если группа указана, формируем массив не входящих в нее студентов
        $group_id = Yii::$app->request->get('group');
        if ($group_id)
        {
            $queryParams['UserGroupsSearch']['GROUP_ID'] = $group_id;

            $user = new User();
            $studentArray = $user->getUsersByRole('student');

            if (Yii::$app->request->get('group'))
            {
                $usergroups = new UserGroups();

                $groupArray = $usergroups->getStudents(Yii::$app->request->get('group'));
                $excludeStudents = [];
                if ($groupArray)
                {
                    foreach ($groupArray as $student)
                    {
                        $excludeStudents[] = $student['ID'];
                    }
                }
                $studentArray = array_diff($studentArray, $excludeStudents);
            }
        }

        //Проверки на отсутствие дубляжей студентов в группе
        if ($model->load(Yii::$app->request->post())) {
            if (!empty($studentArray))
            {
                if (array_search($model->USER_ID, $studentArray) !== false)
                {
                    if ($model->save())
                    {
                        return $this->redirect([$this->redirectTo]);
                    }
                }
                else
                {
                    return $this->redirect([$this->redirectTo]);
                }

            }
            else
            {
                if ($model->save())
                {
                    return $this->redirect([$this->redirectTo]);
                }
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'studentArray' => $studentArray
            ]);
        }
    }

    /**
     * Updates an existing UserGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->ID]);
            return $this->redirect([$this->redirectTo]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UserGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect([$this->redirectTo]);
    }

    /**
     * Finds the UserGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserGroups::findOne(['id'=>$id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
