<?php

namespace app\modules\frontend\controllers;

use app\modules\frontend\models\SocialSearch;
use Yii;
use app\modules\frontend\models\SocialActivities;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use yii\web\HttpException;
/**
 * SocialController implements the CRUD actions for SocialActivities model.
 */
class SocialController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins','teacher','student'],

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all SocialActivities models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SocialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            // find user degrees in table 'Social'

            //send to view
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
    }

    /**
     * Displays a single SocialActivities model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SocialActivities model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SocialActivities();
        if (Yii::$app->request->isAjax){
            $model->load($_POST);
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ActiveForm::validate($model);
    }
        elseif ($model->load(Yii::$app->request->post()) ) {

            $model->DATE_CREATE = date('Y-m-d h:m:s');
            $model->DATE_UPDATE = date('Y-m-d h:m:s');
            $model->USER_ID = Yii::$app->user->identity->id;
            $file = new \app\modules\frontend\models\File();
            $file->saveFile($model);
            $model->save();
            // Save dependent table

            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SocialActivities model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_file = $model->FILE;
        if (!\Yii::$app->user->can('updatePost', ['degree' => $model])) {
            throw new HttpException(403,'You don\'t have premission');
        }
        if (Yii::$app->request->isAjax){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        elseif ($model->load(Yii::$app->request->post())) {
            $file = new \app\modules\frontend\models\File();
            if ($old_file){
                @unlink($old_file);
            }
            $model->file = UploadedFile::getInstance($model, 'file');
            $file->saveFile($model);
            $model->save();
            return $this->redirect(['view', 'id' => $model->ID]);
        }else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SocialActivities model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model =$this->findModel($id);
        if (!\Yii::$app->user->can('updatePost', ['degree' => $model])) {
            throw new HttpException(403,'You don\'t have premission');
        }

        if (file_exists($model->DOCUMENT)) {
            !@unlink($model->DOCUMENT);
        }
        $model->delete();

        return $this->redirect(['index']);
    }
    /**
     * Deletes an existing SocialActivities file.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteimg($id, $field)
    {

        $img = $this->findModel($id)->$field;
        if (!file_exists($img)) {
            $model=$this->findModel($id);
            $model->DOCUMENT = NULL;
            $model->save();
            return $this->redirect(['update', 'id' => $id]);
        };
        if($img){
            if (!@unlink($img)) {
                return false;
            }
        }

        $img = $this->findModel($id);
        $img->$field = NULL;
        $img->update();

        return $this->redirect(['update', 'id' => $id]);
    }
    /**
     * Finds the SocialActivities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SocialActivities the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SocialActivities::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
