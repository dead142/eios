<?php

namespace app\modules\frontend\controllers;

use app\modules\common\models\Country;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use app\modules\common\models\Lang;
use Yii;
use app\modules\frontend\models\Publication;
use app\modules\frontend\models\PublicationSearch;
use app\modules\frontend\models\PublicationIndexes;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\HttpException;

/**
 * PublicationController implements the CRUD actions for Publication model.
 */
class PublicationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins','teacher','student'],

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Publication models.
     * @return mixed
     */
    public function actionIndex()
    {

         $searchModel = new PublicationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['USER_ID'=>\Yii::$app->user->id]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Publication model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'rating' =>null
        ]);
    }

    /**
     * Creates a new Publication model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Publication();

        if ($model->load(Yii::$app->request->post())) {

            $model->DATE_CREATE = date('Y-m-d h:m:s');
            $model->DATE_UPDATE = date('Y-m-d h:m:s');
            $model->USER_ID =  Yii::$app->user->identity->id;
            //save file
            if ($model->file = UploadedFile::getInstance($model,'file')){
                $fileName = Yii::$app->user->identity->id.'-'.date('Y-m-d-h-m-s'); // Generate name of file 'ID-date.ext'
                $model->file->saveAs('uploads/article/'.$fileName.'.'.$model->file->extension);
                $model->FILE = 'uploads/article/'.$fileName.'.'.$model->file->extension;
            }
            //TODO привести в порядок
            if ($model->REVIEW_F = UploadedFile::getInstance($model,'REVIEW_F')){
                $fileName = 'review'.Yii::$app->user->identity->id.'-'.date('Y-m-d-h-m-s'); // Generate name of file 'ID-date.ext'
                $model->REVIEW_F->saveAs('uploads/article/'.$fileName.'.'.$model->REVIEW_F->extension);
                $model->REVIEW_FILE = 'uploads/article/'.$fileName.'.'.$model->REVIEW_F->extension;
            }
            // Update data in relation table
            $model->COUNTRY_ID = $this->SaveCountry($model);// don't test it yet
            $model->LANG_ID = $this->SaveLang($model);
            // save(false) because have some problem, need to fix
            if ($model->save(false) && $model->INDEXES ) {
                // if model save, delete old indexes and add new
                foreach ($model->INDEXES as $index) {
                    $indexes = new PublicationIndexes();
                    $indexes->INDEXES_ID = $index;
                    $indexes->PUBLICATION_ID = $model->ID;
                    $indexes->save();
                }

            }
           return $this->redirect(['view', 'id' => $model->ID]);


        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Publication model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldFile = $model->FILE;
        $oldReviewFile = $model->REVIEW_FILE;
        if (!\Yii::$app->user->can('updatePost', ['degree' => $model])) {
            throw new HttpException(403,'You don\'t have premission');
        }
        $model->INDEXES =ArrayHelper::getColumn($model->publicationIndexes, 'INDEXES_ID');

        if ($model->load(Yii::$app->request->post())) {

            $model->file = UploadedFile::getInstance($model, 'file');
            $model->REVIEW_F = UploadedFile::getInstance($model, 'REVIEW_F');
            $save_file = '';
            $save_review_file = '';
            if($model->file){
                $fileName = Yii::$app->user->identity->id.'-'.date('Y-m-d-h-m-s');
                $model->FILE = 'uploads/article/'.$fileName.'.'.$model->file->extension;
                $save_file = 1;
            }
            if($model->REVIEW_F){
                $fileName = 'review'.Yii::$app->user->identity->id.'-'.date('Y-m-d-h-m-s');
                $model->REVIEW_FILE = 'uploads/article/'.$fileName.'.'.$model->REVIEW_F->extension;
                $save_review_file = 1;
            }
            $model->COUNTRY_ID = $this->SaveCountry($model);
            $model->LANG_ID = $this->SaveLang($model);
            //echo $model->LANG_ID;


            if ($model->save(false)) {
                if ($save_file) {
                    $model->file->saveAs($model->FILE);
                    if (file_exists($oldFile)) {
                        !unlink($oldFile);
                    }
                    if ($save_review_file) {
                        $model->REVIEW_F->saveAs($model->REVIEW_FILE);
                        if (file_exists($oldReviewFile)) {
                            !unlink($oldReviewFile);
                        }
                    }
                }

                PublicationIndexes::deleteAll(['PUBLICATION_ID'=>$model->ID]);
                if ($model->INDEXES) {
                    foreach ($model->INDEXES as $index) {


                        $indexes = new PublicationIndexes();
                        $indexes->INDEXES_ID = $index;
                        $indexes->PUBLICATION_ID = $model->ID;
                        $indexes->save();
                    }
                }

                return $this->redirect(['view', 'id' => $model->ID]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Publication model.
     * Deletes an existing PortfolioPublications model relation with Publication model
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {



       $model =  $this->findModel($id);

        if (!\Yii::$app->user->can('updatePost', ['degree' => $model])) {
            throw new HttpException(403,'You don\'t have premission');
        }

        if (file_exists($model->FILE)) {
            !unlink($model->FILE);
        }
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Publication model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Publication the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Publication::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteimg($id, $field)
    {

        $img = $this->findModel($id)->$field;
        if($img){
            if (!@unlink($img)) {
                return false;
            }
        }

        $img = $this->findModel($id);
        $img->$field = NULL;
        $img->update();

        return $this->redirect(['update', 'id' => $id]);
    }
    /**
     * If not exist Country create new Country
     * If Country exist return ID Country
     * @model object
     * @return country id
     */
    public function SaveCountry($model){
        if($model->COUNTRY && $model->COUNTRY_ID==null){
            $country = new Country();
            $country->NAME = $model->COUNTRY;
            $country->save();
            return $country->ID;
        }
        else return $model->COUNTRY_ID;
    }
    /**
     * If not exist Lang create new Lang
     * If Lang exist return ID Lang
     * @model object
     * @return lang id
     */
    public  function SaveLang($model){
        if($model->LANG && $model->LANG_ID==null){

            $lang = new Lang();
            $lang->NAME = $model->LANG;
            $lang->save();
            return $lang->ID;
        }
        else return $model->LANG_ID;
    }


}
