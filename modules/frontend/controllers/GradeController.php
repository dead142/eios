<?php
namespace app\modules\frontend\controllers;

use app\models\Profile;
use Yii;
use yii\base\Controller;
use \app\modules\backend\models\GradesSearch;
use yii\helpers\VarDumper;
/**
 * Created by PhpStorm.
 * User: dead1
 * Date: 31.05.2016
 * Time: 19:54
 */
class GradeController extends Controller{

    /*
     * @param $id id of user
     */

    public function actionGradeUser(){
        $id = Yii::$app->request->get('id');
        $searchModel = new GradesSearch();
        $dataProvider  =  $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['student_id'=>$id]);
        $userFullName = Profile::findOne(['user_id'=>$id])->getFullName();
        //$dataProvider->pagination->pageSize=10;

       return $this->render('gradeUser', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'userFullName' => $userFullName,
        ]);

    }
}