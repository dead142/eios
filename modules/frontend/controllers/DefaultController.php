<?php

namespace app\modules\frontend\controllers;

use app\modules\backend\models\Config;
use app\modules\common\models\Materials;
use app\modules\frontend\models\Internship;
use app\modules\frontend\models\InternshipSearch;
use app\modules\frontend\models\ProfileSearch;
use app\modules\frontend\models\ReviewsSearch;
use app\models\Profile;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;

use app\modules\frontend\models\DegreeSearch;
use app\modules\frontend\models\ProjectsSearch ;
use app\modules\frontend\models\AwardsSearch;
use app\modules\frontend\models\SocialSearch;
use app\modules\frontend\models\QualificationSearch;
use app\modules\frontend\models\IntellectualSearch;
use app\modules\frontend\models\ConferencesSearch;
use app\modules\frontend\models\PublicationSearch;




class DefaultController extends Controller
{
    public $dataConferences;
    public $dataIntellectual;
    public $dataQualification;
    public $dataSocial;
    public $dataProjects;
    public $dataDegree;
    public $dataAwards;
    public $dataPublication;
    public $dataInternship;
    public $dataReviews;

    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['all','index', 'view'],
                        'allow' => true,
                    ],

                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Materials::find()->where(['STATUS'=>true])->orderBy('ORDER ASC'),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $searchModelProfile = new ProfileSearch();
        $dataProviderProfile  =  $searchModelProfile->search(Yii::$app->request->queryParams);
        $dataProviderProfile->query->where(['auth_assignment.item_name'=> 'student']);
        $dataProviderProfile->pagination->pageSize=10;
        $isShowPortfolio = Config::find(0)->asArray()->one();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModelProfile' => $searchModelProfile,
            'dataProviderProfile' => $dataProviderProfile,
            'isShowPortfolio' => $isShowPortfolio,
        ]);
    }

    public function actionView($id){
        return $this->render('viewMaterial', [
            'model' => Materials::findOne(['ID'=>$id]),
        ]);
    }
    /**
    * Show Portfolio
    * @param integer $id (USER_ID)
    * @return mixed
    **/
    public function actionAll($id)
    {
        $searchModelConferences = new ConferencesSearch();
        $this->dataConferences = $searchModelConferences->search(Yii::$app->request->queryParams, ['USER_ID'=>$id]);
       
        $searchModelIntellectual = new IntellectualSearch();
        $this->dataIntellectual = $searchModelIntellectual->search(Yii::$app->request->queryParams, ['USER_ID'=>$id]);

        $searchModelQualification = new QualificationSearch();
        $this->dataQualification = $searchModelQualification->search(Yii::$app->request->queryParams, ['USER_ID'=>$id]);

        $searchModelSocial = new SocialSearch();
        $this->dataSocial = $searchModelSocial->search(Yii::$app->request->queryParams, ['USER_ID'=>$id]);

        $searchModelAwards = new AwardsSearch();
        $this->dataAwards = $searchModelAwards->search(Yii::$app->request->queryParams, ['USER_ID'=>$id]);

        $searchModelProjects = new ProjectsSearch();
        $this->dataProjects = $searchModelProjects->search(Yii::$app->request->queryParams, ['USER_ID'=>$id]);

        $searchModelDegree = new DegreeSearch();
        $this->dataDegree = $searchModelDegree->search(Yii::$app->request->queryParams, ['USER_ID'=>$id]);

        // find user degrees in table 'Degree'
        $searchModelPublication = new PublicationSearch();
        $this->dataPublication  = $searchModelPublication->search(Yii::$app->request->queryParams, ['USER_ID'=>$id]);

        // find user degrees in table 'internship'
        $searchModelInternship = new InternshipSearch();
        $this->dataInternship  = $searchModelInternship->search(Yii::$app->request->queryParams);
        $this->dataInternship->query->where(['USER_ID'=>$id]);


        // find user degrees in table 'Reviews'
        $searchModelReviews= new ReviewsSearch();
        $this->dataReviews  = $searchModelReviews ->search(Yii::$app->request->queryParams, ['USER_ID'=>$id]);


        $user = Profile::findOne($id);

        //make a array for query

        //send to view
        return $this->render('all', [
            'user'=>$user,
            //Degree
            'dataDegree' => $this->dataDegree,
            'searchModelDegree' => $searchModelDegree,
            //Projects
            'dataProjects' => $this->dataProjects,
            'searchModelProjects' => $searchModelProjects,
            //Awards
            'dataAwards' => $this->dataAwards,
            'searchModelAwards' => $searchModelAwards,
            //Social
            'dataSocial' => $this->dataSocial,
            'searchModelSocial'=> $searchModelSocial,
            //Qualification
            'dataQualification' => $this->dataQualification,
            'searchModelQualification' => $searchModelQualification,
            //dIntellectual
            'dataIntellectual' => $this->dataIntellectual,
            'searchModelIntellectual' => $searchModelIntellectual,
            //Conferences
            'dataConferences' => $this->dataConferences,
            'searchModelConferences' => $searchModelConferences,
            //Publication
            'dataPublication' => $this->dataPublication,
            'searchModelPublication' =>$searchModelPublication,
             //Publication
            'dataInternship' => $this->dataInternship,
            'searchModelInternship' =>$searchModelInternship,
            //Reviews
            'dataReviews' => $this->dataReviews,
            'searchModelReviews' =>$searchModelReviews

        ]);
    }



}
