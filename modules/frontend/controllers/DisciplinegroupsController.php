<?php

namespace app\modules\frontend\controllers;

use Yii;
use app\modules\frontend\models\DisciplineGroups;
use app\modules\frontend\models\DisciplineGroupsSearch;
use app\modules\frontend\models\DisciplineTeacher;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

/**
 * DisciplineGroupsController implements the CRUD actions for DisciplineGroups model.
 */
class DisciplinegroupsController extends Controller
{
    private $teacherDisciplines;

    public function init()
    {
        //TODO: Сделать нормальный AR
        $Disciplines = DisciplineTeacher::find()
            ->select('DISCIPLINE_ID')
            ->where(['TEACHER_ID' => Yii::$app->user->id])
            ->all();
        if ($Disciplines)
        {
            foreach ($Disciplines as $discipline)
            {
                $this->teacherDisciplines[] = $discipline->DISCIPLINE_ID;
            }
        }
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins','teacher'],

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all DisciplineGroups models.
     * Provide bound with disciplines of current teacher
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DisciplineGroupsSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->teacherDisciplines, 1, 0);
        $archieveDataProvider = $searchModel->search(Yii::$app->request->queryParams, $this->teacherDisciplines, 0, 1);

        $searchModel = new DisciplineGroupsSearch();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'archieveDataProvider' => $archieveDataProvider,
        ]);
    }

    /**
     * Displays a single DisciplineGroups model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DisciplineGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DisciplineGroups();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->ID]);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'teacherDisciplines' => $this->teacherDisciplines
            ]);
        }
    }

    /**
     * Updates an existing DisciplineGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $objDisciplines = DisciplineTeacher::find()->where(['TEACHER_ID' => Yii::$app->user->id])->all();
        if ($objDisciplines)
        {
            foreach ($objDisciplines as $objDiscipline)
            {
                $teacherDisciplines[] = $objDiscipline->DISCIPLINE_ID;
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->ID]);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'teacherDisciplines' => $teacherDisciplines
            ]);
        }
    }

    /**
     * Deletes an existing DisciplineGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DisciplineGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DisciplineGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DisciplineGroups::findOne(['id'=>$id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
