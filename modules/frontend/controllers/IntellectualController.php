<?php

namespace app\modules\frontend\controllers;

use app\modules\frontend\models\IntellectualSearch;
use Yii;
use app\modules\frontend\models\Intellectual;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\web\HttpException;
use yii\web\UploadedFile;

/**
 * IntellectualController implements the CRUD actions for Intellectual model.
 */
class IntellectualController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins','teacher','student'],

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Intellectual models.
     * @return mixed
     */
    public function actionIndex()
    {

            // find user degrees in table 'Degree'
        $searchModel = new IntellectualSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['USER_ID'=>\Yii::$app->user->id]);
            //send to view
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
    }

    /**
     * Displays a single Intellectual model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Intellectual model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Intellectual();

        if ($model->load(Yii::$app->request->post())) {
            $model->DATE_CREATE = date('Y-m-d h:m:s');
            $model->DATE_UPDATE = date('Y-m-d h:m:s');
            $model->USER_ID = Yii::$app->user->identity->id;
            $file = new \app\modules\frontend\models\File();
            $file->saveFile($model);
            $model->save();

            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Intellectual model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_file = $model->FILE;
        if (!\Yii::$app->user->can('updatePost', ['degree' => $model])) {
            throw new HttpException(403,'You don\'t have premission');
        }
        if ($model->load(Yii::$app->request->post())) {
            $file = new \app\modules\frontend\models\File();
            if ($old_file){
                @unlink($old_file);
            }
            $model->file = UploadedFile::getInstance($model, 'file');
            $file->saveFile($model);
            $model->save();
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Intellectual model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model =$this->findModel($id);
        if (!\Yii::$app->user->can('updatePost', ['degree' => $model])) {
            throw new HttpException(403,'You don\'t have premission');
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Intellectual model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Intellectual the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Intellectual::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionDeleteimg($id, $field)
    {

        $img = $this->findModel($id)->$field;
        if($img){
            if (!@unlink($img)) {
                return false;
            }
        }

        $img = $this->findModel($id);
        $img->$field = NULL;
        $img->update();

        return $this->redirect(['update', 'id' => $id]);
    }
}
