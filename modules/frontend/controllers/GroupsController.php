<?php

namespace app\modules\frontend\controllers;

use app\modules\backend\models\StudentGroup;
use app\models\Profile;
use app\modules\common\models\search\StudentGroupSearch;
use app\modules\frontend\models\ProfileSearch;
use Yii;
use yii\data\ActiveDataFilter;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\data\ActiveDataProvider;

class GroupsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admins', 'teacher', 'student'],
                    ],
                ],
            ],
        ];
    }

    /*
     * Show all users in group of current user
     */
    public function actionUsersMyGroup()
    {
        $id = Yii::$app->user->identity->id;
        if ($profile = StudentGroup::StudentGroup($id)) {
            $dataProvider = new ActiveDataProvider([
                'query' => Profile::find()->joinWith('studentGroups')->where(['student_group.group_id' => $profile->group_id])->asArray(),
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
            //send to view
            return $this->render('UserMyGroup', [
                'dataProvider' => $dataProvider,
            ]);
        }
        throw new NotFoundHttpException(Yii::t('exception', 'You don\'t have a group, please conact to administration'), 200);
    }

    /**
     * Функция передает студентов всех груп и всех годов
     * @return string
     */
    function actionListUserGroup()
    {
        $searchModel = new ProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['auth_assignment.item_name'=>'student']); //Выбираем только студентов
        $dataProvider->query->andWhere(['groups.active'=>true]); //У которые обучаются сейчас


//        $searchModel = new StudentGroupSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->pagination->pageSize = 10; #Количество записей на странице
        return $this->render('list-user-group', [
            'dataProvider' => $dataProvider,
             'searchModel' => $searchModel,
        ]);

    }


}
