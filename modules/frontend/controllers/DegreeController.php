<?php

namespace app\modules\frontend\controllers;

use app\modules\common\models\Uploads;
use app\modules\frontend\models\organization;
use Yii;
use app\modules\frontend\models\Degree;
use app\modules\frontend\models\DegreeSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * DegreeController implements the CRUD actions for Degree model.
 */
class DegreeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
             
                        'allow' => true,
                        'roles' => ['admins','teacher','student'],

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists user Degree models.
     * @return mixed
     */
    public function actionIndex()
    {
        // find user degrees
        $searchModel = new DegreeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['USER_ID'=>\Yii::$app->user->id]);

       //send to view
      return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);

    }

    /**
     * Displays a single Degree model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //need to fix



        return $this->render('view', [
            'model' => $this->findModel($id),
            //'owner' => $owner,
        ]);
    }

    /**
     * Creates a new Degree model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Degree();
        $files = UploadedFile::getInstancesByName('file');

       if ($model->load(Yii::$app->request->post())) {
          // VarDumper::dump($model,[10],TRUE);
           //Save data to Degree table
            $model->DATE_CREATE = date('Y-m-d h:m:s');
            $model->DATE_UPDATE = date('Y-m-d h:m:s');
            $model->USER_ID = Yii::$app->user->identity->id;
           $file = new \app\modules\frontend\models\File();
           $file->saveFile($model);
            $model->save();

           return $this->redirect(['view', 'id' => $model->ID]);

          //  $uploads = new Uploads();
        //  $uploads->saveUploads($files,'degree',$model->ID);

         //  return $this->redirect(['view', 'id' => $model->ID]);


        } else {
           return $this->render('create', [
               'model' => $model,
           ]);

       }
    }

    /**
     * Updates an existing Degree model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $old_file = $model->FILE;

        if (!\Yii::$app->user->can('updatePost', ['degree' => $model])) {
            throw new HttpException(403,'You don\'t have premission');
        }

        if ($model->load(Yii::$app->request->post())) {
            $file = new \app\modules\frontend\models\File();
            if ($old_file){
               @unlink($old_file);
            }
            $model->file = UploadedFile::getInstance($model, 'file');
            $file->saveFile($model);
            $model->save();
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {

            return $this->render('update', [
                'model' => $model,
               // 'initialPreview'=>$initialPreview,
               // 'initialPreviewConfig'=>$initialPreviewConfig
            ]);
        }


    }

    /**
     * Deletes an existing Degree model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model =$this->findModel($id);
        if (!\Yii::$app->user->can('updatePost', ['degree' => $model])) {
            throw new HttpException(403,'You don\'t have premission');
        }
       $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Degree model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Degree the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Degree::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionDeleteimg($id, $field)
    {

        $img = $this->findModel($id)->$field;
        if($img){
            if (!@unlink($img)) {
                return false;
            }
        }

        $img = $this->findModel($id);
        $img->$field = NULL;
        $img->update();

        return $this->redirect(['update', 'id' => $id]);
    }
}
