<?php

namespace app\modules\frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\frontend\models\Intellectual;

/**
 * IntellectualSearch represents the model behind the search form about `app\modules\frontend\models\Intellectual`.
 */
class IntellectualSearch extends Intellectual
{
    public $DOCUMENT_NAME;
    public $ORGANIZATION_NAME;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ORGANIZATION_ID', 'DOCUMENT_ID'], 'integer'],
            [['DOCUMENT_NAME','ORGANIZATION_NAME','NAME', 'COPYRIGHT', 'DOCUMENT', 'DATE_ISSUE', 'DESC', 'DATE_CREATE', 'DATE_UPDATE','FILE'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Intellectual::find();
        $query->joinWith(['dOCUMENT','oRGANIZATION']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes' => [
                'NAME',
                'DOCUMENT',
                'DATE_ISSUE',
                'FILE',
                'DOCUMENT_NAME' => [
                    'asc' => [
                        'document.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'document.NAME' => SORT_DESC,
                    ],
                    'label' => 'DOCUMENT_NAME',
                    'default' => SORT_ASC
                ],
                'ORGANIZATION_NAME' => [
                    'asc' => [
                        'organization.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'organization.NAME' => SORT_DESC,
                    ],
                    'label' => 'ORGANIZATION_NAME',
                    'default' => SORT_ASC
                ],
            ],
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ORGANIZATION_ID' => $this->ORGANIZATION_ID,
            'DOCUMENT_ID' => $this->DOCUMENT_ID,
            'DATE_ISSUE' => $this->DATE_ISSUE,
            'DATE_CREATE' => $this->DATE_CREATE,
            'DATE_UPDATE' => $this->DATE_UPDATE,
        ]);

        $query->andFilterWhere(['like', 'intellectual.NAME', $this->NAME])
            ->andFilterWhere(['USER_ID' =>  isset($params['id'])? $params['id'] :Yii::$app->user->identity->id])
            ->andFilterWhere(['like', 'document.NAME', $this->DOCUMENT_NAME])
            ->andFilterWhere(['like', 'COPYRIGHT', $this->COPYRIGHT])
            ->andFilterWhere(['like', 'FILE', $this->FILE])
            ->andFilterWhere(['like', 'DOCUMENT', $this->DOCUMENT])
            ->andFilterWhere(['like', 'DESC', $this->DESC]);

        return $dataProvider;
    }
}
