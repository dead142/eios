<?php

namespace app\modules\frontend\models;

use app\modules\common\models\Area;
use app\modules\common\models\document;
use app\modules\common\models\organization;
use Yii;

/**
 * This is the model class for table "degree".
 *
 * @property integer $ID
 * @property string $NAME
 * @property integer $AREA_ID
 * @property integer $ORGANIZATION_ID
 * @property integer $DOCUMENT_ID
 * @property string $ISSUE_DATE
 *  @property string $FILE
 * @property string $DOCUMENT_NUMBER
 * @property string $DATE_CREATE
 * @property string $DATE_UPDATE
 * @property integer $USER_ID
 *
 * @property User $uSER
 * @property Area $aREA
 * @property Document $dOCUMENT
 * @property Organization $oRGANIZATION
 */
class Degree extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'degree';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME'], 'required'],
            [['AREA_ID', 'ORGANIZATION_ID', 'DOCUMENT_ID', 'USER_ID'], 'integer'],
            [['ISSUE_DATE', 'DATE_CREATE', 'DATE_UPDATE'], 'safe'],
            [['NAME'], 'string', 'max' => 50],
            [['file'],'file'],
            [['DOCUMENT_NUMBER'], 'string', 'max' => 255],
           // [['file'], 'file',  'maxFiles' => 4 ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('degrees', 'ID'),
            'NAME' => Yii::t('degrees', 'Name'),
            'AREA_ID' => Yii::t('degrees', 'Area of science'),
            'ORGANIZATION_ID' => Yii::t('degrees', 'The name of the organization'),
            'DOCUMENT_ID' => Yii::t('degrees', 'The document number'),
            'ISSUE_DATE' => Yii::t('degrees', 'Date of issue'),
            'DOCUMENT_NUMBER' => Yii::t('degrees', 'The document TITLE'),
            'DATE_CREATE' => Yii::t('degrees', 'Date  Create'),
            'DATE_UPDATE' => Yii::t('degrees', 'Date  Update'),
            'USER_ID' => Yii::t('degrees', 'User  ID'),
            'DOCUMENT_NAME' => Yii::t('degrees', 'DOCUMENT_NAME'),
            'file' => Yii::t('social', 'DOCUMENT LINK'),


        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSER()
    {
        return $this->hasOne(User::className(), ['id' => 'USER_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAREA()
    {
        return $this->hasOne(Area::className(), ['ID' => 'AREA_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDOCUMENT()
    {
        return $this->hasOne(document::className(), ['ID' => 'DOCUMENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getORGANIZATION()
    {
        return $this->hasOne(organization::className(), ['ID' => 'ORGANIZATION_ID']);
    }
}
