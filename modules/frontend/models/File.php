<?php
namespace app\modules\frontend\models;
use yii\rest\Controller;
use yii\web\UploadedFile;
use Yii;
/**
 * Created by PhpStorm.
 * User: dead1
 * Date: 03.06.2016
 * Time: 20:29
 */
class File {
    /*
     * @param obj $model
     * return true|false
     */
    public function saveFile($model){
        if ($model->file = UploadedFile::getInstance($model,'file')){
            $fileName = Yii::$app->user->identity->id.'-'.date('Y-m-d-h-m-s'); // Generate name of file 'ID-date.ext'
            $model->file->saveAs('uploads/article/'.$fileName.'.'.$model->file->extension);
            $model->FILE = 'uploads/article/'.$fileName.'.'.$model->file->extension;
        }
    }
}