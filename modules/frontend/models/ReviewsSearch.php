<?php

namespace app\modules\frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\frontend\models\Reviews;

/**
 * ReviewsSearch represents the model behind the search form about `app\modules\frontend\models\Reviews`.
 */
class ReviewsSearch extends Reviews
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID',  'USER_ID'], 'integer'],
            [['NAME', 'AUTHOR', 'CREATE_DATE', 'FILE','ORGANIZATION_ID',], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reviews::find();
        $query->joinWith(['oRGANIZATION']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes' => [
                'NAME',
                'AUTHOR',
                'FILE',

                'ORGANIZATION_ID' => [
                    'asc' => [
                        'oRGANIZATION.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'oRGANIZATION.NAME' => SORT_DESC,
                    ],
                    'label' => 'ORGANIZATION_ID',
                    'default' => SORT_ASC
                ],

            ]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
           // 'ORGANIZATION_ID' => $this->ORGANIZATION_ID,
            'USER_ID' => $this->USER_ID,
            'CREATE_DATE' => $this->CREATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'NAME', $this->NAME])
            ->andFilterWhere(['like', 'AUTHOR', $this->AUTHOR])
            ->andFilterWhere(['like', 'oRGANIZATION.NAME', $this->ORGANIZATION_ID])
            ->andFilterWhere(['like', 'FILE', $this->FILE]);

        return $dataProvider;
    }
}
