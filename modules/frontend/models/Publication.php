<?php

namespace app\modules\frontend\models;

use app\modules\common\models\Country;
use app\modules\common\models\Lang;
use app\modules\common\models\TypeEdition;
use Yii;

/**
 * This is the model class for table "publication".
 *
 * @property integer $ID
 * @property string $NAME
 * @property integer $LANG_ID
 * @property string $YEAR
 * @property integer $COUNTRY_ID
 * @property string $CITY
 * @property integer $TYPE_EDITION_ID
 * @property string $PUBLISHER
 * @property string $PAGES
 * @property string $FILE
 * @property string $REVIEW_FILE
 * @property string $DESC
 * @property string $DATE_CREATE
 * @property string $DATE_UPDATE
 * @property integer $USER_ID
 * @property double $rating
 *
 * @property User $uSER
 * @property Country $cOUNTRY
 * @property Lang $lANG
 * @property TypeEdition $tYPEEDITION
 * @property PublicationIndexes[] $publicationIndexes
 */
class Publication extends \yii\db\ActiveRecord
{
    public $INDEXES;
    public $COUNTRY;
    public $LANG;
    public $file;
    public $REVIEW_F;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publication';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME'], 'required'],
            [['LANG_ID','INDEXES', 'COUNTRY_ID', 'TYPE_EDITION_ID', 'USER_ID'], 'integer'],
            [['YEAR','LANG', 'COUNTRY', 'DATE_CREATE', 'DATE_UPDATE'], 'safe'],
            [['rating'], 'number'],
            [['file','REVIEW_FILE'],'file'],
            [['NAME', 'CITY', 'PUBLISHER', 'PAGES', 'FILE', 'DESC'], 'string', 'max' => 255]

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('publication', 'ID'),
            'NAME' => Yii::t('publication', 'Name'),
            'LANG_ID' => Yii::t('publication', 'Lang  ID'),
            'YEAR' => Yii::t('publication', 'Year'),
            'COUNTRY_ID' => Yii::t('publication', 'Country  ID'),
            'CITY' => Yii::t('publication', 'City'),
            'TYPE_EDITION_ID' => Yii::t('publication', 'Type  Edition  ID'),
            'PUBLISHER' => Yii::t('publication', 'Publisher'),
            'PAGES' => Yii::t('publication', 'Pages'),
            'FILE' => Yii::t('publication', 'File'),
            'DESC' => Yii::t('publication', 'Desc'),
            'DATE_CREATE' => Yii::t('publication', 'Date  Create'),
            'DATE_UPDATE' => Yii::t('publication', 'Date  Update'),
            'USER_ID' => Yii::t('publication', 'User  ID'),
            'file' => Yii::t('social', 'DOCUMENT LINK'),
            'INDEXES' => Yii::t('publication', 'INDEXES'),
            'rating' => Yii::t('publication', 'Rating'),
            'REVIEW_F' => Yii::t('publication', 'Review File'),
        ];
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSER()
    {
        return $this->hasOne(User::className(), ['id' => 'USER_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCOUNTRY()
    {
        return $this->hasOne(Country::className(), ['ID' => 'COUNTRY_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLANG()
    {
        return $this->hasOne(Lang::className(), ['ID' => 'LANG_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTYPEEDITION()
    {
        return $this->hasOne(TypeEdition::className(), ['ID' => 'TYPE_EDITION_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicationIndexes()
    {
        return $this->hasMany(PublicationIndexes::className(), ['PUBLICATION_ID' => 'ID']);
    }
}
