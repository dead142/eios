<?php

namespace app\modules\frontend\models;

use app\modules\common\models\Indexes;
use Yii;

/**
 * This is the model class for table "publication_indexes".
 *
 * @property integer $ID
 * @property integer $INDEXES_ID
 * @property integer $PUBLICATION_ID
 *
 * @property Indexes $iNDEXES
 * @property Publication $pUBLICATION
 */
class PublicationIndexes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'publication_indexes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['INDEXES_ID', 'PUBLICATION_ID'], 'required'],
            [['INDEXES_ID', 'PUBLICATION_ID'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('publication', 'ID'),
            'INDEXES_ID' => Yii::t('publication', 'Indexes  ID'),
            'PUBLICATION_ID' => Yii::t('publication', 'Publication  ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getINDEXES()
    {
        return $this->hasOne(Indexes::className(), ['ID' => 'INDEXES_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPUBLICATION()
    {
        return $this->hasOne(Publication::className(), ['ID' => 'PUBLICATION_ID']);
    }
}
