<?php

namespace app\modules\frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\frontend\models\Internship;

/**
 * InternshipSearch represents the model behind the search form about `app\modules\frontend\models\Internship`.
 */
class InternshipSearch extends Internship
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'USER_ID' ], 'integer'],
            [['NAME', 'DATE_START', 'DATE_END', 'DATE_CREATE', 'FILE','COUNTRY_ID'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Internship::find();
        $query->joinWith(['cOUNTRY']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes' => [
                'NAME',
                'DATE_START',
                'DATE_END',
                'FILE',

                'COUNTRY_ID' => [
                    'asc' => [
                        'cOUNTRY.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'cOUNTRY.NAME' => SORT_DESC,
                    ],
                    'label' => 'COUNTRY_ID',
                    'default' => SORT_ASC
                ],

            ]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'DATE_START' => $this->DATE_START,
            'DATE_END' => $this->DATE_END,
            'DATE_CREATE' => $this->DATE_CREATE,
            'USER_ID' => $this->USER_ID,
            //'COUNTRY_ID' => $this->COUNTRY_ID,
        ]);

        $query->andFilterWhere(['like', 'NAME', $this->NAME])
            ->andFilterWhere(['like', 'FILE', $this->FILE])
            ->andFilterWhere(['like', 'cOUNTRY.NAME', $this->COUNTRY_ID]);

        return $dataProvider;
    }
}
