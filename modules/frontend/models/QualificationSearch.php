<?php

namespace app\modules\frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\frontend\models\Qualification;

/**
 * QualificationSearch represents the model behind the search form about `app\modules\frontend\models\Qualification`.
 */
class QualificationSearch extends Qualification
{
    public $DOCUMENT_NAME;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ORGANIZATION_ID', 'DOCUMENT_ID'], 'integer'],
            [['NAME', 'LOCATION','DOCUMENT_NAME', 'DATE_START', 'DATE_END', 'DATE_ISSUE', 'DESC', 'HOURS', 'DATE_CREATE', 'DATE_UPDATE','FILE'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Qualification::find();

        $query->joinWith(['dOCUMENT']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'NAME',
                'LOCATION',
                'DATE_START',
                'DATE_END',
                'DATE_ISSUE',
                'DATE_CREATE',
                'FILE',
                'DOCUMENT_NAME' => [
                    'asc' => [
                        'document.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'document.NAME' => SORT_DESC,
                    ],
                    'label' => 'DOCUMENT_NAME',
                    'default' => SORT_ASC
                ],
            ],
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'DATE_START' => $this->DATE_START,
            'DATE_END' => $this->DATE_END,
            'ORGANIZATION_ID' => $this->ORGANIZATION_ID,
            'DOCUMENT_ID' => $this->DOCUMENT_ID,
            'DATE_ISSUE' => $this->DATE_ISSUE,
            'DATE_CREATE' => $this->DATE_CREATE,
            'DATE_UPDATE' => $this->DATE_UPDATE,
        ]);

        $query->andFilterWhere(['like', 'qualification.NAME', $this->NAME])
            ->andFilterWhere(['USER_ID' => isset($params['id'])? $params['id'] :Yii::$app->user->identity->id])
            ->andFilterWhere(['like', 'document.NAME', $this->DOCUMENT_NAME])
            ->andFilterWhere(['like', 'LOCATION', $this->LOCATION])
            ->andFilterWhere(['like', 'FILE', $this->FILE])
            ->andFilterWhere(['like', 'HOURS', $this->HOURS]);
            //->andFilterWhere(['like', 'DESC', $this->DESC]);

        return $dataProvider;
    }
}
