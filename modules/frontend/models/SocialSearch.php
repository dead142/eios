<?php

namespace app\modules\frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\frontend\models\SocialActivities;
use yii\helpers\VarDumper;

/**
 * SocialSearch represents the model behind the search form about `app\modules\frontend\models\SocialActivities`.
 */
class SocialSearch extends SocialActivities
{
    public $STATUS_NAME;
    public $TYPE_EVENT_NAME;
    public $LEVEL_NAME;
    public $TYPE_PARTICIPATION_NAME;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'STATUS_ID', 'TYPE_EVENT_ID', 'LEVEL_ID', 'TYPE_PARTICIPATION_ID'], 'integer'],
            [['STATUS_NAME','TYPE_EVENT_NAME','LEVEL_NAME','NAME','TYPE_PARTICIPATION_NAME', 'DATE_START', 'DATE_END', 'DESC', 'DATE_CREATE', 'DATE_UPDATE','FILE'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SocialActivities::find();
       $query->joinWith(['sTATUS','tYPEEVENT']);
      // $query->joinWith(['sTATUS','tYPEEVENT']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'NAME',
                'DOCUMENT_NUMBER',
                'DESC',
                'DATE_CREATE',
                'FILE',
                'STATUS_NAME' => [
                    'asc' => [
                        'status.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'status.NAME' => SORT_DESC,
                    ],
                    'label' => 'STATUS_NAME',
                    'default' => SORT_ASC
                ],
                'TYPE_EVENT_NAME' => [
                    'asc' => [
                        'type_event.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'type_event.NAME' => SORT_DESC,
                    ],
                    'label' => 'TYPE_EVENT_NAME',
                    'default' => SORT_ASC
                ],
               ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'DATE_START' => $this->DATE_START,
            'DATE_END' => $this->DATE_END,
            'STATUS_ID' => $this->STATUS_ID,
            'TYPE_EVENT_ID' => $this->TYPE_EVENT_ID,
            'LEVEL_ID' => $this->LEVEL_ID,
            'TYPE_PARTICIPATION_ID' => $this->TYPE_PARTICIPATION_ID,
            'DATE_CREATE' => $this->DATE_CREATE,
            'DATE_UPDATE' => $this->DATE_UPDATE,
        ]);

        $query->andFilterWhere(['like', 'social_activities.NAME', $this->NAME])
            ->andFilterWhere(['USER_ID' =>  isset($params['id'])? $params['id'] :Yii::$app->user->identity->id])
            ->andFilterWhere(['like', 'status.NAME', $this->STATUS_NAME])
         ->andFilterWhere(['like', 'type_event.NAME', $this->TYPE_EVENT_NAME])
            ->andFilterWhere(['like', 'FILE', $this->FILE])
         //  ->andFilterWhere(['like', 'lEVEL.NAME', $this->LEVEL_NAME])
          // ->andFilterWhere(['like', 'tYPEPARTICIPATION.NAME', $this->TYPE_PARTICIPATION_NAME])
            ->andFilterWhere(['like', 'DESC', $this->DESC]);

        return $dataProvider;
    }
}
