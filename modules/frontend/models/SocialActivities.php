<?php

namespace app\modules\frontend\models;

use app\modules\common\models\Level;
use app\modules\common\models\Status;
use app\modules\common\models\TypeEvent;
use app\modules\common\models\TypeParticipation;
use Yii;

/**
 * This is the model class for table "social_activities".
 *
 * @property integer $ID
 * @property string $NAME
 * @property string $DATE_START
 * @property string $DATE_END
 * @property integer $STATUS_ID
 * @property integer $TYPE_EVENT_ID
 * @property integer $LEVEL_ID
 * @property integer $TYPE_PARTICIPATION_ID
 * @property string $DESC
 * @property string $DATE_CREATE
 * @property string $DATE_UPDATE
 * @property integer $USER_ID
 *
 * @property User $uSER
 * @property Level $lEVEL
 * @property Status $sTATUS
 * @property TypeEvent $tYPEEVENT
 * @property TypeParticipation $tYPEPARTICIPATION
 */
class SocialActivities extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'social_activities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME', 'DATE_START', 'DATE_END'], 'required'],
            [['DATE_START', 'DATE_END', 'DATE_CREATE', 'DATE_UPDATE'], 'safe'],
            [['STATUS_ID', 'TYPE_EVENT_ID', 'LEVEL_ID', 'TYPE_PARTICIPATION_ID', 'USER_ID'], 'integer'],
            [['NAME'], 'string', 'max' => 50],
            [['file'],'file'],
            [['DESC'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('social', 'ID'),
            'NAME' => Yii::t('social', 'Name'),
            'DATE_START' => Yii::t('social', 'Date  Start'),
            'DATE_END' => Yii::t('social', 'Date  End'),
            'STATUS_ID' => Yii::t('social', 'Status  ID'),
            'TYPE_EVENT_ID' => Yii::t('social', 'Type of event'),
            'LEVEL_ID' => Yii::t('social', 'Level  ID'),
            'TYPE_PARTICIPATION_ID' => Yii::t('social', 'Type of participation'),

            'DESC' => Yii::t('social', 'Desc'),
            'DATE_CREATE' => Yii::t('social', 'Date  Create'),
            'DATE_UPDATE' => Yii::t('social', 'Date  Update'),
            'USER_ID' => Yii::t('social', 'User  ID'),
            'file' => Yii::t('social', 'DOCUMENT LINK'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSER()
    {
        return $this->hasOne(User::className(), ['id' => 'USER_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLEVEL()
    {
        return $this->hasOne(Level::className(), ['ID' => 'LEVEL_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSTATUS()
    {
        return $this->hasOne(Status::className(), ['ID' => 'STATUS_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTYPEEVENT()
    {
        return $this->hasOne(TypeEvent::className(), ['ID' => 'TYPE_EVENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTYPEPARTICIPATION()
    {
        return $this->hasOne(TypeParticipation::className(), ['ID' => 'TYPE_PARTICIPATION_ID']);
    }
}
