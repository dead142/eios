<?php

namespace app\modules\frontend\models;

use app\modules\common\models\document;
use app\modules\common\models\organization;
use Yii;

/**
 * This is the model class for table "intellectual".
 *
 * @property integer $ID
 * @property string $NAME
 * @property string $COPYRIGHT
 * @property integer $ORGANIZATION_ID
 * @property integer $DOCUMENT_ID
 * @property string $DOCUMENT
 * @property string $DATE_ISSUE
 * @property string $DESC
 * @property string $DATE_CREATE
 * @property string $DATE_UPDATE
 * @property integer $USER_ID
 *
 * @property Organization $Organization
 * @property Document $Document
 * @property Projects $User
 */
class Intellectual extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'intellectual';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME'], 'required'],
            [['ORGANIZATION_ID', 'DOCUMENT_ID', 'USER_ID'], 'integer'],
            [['DATE_ISSUE', 'DATE_CREATE', 'DATE_UPDATE'], 'safe'],
            [['DESC'], 'string'],
            [['file'],'file'],
            [['NAME', 'COPYRIGHT', 'DOCUMENT'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('intellectual', 'ID'),
            'NAME' => Yii::t('intellectual', 'Name'),
            'COPYRIGHT' => Yii::t('intellectual', 'copyright'),
            'ORGANIZATION_ID' => Yii::t('intellectual', 'Organization  ID'),
            'DOCUMENT_ID' => Yii::t('intellectual', 'Document  ID'),
            'DOCUMENT' => Yii::t('intellectual', 'Document'),
            'DATE_ISSUE' => Yii::t('intellectual', 'Date  Issue'),
            'DESC' => Yii::t('intellectual', 'Desc'),
            'DATE_CREATE' => Yii::t('intellectual', 'Date  Create'),
            'DATE_UPDATE' => Yii::t('intellectual', 'Date  Update'),
            'USER_ID' => Yii::t('intellectual', 'User  ID'),
            'file' => Yii::t('social', 'DOCUMENT LINK'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getORGANIZATION()
    {
        return $this->hasOne(organization::className(), ['ID' => 'ORGANIZATION_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDOCUMENT()
    {
        return $this->hasOne(document::className(), ['ID' => 'DOCUMENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSER()
    {
        return $this->hasOne(Projects::className(), ['ID' => 'USER_ID']);
    }


}
