<?php

namespace app\modules\frontend\models;

use app\modules\common\models\Country;
use dektrium\user\models\User;
use Yii;

/**
 * This is the model class for table "internship".
 *
 * @property integer $ID
 * @property string $NAME
 * @property string $DATE_START
 * @property string $DATE_END
 * @property string $DATE_CREATE
 * @property integer $USER_ID
 * @property integer $COUNTRY_ID
 * @property string $FILE
 *
 * @property Country $cOUNTRY
 * @property User $uSER
 */
class Internship extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'internship';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME', 'USER_ID', 'COUNTRY_ID'], 'required'],
            [['DATE_START', 'DATE_END', 'DATE_CREATE'], 'safe'],
            [['USER_ID', 'COUNTRY_ID'], 'integer'],
            [['NAME', 'FILE'], 'string', 'max' => 255],
            [['file'],'file'],
            [['COUNTRY_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['COUNTRY_ID' => 'ID']],
            [['USER_ID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['USER_ID' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('internship', 'ID'),
            'NAME' => Yii::t('internship', 'Name'),
            'DATE_START' => Yii::t('internship', 'Date  Start'),
            'DATE_END' => Yii::t('internship', 'Date  End'),
            'DATE_CREATE' => Yii::t('internship', 'Date  Create'),
            'USER_ID' => Yii::t('internship', 'User  ID'),
            'COUNTRY_ID' => Yii::t('internship', 'Country  ID'),
            'FILE' => Yii::t('internship', 'File'),
            'file' => Yii::t('social', 'DOCUMENT LINK'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCOUNTRY()
    {
        return $this->hasOne(Country::className(), ['ID' => 'COUNTRY_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSER()
    {
        return $this->hasOne(User::className(), ['id' => 'USER_ID']);
    }
}
