<?php

namespace app\modules\frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\frontend\models\Projects;

/**
 * ProjectsSearch represents the model behind the search form about `app\modules\frontend\models\Projects`.
 */
class ProjectsSearch extends Projects
{
    public $DOCUMENT_NAME;
    public $ORGANIZATION_NAME;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ORGANIZATION_ID', 'DOCUMENT_ID', 'FUNDING'], 'integer'],
            [['DOCUMENT_NAME','ORGANIZATION_NAME', 'NAME', 'ROLE', 'TITLE_COMPETENTION', 'TITLE_SECTION', 'CODE', 'WINNER', 'DOCUMENT_NUMBER', 'DATE_ISSUE', 'DATE_START', 'DATE_END', 'CURRENCY', 'DATE_CREATE', 'DATE_UPDATE','FILE'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Projects::find();
        $query->joinWith(['dOCUMENT','oRGANIZATION']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'NAME',
                'DATE_ISSUE',
                'DATE_START',
                'DATE_END',
                'DATE_CREATE',
                'FILE',
                'DOCUMENT_NAME' => [
                    'asc' => [
                        'document.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'document.NAME' => SORT_DESC,
                    ],
                    'label' => 'DOCUMENT_NAME',
                    'default' => SORT_ASC
                ],
                'ORGANIZATION_NAME' => [
                    'asc' => [
                        'organization.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'organization.NAME' => SORT_DESC,
                    ],
                    'label' => 'ORGANIZATION_NAME',
                    'default' => SORT_ASC
                ],
            ],
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ORGANIZATION_ID' => $this->ORGANIZATION_ID,
            'DOCUMENT_ID' => $this->DOCUMENT_ID,
            'DATE_ISSUE' => $this->DATE_ISSUE,
            'DATE_START' => $this->DATE_START,
            'DATE_END' => $this->DATE_END,
            'FUNDING' => $this->FUNDING,
            'DATE_CREATE' => $this->DATE_CREATE,
            'DATE_UPDATE' => $this->DATE_UPDATE,
        ]);

        $query->andFilterWhere(['like', 'projects.NAME', $this->NAME])
            ->andFilterWhere(['USER_ID' => Yii::$app->user->identity->id])
            ->andFilterWhere(['like', 'document.NAME', $this->DOCUMENT_NAME])
            ->andFilterWhere(['like', 'organization.NAME', $this->ORGANIZATION_NAME])
            ->andFilterWhere(['like', 'ROLE', $this->ROLE])
            ->andFilterWhere(['like', 'TITLE_COMPETENTION', $this->TITLE_COMPETENTION])
            ->andFilterWhere(['like', 'TITLE_SECTION', $this->TITLE_SECTION])
            ->andFilterWhere(['like', 'CODE', $this->CODE])
            ->andFilterWhere(['like', 'WINNER', $this->WINNER])
            ->andFilterWhere(['like', 'DOCUMENT_NUMBER', $this->DOCUMENT_NUMBER])
            ->andFilterWhere(['like', 'FILE', $this->FILE])
            ->andFilterWhere(['like', 'CURRENCY', $this->CURRENCY]);

        return $dataProvider;
    }
}
