<?php

namespace app\modules\frontend\models;

use Yii;

/**
 * This is the model class for table "rating".
 *
 * @property int $id
 * @property string $url
 * @property int $grade
 * @property int $count_vote
 */
class Rating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['grade', 'count_vote'], 'integer'],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'url' => Yii::t('backend', 'Url'),
            'grade' => Yii::t('backend', 'Grade'),
            'count_vote' => Yii::t('backend', 'Count Vote'),
        ];
    }
}
