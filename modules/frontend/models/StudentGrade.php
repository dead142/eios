<?php

namespace app\modules\frontend\models;

use Yii;
use app\models\Profile;

/**
 * This is the model class for table "student_grade".
 *
 * @property integer $ID
 * @property string $GRADE
 * @property integer $GRADE_TYPE_ID
 * @property integer $STUDENT_ID
 * @property integer $TEACHER_ID
 * @property string $CREATED_DATE
 * @property string $UPDATED_DATE
 *
 * @property GradeTypes $gRADETYPE
 * @property User $sTUDENT
 * @property User $tEACHER
 */
class StudentGrade extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_grade';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'GRADE_TYPE_ID', 'STUDENT_ID', 'TEACHER_ID'], 'integer'],
            [['GRADE', 'GRADE_TYPE_ID', 'STUDENT_ID', 'TEACHER_ID'], 'required'],
            [['GRADE'], 'string'],
            [['CREATED_DATE', 'UPDATED_DATE'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'GRADE' => 'Оценка',
            'GRADE_TYPE_ID' => 'Тип оценки',
            'STUDENT_ID' => 'Студент',
            'TEACHER_ID' => 'Teacher ID',
            'CREATED_DATE' => 'Дата выставления',
            'UPDATED_DATE' => 'Updated  Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGRADETYPE()
    {
        return $this->hasOne(GradeTypes::className(), ['ID' => 'GRADE_TYPE_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSTUDENT()
    {
        return $this->hasOne(User::className(), ['id' => 'STUDENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSTUDENTPROFILE()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'STUDENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTEACHERPROFILE()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'TEACHER_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTEACHER()
    {
        return $this->hasOne(User::className(), ['id' => 'TEACHER_ID']);
    }

}
