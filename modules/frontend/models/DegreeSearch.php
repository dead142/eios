<?php

namespace app\modules\frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\frontend\models\Degree;

/**
 * DegreeSearch represents the model behind the search form about `app\modules\frontend\models\Degree`.
 */
class DegreeSearch extends Degree
{
    public $DOCUMENT_NAME;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'AREA_ID', 'ORGANIZATION_ID', 'DOCUMENT_ID', 'USER_ID'], 'integer'],
            [['NAME', 'ISSUE_DATE', 'DOCUMENT_NUMBER', 'DATE_CREATE', 'DATE_UPDATE','DOCUMENT_NAME','FILE'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Degree::find();
        $query->joinWith(['dOCUMENT']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);



        $dataProvider->setSort([
            'attributes' => [
                'NAME',
                'DOCUMENT_NUMBER',
                'ISSUE_DATE',
                'DATE_CREATE',
                'FILE',
                'DOCUMENT_NAME' => [
                    'asc' => [
                        'document.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'document.NAME' => SORT_DESC,
                    ],
                    'label' => 'DOCUMENT_NAME',
                    'default' => SORT_ASC
                ],
                ],
            ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'AREA_ID' => $this->AREA_ID,
            'ORGANIZATION_ID' => $this->ORGANIZATION_ID,
            'DOCUMENT_ID' => $this->DOCUMENT_ID,
            'ISSUE_DATE' => $this->ISSUE_DATE,
            'DATE_CREATE' => $this->DATE_CREATE,
            'DATE_UPDATE' => $this->DATE_UPDATE,
            'USER_ID' => $this->USER_ID,
        ]);

        $query->andFilterWhere(['like', 'degree.NAME', $this->NAME])
         ->andFilterWhere(['USER_ID' => isset($params['id'])? $params['id'] :Yii::$app->user->identity->id])
            ->andFilterWhere(['like', 'document.NAME', $this->DOCUMENT_NAME])
            ->andFilterWhere(['like', 'FILE', $this->FILE])
            ->andFilterWhere(['like', 'DOCUMENT_NUMBER', $this->DOCUMENT_NUMBER]);

        return $dataProvider;
    }
}