<?php

namespace app\modules\frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\frontend\models\Conferences;

/**
 * ConferencesSearch represents the model behind the search form about `app\modules\frontend\models\Conferences`.
 */
class ConferencesSearch extends Conferences
{
    public $STATUS_NAME;
    public $KIND_INVOVLVEMENT_NAME;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'STATUS_ID', 'KIND_INVOVLVEMENT_ID'], 'integer'],
            [['STATUS_NAME','KIND_INVOVLVEMENT_NAME','NAME', 'LOCATION', 'DATE_START', 'DATE_END', 'NAME_REPORT', 'NUMBER_NIR', 'DATE_CREATE', 'DATE_UPDATE','FILE'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Conferences::find();
        $query->joinWith(['sTATUS','kINDINVOVLVEMENT']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes' => [
                'NAME',
                'LOCATION',
                'DATE_START',
                'DATE_END',
                'NAME_REPORT',
                'FILE',
                'STATUS_NAME' => [
                    'asc' => [
                        'status.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'status.NAME' => SORT_DESC,
                    ],
                    'label' => 'STATUS_NAME',
                    'default' => SORT_ASC
                ],
                'KIND_INVOVLVEMENT_NAME' => [
                    'asc' => [
                        'kind_involvement.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'kind_involvement.NAME' => SORT_DESC,
                    ],
                    'label' => 'KIND_INVOVLVEMENT_NAME',
                    'default' => SORT_ASC
                ],
            ]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'DATE_START' => $this->DATE_START,
            'DATE_END' => $this->DATE_END,
            'STATUS_ID' => $this->STATUS_ID,
            'KIND_INVOVLVEMENT_ID' => $this->KIND_INVOVLVEMENT_ID,
            'DATE_CREATE' => $this->DATE_CREATE,
            'DATE_UPDATE' => $this->DATE_UPDATE,
        ]);

        $query->andFilterWhere(['like', 'conferences.NAME', $this->NAME])
            ->andFilterWhere(['USER_ID' =>  isset($params['id'])? $params['id'] :Yii::$app->user->identity->id])
            ->andFilterWhere(['like', 'status.NAME', $this->STATUS_NAME])
            ->andFilterWhere(['like', 'kind_involvement.NAME', $this->KIND_INVOVLVEMENT_NAME])
            ->andFilterWhere(['like', 'LOCATION', $this->LOCATION])
            ->andFilterWhere(['like', 'NAME_REPORT', $this->NAME_REPORT])
            ->andFilterWhere(['like', 'FILE', $this->FILE])
            ->andFilterWhere(['like', 'NUMBER_NIR', $this->NUMBER_NIR]);

        return $dataProvider;
    }
}
