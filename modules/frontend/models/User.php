<?php

namespace app\modules\frontend\models;

use Yii;
use yii\db\Query;
 /*
 * @property StudentGrade[] $studentGrades
 * @property StudentGrade[] $studentGrades0
 * @property UserGroups[] $userGroups
 */
class User extends \dektrium\user\models\User
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentGrades()
    {
        return $this->hasMany(StudentGrade::className(), ['STUDENT_ID' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentGrades0()
    {
        return $this->hasMany(StudentGrade::className(), ['TEACHER_ID' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserGroups()
    {
        return $this->hasMany(UserGroups::className(), ['USER_ID' => 'id']);
    }

    /**
     * @return array
     */
    public function getAllUserGroups ($user_id)
    {
        $data = UserGroups::find()->with('gROUPS')->where(['USER_ID'=>$user_id])->all();
        return $data;
    }

    /**
     * @return array
     */
    public function getUserGroupGrades ($user_id, $group_id, $archiveFlag = null)
    {
        $query = new Query;
        $query->select('profile.NAME as USERNAME, groups.NUMBER as GROUP_NUMBER, discipline.NAME as DISCIPLINE_NAME,
        discipline.ID as DISCIPLINE_ID, grade_types.ID as GRADE_TYPE_ID, grade_types.NAME as GRADE_TYPE_NAME, student_grade.GRADE, discipline_groups.GROUP_ID')
            ->from('user_groups')
            ->leftJoin('user', 'user_groups.USER_ID = user.id')
            ->leftJoin('profile', 'user.id = profile.user_id')
            ->leftJoin('groups', 'user_groups.GROUP_ID = groups.ID')
            ->leftJoin('discipline_groups', 'groups.ID = discipline_groups.GROUP_ID')
            ->leftJoin('discipline', 'discipline_groups.DISCIPLINE_ID = discipline.ID')
            ->leftJoin('grade_types', 'discipline.ID = grade_types.DISCIPLINE_ID')
            ->leftJoin('student_grade', 'grade_types.ID = student_grade.GRADE_TYPE_ID')
            ->where(['user_groups.USER_ID' => $user_id, 'user_groups.GROUP_ID' => $group_id])
            ->having(['discipline_groups.GROUP_ID' => $group_id]);

        if ($archiveFlag)
        {
            $query->where(['>=', 'discipline_groups.DATE_END', date('Y-m-d')]);
        }
        else
        {
            $query->where(['<', 'discipline_groups.DATE_END', date('Y-m-d')]);
        }

        $command = $query->createCommand();

        return $command->queryAll();
    }

    public function checkGroup ($user_id, $group_id)
    {
        $query = new Query;
        $query->select('groups.ID, groups.NUMBER, groups.DATE_BEGIN, groups.DATE_END')
            ->from('user_groups')
            ->leftJoin('groups', 'user_groups.GROUP_ID = groups.ID')
            ->where(['user_groups.USER_ID' => $user_id, 'user_groups.GROUP_ID' => $group_id]);
        $command = $query->createCommand();

        return $command->queryOne();
    }

    public function getUsersByRole($role)
    {
        $query = new Query;
        $query->select('auth_assignment.user_id')
            ->from('auth_assignment')
            ->where(['item_name' => $role]);

        $command = $query->createCommand();

        $result = $command->queryAll();

        if ($result)
        {
            foreach ($result as $item)
            {
                $finalResult[] = $item['user_id'];
            }
        }

        return $finalResult;
    }
}
