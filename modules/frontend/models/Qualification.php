<?php

namespace app\modules\frontend\models;

use app\modules\common\models\document;
use app\modules\common\models\organization;
use Yii;

/**
 * This is the model class for table "qualification".
 *
 * @property integer $ID
 * @property string $NAME
 * @property string $LOCATION
 * @property string $DATE_START
 * @property string $DATE_END
 * @property integer $ORGANIZATION_ID
 * @property integer $DOCUMENT_ID
 * @property string $DATE_ISSUE
 * @property string $DESC
 * @property string $HOURS
 * @property string $DATE_CREATE
 * @property string $DATE_UPDATE
 * @property integer $USER_ID
 *
 * @property Qualification[] $portfolioQualifications
 * @property User $User
 * @property Document $Document
 * @property Organization $Organization
 */
class Qualification extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qualification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME'], 'required'],
            [['DATE_START', 'DATE_END', 'DATE_ISSUE', 'DATE_CREATE', 'DATE_UPDATE'], 'safe'],
            [['DESC'], 'string'],
            [['ORGANIZATION_ID', 'DOCUMENT_ID', 'USER_ID'], 'integer'],
            [['NAME'], 'string', 'max' => 50],
            [['file'],'file'],
            [['LOCATION', 'HOURS'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('qualification', 'ID'),
            'NAME' => Yii::t('qualification', 'Name'),
            'LOCATION' => Yii::t('qualification', 'Location'),
            'DATE_START' => Yii::t('qualification', 'Date  Start'),
            'DATE_END' => Yii::t('qualification', 'Date  End'),
            'ORGANIZATION_ID' => Yii::t('qualification', 'Organization  ID'),
            'DOCUMENT_ID' => Yii::t('qualification', 'Document  ID'),
            'DATE_ISSUE' => Yii::t('qualification', 'Date  Issue'),
            'DESC' => Yii::t('qualification', 'Описание'),
            'HOURS' => Yii::t('qualification', 'Hours'),
            'DATE_CREATE' => Yii::t('qualification', 'Date  Create'),
            'DATE_UPDATE' => Yii::t('qualification', 'Date  Update'),
            'USER_ID' => Yii::t('qualification', 'User  ID'),
            'file' => Yii::t('social', 'DOCUMENT LINK'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQualifications()
    {
        return $this->hasMany(Qualification::className(), ['QUALIFICATION_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSER()
    {
        return $this->hasOne(User::className(), ['id' => 'USER_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDOCUMENT()
    {
        return $this->hasOne(document::className(), ['ID' => 'DOCUMENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getORGANIZATION()
    {
        return $this->hasOne(organization::className(), ['ID' => 'ORGANIZATION_ID']);
    }
}
