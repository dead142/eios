<?php

namespace app\modules\frontend\models;

use app\modules\common\models\document;
use app\modules\common\models\organization;
use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property integer $ID
 * @property string $NAME
 * @property string $ROLE
 * @property string $TITLE_COMPETENTION
 * @property string $TITLE_SECTION
 * @property string $CODE
 * @property string $WINNER
 * @property integer $ORGANIZATION_ID
 * @property integer $DOCUMENT_ID
 * @property string $DOCUMENT_NUMBER
 * @property string $DATE_ISSUE
 * @property string $DATE_START
 * @property string $DATE_END
 * @property integer $FUNDING
 * @property string $CURRENCY
 * @property string $DATE_CREATE
 * @property string $DATE_UPDATE
 * @property integer $USER_ID
 * @property string $file
 *
 * @property User $uSER
 * @property Document $dOCUMENT
 * @property Organization $oRGANIZATION
 */
class Projects extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME', 'ROLE', 'TITLE_COMPETENTION', 'WINNER'], 'required'],
            [['ROLE', 'WINNER', 'CURRENCY'], 'string'],
            [['ORGANIZATION_ID', 'DOCUMENT_ID', 'FUNDING', 'USER_ID'], 'integer'],
            [['DATE_ISSUE', 'DATE_START', 'DATE_END', 'DATE_CREATE', 'DATE_UPDATE'], 'safe'],
            [['NAME'], 'string', 'max' => 50],
            [['file'],'file'],
            [['TITLE_COMPETENTION', 'TITLE_SECTION', 'CODE', 'DOCUMENT_NUMBER'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('projects', 'ID'),
            'NAME' => Yii::t('projects', 'Name'),
            'ROLE' => Yii::t('projects', 'Role'),
            'TITLE_COMPETENTION' => Yii::t('projects', 'The title of the competition'),
            'TITLE_SECTION' => Yii::t('projects', 'Title  Section'),
            'CODE' => Yii::t('projects', 'Code system project contest'),
            'WINNER' => Yii::t('projects', 'The winner of the contest'),
            'ORGANIZATION_ID' => Yii::t('projects', 'Organization  ID'),
            'DOCUMENT_ID' => Yii::t('projects', 'Document  ID'),
            'DOCUMENT_NUMBER' => Yii::t('projects', 'document number'),
            'DATE_ISSUE' => Yii::t('projects', 'Date  Issue'),
            'DATE_START' => Yii::t('projects', 'Date  Start'),
            'DATE_END' => Yii::t('projects', 'Date  End'),
            'FUNDING' => Yii::t('projects', 'Funding'),
            'CURRENCY' => Yii::t('projects', 'currency unit'),
            'DATE_CREATE' => Yii::t('projects', 'Date  Create'),
            'DATE_UPDATE' => Yii::t('projects', 'Date  Update'),
            'USER_ID' => Yii::t('projects', 'User  ID'),
            'FILE' => Yii::t('publication', 'File'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSER()
    {
        return $this->hasOne(User::className(), ['id' => 'USER_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDOCUMENT()
    {
        return $this->hasOne(document::className(), ['ID' => 'DOCUMENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getORGANIZATION()
    {
        return $this->hasOne(organization::className(), ['ID' => 'ORGANIZATION_ID']);
    }
}
