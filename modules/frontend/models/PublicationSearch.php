<?php

namespace app\modules\frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * PublicationSearch represents the model behind the search form about `app\modules\frontend\models\Publication`.
 */
class PublicationSearch extends Publication
{
    public $LANG_NAME;
    public $COUNTRY_NAME;
    public $TYPE_EDITION_NAME;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'LANG_ID', 'COUNTRY_ID', 'TYPE_EDITION_ID'], 'integer'],
            [['NAME','LANG_NAME','COUNTRY_NAME','TYPE_EDITION_NAME', 'YEAR', 'CITY', 'PUBLISHER', 'PAGES', 'FILE', 'DESC', 'DATE_CREATE', 'DATE_UPDATE'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Publication::find();
        $query->joinWith(['lANG','cOUNTRY','tYPEEDITION']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes' => [
                'NAME',

                'YEAR',
                'DESC',
                'FILE',

                'LANG_NAME' => [
                    'asc' => [
                        'lang.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'lang.NAME' => SORT_DESC,
                    ],
                    'label' => 'LANG_NAME',
                    'default' => SORT_ASC
                ],
                'COUNTRY_NAME' => [
                    'asc' => [
                        'country.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'country.NAME' => SORT_DESC,
                    ],
                    'label' => 'COUNTRY_NAME',
                    'default' => SORT_ASC
                ],
                'TYPE_EDITION_NAME' => [
                    'asc' => [
                        'type_edition.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'type_edition.NAME' => SORT_DESC,
                    ],
                    'label' => 'TYPE_EDITION_NAME',
                    'default' => SORT_ASC
                ],
            ]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'LANG_ID' => $this->LANG_ID,
            'YEAR' => $this->YEAR,
            'COUNTRY_ID' => $this->COUNTRY_ID,
            'TYPE_EDITION_ID' => $this->TYPE_EDITION_ID,
            'DATE_CREATE' => $this->DATE_CREATE,
            'DATE_UPDATE' => $this->DATE_UPDATE,
        ]);

        $query->andFilterWhere(['like', 'publication.NAME', $this->NAME])
            ->andFilterWhere(['USER_ID' => isset($params['id'])? $params['id'] :Yii::$app->user->identity->id])
            ->andFilterWhere(['like', 'lang.NAME', $this->LANG_NAME])
            ->andFilterWhere(['like', 'country.NAME', $this->COUNTRY_NAME])
            ->andFilterWhere(['like', 'type_edition.NAME', $this->TYPE_EDITION_NAME])
            ->andFilterWhere(['like', 'CITY', $this->CITY])
            ->andFilterWhere(['like', 'PUBLISHER', $this->PUBLISHER])
            ->andFilterWhere(['like', 'PAGES', $this->PAGES])
            ->andFilterWhere(['like', 'FILE', $this->FILE])
            ->andFilterWhere(['like', 'DESC', $this->DESC]);

        return $dataProvider;
    }
}
