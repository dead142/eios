<?php

namespace app\modules\frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\frontend\models\StudentGrade;

/**
 * StudentGradeSearch represents the model behind the search form about `app\modules\frontend\models\StudentGrade`.
 */
class StudentGradeSearch extends StudentGrade
{
    public $USER_NAME;
    public $USER_SURNAME;
    public $USER_MIDDLENAME;
    public $GRADE_TYPE_NAME;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'GRADE_TYPE_ID', 'STUDENT_ID', 'TEACHER_ID'], 'integer'],
            [['GRADE', 'CREATED_DATE', 'UPDATED_DATE', 'USER_NAME', 'USER_SURNAME', 'USER_MIDDLENAME', 'GRADE_TYPE_NAME'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $gradeTypes = null, $actualFlag = null, $archieveFlag = null)
    {
        $query = StudentGrade::find();
        $query->joinWith(['sTUDENTPROFILE', 'gRADETYPE']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => [
                'ID',
                'GRADE',
                'GRADE_TYPE_ID',
                'STUDENT_ID',
                'TEACHER_ID',
                'CREATED_DATE',
                'UPDATED_DATE',

                'USER_NAME' => [
                    'asc' => [
                        'profile.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'profile.name' => SORT_DESC,
                    ],
                    'label' => 'DISCIPLINE_NAME',
                    'default' => SORT_ASC
                ],
                'USER_SURNAME' => [
                    'asc' => [
                        'profile.surname' => SORT_ASC,
                    ],
                    'desc' => [
                        'profile.surname' => SORT_DESC,
                    ],
                    'label' => 'DISCIPLINE_NAME',
                    'default' => SORT_ASC
                ],
                'USER_MIDDLENAME' => [
                    'asc' => [
                        'profile.middlename' => SORT_ASC,
                    ],
                    'desc' => [
                        'profile.middlename' => SORT_DESC,
                    ],
                    'label' => 'DISCIPLINE_NAME',
                    'default' => SORT_ASC
                ],
                'GRADE_TYPE_NAME' => [
                    'asc' => [
                        'grade_types.NAME' => SORT_ASC,
                    ],
                    'desc' => [
                        'grade_types.NAME' => SORT_DESC,
                    ],
                    'label' => 'GRADE_TYPE_NAME',
                    'default' => SORT_ASC
                ],
            ]
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'GRADE_TYPE_ID' => $this->GRADE_TYPE_ID,
            'STUDENT_ID' => $this->STUDENT_ID,
            'TEACHER_ID' => $this->TEACHER_ID,
            'CREATED_DATE' => $this->CREATED_DATE,
            'UPDATED_DATE' => $this->UPDATED_DATE,
        ]);

        if ($gradeTypes)
        {
            $query->andFilterWhere(['in', 'GRADE_TYPE_ID', $gradeTypes]);
        }
        if ($actualFlag)
        {
            $query->andFilterWhere(['>=', 'grade_types.DATE', date('Y-m-d')]);
        }
        if ($archieveFlag)
        {
            $query->andFilterWhere(['<', 'grade_types.DATE', date('Y-m-d')]);
        }

        $query->andFilterWhere(['like', 'GRADE', $this->GRADE]);

        $query->andFilterWhere(['like', 'profile.name', $this->USER_NAME]);
        $query->andFilterWhere(['like', 'profile.surname', $this->USER_SURNAME]);
        $query->andFilterWhere(['like', 'profile.middlename', $this->USER_MIDDLENAME]);
        $query->andFilterWhere(['like', 'grade_types.NAME', $this->GRADE_TYPE_NAME]);

        return $dataProvider;
    }
}
