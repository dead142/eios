<?php

namespace app\modules\frontend\models;
use app\modules\common\models\organization;

use Yii;

/**
 * This is the model class for table "reviews".
 *
 * @property integer $ID
 * @property string $NAME
 * @property string $AUTHOR
 * @property integer $ORGANIZATION_ID
 * @property integer $USER_ID
 * @property string $CREATE_DATE
 * @property string $FILE
 *
 * @property Organization $oRGANIZATION
 * @property User $uSER
 */
class Reviews extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME', 'ORGANIZATION_ID', 'USER_ID'], 'required'],
            [['ORGANIZATION_ID', 'USER_ID'], 'integer'],
            [['CREATE_DATE'], 'safe'],
            [['file'],'file'],
            [['NAME', 'AUTHOR', 'FILE'], 'string', 'max' => 255],
            [['ORGANIZATION_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Organization::className(), 'targetAttribute' => ['ORGANIZATION_ID' => 'ID']],
            [['USER_ID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['USER_ID' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('reviews', 'ID'),
            'NAME' => Yii::t('reviews', 'Name'),
            'AUTHOR' => Yii::t('reviews', 'Author'),
            'ORGANIZATION_ID' => Yii::t('reviews', 'Organization  ID'),
            'USER_ID' => Yii::t('reviews', 'User  ID'),
            'CREATE_DATE' => Yii::t('reviews', 'Create  Date'),
            'FILE' => Yii::t('reviews', 'File'),
            'file' => Yii::t('social', 'DOCUMENT LINK'),

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getORGANIZATION()
    {
        return $this->hasOne(Organization::className(), ['ID' => 'ORGANIZATION_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSER()
    {
        return $this->hasOne(User::className(), ['id' => 'USER_ID']);
    }
}
