<?php
/**
 *
 */
namespace app\modules\frontend\models;

use app\models\Profile;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

/**
 * OrganizationSearch represents the model behind the search form about `app\modules\frontend\models\organization`.
 */
class ProfileSearch extends Profile
{
    public $GROUP;
    public $surname;
    public $middlename;

    /**
     * Используются в /frontend/groups/list-user-group
     * @var $fullName
     * @var $groupName
     * @var $groupYear
     * @var $studentStatus
     */
    public $fullName;
    public $groupName;
    public $groupYear;
    public $studentStatus;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['name', 'surname', 'middlename'], 'safe'],
            [['fullName','groupName','groupYear','studentStatus'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Profile::find();
        $query->joinWith(['studentGroups', 'studentGroups.group', 'studentGroups.group.idYear', 'studentGroups.studentStatuses']);
        $query->join('LEFT JOIN', 'auth_assignment', 'auth_assignment.user_id=profile.user_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'name',
                'surname',
                'middlename',
                'fullName' => [
                    'asc' => [
                        'name' => SORT_ASC,
                        'surname' => SORT_ASC,
                        'middlename' => SORT_ASC,
                    ],
                    'desc' => [
                        'name' => SORT_DESC,
                        'surname' => SORT_DESC,
                        'middlename' => SORT_DESC,
                    ],
                    'default' => SORT_ASC
                ],
                'groupName' => [
                    'asc' => [
                        'groups.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'groups.name' => SORT_DESC,
                    ],
                    'default' => SORT_ASC
                ],
                'groupYear' => [
                    'asc' => [
                        'year.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'year.name' => SORT_DESC,
                    ],
                    'default' => SORT_ASC
                ],
                'studentStatus' => [
                    'asc' => [
                        'student_status.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'student_status.name' => SORT_DESC,
                    ],
                    'default' => SORT_ASC
                ],



            ],
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'year.name' => $this->groupYear,

                'student_status.name' => $this->studentStatus,
             //   'student_group.student_group_id' => ($this->getLastStudentGroup($this->studentGroups))->id ,


        ]);
    //VarDumper::dump($this->studentStatus,10,true);
        $query->andFilterWhere(['like', 'profile.name', $this->name])
            ->andFilterWhere(['like', 'profile.surname', $this->surname])
            ->andFilterWhere(['like', 'profile.middlename', $this->middlename])
             ->andFilterWhere([
                 'or',
                 ['like','profile.name',   $this->fullName],
                 ['like','profile.surname',   $this->fullName],
                 ['like','profile.middlename',   $this->fullName]])
            ->andFilterWhere(['like', 'groups.name', $this->groupName])
//            ->andFilterWhere([
//                'and',
//                ['student_status.name' => $this->studentStatus],
//                ['student_status.student_group_id' => ($this->getLastStudentGroup($this->studentGroups))->id ],
//            ])

        ;

        return $dataProvider;
    }
}
