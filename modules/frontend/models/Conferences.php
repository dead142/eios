<?php

namespace app\modules\frontend\models;

use app\modules\common\models\KindInvolvement;
use app\modules\common\models\Status;
use Yii;

/**
 * This is the model class for table "conferences".
 *
 * @property integer $ID
 * @property string $NAME
 * @property string $LOCATION
 * @property string $DATE_START
 * @property string $DATE_END
 * @property integer $STATUS_ID
 * @property integer $KIND_INVOVLVEMENT_ID
 * @property string $NAME_REPORT
 * @property string $NUMBER_NIR
 * @property string $DATE_CREATE
 * @property string $DATE_UPDATE
 * @property integer $USER_ID
 *
 * @property User $uSER
 * @property KindInvolvement $kINDINVOVLVEMENT
 * @property Status $sTATUS
 */
class Conferences extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'conferences';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME', 'NAME_REPORT'], 'required'],
            [['DATE_START', 'DATE_END', 'DATE_CREATE', 'DATE_UPDATE'], 'safe'],
            [['STATUS_ID', 'KIND_INVOVLVEMENT_ID', 'USER_ID'], 'integer'],
            [['NAME', 'NAME_REPORT'], 'string', 'max' => 50],
            [['file'],'file'],
            [['LOCATION', 'NUMBER_NIR'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('conferences', 'ID'),
            'NAME' => Yii::t('conferences', 'Name'),
            'LOCATION' => Yii::t('conferences', 'Location'),
            'DATE_START' => Yii::t('conferences', 'Date  Start'),
            'DATE_END' => Yii::t('conferences', 'Date  End'),
            'STATUS_ID' => Yii::t('conferences', 'Status  ID'),
            'KIND_INVOVLVEMENT_ID' => Yii::t('conferences', 'Type of participation'),
            'NAME_REPORT' => Yii::t('conferences', 'Name  Report'),
            'NUMBER_NIR' => Yii::t('conferences', 'Number  Nir'),
            'DATE_CREATE' => Yii::t('conferences', 'Date  Create'),
            'DATE_UPDATE' => Yii::t('conferences', 'Date  Update'),
            'USER_ID' => Yii::t('conferences', 'User  ID'),
            'file' => Yii::t('social', 'DOCUMENT LINK'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSER()
    {
        return $this->hasOne(User::className(), ['id' => 'USER_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKINDINVOVLVEMENT()
    {
        return $this->hasOne(KindInvolvement::className(), ['ID' => 'KIND_INVOVLVEMENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSTATUS()
    {
        return $this->hasOne(Status::className(), ['ID' => 'STATUS_ID']);
    }


}
