<?php

namespace app\modules\frontend\models;

use app\modules\common\models\document;
use app\modules\common\models\organization;
use Yii;

/**
 * This is the model class for table "awards".
 *
 * @property integer $ID
 * @property string $NAME
 * @property integer $ORGANIZATION_ID
 * @property integer $DOCUMENT_ID
 * @property string $ISSUE_DATE
 * @property string $DESC
 * @property string $DATE_CREATE
 * @property string $DATE_UPDATE
 * @property integer $USER_ID
 *
 * @property Document $dOCUMENT
 * @property Organization $oRGANIZATION
 * @property User $uSER
 */
class Awards extends \yii\db\ActiveRecord
{
    public $file;
    /**
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'awards';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NAME', 'ISSUE_DATE', 'DATE_CREATE', 'DATE_UPDATE'], 'required'],
            [['ORGANIZATION_ID', 'DOCUMENT_ID', 'USER_ID'], 'integer'],
            [['ISSUE_DATE', 'DATE_CREATE', 'DATE_UPDATE'], 'safe'],
            [['DESC'], 'string'],
            [['file'],'file'],
            [['NAME'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('awards', 'ID'),
            'NAME' => Yii::t('awards', 'Name'),
            'ORGANIZATION_ID' => Yii::t('awards', 'Organization  ID'),
            'DOCUMENT_ID' => Yii::t('awards', 'Document  ID'),
            'ISSUE_DATE' => Yii::t('awards', 'Issue  Date'),
            'DESC' => Yii::t('awards', 'Desc'),
            'DATE_CREATE' => Yii::t('awards', 'Date  Create'),
            'DATE_UPDATE' => Yii::t('awards', 'Date  Update'),
            'USER_ID' => Yii::t('awards', 'User  ID'),
            'file' => Yii::t('social', 'DOCUMENT LINK'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDOCUMENT()
    {
        return $this->hasOne(document::className(), ['ID' => 'DOCUMENT_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getORGANIZATION()
    {
        return $this->hasOne(organization::className(), ['ID' => 'ORGANIZATION_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUSER()
    {
        return $this->hasOne(User::className(), ['id' => 'USER_ID']);
    }


}
