<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\frontend\models\AwardsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('awards', 'Awards');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="awards-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('awards', 'Create Awards'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'ID',
            'NAME',
            [
                'label'=>Yii::t('awards', 'Organization  ID'),
                'attribute' => 'ORGANIZATION_NAME',
                'value'=> 'oRGANIZATION.NAME'
            ],
            [
                'label'=>Yii::t('awards', 'Document  ID'),
                'attribute' => 'DOCUMENT_NAME',
                'value'=> 'dOCUMENT.NAME'
            ],
            'ISSUE_DATE:date',
             'DESC:html',
            [
                'format' => 'raw',
                'label'=>Yii::t('publication', 'File'),
                'attribute' => 'FILE',
                'value'=> function($data){
                    return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

                }
            ],
            // 'DATE_CREATE',
            // 'DATE_UPDATE',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
