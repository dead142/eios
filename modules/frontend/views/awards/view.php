<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\social\Disqus;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Awards */

$this->title = $model->NAME;
$this->params['breadcrumbs'][] = ['label' => Yii::t('awards', 'Awards'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="awards-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?=  Html::a(Yii::t('awards', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary'])  ?>
        <?= Html::a(Yii::t('awards', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('awards', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'ID',
            'NAME',
            [
                'label'=>Yii::t('awards', 'Organization  ID'),
                'value'=> $model->oRGANIZATION ? $model->oRGANIZATION->NAME :''
            ],
            [
                'label'=>Yii::t('awards', 'Document  ID'),
                'value'=> $model->dOCUMENT ? $model->dOCUMENT->NAME : ''
            ],
            'ISSUE_DATE',
            'DESC:html',
            'DATE_CREATE',
            'DATE_UPDATE',
            [
                'label'=>Yii::t('publication', 'File'),
                'value'=> Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE,['data-pjax'=>0,'target'=>'_blank']),
                'format'=>'html'
            ],
        ],
    ]) ?>
    <?php //echo Comments\widgets\CommentListWidget::widget([
       // 'entity' => (string) \Yii::$app->controller->route.'?id='.$model->ID, // type and id
  //  ]);
    ?>
    <?= $this->render('../_comment') ?>
</div>
