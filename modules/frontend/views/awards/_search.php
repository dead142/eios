<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\AwardsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="awards-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'NAME') ?>

    <?= $form->field($model, 'ORGANIZATION_ID') ?>

    <?= $form->field($model, 'DOCUMENT_ID') ?>

    <?= $form->field($model, 'ISSUE_DATE') ?>

    <?php // echo $form->field($model, 'DESC') ?>

    <?php // echo $form->field($model, 'DATE_CREATE') ?>

    <?php // echo $form->field($model, 'DATE_UPDATE') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('awards', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('awards', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
