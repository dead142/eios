<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use mihaildev\ckeditor\CKEditor;
use yii\bootstrap\Modal;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use mihaildev\elfinder\ElFinder;
//models
use app\modules\common\models\document;
/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Awards */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="awards-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class=" col-sm-12">
        <?= $form->field($model, 'NAME')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class=" col-sm-6 ">

            <?= $form->field($model, 'DOCUMENT_ID')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(document::find()->all(), 'ID', 'NAME'),
                'options' => ['placeholder' => 'Select document'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class=" col-sm-6 ">
        <?= $form->field($model, 'ISSUE_DATE')->widget(\yii\jui\DatePicker::classname(), [
            'options' => ['class' => 'form-control'],
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ])  ?>
        </div>
    </div>
    <div class="row">
        <div class=" col-sm-8">
            <?php
            //hidden input for ORGANIZATION
            echo  $form->field($model, 'ORGANIZATION_ID')->hiddenInput();
            ?>
            <?php
            // Input for ORGANIZATION
            echo  AutoComplete::widget( [
                    'name' => 'ORGANIZATION',
                    'id' => 'ORGANIZATION',
                    'value' => $model->isNewRecord ? NULL: ($model->oRGANIZATION ? $model->oRGANIZATION->NAME : NULL),

                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Начните набирать название',
                    ],
                    'clientOptions' => [
                        'source' => Url::to(['/backend/ajax/organizations']),
                        'autoFill' => true,
                        'minLength' => '0',
                        // Get and set value to hidden field
                        'select' => new JsExpression("function( event, ui ) {
            $('#". Html::getInputId($model, 'ORGANIZATION_ID')."').val(ui.item.value).trigger('change');
            $('#ORGANIZATION').val(ui.item.label);
             console.log($('#degree-organization_id').val());
            return false;
        }")
                    ],
                ]
            );
            ?>
        </div>
        <div class=" col-sm-4">
            <?php echo  Html::button(Yii::t('degrees', 'Add organization to Database'), [
                'value'=> Url::toRoute('/backend/organization/create'),
                'class'=>'btn btn-primary',
                'id'=>'modalButton',
                'style'=>'margin: 10%;']);
            ?>
        </div>
    </div>

<div class="row">
    <div class=" col-sm-6">
    <?= $form->field($model, 'DESC')->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions(['elfinder'], [
            'preset' => 'basic',
            //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false,
            'path' => '/awards',
            'filter'  => 'pdf'
            //по умолчанию false
        ]),

    ])  ?>
        </div>
    <div class=" col-sm-6">
        <?= $form->field($model, 'file')->fileInput(['class'=>'btn btn-primary']) ?>
        <?php
        if ($model->FILE) {
            echo Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE);
            echo '&nbsp;&nbsp;&nbsp;';
            echo Html::a(Yii::t('social', 'Delete File'), ['deleteimg', 'id'=>$model->ID, 'field'=> 'FILE'], ['class'=>'btn btn-danger']).'<p>';
        }
        ?>
    </div>
</div>
    <?php // Modal for adding organizations
    Modal::begin([
        'id'=>'modal',
        'size'=>'modal-lg',
    ]);
    echo "<div id='modalContent'></div>";

    Modal::end();
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('awards', 'Create') : Yii::t('awards', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
