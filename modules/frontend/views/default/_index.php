<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/19/2016
 * Time: 3:53 PM
 */

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>


        <div class="col-xs-6 col-sm-4">
            <h3><a   href="<?= \yii\helpers\Url::toRoute(['/frontend/default/view','id'=>$model->ID]) ?>"><?= Html::encode($model->NAME) ?></a></h3>

            <p><?php
                $string = substr($model->DESC, 0, 300);
                $string = rtrim($string, "!,.-");
                $string = substr($string, 0, strrpos($string, ' '));
                echo $string."… ";
                ?></p>

            <p><a class="btn btn-default" href="<?= \yii\helpers\Url::toRoute(['/frontend/default/view','id'=>$model->ID]) ?>"><?= Yii::t('frontend/main', 'More...') ?></a></p>
        </div>



