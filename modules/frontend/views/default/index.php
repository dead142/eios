<?
use yii\widgets\ListView;
?>
<div class="frontend-default-index">
    <div class="body-content">

        <div class="row">
            <? echo ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_index',
                'summary'=>''

            ]);?>
            </div>
    <?php if ($isShowPortfolio['option_value'] ==1) : ?>
        <hr>
        <div class="row">
            <h1> Портфолио </h1>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProviderProfile,
            'filterModel' => $searchModelProfile,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => Yii::t('groups', 'Surname'),
                    'attribute' => 'surname',
                    'value' => 'surname',
                    'format' => 'raw',
                    //  'value' => 'user.profile.surname',

                ],

                [

                    'attribute' => 'name',
                    'value' => 'name',
                    'format' => 'raw',


                ],

                [
                    'value' => 'middlename',
                    'label' => Yii::t('groups', 'Middlename'),
                    'attribute' => 'middlename',

                    'format' => 'raw',

                ],
                [
                    'label'=>Yii::t('groups', 'Group'),
                    'attribute' => 'GROUP',
                    'value' =>function($data){

                        if ((\app\modules\backend\models\StudentGroup::StudentGroup($data->user_id))) {
                            return  \app\modules\backend\models\StudentGroup::StudentGroup($data->user_id)->group->name;
                        }
                    }



                ],

                [
                    'format' => 'raw',
                    'label'=>Yii::t('frontend/main', 'Portfolio user'),
                    'value'=> function($data){

                        return  \kartik\helpers\Html::a(Yii::t('frontend/main', 'See'), ['/frontend/default/all','id'=>$data['user_id']]);
                    }
                ],
                [
                    'format' => 'raw',
                    'label'=>Yii::t('frontend/main', 'Grade user'),
                    'value'=> function($data){

                        return  \yii\helpers\Html::a(Yii::t('frontend/main', 'See'), ['/frontend/grade/grade-user','id'=>$data['user_id']]);
                    }
                ],
              /*  [
                    'class' => \yii\grid\ActionColumn::className(),

                    'template'=>'',
                ]*/
            ],
        ]); ?>
        <?php \yii\widgets\Pjax::end(); ?>
        </div>
        <? endif; ?>
</div>
</div>