<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\common\models\TypeEvent;
use app\modules\common\models\Status;
use app\modules\common\models\KindInvolvement;
use app\modules\common\models\TypeEdition;
use rmrevin\yii\module\Comments;
use dektrium\user\models\Profile;

$userIdentity = Yii::$app->user->identity;
?>
<?//= Html::img($userIdentity->profile->getAvatarUrl(24), [
//    'class' => 'img-rounded',
//    'alt' => $userIdentity->username,
//    'width'=>100,
//    'height'=>100,
//]) ?>

<h2 class="pull-right">
    <?= Yii::t('frontend\main','Portfolio user').' '.$user->surname.' '.$user->name.' '.$user->middlename ?>
</h2>



<?php \yii\widgets\Pjax::begin(); ?>
<h2><?= Yii::t('degrees', 'Degrees')?></h2>
<?= GridView::widget([
    'dataProvider' => $dataDegree,
    'filterModel' => $searchModelDegree,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
            'NAME',
            [
                'attribute' => 'DOCUMENT_NAME',
                'value' => 'dOCUMENT.NAME',
            ],
            'DOCUMENT_NUMBER',
            'ISSUE_DATE:date',
            'DATE_CREATE:datetime',
            [
                'format' => 'raw',
                'label'=>Yii::t('publication', 'File'),
                'attribute' => 'FILE',
                'value'=> function($data){
                    return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);
                }
            ],
            [
            'class' => \yii\grid\ActionColumn::className(),
            'buttons'=>[
                'view'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['/frontend/degree/view','id'=>$model->ID]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                        ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                }
            ],
            'template'=>'{view}',
        ]
    ],


]); ?>

<h2><?= Yii::t('projects', 'Projects') ?></h2>
<?= GridView::widget([
    'dataProvider' => $dataProjects,
    'filterModel' => $searchModelProjects,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        //  'ID',
        'NAME',
        //'ROLE',
        //'TITLE_COMPETENTION',
        // 'TITLE_SECTION',
        //'CODE',
        // 'WINNER',
        [
            'label'=>Yii::t('projects', 'Organization  ID'),
            'attribute' => 'ORGANIZATION_NAME',
            'value'=> 'oRGANIZATION.NAME'
        ],[
            'label'=>Yii::t('projects', 'Document  ID'),
            'attribute' => 'DOCUMENT_NAME',
            'value'=> 'dOCUMENT.NAME'
        ],

        // 'DOCUMENT_NUMBER',
        'DATE_ISSUE:date',
        'DATE_START:date',
        'DATE_END:date',
        // 'FUNDING',
        // 'CURRENCY',
       //'DATE_CREATE:date',
        // 'DATE_UPDATE',
        [
            'format' => 'raw',
            'label'=>Yii::t('publication', 'File'),
            'attribute' => 'FILE',
            'value'=> function($data){
                return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

            }
        ],

        [
            'class' => \yii\grid\ActionColumn::className(),
            'buttons'=>[
                'view'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['/frontend/projects/view','id'=>$model->ID]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                        ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                }
            ],
            'template'=>'{view}',
        ]
    ],
]); ?>


<h2><?= Yii::t('awards', 'Awards'); ?></h2>
<?= GridView::widget([
    'dataProvider' => $dataAwards,
    'filterModel' => $searchModelAwards,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        //  'ID',
        'NAME',
        [
            'label'=>Yii::t('awards', 'Organization  ID'),
            'attribute' => 'ORGANIZATION_NAME',
            'value'=> 'oRGANIZATION.NAME'
        ],
        [
            'label'=>Yii::t('awards', 'Document  ID'),
            'attribute' => 'DOCUMENT_NAME',
            'value'=> 'dOCUMENT.NAME'
        ],
        'ISSUE_DATE:date',
        'DESC:html',
        // 'DATE_CREATE',
        // 'DATE_UPDATE',
        [
            'format' => 'raw',
            'label'=>Yii::t('publication', 'File'),
            'attribute' => 'FILE',
            'value'=> function($data){
                return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

            }
        ],

        [
            'class' => \yii\grid\ActionColumn::className(),
            'buttons'=>[
                'view'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['/frontend/awards/view','id'=>$model->ID]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                        ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                }
            ],
            'template'=>'{view}',
        ]
    ],
]); ?>

<h2><?= Yii::t('social', 'Social Activities') ?></h2>
<?= GridView::widget([
    'dataProvider' => $dataSocial,
    'filterModel' => $searchModelSocial,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        // 'ID',
        'NAME',
        // 'DATE_START',
        //  'DATE_END',
        [
            'label'=>Yii::t('social', 'Status  ID'),
            'attribute' => 'STATUS_NAME',
            'value'=> 'sTATUS.NAME'
        ],
        [
            'label'=>Yii::t('social', 'Type of event'),
            'attribute' => 'TYPE_EVENT_NAME',
            'value'=> 'tYPEEVENT.NAME',
            'filter' =>ArrayHelper::map(TypeEvent::find()->all(), 'NAME', 'NAME')
        ],

        // 'LEVEL_ID',
        // 'TYPE_PARTICIPATION_ID',
        // 'DOCUMENT',
        'DESC:html',
      //  'DATE_CREATE:datetime',
        // 'DATE_UPDATE',
        [
            'format' => 'raw',
            'label'=>Yii::t('publication', 'File'),
            'attribute' => 'FILE',
            'value'=> function($data){
                return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

            }
        ],

        [
            'class' => \yii\grid\ActionColumn::className(),
            'buttons'=>[
                'view'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['/frontend/social/view','id'=>$model->ID]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                        ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                }
            ],
            'template'=>'{view}',
        ]
    ],
]); ?>


<h2><?= Yii::t('qualification', 'Qualifications') ?></h2>
<?= GridView::widget([
    'dataProvider' => $dataQualification,
    'filterModel' => $searchModelQualification,

    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        //  'ID',
        'NAME',
        'LOCATION',
        'DATE_START:date',
        'DATE_END:date',
        [
            'format' => 'raw',
            'label'=>Yii::t('publication', 'File'),
            'attribute' => 'FILE',
            'value'=> function($data){
                return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

            }
        ],
        // 'ORGANIZATION_ID',
        [
            'label'=>Yii::t('qualification', 'Document  ID'),
            'attribute' => 'DOCUMENT_NAME',
            'value' => 'dOCUMENT.NAME',
        ],
        'DATE_ISSUE:date',
        // 'HOURS',
      //  'DATE_CREATE:datetime',
        // 'DATE_UPDATE',
        [
            'format' => 'raw',
            'label'=>Yii::t('publication', 'File'),
            'attribute' => 'FILE',
            'value'=> function($data){
                return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

            }
        ],

        [
            'class' => \yii\grid\ActionColumn::className(),
            'buttons'=>[
                'view'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['/frontend/qualification/view','id'=>$model->ID]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                        ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                }
            ],
            'template'=>'{view}',
        ]
    ],
]); ?>


<h2><?= Yii::t('intellectual', 'Intellectuals') ?></h2>
<?= GridView::widget([
    'dataProvider' => $dataIntellectual,
    'filterModel' => $searchModelIntellectual,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        //  'ID',
        'NAME',
        // 'COPYRIGHT',
        //   'ORGANIZATION_ID',
        [
            'label'=>Yii::t('qualification', 'Document  ID'),
            'attribute' => 'DOCUMENT_NAME',
            'value'=> 'dOCUMENT.NAME'
        ],

        'DOCUMENT',
        'DATE_ISSUE:date',
        [
            'format' => 'raw',
            'label'=>Yii::t('publication', 'File'),
            'attribute' => 'FILE',
            'value'=> function($data){
                return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

            }
        ],
        // 'DESC:ntext',
        // 'DATE_CREATE',
        // 'DATE_UPDATE',

        [
            'class' => \yii\grid\ActionColumn::className(),
            'buttons'=>[
                'view'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['/frontend/intellectual/view','id'=>$model->ID]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                        ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                }
            ],
            'template'=>'{view}',
        ]
    ],
]); ?>

<h2><?= Yii::t('conferences', 'Conferences') ?></h2>
<?= GridView::widget([
    'dataProvider' => $dataConferences,
    'filterModel' => $searchModelConferences,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        // 'ID',
        'NAME',
        'LOCATION',
        'DATE_START:date',
        'DATE_END:date',
        [
            'format' => 'raw',
            'label'=>Yii::t('publication', 'File'),
            'attribute' => 'FILE',
            'value'=> function($data){
                return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

            }
        ],
        [
            'label'=>Yii::t('conferences', 'Status  ID'),
            'attribute' => 'STATUS_NAME',
            'value'=> 'sTATUS.NAME',
            'filter' =>ArrayHelper::map(Status::find()->all(), 'NAME', 'NAME')

        ],
        [
            'label'=>Yii::t('conferences', 'Type of participation'),
            'attribute' => 'KIND_INVOVLVEMENT_NAME',
            'value'=> 'kINDINVOVLVEMENT.NAME',
            'filter' =>ArrayHelper::map(KindInvolvement::find()->all(), 'NAME', 'NAME')
        ],

        'NAME_REPORT',
        [
            'format' => 'raw',
            'label'=>Yii::t('publication', 'File'),
            'attribute' => 'FILE',
            'value'=> function($data){
                return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

            }
        ],
        //'NUMBER_NIR',
        // 'DATE_CREATE',
        // 'DATE_UPDATE',

        [
            'class' => \yii\grid\ActionColumn::className(),
            'buttons'=>[
                'view'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['/frontend/conferences/view','id'=>$model->ID]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                        ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                }
            ],
            'template'=>'{view}',
        ]
    ],
]); ?>

<h2><?= Yii::t('publication', 'Publications');?></h2>
<?= GridView::widget([
    'dataProvider' => $dataPublication,
    'filterModel' => $searchModelPublication,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],


        'NAME',
        'YEAR',
        [
            'label'=>Yii::t('publication', 'Type  Edition  ID'),
            'attribute' => 'TYPE_EDITION_NAME',
            'value'=> 'tYPEEDITION.NAME',
            'filter' =>ArrayHelper::map(TypeEdition::find()->all(), 'NAME', 'NAME')
        ],


        'DESC:html',
        [
            'format' => 'raw',
            'label'=>Yii::t('publication', 'File'),
            'attribute' => 'FILE',
            'value'=> function($data){
                return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

            }
        ],
        //  'ID',
        //   'NAME',
        //'LANG_ID',
        //   'YEAR',
        // 'COUNTRY_ID',
        // 'CITY',
        //  'tYPEEDITION.NAME',
        // 'PUBLISHER',
        // 'PAGES',
        //  'FILE',
        //  'DESC',
        // 'DATE_CREATE',
        // 'DATE_UPDATE',

        [
            'class' => \yii\grid\ActionColumn::className(),
            'buttons'=>[
                'view'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['/frontend/publication/view','id'=>$model->ID]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                        ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                }
            ],
            'template'=>'{view}',
        ]
    ],
]); ?>
<h2><?=  Yii::t('internship', 'Internships'); ?></h2>
<?= GridView::widget([
    'dataProvider' => $dataInternship,
    'filterModel' => $searchModelInternship,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        // 'USER_ID',
        'NAME',
        'DATE_START:date',
        'DATE_END:date',

        [
            'attribute' => 'COUNTRY_ID',
            'value'=>'cOUNTRY.NAME'
        ],
        [
            'format' => 'raw',
            'label'=>Yii::t('publication', 'File'),
            'attribute' => 'FILE',
            'value'=> function($data){
                return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

            }
        ],

        [
            'class' => \yii\grid\ActionColumn::className(),
            'buttons'=>[
                'view'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['/frontend/internship/view','id'=>$model->ID]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                        ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                }
            ],
            'template'=>'{view}',
        ]
    ],
]); ?>
<h2><?=  Yii::t('reviews', 'Reviews'); ?></h2>
<?= GridView::widget([
    'dataProvider' => $dataReviews,
    'filterModel' => $searchModelReviews,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        //  'ID',
        'NAME',
        'AUTHOR',

        [
            'attribute'=> 'ORGANIZATION_ID',
            'value' => 'oRGANIZATION.NAME'
        ],
        // 'USER_ID',
        // 'CREATE_DATE',
        [
            'format' => 'raw',
            'label'=>Yii::t('publication', 'File'),
            'attribute' => 'FILE',
            'value'=> function($data){
                return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

            }
        ],

        [
            'class' => \yii\grid\ActionColumn::className(),
            'buttons'=>[
                'view'=>function ($url, $model) {
                    $customurl=Yii::$app->getUrlManager()->createUrl(['/frontend/reviews/view','id'=>$model->ID]); //$model->id для AR
                    return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $customurl,
                        ['title' => Yii::t('yii', 'View'), 'data-pjax' => '0']);
                }
            ],
            'template'=>'{view}',
        ]
    ],
]); ?>
<?php //echo Comments\widgets\CommentListWidget::widget([
//'entity' => (string) 'portfolio', // type and id
//]);
?>
<?php \yii\widgets\Pjax::end(); ?>
</div>