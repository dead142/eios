<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\frontend\models\IntellectualSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('intellectual', 'Intellectuals');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intellectual-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
            <?= Html::a(Yii::t('intellectual', 'Create Intellectual'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'ID',
            'NAME',
           // 'COPYRIGHT',
         //   'ORGANIZATION_ID',
            [
                'label'=>Yii::t('qualification', 'Document  ID'),
                'attribute' => 'DOCUMENT_NAME',
                'value'=> 'Document.NAME'
            ],

              'DOCUMENT',
              'DATE_ISSUE:date',
            [
                'format' => 'raw',
                'label'=>Yii::t('publication', 'File'),
                'attribute' => 'FILE',
                'value'=> function($data){
                    return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

                }
            ],
            // 'DESC:ntext',
            // 'DATE_CREATE',
            // 'DATE_UPDATE',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>

</div>
