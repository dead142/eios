<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Intellectual */

$this->title = Yii::t('intellectual', 'Create Intellectual');
$this->params['breadcrumbs'][] = ['label' => Yii::t('intellectual', 'Intellectuals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intellectual-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
