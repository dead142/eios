<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\social\Disqus;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Intellectual */

$this->title = $model->NAME;
$this->params['breadcrumbs'][] = ['label' => Yii::t('intellectual', 'Intellectuals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intellectual-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('intellectual', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('intellectual', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('intellectual', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
         //   'ID',
            'NAME',
            'COPYRIGHT',
            [
                'label'=>Yii::t('intellectual', 'Organization  ID'),
                'value'=> $model->Organization ? $model->Organization->NAME : ''
            ],
            [
                'label'=>Yii::t('intellectual', 'Document  ID'),
                'value'=> $model->$Document ? $model->$Document->NAME :''
            ],
            'DOCUMENT',
            'DATE_ISSUE:date',
            'DESC:html',
            'DATE_CREATE:datetime',
            'DATE_UPDATE:datetime',
            [
                'label'=>Yii::t('publication', 'File'),
                'value'=> Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE,['data-pjax'=>0,'target'=>'_blank']),
                'format'=>'html'
            ],
        ],
    ]) ?>
    <?= $this->render('../_comment') ?>
</div>
