<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\IntellectualSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="intellectual-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'NAME') ?>

    <?= $form->field($model, 'COPYRIGHT') ?>

    <?= $form->field($model, 'ORGANIZATION_ID') ?>

    <?= $form->field($model, 'DOCUMENT_ID') ?>

    <?php // echo $form->field($model, 'DOCUMENT') ?>

    <?php // echo $form->field($model, 'DATE_ISSUE') ?>

    <?php // echo $form->field($model, 'DESC') ?>

    <?php // echo $form->field($model, 'DATE_CREATE') ?>

    <?php // echo $form->field($model, 'DATE_UPDATE') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('intellectual', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('intellectual', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
