<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;

use app\modules\common\models\Country;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Internship */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="internship-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class=" col-sm-6">
    <?= $form->field($model, 'NAME')->textInput(['maxlength' => true]) ?>
        </div>
        <div class=" col-sm-6">
            <?= $form->field($model, 'COUNTRY_ID')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Country::find()->all(), 'ID', 'NAME'),
                'options' => ['placeholder' => Yii::t('frontend/buttons','Select country')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>

        </div>
    </div>
    <div class="row">
        <div class=" col-sm-6">

    <?= $form->field($model, 'DATE_START')->widget(DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
        </div>
        <div class=" col-sm-6">
            <?= $form->field($model, 'DATE_END')->widget(DatePicker::classname(), [
                'options' => ['class' => 'form-control'],
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
            ]) ?>

        </div>
    </div>
    <?= $form->field($model, 'file')->fileInput(['class'=>'btn btn-primary']) ?>
    <?php
    if ($model->FILE) {
        echo Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE);
        echo '&nbsp;&nbsp;&nbsp;';
        echo Html::a(Yii::t('social', 'Delete File'), ['deleteimg', 'id'=>$model->ID, 'field'=> 'FILE'], ['class'=>'btn btn-danger']).'<p>';
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend/buttons', 'Create') : Yii::t('frontend/buttons', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
