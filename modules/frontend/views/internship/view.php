<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Internship */

$this->title = $model->NAME;
$this->params['breadcrumbs'][] = ['label' => Yii::t('internship', 'Internships'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="internship-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('frontend/buttons', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('frontend/buttons', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('internship', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'ID',
            'NAME',
            'DATE_START:date',
            'DATE_END:date',
           // 'DATE_CREATE',
            [
                'attribute'=>'COUNTRY_ID',
                'value'=> $model->uSER->profile->FullName,
            ],

            'cOUNTRY.NAME',
            [
                'label'=>Yii::t('publication', 'File'),
                'value'=> Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE,['data-pjax'=>0,'target'=>'_blank']),
                'format'=>'html'
            ],
        ],
    ]) ?>

</div>
<?= $this->render('../_comment') ?>