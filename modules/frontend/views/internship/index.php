<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\frontend\models\InternshipSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('internship', 'Internships');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="internship-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('internship', 'Create Internship'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'ID',
            'NAME',
            'DATE_START:date',
            'DATE_END:date',
            //'DATE_CREATE',
            // 'USER_ID',

            [
                'attribute' => 'COUNTRY_ID',
                 'value'=>'cOUNTRY.NAME'
            ],
            [
                'format' => 'raw',
                'label'=>Yii::t('publication', 'File'),
                'attribute' => 'FILE',
                'value'=> function($data){
                    return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
