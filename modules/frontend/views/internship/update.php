<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Internship */

$this->title = Yii::t('internship', 'Update {modelClass}: ', [
    'modelClass' => 'Internship',
]) . $model->NAME;
$this->params['breadcrumbs'][] = ['label' => Yii::t('internship', 'Internships'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NAME, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('internship', 'Update');
?>
<div class="internship-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
