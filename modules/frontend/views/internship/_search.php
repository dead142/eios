<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\InternshipSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="internship-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'NAME') ?>

    <?= $form->field($model, 'DATE_START') ?>

    <?= $form->field($model, 'DATE_END') ?>

    <?= $form->field($model, 'DATE_CREATE') ?>

    <?php // echo $form->field($model, 'USER_ID') ?>

    <?php // echo $form->field($model, 'COUNTRY_ID') ?>

    <?php // echo $form->field($model, 'FILE') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('internship', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('internship', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
