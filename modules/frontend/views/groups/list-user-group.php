<?php
/**
 * @var $searchModel \app\modules\frontend\models\ProfileSearch
 * @var $dataProvider \app\models\Profile
 * @var $model \app\models\Profile
 * @var $userStudentLastGroup \app\modules\backend\models\StudentGroup
 */

use app\modules\backend\models\Year;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\modules\common\models\StudentStatus;

?>
<?php
$this->title = 'Студенты и выпускники';
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?></h1>
<?php //  TODO echo $this->render('_search', ['model' => $searchModel]); ?>

<?php \yii\widgets\Pjax::begin(); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'fullName',
            'label' => 'ФИО студента',
            'format' => 'raw',
        ],

        [
            'attribute' => 'groupName',
            'label' => 'Группа',
            'value' => function ($model) {
                $userStudentLastGroup = $model->getLastStudentGroup($model->studentGroups);
                return $userStudentLastGroup->group->name;
            },
            'format' => 'raw',
        ],

        [
            'attribute' => 'groupYear',
            'label' => 'Год обучения',
            'value' => function ($model) {
                $userStudentLastGroup = $model->getLastStudentGroup($model->studentGroups);
                return $userStudentLastGroup->year->name;
            },
            'filter' => Year::get_list()
        ],

        [
            'attribute' => 'studentStatus',
            'label' => 'Статус',
            'value' => function ($model) {
                $userStudentLastGroup = $model->getLastStudentGroup($model->studentGroups);
                return $userStudentLastGroup->LatestStatusName;
            },
              // 'filter' => StudentStatus::status_list()
               'filter' => StudentStatus::status_list()
        ],


        [
            'format' => 'raw',
            'label' => 'Портфолио',
            'value' => function ($model) {
                return Html::a(Yii::t('frontend/main', 'See'), ['/frontend/default/all', 'id' => $model->user_id]);
            }
        ],

        [
            'format' => 'raw',
            'label' => 'Оценки',
            'value' => function ($model) {

                return Html::a(Yii::t('frontend/main', 'See'), ['/frontend/grade/grade-user', 'id' => $model->user_id]);
            }
        ],

    ],
]); ?>
<?php \yii\widgets\Pjax::end(); ?>


