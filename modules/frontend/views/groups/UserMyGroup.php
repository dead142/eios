<?php
use yii\grid\GridView;
use yii\helpers\Html;
Use yii\helpers\Url;
/**
 * Created by PhpStorm.
 * User: user
 * Date: 2/15/2016
 * Time: 11:37 AM
 */

$this->title = Yii::t('groups', 'My group');
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?></h1>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'label' =>Yii::t('groups', 'Name'),
            'attribute' => 'NAME',
            // if not set NAME , display username
            'value' =>  'name',
            'format' => 'raw',
        ],
        [
            'label' =>Yii::t('groups', 'Surname'),
            //'attribute' => 'NAME',
            // if not set NAME , display username
            'value' => 'surname',
            'format' => 'raw',
        ],
        [
            'label' =>Yii::t('groups', 'Middlename'),

            // if not set NAME , display username
            'value' => 'middlename',
            'format' => 'raw',
        ],
        [
            'format' => 'raw',
            'label'=>Yii::t('frontend/main', 'Portfolio user'),
            'value'=> function($data){

                 return  Html::a(Yii::t('frontend/main', 'See'), ['/frontend/default/all','id'=>$data['user_id']]);
            }
        ],
        [
            'format' => 'raw',
            'label'=>Yii::t('frontend/main', 'Grade user'),
            'value'=> function($data){

                return  Html::a(Yii::t('frontend/main', 'See'), ['/frontend/grade/grade-user','id'=>$data['user_id']]);
            }
        ],
       /* [
            'label'=>'NAME',
            'value'=> 'uSER.profile.name'
        ],*/




      //  ['class' => 'yii\grid\ActionColumn'],
    ],
]);

?>