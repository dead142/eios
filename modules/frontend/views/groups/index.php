<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $archieveDataProvider yii\data\ActiveDataProvider */

$this->title = 'Группы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groups-index">

    <div class="col-sm-6">
        <div class="row">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
    <div class="col-sm-6">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" onclick="openArchieve()">
            <div class="archieve-link">
                Архив <span class="glyphicon glyphicon-dashboard"></span>
            </div>
        </a>
    </div>

    <p>
        <?= Html::a('Создать группу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'ID',
            'NUMBER',
            'DATE_BEGIN',
            'DATE_END',
            [
                'attribute' => 'Добавление студентов',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Добавить студента', ['/frontend/usergroups/create?group='.$model->ID], ['class' => 'btn btn-success']);
                },
            ],
            [
                'attribute' => 'Управление студентами',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Управление', ['/frontend/usergroups?group='.$model->ID], ['class' => 'btn btn-primary']);
                },
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                Архив
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?= GridView::widget([
                                'dataProvider' => $archieveDataProvider,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    'NUMBER',
                                    'DATE_BEGIN',
                                    'DATE_END',
                                    ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div> 