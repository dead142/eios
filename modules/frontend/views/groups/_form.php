<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Groups */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="groups-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'NUMBER')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?//= $form->field($model, 'DATE_BEGIN')->textInput() ?>
            <?= $form->field($model, 'DATE_BEGIN')->widget(DatePicker::classname(), [
                'options' => ['class' => 'form-control'],
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?//= $form->field($model, 'DATE_END')->textInput() ?>
            <?= $form->field($model, 'DATE_END')->widget(DatePicker::classname(), [
                'options' => ['class' => 'form-control'],
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-12">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div> 