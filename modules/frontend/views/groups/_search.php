<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\AwardsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="awards-search">

    <?php $form = ActiveForm::begin([
        'action' => ['find-user'],
        'method' => 'get',
    ]); ?>


    <?php   echo $form->field($model, 'GROUP')->label(Yii::t('groups', 'Group')) ?>

    <?php //echo $form->field($model, 'name') ?>





    <?php    echo $form->field($model, 'surname')->label(Yii::t('groups', 'Surname')) ?>

    <?php  // echo $form->field($model, 'middlename')->label(Yii::t('groups', 'Middlename')) ?>



    <?php // echo $form->field($model, 'DATE_CREATE') ?>

    <?php // echo $form->field($model, 'DATE_UPDATE') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('buttons', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('buttons', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
