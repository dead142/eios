<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\common\models\Status;
use app\modules\common\models\KindInvolvement;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\frontend\models\ConferencesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('conferences', 'Conferences');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conferences-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('conferences', 'Create Conferences'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'ID',
            'NAME',
            'LOCATION',
            'DATE_START:date',
            'DATE_END:date',
            [
                'label'=>Yii::t('conferences', 'Status  ID'),
                'attribute' => 'STATUS_NAME',
                'value'=> 'sTATUS.NAME',
                 'filter' =>ArrayHelper::map(Status::find()->all(), 'NAME', 'NAME')

            ],
            [
                'label'=>Yii::t('conferences', 'Type of participation'),
                'attribute' => 'KIND_INVOVLVEMENT_NAME',
                'value'=> 'kINDINVOVLVEMENT.NAME',
                  'filter' =>ArrayHelper::map(KindInvolvement::find()->all(), 'NAME', 'NAME')
            ],


             'NAME_REPORT',
            [
                'format' => 'raw',
                'label'=>Yii::t('publication', 'File'),
                'attribute' => 'FILE',
                'value'=> function($data){
                    return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

                }
            ],
             //'NUMBER_NIR',
            // 'DATE_CREATE',
            // 'DATE_UPDATE',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
