<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
//models
use app\modules\common\models\Status;
use app\modules\common\models\KindInvolvement;
/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Conferences */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="conferences-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class=" col-sm-6">
    <?= $form->field($model, 'NAME')->textInput(['maxlength' => true]) ?>
        </div>
        <div class=" col-sm-6">
    <?= $form->field($model, 'NAME_REPORT')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class=" col-sm-6">
    <?= $form->field($model, 'DATE_START')->widget(DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
        </div>
        <div class=" col-sm-6">
    <?= $form->field($model, 'DATE_END')->widget(DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
        </div>
    </div>

    <div class="row">
        <div class=" col-sm-6">
    <?= $form->field($model, 'STATUS_ID')->dropDownList(ArrayHelper::map(Status::find()->all(), 'ID', 'NAME')) ?>
        </div>
        <div class=" col-sm-6">
    <?= $form->field($model, 'KIND_INVOVLVEMENT_ID')->dropDownList(ArrayHelper::map(KindInvolvement::find()->all(), 'ID', 'NAME')) ?>
        </div>
    </div>
    <div class="row">
        <div class=" col-sm-6">
            <?= $form->field($model, 'LOCATION')->textInput(['maxlength' => true]) ?>
        </div>
        <div class=" col-sm-6">
    <?= $form->field($model, 'NUMBER_NIR')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div id="row">
        <div class=" col-sm-6">
            </div>
        <div class=" col-sm-6">
            <?= $form->field($model, 'file')->fileInput(['class'=>'btn btn-primary']) ?>
            <?php
            if ($model->FILE) {
                echo Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE);
                echo '&nbsp;&nbsp;&nbsp;';
                echo Html::a(Yii::t('social', 'Delete File'), ['deleteimg', 'id'=>$model->ID, 'field'=> 'FILE'], ['class'=>'btn btn-danger']).'<p>';
            }
            ?>
        </div>

    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('conferences', 'Create') : Yii::t('conferences', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
