<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\social\Disqus;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Conferences */

$this->title = $model->NAME;
$this->params['breadcrumbs'][] = ['label' => Yii::t('conferences', 'Conferences'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conferences-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('conferences', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('conferences', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('conferences', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          //  'ID',
            'NAME',
            'LOCATION',
            'DATE_START:date',
            'DATE_END:date',
            [
                'label'=>Yii::t('conferences', 'Status  ID'),
                'value'=> $model->sTATUS->NAME
            ],
            [
                'label'=>Yii::t('conferences', 'Type of participation'),
                'value'=> $model->kINDINVOVLVEMENT->NAME
            ],

            'NAME_REPORT',
            'NUMBER_NIR',
            'DATE_CREATE:datetime',
            'DATE_UPDATE:datetime',
            [
                'label'=>Yii::t('publication', 'File'),
                'value'=> Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE,['data-pjax'=>0,'target'=>'_blank']),
                'format'=>'html'
            ],
        ],
    ]) ?>
    <?= $this->render('../_comment') ?>
</div>
