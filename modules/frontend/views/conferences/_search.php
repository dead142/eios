<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\ConferencesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="conferences-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'NAME') ?>

    <?= $form->field($model, 'LOCATION') ?>

    <?= $form->field($model, 'DATE_START') ?>

    <?= $form->field($model, 'DATE_END') ?>

    <?php // echo $form->field($model, 'STATUS_ID') ?>

    <?php // echo $form->field($model, 'KIND_INVOVLVEMENT_ID') ?>

    <?php // echo $form->field($model, 'NAME_REPORT') ?>

    <?php // echo $form->field($model, 'NUMBER_NIR') ?>

    <?php // echo $form->field($model, 'DATE_CREATE') ?>

    <?php // echo $form->field($model, 'DATE_UPDATE') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('conferences', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('conferences', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
