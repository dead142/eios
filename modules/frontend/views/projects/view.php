<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\social\Disqus;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Projects */

$this->title = $model->NAME;
$this->params['breadcrumbs'][] = ['label' => Yii::t('projects', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('projects', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('projects', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('projects', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'NAME',
            'ROLE',
            'TITLE_COMPETENTION',
            'TITLE_SECTION',
            'CODE',
            'WINNER',
            [
                'label'=>Yii::t('projects', 'Organization  ID'),
                'value'=> $model->oRGANIZATION ? $model->oRGANIZATION->NAME : ''
            ],
            [
                'label'=>Yii::t('projects', 'Document  ID'),
                'value'=> $model->dOCUMENT ? $model->dOCUMENT->NAME : ''
            ],
            'DOCUMENT_NUMBER',
            'DATE_ISSUE:date',
            'DATE_START:date',
            'DATE_END:date',
            'FUNDING',
            'CURRENCY',
            'DATE_CREATE:datetime',
            'DATE_UPDATE:datetime',
            [
                'label'=>Yii::t('publication', 'File'),
                'value'=> Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE,['data-pjax'=>0,'target'=>'_blank']),
                'format'=>'html'
            ],
        ],
    ]) ?>
    <?= $this->render('../_comment') ?>
</div>
