<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\ProjectsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'NAME') ?>

    <?= $form->field($model, 'ROLE') ?>

    <?= $form->field($model, 'TITLE_COMPETENTION') ?>

    <?= $form->field($model, 'TITLE_SECTION') ?>

    <?php // echo $form->field($model, 'CODE') ?>

    <?php // echo $form->field($model, 'WINNER') ?>

    <?php // echo $form->field($model, 'ORGANIZATION_ID') ?>

    <?php // echo $form->field($model, 'DOCUMENT_ID') ?>

    <?php // echo $form->field($model, 'DOCUMENT_NUMBER') ?>

    <?php // echo $form->field($model, 'DATE_ISSUE') ?>

    <?php // echo $form->field($model, 'DATE_START') ?>

    <?php // echo $form->field($model, 'DATE_END') ?>

    <?php // echo $form->field($model, 'FUNDING') ?>

    <?php // echo $form->field($model, 'CURRENCY') ?>

    <?php // echo $form->field($model, 'DATE_CREATE') ?>

    <?php // echo $form->field($model, 'DATE_UPDATE') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('projects', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('projects', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
