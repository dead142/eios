<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\frontend\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('projects', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="projects-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <p>
        <?= Html::a(Yii::t('projects', 'Create Projects'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin(); ?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //  'ID',
            'NAME',
             //'ROLE',
            //'TITLE_COMPETENTION',
           // 'TITLE_SECTION',
            //'CODE',
           // 'WINNER',
            [
                'label'=>Yii::t('projects', 'Organization  ID'),
                'attribute' => 'ORGANIZATION_NAME',
                'value'=> 'oRGANIZATION.NAME'
            ],[
                'label'=>Yii::t('projects', 'Document  ID'),
                'attribute' => 'DOCUMENT_NAME',
                'value'=> 'dOCUMENT.NAME'
            ],

           // 'DOCUMENT_NUMBER',
            'DATE_ISSUE:date',
           // 'DATE_START:date',
           // 'DATE_END:date',
           // 'FUNDING',
           // 'CURRENCY',
            'DATE_CREATE:date',
           // 'DATE_UPDATE',
            [
                'format' => 'raw',
                'label'=>Yii::t('publication', 'File'),
                'attribute' => 'FILE',
                'value'=> function($data){
                    return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
