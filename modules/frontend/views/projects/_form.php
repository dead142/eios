<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\bootstrap\Modal;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
//models
use app\modules\common\models\document;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="row">
            <div class=" col-sm-6 ">
    <?= $form->field($model, 'NAME')->textInput(['maxlength' => true]) ?>
            </div>
            <div class=" col-sm-6 ">
    <?= $form->field($model, 'ROLE')->dropDownList([ 'Исполнитель' => 'Исполнитель', 'Ответственный исполнитель' => 'Ответственный исполнитель', 'Руководитель' => 'Руководитель', ], ['prompt' => '']) ?>
            </div>
        </div>

        <div class="row">
            <div class=" col-sm-4 ">
    <?= $form->field($model, 'TITLE_COMPETENTION')->textInput(['maxlength' => true]) ?>
            </div>
            <div class=" col-sm-4 ">
    <?= $form->field($model, 'TITLE_SECTION')->textInput(['maxlength' => true]) ?>
            </div>
            <div class=" col-sm-4 ">
    <?= $form->field($model, 'CODE')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class=" col-sm-6">
                <?= $form->field($model, 'DOCUMENT_ID')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(document::find()->all(), 'ID', 'NAME'),
                    'options' => ['placeholder' => 'Select document'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
            <div class=" col-sm-6">
    <?= $form->field($model, 'DOCUMENT_NUMBER')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

    <div class="row">
        <div class=" col-sm-6">
    <?= $form->field($model, 'DATE_ISSUE')->widget(DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
        </div>
        <div class=" col-sm-6">
    <?= $form->field($model, 'WINNER')->dropDownList([ 'no' => Yii::t('projects', 'No'), 'yes' => Yii::t('projects', 'Yes'), ], ['prompt' => '']) ?>
        </div>
    </div>

    <div class="row">
        <div class=" col-sm-6">
    <?= $form->field($model, 'DATE_START')->widget(DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
        </div>
        <div class=" col-sm-6">
    <?= $form->field($model, 'DATE_END')->widget(DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
        </div>
    </div>

    <div class="row">
        <div class=" col-sm-6">
    <?= $form->field($model, 'FUNDING')->textInput() ?>
        </div>
        <div class=" col-sm-6">
    <?= $form->field($model, 'CURRENCY')->dropDownList([ 'USD' => 'USD', 'RUR' => 'RUR', 'EUR' => 'EUR', 'ANOTHER' => 'ANOTHER', ], ['prompt' => '']) ?>
        </div>
    </div>

    <div class="row">
        <div class=" col-sm-4">
            <?php
    //hidden input for ORGANIZATION
    echo  $form->field($model, 'ORGANIZATION_ID')->hiddenInput();
    ?>
    <?php
    // Input for ORGANIZATION
    echo  AutoComplete::widget( [
            'name' => 'ORGANIZATION',
            'id' => 'ORGANIZATION',
            'value' => $model->isNewRecord ? NULL : ($model->oRGANIZATION ? $model->oRGANIZATION->NAME : ''),

            'options' => [
                'class' => 'form-control',
                'placeholder' => 'Начните набирать название',
            ],
            'clientOptions' => [
                'source' => Url::to(['/backend/ajax/organizations']),
                'autoFill' => true,
                'minLength' => '0',
                // Get and set value to hidden field
                'select' => new JsExpression("function( event, ui ) {
            $('#". Html::getInputId($model, 'ORGANIZATION_ID')."').val(ui.item.value).trigger('change');
            $('#ORGANIZATION').val(ui.item.label);
             console.log($('#degree-organization_id').val());
            return false;
        }")
            ],
        ]
    );
    ?>
        </div>
        <div class=" col-sm-4">
    <?php echo  Html::button(Yii::t('degrees', 'Add organization to Database'), [
        'value'=> Url::toRoute('/backend/organization/create'),
        'class'=>'btn btn-primary',
        'id'=>'modalButton',
        'style'=>'margin: 10%;']);
    ?>
        </div>
        <div class=" col-sm-4">
            <?= $form->field($model, 'file')->fileInput(['class'=>'btn btn-primary']) ?>
            <?php
            if ($model->FILE) {
                echo Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE);
                echo '&nbsp;&nbsp;&nbsp;';
                echo Html::a(Yii::t('social', 'Delete File'), ['deleteimg', 'id'=>$model->ID, 'field'=> 'FILE'], ['class'=>'btn btn-danger']).'<p>';
            }
            ?>
        </div>

    </div>

    <?php // Modal for adding organizations
        Modal::begin([
            'id'=>'modal',
            'size'=>'modal-lg',
            ]);
        echo "<div id='modalContent'></div>";

        Modal::end();
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('projects', 'Create') : Yii::t('projects', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
