<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\rating\StarRating;
use app\modules\frontend\models\Rating;
use yii\helpers\Url;

?>
<?php
/**
 * Получаем оценку по URL из БД
 *
 */
 $currentUrl = Yii::$app->request->url;
 $rating = Rating::find()->where(['url'=>$currentUrl])->one();
 $ratingId = $rating ? $rating->id : null;

?>
<?php Pjax::begin(['enablePushState' => true]); ?>
<?= Html::beginForm(yii\helpers\Url::to(['/frontend/rating/vote', 'url' => $currentUrl, 'id'=>$ratingId]), 'post', ['data-pjax' => true, 'class' => 'form-inline']); ?>
<?= '<label class="control-label">Оценка: </label>';
echo  StarRating::widget([
    'name' => 'grade',
    'value' => $rating ? $rating->grade/$rating->count_vote : 0,
    'pluginOptions' => [
        'step' => 0.5,
        'starCaptions' => [
            0 => 'Extremely Poor',
            1 => 'оценка 1',
            2 => 'оценка 2',
            3 => 'оценка 3',
            4 => 'оценка 4',
            5 => 'оценка 5',

        ],
    ],
    'pluginEvents' => [


        'change' => 'function() { $("#refreshButton").click(); }',
    ],


]); ?>
<h3>  <?= $rating ? 'Всего голосов: '.$rating->count_vote : "Пока никто не оценил эту статью, станьте первым!" ?></h3>
<?= Html::submitButton('refreshButton', ['id' => 'refreshButton', 'hidden' => true]) ?>
<?= Html::endForm() ?>
<?php Pjax::end() ?>
<div id="isu-comments">
 </div>