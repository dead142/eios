<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\common\models\TypeEdition;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\frontend\models\PublicationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('publication', 'Publications');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publication-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('publication', 'Create Publication'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'NAME',
            'YEAR',
            [
                'label'=>Yii::t('publication', 'Type  Edition  ID'),
                'attribute' => 'TYPE_EDITION_NAME',
                'value'=> 'tYPEEDITION.NAME',
                'filter' =>ArrayHelper::map(TypeEdition::find()->all(), 'NAME', 'NAME')
            ],

            [
                'format' => 'raw',
                'label'=>Yii::t('publication', 'File'),
                'attribute' => 'FILE',
                'value'=> function($data){
                    return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

                }
            ],
            'DESC',
          //  'ID',
         //   'NAME',
            //'LANG_ID',
         //   'YEAR',
           // 'COUNTRY_ID',
            // 'CITY',
           //  'tYPEEDITION.NAME',
             // 'PUBLISHER',
            // 'PAGES',
           //  'FILE',
           //  'DESC',
            // 'DATE_CREATE',
            // 'DATE_UPDATE',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
