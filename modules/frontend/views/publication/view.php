<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\social\Disqus;
use yii\widgets\Pjax;
use kartik\rating\StarRating;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Publication */

$this->title = $model->NAME;
$this->params['breadcrumbs'][] = ['label' => Yii::t('publication', 'Publications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publication-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('publication', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('publication', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('publication', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //  'ID',
            'NAME',

            [
                'label' => Yii::t('publication', 'Lang  ID'),
                'value' => $model->lANG ? $model->lANG->NAME : ''
            ],
            'YEAR',
            [
                'label' => Yii::t('publication', 'Country  ID'),
                'value' => $model->cOUNTRY ? $model->cOUNTRY->NAME : ''
            ],
            'CITY',
            [
                'label' => Yii::t('publication', 'Type  Edition  ID'),
                'value' => $model->tYPEEDITION ? $model->tYPEEDITION->NAME : ''
            ],
            'PUBLISHER',
            'PAGES',

            [
                'label' => Yii::t('publication', 'File'),
                'value' => Html::a($model->FILE, \Yii::$app->request->BaseUrl . '/' . $model->FILE, ['data-pjax' => 0, 'target' => '_blank']),
                'format' => 'html'
            ],
            [
                'label' => Yii::t('publication', 'Review F'),
                'value' => Html::a($model->REVIEW_FILE, \Yii::$app->request->BaseUrl . '/' . $model->REVIEW_FILE, ['data-pjax' => 0, 'target' => '_blank']),
                'format' => 'html'
            ],
            'DESC:html',
            'DATE_CREATE',
            'DATE_UPDATE',
        ],
    ]) ?>

    <?= $this->render('../_comment') ?>
</div>
<?php $script = <<< JS
// $(document).ready(function() {
// //     if ($(".star").click()){
// //     
// //     }
// // 
// // });
JS;
$this->registerJs($script);
?>
