<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\PublicationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="publication-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'NAME') ?>

    <?= $form->field($model, 'LANG_ID') ?>

    <?= $form->field($model, 'YEAR') ?>

    <?= $form->field($model, 'COUNTRY_ID') ?>

    <?php // echo $form->field($model, 'CITY') ?>

    <?php // echo $form->field($model, 'TYPE_EDITION_ID') ?>

    <?php // echo $form->field($model, 'PUBLISHER') ?>

    <?php // echo $form->field($model, 'PAGES') ?>

    <?php // echo $form->field($model, 'FILE') ?>

    <?php // echo $form->field($model, 'DESC') ?>

    <?php // echo $form->field($model, 'DATE_CREATE') ?>

    <?php // echo $form->field($model, 'DATE_UPDATE') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('publication', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('publication', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
