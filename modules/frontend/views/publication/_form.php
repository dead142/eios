<?php

use app\modules\common\models\Indexes;
use app\modules\common\models\TypeEdition;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Publication */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$js
    = <<< 'SCRIPT'
     /*
    /* Script need for Country and Lang Automplete
    /* if hidden input have value (from Autocomplete. example)
    /* we send ID of Country(Lang) to Controller
    /* Else send name of Countrt(Lang) to Controller
    /* And add new record to DB (Country or Lang)
    */
  $('#publication-country').change(function(){
        $('#publication-country_id').val(null);
    });
     $('#publication-lang').change(function(){
        $('#publication-lang_id').val(null);
    });

SCRIPT;
// Register tooltip/popover initialization javascript
$this->registerJs($js);
?>

<div class="publication-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="row">
        <div class=" col-sm-12">
            <?= $form->field($model, 'NAME')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class=" col-sm-4">
            <?= Html::activeHiddenInput($model, 'LANG_ID', $options = []) ?>
            <?= Html::label(Yii::t('publication', 'Lang  ID')) ?>
            <?= AutoComplete::widget([
                'model'     => $model,
                'attribute' => 'LANG',
                'value'     => $model->isNewRecord ? NULL : ($model->lANG ? $model->lANG->NAME : NULL),

                'options' => [
                    'label'       => 'fff',
                    'class'       => 'form-control',
                    'placeholder' => $model->isNewRecord ? 'Начните набирать название' : ($model->lANG ? $model->lANG->NAME : NULL),
                    'title'       => 'Выберите из выпадающего списка или введите новый язык',
                    'data-toggle' => 'tooltip',
                ],

                'clientOptions' => [
                    'source'    => Url::to(['/backend/ajax/lang']),
                    'autoFill'  => true,
                    'minLength' => '0',
                    // Get and set value to hidden field
                    'select'    => new JsExpression("function( event, ui ) {
                            $('#" . Html::getInputId($model, 'LANG_ID') . "').val(ui.item.value).trigger('change');
                            $('#" . Html::getInputId($model, 'LANG') . "').val(ui.item.label);
                            console.log($('#degree-organization_id').val());
                            return false;
                            }"),
                ],
            ]);
            ?>
        </div>
        <div class=" col-sm-4">
            <?= Html::activeHiddenInput($model, 'COUNTRY_ID', $options = []) ?>
            <?= Html::label(Yii::t('publication', 'Country  ID')) ?>
            <?= AutoComplete::widget([
                'model'     => $model,
                'attribute' => 'COUNTRY',

                'options' => [
                    'class'       => 'form-control',
                    'placeholder' => $model->isNewRecord ? 'Начните набирать название' : ($model->cOUNTRY ? $model->cOUNTRY->NAME : NULL),
                    'title'       => 'Выберите из выпадающего списка или введите новую страну',
                    'data-toggle' => 'tooltip',
                ],

                'clientOptions' => [
                    'source'    => Url::to(['/backend/ajax/country']),
                    'autoFill'  => true,
                    'minLength' => '0',

                    // Get and set value to hidden field
                    'select'    => new JsExpression("function( event, ui ) {
                            $('#" . Html::getInputId($model, 'COUNTRY_ID') . "').val(ui.item.value).trigger('change');
                            $('#" . Html::getInputId($model, 'COUNTRY') . "').val(ui.item.label);
                            return false;
                            }"),
                ],
            ]);
            ?>
        </div>
        <div class=" col-sm-4">
            <?= $form->field($model, 'CITY')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class=" col-sm-3">
            <?= $form->field($model, 'TYPE_EDITION_ID')->dropDownList(ArrayHelper::map(TypeEdition::find()->all(), 'ID', 'NAME')) ?>
        </div>
        <div class=" col-sm-3">
            <?= $form->field($model, 'PUBLISHER')->textInput(['maxlength' => true]) ?>
        </div>
        <div class=" col-sm-3">
            <?= $form->field($model, 'YEAR')
                     ->textInput(['maxlength' => true])
                     ->widget(\yii\widgets\MaskedInput::className(), [
                         'mask' => '9999',
                     ]) ?>
        </div>
        <div class=" col-sm-3">
            <?= $form->field($model, 'PAGES')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class=" col-sm-12">
            <?= $form->field($model, 'INDEXES')
                     ->checkboxList(ArrayHelper::map(Indexes::find()->all(), 'ID', 'NAME')) ?>
        </div>
    </div>

    <div class="row">
        <div class=" col-sm-6">
            <?= $form->field($model, 'DESC')->widget(CKEditor::className(), [
                //                 'options' => ['rows' => 6],
                //                 'preset' => 'basic',
                'editorOptions' => ElFinder::ckeditorOptions(['elfinder'], [
                    'preset' => 'basic',
                    //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false,
                    'path' => '/publications/',
                     'filter'  => 'pdf'
                    //по умолчанию false
                ]),

            ]) ?>
        </div>
        <div class=" col-sm-6">
            <?= $form->field($model, 'file')->fileInput(['class' => 'btn btn-primary']) ?>
            <?php
            if ($model->FILE) {
                echo Html::a($model->FILE, \Yii::$app->request->BaseUrl . '/' . $model->FILE);
                echo '&nbsp;&nbsp;&nbsp;';
                echo Html::a(Yii::t('social', 'Delete File'), [
                        'deleteimg',
                        'id'    => $model->ID,
                        'field' => 'FILE',
                    ], ['class' => 'btn btn-danger']) . '<p>';
            }
            ?>
            <?= $form->field($model, 'REVIEW_F')->fileInput(['class' => 'btn btn-primary']) ?>
            <?php
            if ($model->REVIEW_FILE) {
                echo Html::a($model->REVIEW_FILE, \Yii::$app->request->BaseUrl . '/' . $model->REVIEW_FILE);
                echo '&nbsp;&nbsp;&nbsp;';
                echo Html::a(Yii::t('social', 'Delete File'), [
                        'deleteimg',
                        'id'    => $model->ID,
                        'field' => 'FILE',
                    ], ['class' => 'btn btn-danger']) . '<p>';
            }
            ?>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('publication', 'Create') : Yii::t('publication', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
