<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Publication */

$this->title = Yii::t('publication', 'Create Publication');
$this->params['breadcrumbs'][] = ['label' => Yii::t('publication', 'Publications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="publication-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
