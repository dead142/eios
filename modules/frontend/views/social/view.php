<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\social\Disqus;
/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\SocialActivities */

$this->title = $model->NAME;
$this->params['breadcrumbs'][] = ['label' => Yii::t('social', 'Social Activities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-activities-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('social', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('social', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('social', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'ID',
            'NAME',
            'DATE_START:date',
            'DATE_END:date',
            [
                'label'=>Yii::t('social', 'Status  ID'),
                'value'=> $model->sTATUS ? $model->sTATUS->NAME : ''
            ],
            [
                'label'=>Yii::t('social', 'Type of event'),
                'value'=> $model->tYPEEVENT ? $model->tYPEEVENT->NAME :''
            ],
            [
                'label'=>Yii::t('social', 'Level  ID'),
                'value'=> $model->lEVEL->NAME
            ],
            [
                'label'=>Yii::t('social', 'Type of participation'),
                'value'=> $model->tYPEPARTICIPATION->NAME
            ],
            [
                'label'=>Yii::t('publication', 'File'),
                'value'=> Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE,['data-pjax'=>0,'target'=>'_blank']),
                'format'=>'html'
            ],

            'DESC:html',
            'DATE_CREATE:datetime',
            'DATE_UPDATE:datetime',
        ],
    ]) ?>
    <?= $this->render('../_comment') ?>
</div>
