<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\common\models\TypeEvent;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\frontend\models\SocialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('social', 'Social Activities');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-activities-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('social', 'Create Social Activities'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          // 'ID',
            'NAME',
           // 'DATE_START',
          //  'DATE_END',
            [
                'label'=>Yii::t('social', 'Status  ID'),
                'attribute' => 'STATUS_NAME',
                'value'=> 'sTATUS.NAME'
            ],
            [
                'label'=>Yii::t('social', 'Type of event'),
                'attribute' => 'TYPE_EVENT_NAME',
               'value'=> 'tYPEEVENT.NAME',
                 'filter' =>ArrayHelper::map(TypeEvent::find()->all(), 'NAME', 'NAME')
            ],

            // 'LEVEL_ID',
            // 'TYPE_PARTICIPATION_ID',
            // 'DOCUMENT',
             'DESC:html',
             'DATE_CREATE:datetime',
            // 'DATE_UPDATE',
            [
                'format' => 'raw',
                'label'=>Yii::t('publication', 'File'),
                'attribute' => 'FILE',
                'value'=> function($data){
                    return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
