<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\SocialSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="social-activities-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'NAME') ?>

    <?= $form->field($model, 'DATE_START') ?>

    <?= $form->field($model, 'DATE_END') ?>

    <?= $form->field($model, 'STSTUS_ID') ?>

    <?php // echo $form->field($model, 'TYPE_EVENT_ID') ?>

    <?php // echo $form->field($model, 'LEVEL_ID') ?>

    <?php // echo $form->field($model, 'TYPE_PARTICIPATION_ID') ?>

    <?php // echo $form->field($model, 'DOCUMENT') ?>

    <?php // echo $form->field($model, 'DESC') ?>

    <?php // echo $form->field($model, 'DATE_CREATE') ?>

    <?php // echo $form->field($model, 'DATE_UPDATE') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('social', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('social', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
