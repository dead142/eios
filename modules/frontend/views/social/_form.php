<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
//models
use app\modules\common\models\Status;
use app\modules\common\models\TypeEvent;
use app\modules\common\models\Level;
use app\modules\common\models\TypeParticipation;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\SocialActivities */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="social-activities-form">

    <?php $form = ActiveForm::begin([  'enableAjaxValidation' => true,'options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class=" col-sm-12">
    <?= $form->field($model, 'NAME')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class=" col-sm-6">
    <?= $form->field($model, 'DATE_START')->widget(DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
        </div>
        <div class=" col-sm-6">
    <?= $form->field($model, 'DATE_END')->widget(DatePicker::classname(), [
        'options' => ['class' => 'form-control'],
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
        </div>
    </div>

    <div class="row">
        <div class=" col-sm-3">
    <?= $form->field($model, 'STATUS_ID')->dropDownList(ArrayHelper::map(Status::find()->all(), 'ID', 'NAME')) ?>
        </div>
        <div class=" col-sm-3">
    <?= $form->field($model, 'TYPE_EVENT_ID')->dropDownList(ArrayHelper::map(TypeEvent::find()->all(), 'ID', 'NAME')) ?>
        </div>
        <div class=" col-sm-3">
    <?= $form->field($model, 'LEVEL_ID')->dropDownList(ArrayHelper::map(Level::find()->all(), 'ID', 'NAME')) ?>
        </div>
        <div class=" col-sm-3">
    <?= $form->field($model, 'TYPE_PARTICIPATION_ID')->dropDownList(ArrayHelper::map(TypeParticipation::find()->all(), 'ID', 'NAME')) ?>
        </div>
    </div>

   <?php // $form->field($model, 'DOCUMENT')->textInput(['maxlength' => true])
   ?>
    <div class="row">
        <div class=" col-sm-6">
            <?= $form->field($model, 'DESC')->widget(CKEditor::className(), [
                //                 'options' => ['rows' => 6],
                //                 'preset' => 'basic',
                'editorOptions' => ElFinder::ckeditorOptions(['elfinder'], [
                    'preset' => 'basic',
                    //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                    'inline' => false,
                    'path' => '/social',
                    'filter'  => 'pdf'
                    //по умолчанию false
                ]),

            ])  ?>
        </div>
        <div class=" col-sm-6">
            <?= $form->field($model, 'file')->fileInput(['class'=>'btn btn-primary']) ?>
            <?php
            if ($model->FILE) {
                echo Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE);
                echo '&nbsp;&nbsp;&nbsp;';
                echo Html::a(Yii::t('social', 'Delete File'), ['deleteimg', 'id'=>$model->ID, 'field'=> 'FILE'], ['class'=>'btn btn-danger']).'<p>';
            }
            ?>
        </div>
     </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('social', 'Create') : Yii::t('social', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
