<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\social\Disqus;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Qualification */

$this->title = $model->NAME;
$this->params['breadcrumbs'][] = ['label' => Yii::t('qualification', 'Qualifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qualification-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('qualification', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('qualification', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('qualification', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'ID',
            'NAME',
            'LOCATION',
            'DATE_START:date',
            'DATE_END:date',
            [
                'label'=>Yii::t('qualification', 'Organization  ID'),
                'value'=> $model->oRGANIZATION ? $model->oRGANIZATION->NAME :''
            ],
            [
                'label'=>Yii::t('qualification', 'Document  ID'),
                'value'=> $model->dOCUMENT ? $model->dOCUMENT->NAME :''
            ],
            'DATE_ISSUE:date',
            'DESC:html',
            'HOURS',
            'DATE_CREATE:datetime',
            'DATE_UPDATE:datetime',
            [
                'label'=>Yii::t('publication', 'File'),
                'value'=> Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE,['data-pjax'=>0,'target'=>'_blank']),
                'format'=>'html'
            ],
        ],
    ]) ?>
    <?= $this->render('../_comment') ?>
</div>
