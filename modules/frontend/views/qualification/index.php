<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\frontend\models\QualificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('qualification', 'Qualifications');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="qualification-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('qualification', 'Create Qualification'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'ID',
            'NAME',
            'LOCATION',
            'DATE_START:date',
            'DATE_END:date',
            // 'ORGANIZATION_ID',
            [
                'label'=>Yii::t('qualification', 'Document  ID'),
                'attribute' => 'DOCUMENT_NAME',
                'value' => 'dOCUMENT.NAME',
            ],
              'DATE_ISSUE:date',
            // 'HOURS',
             'DATE_CREATE:datetime',
            [
                'format' => 'raw',
                'label'=>Yii::t('publication', 'File'),
                'attribute' => 'FILE',
                'value'=> function($data){
                    return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

                }
            ],
            // 'DESC:ntext',
            // 'DATE_UPDATE',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
