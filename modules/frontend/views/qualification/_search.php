<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\QualificationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="qualification-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'NAME') ?>

    <?= $form->field($model, 'LOCATION') ?>

    <?= $form->field($model, 'DATE_START') ?>

    <?= $form->field($model, 'DATE_END') ?>

    <?php // echo $form->field($model, 'ORGANIZATION_ID') ?>

    <?php // echo $form->field($model, 'DOCUMENT_ID') ?>

    <?php // echo $form->field($model, 'DATE_ISSUE') ?>

    <?php // echo $form->field($model, 'DESC') ?>

    <?php // echo $form->field($model, 'HOURS') ?>

    <?php // echo $form->field($model, 'DATE_CREATE') ?>

    <?php // echo $form->field($model, 'DATE_UPDATE') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('qualification', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('qualification', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
