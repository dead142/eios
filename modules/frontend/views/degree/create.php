<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Degree */

$this->title = Yii::t('degrees', 'Create Degree');
$this->params['breadcrumbs'][] = ['label' => Yii::t('degrees', 'Degrees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="degree-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
       // 'initialPreview'=>[],
      //  'initialPreviewConfig'=>[]
    ]) ?>

</div>
