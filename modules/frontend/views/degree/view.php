<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\social\Disqus;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Degree */

$this->title = $model->NAME;
$this->params['breadcrumbs'][] = ['label' => Yii::t('degrees', 'Degrees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="degree-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?=  Html::a(Yii::t('degrees', 'Update Degree'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('degrees', 'Delete Degree'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'ID',
            'NAME',
            [
                'label'=>Yii::t('degrees', 'Area of science'),
                'value'=> $model->aREA ? $model->aREA->NAME :''
            ],
            [
                'label'=>Yii::t('degrees', 'The name of the organization'),
                'value'=> $model->oRGANIZATION ? $model->oRGANIZATION->NAME :''
            ],
            [
                'label'=>Yii::t('degrees', 'The document type'),
                'value'=> $model->dOCUMENT ? $model->dOCUMENT->NAME :''
            ],

            'ISSUE_DATE',
            'DOCUMENT_NUMBER',
            'DATE_CREATE',
            'DATE_UPDATE',
            [
                'label'=>Yii::t('publication', 'File'),
                'value'=> Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE,['data-pjax'=>0,'target'=>'_blank']),
                'format'=>'html'
            ],
        ],
    ]) ?>
    <?= $this->render('../_comment') ?>
</div>
