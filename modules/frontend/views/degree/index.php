<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\frontend\models\DegreeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('degrees', 'Degrees');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="degree-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('degrees', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'ID',
            'NAME',
         //   'AREA_ID',
           // 'ORGANIZATION_ID',

            [
                'attribute' => 'DOCUMENT_NAME',
                'value' => 'dOCUMENT.NAME',

            ],
            'DOCUMENT_NUMBER',
           'ISSUE_DATE:date',


              'DATE_CREATE:datetime',
            [
                'format' => 'raw',
                'label'=>Yii::t('publication', 'File'),
                'attribute' => 'FILE',
                'value'=> function($data){
                    return Html::a($data->FILE,\Yii::$app->request->BaseUrl.'/'.$data->FILE,['data-pjax'=>0,'target'=>'_blank']);

                }
            ],
           //  'DATE_UPDATE',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>

</div>
