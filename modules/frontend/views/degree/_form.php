<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\modules\common\models\Area;
use app\modules\common\models\document;
use yii\bootstrap\Modal;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
//use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Degree */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="degree-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="row">
        <div class=" col-sm-6 ">

            <?= $form->field($model, 'NAME')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'AREA_ID')->dropDownList(ArrayHelper::map(Area::find()->all(), 'ID', 'NAME')); ?>

    </div>
    <div class=" col-sm-6 ">

        <?= $form->field($model, 'DOCUMENT_ID')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(document::find()->all(), 'ID', 'NAME'),
            'options' => ['placeholder' => 'Select document'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

            <?= $form->field($model, 'ISSUE_DATE')
            ->widget(DatePicker::classname(), [
                'options' => ['class' => 'form-control'],
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
            ])
        ?>
    </div>
        </div>
    <div class="row">
        <div class=" col-sm-6">

            <?= $form->field($model, 'DOCUMENT_NUMBER')->textInput(['maxlength' => true]) ?>
</div>
            <div class=" col-sm-6">
                <?= $form->field($model, 'file')->fileInput(['class'=>'btn btn-primary']) ?>
                <?php
                if ($model->FILE) {
                    echo Html::a($model->FILE,\Yii::$app->request->BaseUrl.'/'.$model->FILE);
                    echo '&nbsp;&nbsp;&nbsp;';
                    echo Html::a(Yii::t('social', 'Delete File'), ['deleteimg', 'id'=>$model->ID, 'field'=> 'FILE'], ['class'=>'btn btn-danger']).'<p>';
                }
                ?>
            </div>
    </div>
    <div class="row">
        <div class=" col-sm-8">
         <?php
    //hidden input for ORGANIZATION
                 echo  $form->field($model, 'ORGANIZATION_ID')->hiddenInput();
             ?>
            <?php
    // Input for ORGANIZATION
    echo  AutoComplete::widget( [
            'name' => 'ORGANIZATION',
            'id' => 'ORGANIZATION',
             'value' => $model->isNewRecord ? NULL : ($model->oRGANIZATION ? $model->oRGANIZATION->NAME : NULL) ,

            'options' => [
                'class' => 'form-control',
                'placeholder' => 'Начните набирать название',
            ],
            'clientOptions' => [
                'source' => Url::to(['/backend/ajax/organizations']),
                'autoFill' => true,
                'minLength' => '0',
                // Get and set value to hidden field
                'select' => new JsExpression("function( event, ui ) {
            $('#". Html::getInputId($model, 'ORGANIZATION_ID')."').val(ui.item.value).trigger('change');
            $('#ORGANIZATION').val(ui.item.label);
             console.log($('#degree-organization_id').val());
            return false;
        }")
            ],
        ]
    );
    ?>

        </div>
        <div class=" col-sm-4">

    <?php echo  Html::button(Yii::t('degrees', 'Add organization to Database'), [
        'value'=> Url::toRoute('/backend/organization/create'),
        'class'=>'btn btn-primary',
        'id'=>'modalButton',
        'style'=>'margin: 10%;']);
    ?>
</div>
    </div>
    <?php // Modal for adding organizations
        Modal::begin([
                'id'=>'modal',
                'size'=>'modal-lg',
         ]);
          echo "<div id='modalContent'></div>";

         Modal::end();
    ?>

    <div class="row">



     <?  /* echo FileInput::widget([

        'name' => 'file[]',

        'language' => 'ru',
        'options' => ['multiple' => true],

         'pluginOptions' => [//['previewFileType' => 'any', 'uploadUrl' => Url::to(['/common/file/multiple']),
             'showUpload' => false,
             'showRemove' => true,
             'overwriteInitial'=>false,
             'initialPreviewShowDelete'=>true,
           //  'initialPreview' => [HTML::img($model->file, ['class'=>'file-preview-image', 'alt'=>$model->file, 'title'=>$model->file,])],
              'initialPreview'=> $initialPreview,
             'initialPreviewConfig'=> $initialPreviewConfig,
          //  'initialPreviewConfig'=>
          //  [['caption'=>$model->file,'width' => '120px','url'=> '/vvfvfd/acvfdvdfvtion', 'key'=> 1],],
             'uploadExtraData' => [
                 'ref' => '1',
                 'controller'=>Yii::$app->controller->id,
             ],
             'maxFileCount' => 3
        ]

        ]); */ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('degrees', 'Create') : Yii::t('degrees', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
