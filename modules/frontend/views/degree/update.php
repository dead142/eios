<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\Degree */

$this->title = Yii::t('degrees', 'Update {modelClass}: ', [
    'modelClass' => 'Degree',
]) . ' ' . $model->NAME;
$this->params['breadcrumbs'][] = ['label' => Yii::t('degrees', 'Degrees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NAME, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('degrees', 'Update');
?>
<div class="degree-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
       // 'initialPreview'=>$initialPreview,
      //  'initialPreviewConfig'=>$initialPreviewConfig
    ]) ?>

</div>
