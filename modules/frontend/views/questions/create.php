<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\questions */

$this->title = Yii::t('frontend/main', 'Create Questions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend/main', 'Questions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
