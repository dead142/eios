<?php

namespace app\modules\backend\controllers;

use app\models\Profile;
use app\modules\backend\models\Formgrade;
use app\modules\backend\models\Grades;
use app\modules\backend\models\GroupsDiscipline;
use app\modules\backend\models\StudentGroup;
use app\modules\backend\models\Year;
use app\modules\common\models\ProfileSearch;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\Controller;

/**
 * StatusController implements the CRUD actions for status model.
 */
class StudentToGroupController extends Controller
{
    public $currentYear = array();
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins','teacher'],

                    ],
                ],
            ],
        ];
    }
/*
 *
 */
   public function  actionIndex(){
       $searchModel = new ProfileSearch();
       $queryParams = array_merge(array(),Yii::$app->request->getQueryParams());
       $queryParams["ProfileSearch"]["group_id"] = null ;
       $dataProvider =  $searchModel->search($queryParams);

       return $this->render('index', [
           'dataProvider' => $dataProvider,
           'searchModel' => $searchModel,
       ]);
      /* $users = Profile::find()->where(['group_id'=> NULL])->all();
      $group = Groups::find()->joinWith('idYear')->where(['year.id'=>1])->all();
       return $this->render('index', [
          'users' => $users,
            'groups'=> $group]);*/
   }
    /*
     *
     */
    public function currentYear(){
        $year = new Year();
        $this->currentYear = $year->find()
            ->where(['<','year_start','DATE'])
            ->andWhere(['>=','year_end','DATE'])
            ->asArray()->all();

       return $this->currentYear;

	}

	/*
	 *
	 */
	public function actionAddToGroup() {
		$code = 100;
		if ( Yii::$app->request->isAjax ) {

			$data              = Yii::$app->request->post();
			$id                = $data['id'];
			$groupSelected     = $data['groupSelected'];
			$profile           = Profile::findOne( $id );
			$profile->group_id = (int) $groupSelected;

			if ( $profile->save() ) {
				$code                        = 200;
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

				return $this->redirect( 'index' );
			}


		}

	}

	/**
	 * Функция ищет студентов без группы, и вывод список
	 * если администратор хочет привязать студента к гурппе, то принимает ajax запрос
	 * проверяет, привязан ли уже пользователь к данной группе
	 * проверяет, если формы контроля у этой группы
	 * елси есть - то добаляет оценку 0 по каждой форме контроля
	 * @var $errors [] array | массив ошибок и сообщений для отслеживания работы
	 *
	 * @param null $studentId
	 * @param null $groupId
	 *
	 * @return string
	 */
	public function actionStudentAssignGroup( $studentId = null, $groupId = null ) {

		$studentWithGroup = Profile::getStudentsWithGroups();
		if ( \Yii::$app->request->isAjax ) {
			$errors    = [];
			$studentId = \Yii::$app->request->post( 'user_id' );
			$groupId   = \Yii::$app->request->post( 'group_id' );
			$errors[]  = StudentGroup::saveRelationStudentGroup( $studentId, $groupId );

			$groupDisciplines = GroupsDiscipline::getDisciplinesWithFormgradesByGroupId( $groupId );

			foreach ( $groupDisciplines as $formgrades ) {
				foreach ( $formgrades->formgrades as $formgrade ) {
					if ( Grades::checkExists( $formgrade->groups_discipline_id, $formgrade->id, $studentId ) ) {
						$errors[] = 'Formgrade ' . $formgrade->id . ' for user ' . $studentId . ' already exists';
					} else {
						$gradeModel                = new Grades();
						$gradeModel->discipline_id = $formgrade->groups_discipline_id;
						$gradeModel->formgrade_id  = $formgrade->id;
						$gradeModel->grade         = '0';
						$gradeModel->student_id    = $studentId;
						$gradeModel->save();
					}
				}
			}

			$this->redirect('student-assign-group');
		} else {
			return $this->render( 'selectUser', [ 'studentWithGroup' => $studentWithGroup ] );
		}

	}

	public function actionStudentAssignFormgrade( $groupId = 13, $userId= 81 ) {
		 $studentFormgrade = Grades::find()->with('formgrade')->where(['student_id'=>$userId])->all();

		$groupFormgrade = GroupsDiscipline::find()
		                                 ->with( 'formgrades' )
		                                  ->where( [ 'groups_discipline.group_id' => $groupId ] )
		                                  ->all();
		//$result = array_diff($studentFormgrade->, $array2);
		VarDumper::dump($groupFormgrade,10,true);
		VarDumper::dump($studentFormgrade,10,true);
	}

}
