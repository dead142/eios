<?php

namespace app\modules\backend\controllers;

use app\modules\backend\models\Disciplines;
use app\modules\backend\models\DisciplinesSearch;
use app\modules\backend\models\Groups;
use app\modules\backend\models\GroupsDiscipline;
use app\modules\backend\models\GroupsDisciplineSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * DisciplinesController implements the CRUD actions for Disciplines model.
 */
class DisciplinesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins', 'teacher'],

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Disciplines models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DisciplinesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Disciplines model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Disciplines model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Disciplines();

        if ($model->load(Yii::$app->request->post())) {
            $model->create_at = date('Y-m-d h:m:s');
            $model->create_by = Yii::$app->user->identity->id;
            $model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Disciplines model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Disciplines model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Disciplines model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Disciplines the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Disciplines::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*
     * @param integer $id
     */
    public function actionDisciplineByGroup($id)
    {
        $searchModel = new GroupsDisciplineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['groups_discipline.group_id' => $id]);

        $groupName = ArrayHelper::getValue(Groups::findOne(['id' => $id]), 'name');
        return $this->render('groupDiscipline', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'groupName' => $groupName,
            'groupId' => $id,
        ]);
    }

    public function actionDisciplinesByTeacher()
    {
        if (Yii::$app->user->identity) {
            $user_id = Yii::$app->user->identity->id;
            $disciplines = GroupsDiscipline::find()->with('group.idYear')->where(['teacher_id' => $user_id]);
            $dataProvider = new ActiveDataProvider([
                'query' => $disciplines,
            ]);
            return $this->render('disciplineTeacher', [
                'dataProvider' => $dataProvider,
            ]);
        }
        return $this->redirect(['/user/login']);
    }

    /**
     * Метод удаляет дисциплину из группы
     * @param $id
     * @return \yii\web\Response
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteDisciplineGroup($id)
    {
        $model = GroupsDiscipline::findOne(['id' => $id]);
        $groupId = $model->group_id;
        if ($model->delete()) {
            \Yii::$app->getSession()->setFlash('success', 'Запись успешно удалена');
        } else {
            \Yii::$app->getSession()->setFlash('error', 'Возникла ошибка при удалении');
        }
        return $this->redirect(['discipline-by-group', 'id' => $groupId]);


    }

    /**
     * Метод для обновления данных о дисциплине
     * @param $id
     * @return string
     */
    public function actionUpdateDisciplineGroup($id)
    {
        $model = GroupsDiscipline::findOne(['id' => $id]);
        $disciplineModel = $model->discipline;
        if ($model->load(Yii::$app->request->post()) && $disciplineModel->load(Yii::$app->request->post())){
            if ($disciplineModel->save() && $model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Запись успешно Обновлена');
            } else {
                \Yii::$app->getSession()->setFlash('error', 'Ошибка при сохранении данных');
            }
        } else {
            \Yii::$app->getSession()->setFlash('error', 'Ошибка при получении');
        }

        return $this->render('update-discipline-group',
            [
                'model' => $model,
                'disciplineModel' => $disciplineModel
            ]);


    }
}
