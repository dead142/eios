<?php

namespace app\modules\backend\controllers;

use app\helpers\MainHelper;
use app\models\Profile;
use app\modules\backend\models\Grades;
use app\modules\backend\models\Groups;
use app\modules\backend\models\GroupsDiscipline;
use app\modules\backend\models\StudentGroup;
use app\modules\backend\models\Year;
use app\modules\common\models\StudentStatus;
use dektrium\user\helpers\Password;
use app\models\User;
use Yii;
use yii\base\DynamicModel;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * ParticipationController implements the CRUD actions for TypeParticipation model.
 */
class RegistrationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins', 'teacher'],

                    ],
                ],
            ],
        ];
    }


    /**
     * Export to Excel
     * FIO | Login | Password
     */
    public function actionDownloadExcel()
    {
        header('Content-Type: text/csv; charset=windows-1251');
        header('Content-Disposition: attachment; filename=data.csv');
        $data = Yii::$app->request->post();
        $data = unserialize($data['data']);


        $output = fopen('php://output', 'w');
        fwrite($output, "\xEF\xBB\xBF");
        fputcsv($output, ['user', 'password', 'login'], ';');
        foreach ($data as   $value) {

            fputcsv($output, $value, ';');
        }
    }

    /**
     * Метод создает модель для выбора группы и количества добавляемых студентов
     * @param null $year_id год обучения
     * @param null $count количество добавляемых студентов в группу
     * @param null $group_id группа
     * @var $years Year
     * @return string
     */
    public function actionIndex($year_id = null, $count = null, $group_id = null)
    {
        $model = new DynamicModel(compact('year_id', 'count', 'group_id'));
        $model->addRule(['year_id', 'group_id', 'count'], 'integer', ['message' => 'Значение должно быть целым числом'])
            ->addRule(['year_id', 'group_id', 'count'], 'required', ['message' => 'Это поле обязательно для заполнения'])
            ->validate();
        $years = ArrayHelper::map(Year::find()->all(), 'id', 'name');
        return $this->render('index', ['years' => $years, 'model' => $model]);
    }

    /**
     * Метод отображает форму для заполенния ФИО студентов
     * @var $model DynamicModel
     * @return string
     */
    public function actionFormUsers()
    {
        if (Yii::$app->request->post()) {
            $data = (object)current(Yii::$app->request->post());
            $groupName = ArrayHelper::getValue(Groups::findOne(['id' => $data->group_id]), 'fullNameGroup');
            $model = new DynamicModel(['middlename', 'name', 'surname']);
            \Yii::$app->getSession()->setFlash('success', "Все данные формы получены");
            return $this->render('users-form', [
                'groupId' => $data->group_id,
                'groupName' => $groupName,
                'usersCount' => $data->count,
                'model' => $model
            ]);
        } else {
            \Yii::$app->getSession()->setFlash('danger', "Не получены данные формы. Информация для отладки: backend\RegistrationController");
            $this->redirect('index');
        }

    }

    /**
     * @return string
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public function actionSaveUsers()
    {

        $data = (object)Yii::$app->request->post();
        $groupModel = Groups::findOne(['id'=>$data->groupId]);


        for ($i = 0; $i < $data->usersCount; $i++) {

            $middlename = $data->DynamicModel['middlename'][$i];
            $name = $data->DynamicModel['name'][$i];
            $surname = $data->DynamicModel['surname'][$i];
            $username = User::GenerateLogin($surname,$groupModel->id);

            $models[] = [
                'user'=> $userModel =\app\models\User::createUserByUserName($username),
                'profile' => $profile = Profile::createProfile($middlename,$name,$surname,$userModel),
                'group' => $groupModel
            ];
             $studentGroupModel = StudentGroup::saveRelationStudentGroup($userModel->id,$groupModel->id);
             StudentStatus::createStudentStatus('active','Создано автоматичеcки',1, date("Y-m-d H:i:s"),$studentGroupModel->id);
             $this->actionSetGrades($groupModel->id, $userModel->id);
        }
          return $this->render('saved-users', ['usersModels' => $models]);
    }

    /**
     * TODO
     * Import Users from files
     * @return string
     */
    public function actionImportUser()
    {
        if (\Yii::$app->request->post()) {
            $groupID = \Yii::$app->request->post('group');
            $file = UploadedFile::getInstanceByName('file');
            if ($this->checkFile($file) && $this->checkGroup($groupID)) {
                $extensionFile = $file->extension;
                if ($extensionFile == 'xls' || $extensionFile == 'xlsx') {
                    $studentsNames = $this->getUsersFromExcel($file->tempName);
                    $models = $this->createUsersAndProfiles($studentsNames, $groupID);

                    return $this->render('saved-users', ['usersModels' => $models]);
                } else {
                    \Yii::$app->session->setFlash( 'error', 'File is not supported, use xls or xlsx');

                    return $this->render('uploadExcel');
                }
            }
        }

        return $this->render('uploadExcel');


    }

    public function getUsersFromExcel($fileTempName)
    {
        $data = \moonland\phpexcel\Excel::import($fileTempName, ['setFirstRecordAsKeys' => false]); #https://github.com/moonlandsoft/yii2-phpexcel

        return $data;

    }

    /**
     * @param $studentsNames
     * @param $groupID
     * @return array
     * @throws \Exception
     * @throws \yii\base\Exception
     */
    public function createUsersAndProfiles($studentsNames, $groupID)
    {
        $groupModel = Groups::findOne(['id'=>$groupID]);
        foreach ($studentsNames as $student) {
            list($middlename, $name, $surname) = explode(" ", $student['A']); # Get first column in excel
            $userName = \app\models\User::GenerateLogin($surname, $groupID);
            $models[] = [
                'user'=> $userModel =\app\models\User::createUserByUserName($userName),
                'profile' => $profile = Profile::createProfile($middlename,$name,$surname,$userModel),
                'group' => $groupModel
            ];
            $studentGroupModel = StudentGroup::saveRelationStudentGroup($userModel->id,$groupID);
            StudentStatus::createStudentStatus('active','Создано автоматичеки',1, date("Y-m-d H:i:s"),$studentGroupModel->id);
            $this->actionSetGrades($groupID, $userModel->id);

        }

        return $models;
    }





    /**
     * Function check selected group
     * If group doesn't select return error
     *
     * @param $groupID
     *
     * @return bool|string
     */
    public function checkGroup($groupID)
    {
        if ($groupID) {
            return true;
        } else {
            \Yii::$app->session->setFlash('error', 'Group doesn\'t select');

            return false;
        }
    }


    /**
     * Function check selected file
     * If file doesn't select return error
     *
     * @param $file
     *
     * @return bool|string
     */
    public function checkFile($file)
    {
        if ($file) {
            return true;
        } else {
            \Yii::$app->session->setFlash('error', 'File doesn\'t select');

            return false;
        }
    }


    public function actionSetGrades($groupId, $studentId)
    {
        $groupDisciplines = GroupsDiscipline::find()
            // ->select('discipline_id')
            ->joinWith('formgrades')
            ->where(['group_id' => $groupId])
            ->all();

        foreach ($groupDisciplines as $formgrades) {

            foreach ($formgrades->formgrades as $formgrade) {
                $gradeModel = new Grades();
                $gradeModel->discipline_id = $formgrade->groups_discipline_id;
                $gradeModel->formgrade_id = $formgrade->id;
                $gradeModel->grade = '0';
                $gradeModel->student_id = $studentId;
                $gradeModel->save();
            }

        }


    }
}