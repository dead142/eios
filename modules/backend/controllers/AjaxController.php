<?php
/**
 * Created by PhpStorm2.
 * User: user
 * Date: 17.11.2015
 * Time: 20:03
 */
namespace app\modules\backend\controllers;

use app\models\Profile;
use app\models\User;
use app\modules\common\models\organization;
use app\modules\common\models\Country;
use app\modules\common\models\Lang;

use app\modules\frontend\models\File;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\UploadedFile;


class AjaxController extends Controller
{
    public function actionOrganizations($term)
    {
        $data=[];
        $domains = organization::find()->andFilterWhere(['like', 'NAME', $term])->limit(10)->all();
        if ($domains) foreach ($domains as $domain) {
            $data[] = [
                'label' => $domain->NAME,
                'value' => $domain->ID,
            ];
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $data;

    }
    public function actionCountry($term)
    {
        $data=[];
        $country = Country::find()->andFilterWhere(['like', 'NAME', $term])->limit(10)->all();
        if ($country) foreach ($country as $countr) {
            $data[] = [
                'label' => $countr->NAME,
                'value' => $countr->ID,
            ];
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $data;

    }
    public function actionLang($term)
    {
        $data=[];
        $languages = Lang::find()->andFilterWhere(['like', 'NAME', $term])->limit(10)->all();
        if ($languages) foreach ($languages as $language) {
            $data[] = [
                'label' => $language->NAME,
                'value' => $language->ID,
            ];
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $data;

    }
    public function actionImageUpload()
    {

        $user_id    = \Yii::$app->user->getID();
        $model      = Profile::find()->where(['user_id' => $user_id])->one();
        $oldfile    = $model->photo_url;                                        // Old photo
        $imageFile  = UploadedFile::getInstance($model, 'photoImg');
        $directory  = 'uploads/user_photo/';

        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }

        if ($imageFile) {
            $uid        = uniqid(time(), true);
            $fileName   = $user_id . '-' . $uid. '.' . $imageFile->extension;
            $filePath   = $directory . $fileName;

            if ($imageFile->saveAs($filePath)) {
                if (is_file($oldfile)) {
                    unlink($oldfile);                                           // Delete old file
                }

                $model->photo_url = $filePath;
                $model->save();
                $path = '/img/temp/' . \Yii::$app->session->id . DIRECTORY_SEPARATOR . $fileName;
                return Json::encode([


                            'name'          => $fileName,
                            'size'          => $imageFile->size,
                            'url'           => $path,
                            'thumbnailUrl'  => $filePath,                       // It's really used =)
                            'deleteUrl'     => 'image-delete?name=' . $fileName,//TODO Delete img
                            'deleteType'     => 'POST',

                ]);
            }
        }

        return '';
    }


}