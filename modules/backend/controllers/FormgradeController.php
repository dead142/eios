<?php

namespace app\modules\backend\controllers;

use app\modules\backend\models\Grades;
use app\models\Profile;
use Yii;
use app\modules\backend\models\Formgrade;
use app\modules\backend\models\FormgradeSearch;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FormgradeController implements the CRUD actions for Formgrade model.
 */
class FormgradeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins','teacher'],

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Formgrade models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FormgradeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Formgrade model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Formgrade model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($group_id = null, $groupDiscipline_id = null)
    {
        $model = new Formgrade();

        if ($model->load(Yii::$app->request->post())) {

       $model->save();

            $profiles = Profile::find()->joinWith('studentGroups')->where(['student_group.group_id'=>$group_id])->all();

            foreach ($profiles as $item){
                $grade = new Grades();
                $grade->student_id = $item->user_id;
                $grade->discipline_id = $model->groups_discipline_id;
                $grade->formgrade_id = $model->id;
                $grade->grade = '0';

            $grade->save();


            }

        return $this->redirect(['/backend/grades/grades-disciplines',
            'groupDiscipline_id' => $groupDiscipline_id,
            'group_id' => $group_id
        ]);
        } else {
            $model->groups_discipline_id = $groupDiscipline_id;
            return $this->render('create', [
                'model' => $model,

            ]);
        }
    }

    /**
     * Updates an existing Formgrade model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Url::previous());
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Formgrade model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
       return $this->redirect(Url::previous());
    }

    /**
     * Finds the Formgrade model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Formgrade the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Formgrade::findOne(['id'=>$id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
