<?php

namespace app\modules\backend\controllers;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class DefaultController extends Controller
{
    public $layout = 'backend';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['admins','teacher'],
                    ],
                ],
            ],
            ];
    }
    public function actionIndex()
    {
        return $this->render('index');
    }
}
