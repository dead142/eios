<?php

namespace app\modules\backend\controllers;

use app\models\Profile;
use app\modules\backend\models\Groups;
use app\modules\backend\models\StudentGroup;
use Yii;
use app\modules\common\models\StudentStatus;
use app\modules\common\models\search\StudentStatusSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * StudentStatusController implements the CRUD actions for StudentStatus model.
 */
class StudentStatusController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins','teacher'],

                    ],
                ],
            ],
        ];
    }


    /**
     * Lists all StudentStatus models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StudentStatusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single StudentStatus model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StudentStatus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StudentStatus();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()){
                
            }
           VarDumper::dump($model,10,true);
//            $model->group_id =  $studentGroup->id;
//            $model->save();

       //  return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StudentStatus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing StudentStatus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StudentStatus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StudentStatus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StudentStatus::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetJsonGroup() {

        $out = [];

        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
               $cat_id =  (integer)$parents[0];
               $out = Groups::find()
                ->where(['groups.id_year'=>$cat_id])->asArray()->all();
                 echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'1']);
    }
    public function actionGetJsonProfiles() {

        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $cat_id = empty($ids[0]) ? null : $ids[0]; #id_year
            $subcat_id = empty($ids[1]) ? null : $ids[1]; #group_id
            if ($cat_id != null) {
                $data['out'] = StudentGroup::find()
                    ->leftJoin('profile', '`profile`.`user_id` = `student_group`.`student_id`')
                    ->select(['profile.user_id as id','CONCAT (profile.middlename, " ",profile.name, " ",profile.surname ) as name'])
                    ->where(['student_group.group_id'=>$subcat_id])
                    ->asArray()
                    ->all();


                echo Json::encode(['output'=>$data['out'], 'selected'=>$data['selected']]);

                return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

}
