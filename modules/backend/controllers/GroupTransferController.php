<?php
/**
 *  @version  1.0.1
 */
namespace app\modules\backend\controllers;

use app\modules\backend\models\Groups;
use app\modules\backend\models\TransferOneUserForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\Controller;

class GroupTransferController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins', 'teacher'],

                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all GroupsDiscipline models.
     *
     */
    public function actionIndex()
    {

        return $this->render('index');


    }

    /**
     * Метод переводит несколько групп на следующий год
     * @return string|\yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionTransfer()
    {
        $toYearId = (int)\Yii::$app->request->post('year_to');
        $yearFromId = (int)\Yii::$app->request->post('year_from');

        $transaction = \Yii::$app->db->beginTransaction();
        $flagTransaction = true;
        if (!empty($_POST['Groups'])) {
            foreach ($_POST['Groups'] as $index => $item) {
                if ($item['isTransfer'] == true) {

                    if ($item['active'] == true) { //если группа переводится на следующий год, то создаем новую
                        $model = new Groups();
                        $model->name = $item['name'];
                        $model->id_year = $item['id_year'];
                        $model->course = $item['course'];
                        $model->p_id = $item['p_id'];
                        $model->active = $item['active'];
                        if ($model->save()) {
                            $flagTransaction = $flagTransaction && true;
                            (Groups::TransferStudentToGroup($item['p_id'], $model->id, $item['id_year'], $model->course)) ? $flagTransaction = $flagTransaction && true : $flagTransaction = $flagTransaction && false;
                        } else {
                            $flagTransaction = $flagTransaction && false;
                            \Yii::$app->session->setFlash('error', 'Ошибка при установке создании новой группы (backend/GroupTrandferController/Tranfer');
                        }

                    } else {
                        if (Groups::disableGroup($item['p_id']) instanceof Groups) {
                            $flagTransaction = $flagTransaction && true;
                        } else {
                            \Yii::$app->session->setFlash('error', 'Ошибка при установке статуса группе (backend/GroupTrandferController/Tranfer');
                            $flagTransaction = $flagTransaction && false;
                        }

                    }
                }
            }
            if ($flagTransaction == true) {
                $transaction->commit();
                \Yii::$app->session->setFlash('success', 'Успешно!');
            } else {
                $transaction->rollback();
                \Yii::$app->session->setFlash('error', 'Перевод не удался =( что-то пошло не так');

            }

            return $this->redirect(['index']);

        } else {
            /**
             * Запрос проверяет есть ли дочерние года (последующие)
             * Если есть, то такие записи не выводятся
             **/
            $model = new Groups();
            $models = $model::find()->alias('Mgroups')->where(['id_year' => $yearFromId])
                ->andWhere(["(SELECT id as idg FROM groups WHERE p_id=Mgroups.id)" => null])
                ->all();

            foreach ($models as $item) {                                //Добавлем в имя группы
                $item->ParentChildInfo = Groups::GetListGroups($item->id);         //предыдущие и послежующие года
            }

            return $this->render('_form.php', [
                'models' => $models,
                'yearToId' => $toYearId,
            ]);
        }


    }


    public function actionTransferStudent()
    {
        $model = new TransferOneUserForm();
        return $this->render('_one_user_form', ['model' => $model]);
        // set user status 'переведенэ
    }


}