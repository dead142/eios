<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 28.05.2018
 * Time: 23:52
 */

namespace app\modules\backend\controllers;


use app\models\Profile;
use app\modules\backend\models\StudentGroup;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;

class StudentController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'allow' => true,
                        'roles' => ['admins', 'teacher'],

                    ],
                ],
            ],
        ];
    }


    /**
     * Метод возврашает студетов  группы
     * ['id' => id, 'name' => FIO]
     */
    public function actionStudentsListByYearAjax()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $group_id = $parents[0];
                $out = StudentGroup::find()->leftJoin('profile', 'profile.user_id=student_group.student_id')
                    ->select(
                        [
                            'profile.user_id AS id',
                            'CONCAT(profile.name," " , profile.surname, " ", profile.middlename) AS name',
                        ])->where(['student_group.group_id' => $group_id])
                    ->asArray()
                    ->all();
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    /**
     * Метод отображает профиль студента
     * @param $userId
     * @return string
     */
    public function actionProfile($userId)
    {
        $studentProfile = Profile::getStudentProfile($userId);
        $student = $studentProfile['student'];
        $studentGroups = $studentProfile['studentGroups'];
        return $this->render('profile',
            [
                'studentGroups' => $studentGroups,
                'student' => $student
            ]);
    }
}