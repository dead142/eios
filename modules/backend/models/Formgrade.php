<?php

namespace app\modules\backend\models;

use Yii;

/**
 * This is the model class for table "formgrade".
 *
 * @property integer $id
 * @property string $type
 * @property string $name
 * @property string $date
 * @property integer $groups_discipline_id
 *
 * @property GroupsDiscipline $groupsDiscipline
 * @property Grades[] $grades
 */
class Formgrade extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'formgrade';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'name', 'groups_discipline_id'], 'required'],
            [['type'], 'string'],
            [['date'], 'safe'],
            [['groups_discipline_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['groups_discipline_id'], 'exist', 'skipOnError' => true, 'targetClass' => GroupsDiscipline::className(), 'targetAttribute' => ['groups_discipline_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('evaluation', 'ID'),
            'type' => Yii::t('evaluation', 'Type'),
            'name' => Yii::t('evaluation', 'Name'),
            'date' => Yii::t('evaluation', 'Date of control'),
            'groups_discipline_id' => Yii::t('evaluation', 'Groups Discipline ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupsDiscipline()
    {
        return $this->hasOne(GroupsDiscipline::className(), ['id' => 'groups_discipline_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrades()
    {
        return $this->hasMany(Grades::className(), ['formgrade_id' => 'id']);
    }
}
