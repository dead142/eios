<?php

namespace app\modules\backend\models;

use app\models\Profile;
use app\modules\common\models\StudentStatus;
use Yii;

/**
 * This is the model class for table "student_group".
 *
 * @property int $id
 * @property int $student_id
 * @property int $group_id
 * @property int $year_id
 *
 * @property Groups $group
 * @property Profile $student
 * @property Year $year
 * @property StudentStatus[] $studentStatuses
 */
class StudentGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_id', 'group_id', 'year_id'], 'required'],
            [['student_id', 'group_id', 'year_id'], 'integer'],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['student_id' => 'user_id']],
            [['year_id'], 'exist', 'skipOnError' => true, 'targetClass' => Year::className(), 'targetAttribute' => ['year_id' => 'id']],
            [
                [
                    'student_id',
                    'group_id',
                    'year_id',
                ],
                'required',
            ],
            [
                [
                    'student_id',
                    'group_id',
                    'year_id',
                ],
                'integer',
            ],
            [
                ['group_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Groups::className(),
                'targetAttribute' => ['group_id' => 'id'],
            ],
            [
                ['student_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Profile::className(),
                'targetAttribute' => ['student_id' => 'user_id'],
            ],
            [
                ['year_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Year::className(),
                'targetAttribute' => ['year_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('evaluation', 'ID'),
            'student_id' => Yii::t('evaluation', 'Student ID'),
            'group_id' => Yii::t('evaluation', 'Group ID'),
            'year_id' => Yii::t('evaluation', 'Year ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYear()
    {
        return $this->hasOne(Year::className(), ['id' => 'year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentStatuses()
    {
        return $this->hasMany(StudentStatus::className(), ['student_group_id' => 'id']);
    }

    public function getLatestStatusName()
    {
        $studentStatuses = $this->studentStatuses;
        $lastStatus = '';
        foreach ($studentStatuses as $studentStatus) {
            if ($studentStatus->id > $lastStatus) {
                $lastStatus = $studentStatus;
            } else {
                $lastStatus = $studentStatus;
            }
        }
        return $lastStatus->name;

    }

    /**
     * функция сохраняет связь много-ко-многим между студентом и группой
     * предвариетльно проверяет, существует ли такая связь
     * @param $student_id
     * @param $group_id
     * @return StudentGroup | false
     */
    public static function saveRelationStudentGroup($student_id, $group_id)
    {
        if (self::checkExistsRecord($student_id, $group_id)) {
            \Yii::$app->getSession()->setFlash('danger', 'Такая запись уже существует');
            return false;
        } else {
            $userGroup = new StudentGroup();
            $userGroup->student_id = $student_id;
            $userGroup->group_id = $group_id;
            $userGroup->year_id = Groups::getYearIdByGroupId($group_id);
            $userGroup->save();
            \Yii::$app->getSession()->setFlash('success', 'Запись успешно добавлена');
            return $userGroup;
        }

    }

    /**
     * функция проверяет существует ли запись в таблице
     * @param $student_id
     * @param $group_id
     * @return bool
     */
    public static function checkExistsRecord($student_id, $group_id)
    {
        if (self::find()->where(['student_id' => $student_id])->andWhere(['group_id' => $group_id])->all()) {
            return true;
        } else return false;
    }

    /**
     * @return GroupsDiscipline[]|StudentGroup[]|Year[]|array|\yii\db\ActiveRecord[]
     */
    public static function getArrayStudentsWithoutGroup()
    {
        return self::find()
            ->select([
                'profile.*',
                'CONCAT (profile.name," ",profile.surname," ",profile.middlename) as name',
            ])
            ->rightJoin('profile', 'student_group.student_id = profile.user_id')
            ->leftJoin('auth_assignment', 'profile.user_id = auth_assignment.user_id')
            //->where(['student_group.group_id' => null])
            ->andWhere([
                '<>',
                'auth_assignment.item_name',
                'admins',
            ])
            ->asArray()
            ->all();
    }


}
