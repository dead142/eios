<?php

namespace app\modules\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\backend\models\GroupsDiscipline;
use yii\debug\models\search\Profile;
use yii\helpers\VarDumper;

/**
 * GroupsDisciplineSearch represents the model behind the search form about `app\modules\backend\models\GroupsDiscipline`.
 */
class GroupsDisciplineSearch extends GroupsDiscipline
{
    public $group_name;
    public $discipline;
    public $teacher;
    public $year_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'group_id'], 'integer'],
            [['semestr_id','group_name','discipline','teacher','year_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GroupsDiscipline::find();

         $query->joinWith('group');
         $query->joinWith('group.idYear');



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes' => [
                'group_name' => [
                    'asc' => [
                        'groups.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'groups.name' => SORT_DESC,
                    ],
                    'label' => 'group_name',
                    'default' => SORT_ASC
                ],
                'discipline' => [
                    'asc' => [
                        'disciplines.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'disciplines.name' => SORT_DESC,
                    ],
                    'label' => 'discipline',
                    'default' => SORT_ASC
                ],
                'semestr_id',
                'teacher' => [
                    'asc' => [
                        'profile.surname' => SORT_ASC,
                    ],
                    'desc' => [
                        'profile.surname' => SORT_DESC,
                    ],
                    'label' => 'teacher',
                    'default' => SORT_ASC
                ],
                'semestr_id',
                'year_name' => [
                    'asc' => [
                        'year.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'year.name' => SORT_DESC,
                    ],
                    'label' => 'year_name',
                    'default' => SORT_ASC
                ],
            ],
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'group_id' => $this->group_id,
          //  'discipline_id' => $this->discipline_id,
           // 'teacher_id' => $this->teacher_id,
        ]);

        $query->andFilterWhere(['like', 'semestr_id', $this->semestr_id])
            ->andFilterWhere(['like', 'disciplines.name', $this->discipline])
            ->andFilterWhere([
                'or',
                ['like', 'profile.surname', $this->teacher],
                ['like', 'profile.name', $this->teacher],
                ['like', 'profile.middlename', $this->teacher]
                    ]
            )
            ->andFilterWhere(['like', 'groups.name', $this->group_name])
            ->andFilterWhere(['like', 'year.name', $this->year_name])
        ;


        return $dataProvider;
    }
}
