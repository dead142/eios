<?php

namespace app\modules\backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "year".
 *
 * @property integer $id
 * @property string $year_start
 * @property string $year_end
 * @property string $name
 *
 * @property Groups[] $groups
 * @property StatusStudent[] $statusStudents
 *  @property StudentGroup[] $studentGroups
 */
class Year extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'year';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year_start', 'year_end'], 'required'],
            [['year_start', 'year_end'], 'safe'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('evaluation', 'ID'),
            'year_start' => Yii::t('evaluation', 'Year Start'),
            'year_end' => Yii::t('evaluation', 'Year End'),
            'name' => Yii::t('evaluation', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Groups::className(), ['id_year' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusStudents()
    {
        return $this->hasMany(StatusStudent::className(), ['year_id' => 'id']);
    }

    public function getStudentGroups()
    {
        return $this->hasMany(StudentGroup::className(), ['year_id' => 'id']);
    }

    public static function CurrentYearID(){
        return Year::find()->select('id')->where(['and',  'year.year_start<=CURRENT_DATE', 'CURRENT_DATE<=year.year_end'])->one();
    }

    /**
     * Метод возвращает массив ['name'=>'', 'name' => '']
     * @return array
     */
    public static function get_list(){
        return ArrayHelper::map(Year::find()->all(),'name','name');
    }

}
