<?php

namespace app\modules\backend\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * GroupsSeacrh represents the model behind the search form about `app\modules\backend\models\Groups`.
 */
class GroupsSearch extends Groups
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_year', 'p_id','course'], 'integer'],
            [['name', 'created_at','active','year_start_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Groups::find();
        $query->joinWith(['idYear']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes' => [
                'id',
                'name',
                'course',
                'created_at',
                'active',
                'year_start_id' =>[
                    'asc' => [
                        'year.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'year.name' => SORT_DESC,
                    ],
                    'label' => 'year_name',
                    'default' => SORT_ASC
                ],
                'id_year' => [
                    'asc' => [
                        'year.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'year.name' => SORT_DESC,
                    ],
                    'label' => 'year_name',
                    'default' => SORT_ASC
                ],
            ],
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_year' => $this->id_year,
            'created_at' => $this->created_at,
            'course' => $this->course,
            'active' => $this->active,
            'year_start_id' => $this->year_start_id,
        ]);

        $query->andFilterWhere(['like', 'groups.name', $this->name])
             ;

        return $dataProvider;
    }
}
