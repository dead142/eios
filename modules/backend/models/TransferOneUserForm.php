<?php
/**
 * Модель описывает форму для перевода студента в другую гурппу
 * old - свойства, группа откуда переводиься студент
 * new - свойства, группа куда переводится студент
 * isSaveGrades - удаление всех предыдущих оценок ['true'=> Сохранить, 'false' => Не сохранять]
 * @property integer $old_year_id
 * @property integer $old_group_id
 * @property integer $new_year_id
 * @property integer $new_group_id
 * @property integer $student_id
 * @property bool $isSaveGrades
 */

namespace app\modules\backend\models;


use Yii;
use yii\base\Model;

class TransferOneUserForm extends Model
{
    public $old_year_id;
    public $old_group_id;
    public $new_year_id;
    public $new_group_id;
    public $student_id;
    public $isSaveGrades;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['old_year_id', 'old_group_id', 'new_year_id', 'new_group_id', 'student_id'], 'integer'],
            [['old_year_id', 'old_group_id', 'new_year_id', 'new_group_id', 'student_id'], 'required'],
            [['isSaveGrades'], 'safe'],


        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_year' => Yii::t('evaluation', 'Id Year'),

        ];
    }


}