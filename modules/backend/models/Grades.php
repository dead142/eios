<?php

namespace app\modules\backend\models;

use app\models\Profile;
use Yii;

/**
 * This is the model class for table "grades".
 *
 * @property integer $id
 * @property integer $student_id
 * @property string $grade
 * @property integer $formgrade_id
 * @property integer $discipline_id
 *
 * @property Formgrade $formgrade
 * @property GroupsDiscipline $discipline
 * @property Profile $student
 */
class Grades extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'student_id',
                    'formgrade_id',
                    'discipline_id',
                ],
                'required',
            ],
            [
                [
                    'student_id',
                    'formgrade_id',
                    'discipline_id',
                ],
                'integer',
            ],
            [
                ['grade'],
                'string',
            ],
            [
                ['formgrade_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Formgrade::className(),
                'targetAttribute' => ['formgrade_id' => 'id'],
            ],
            [
                ['discipline_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => GroupsDiscipline::className(),
                'targetAttribute' => ['discipline_id' => 'id'],
            ],
            [
                ['student_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Profile::className(),
                'targetAttribute' => ['student_id' => 'user_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('evaluation', 'ID'),
            'student_id'    => Yii::t('evaluation', 'Student ID'),
            'grade'         => Yii::t('evaluation', 'Grade'),
            'formgrade_id'  => Yii::t('evaluation', 'Formgrade ID'),
            'discipline_id' => Yii::t('evaluation', 'Discipline ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormgrade()
    {
        return $this->hasOne(Formgrade::className(), ['id' => 'formgrade_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipline()
    {
        return $this->hasOne(GroupsDiscipline::className(), ['id' => 'discipline_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'student_id']);
    }

    /**
     * Функция проверяет, существует такая запись
     * @param $discipline_id
     * @param $formgrade_id
     * @param $student_id
     * @return bool
     */
    public static function checkExists($discipline_id, $formgrade_id, $student_id)
    {
        if (Grades::find()->where([
            'discipline_id' => $discipline_id,
            'formgrade_id'  => $formgrade_id,
            'student_id'    => $student_id,
        ])
                  ->exists()) {
            return true;
        } else {
            return false;
        }
    }
}
