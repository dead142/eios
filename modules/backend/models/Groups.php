<?php
/**
 *  @version  1.0.1
 */
namespace app\modules\backend\models;

use app\modules\common\models\StudentStatus;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "groups".
 *
 * @property integer $id
 * @property string $name
 * @property integer $id_year
 * @property string $created_at
 * @property string $course
 * @property boolean $active
 * @property Year $idYear
 * @property GroupsDiscipline[] $groupsDisciplines
 * @property StudentGroup[] $studentGroups
 * @property Year[] $idStartYear
 * @property int $p_id [int(11)]  A pointer to the parent group
 */
class Groups extends \yii\db\ActiveRecord
{
    /**
     * Используется в GroupTransferController
     * для храненния информации о родителях и потомках группы
     * @var $ParentChildInfo
     */
    public $ParentChildInfo;
    public $isTransfer;

    /**
     * Группа выпустилась в  году
     */
    const GROUP_END = 0;
    /**
     *Группа обучалась в году
     */
    const GROUP_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'id_year', 'course'], 'required'],
            [['id_year', 'p_id', 'year_start_id'], 'integer'],
            [['p_id'], 'unique'],
            [['created_at','isTransfer'], 'safe'],
            [['course'], 'string'],
            [['active'], 'boolean'],
            [['name'], 'string', 'max' => 50],
            [
                ['id_year'], 'exist', 'skipOnError' => true, 'targetClass' => Year::className(),
                'targetAttribute' => ['id_year' => 'id'],
            ],
            [
                ['year_start_id'], 'exist', 'skipOnError' => true, 'targetClass' => Year::className(),
                'targetAttribute' => ['id_year' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('evaluation', 'ID'),
            'name' => Yii::t('evaluation', 'Name group'),
            'id_year' => Yii::t('evaluation', 'Id Year'),
            'created_at' => Yii::t('evaluation', 'Created At'),
            'course' => Yii::t('evaluation', 'Course'),
            'p_id' => 'Родительская группа',
            'active' => Yii::t('evaluation', 'Active'),
            'year_start_id' => Yii::t('evaluation', 'Year start'),
        ];
    }

    public function getFullNameGroup()
    {
        return $this->name . ' ( ' . $this->idYear->name . ' , курс: ' . $this->course . ' )';
    }

    /**
     * @param $id
     * @return string
     */
    public static function getFullNameById($id)
    {
        return Groups::findOne(['id' => $id])->fullNameGroup;

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdYear()
    {
        return $this->hasOne(Year::className(), ['id' => 'id_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdStartYear()
    {
        return $this->hasOne(Year::className(), ['id' => 'year_start_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupsDisciplines()
    {
        return $this->hasMany(GroupsDiscipline::className(), ['group_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentGroups()
    {
        return $this->hasMany(StudentGroup::className(), ['group_id' => 'id']);
    }

    /**
     * Получение полного наименование года и курса обучения группы
     * @return string
     */
    public function getYearGroup()
    {
        return $this->name . ' [ ' . $this->course . ' курс ] [' . $this->idYear->name . ' ]';
    }

    /**
     * TODO refactor
     * @return null|static
     */
    public function getCurrentYears()
    {
        return Year::findOne([(['and', 'year.year_start<=CURRENT_DATE', 'CURRENT_DATE<=year.year_end'])]);
    }

    /**
     * Функция возвращает год группы
     * @return string
     */
    public function getYear()
    {
        return $this->idYear->name;
    }

    /**
     * Function return year id of group
     * @param $id
     * @return int
     *
     */
    public static function getYearIdByGroupId($id)
    {
        return (int)ArrayHelper::getValue(Groups::find()->select('id_year')->where(['id' => $id])->asArray()->one(), 'id_year');
    }

    /**
     * @param $id
     * @return Groups|array|null|\yii\db\ActiveRecord
     */
    public static function getNameByID($id)
    {
        return self::find()->select(['name'])->where(['id' => $id])->asArray()->one();
    }

    /**
     * Функция возвращает количество студентов в группе
     * @param $groupId
     * @return int|string
     */
    public static function getCountStudentsGroup($groupId)
    {
        return StudentGroup::find()->where(['group_id' => $groupId])->count();
    }

    /**
     * @param $groupId
     * @return int|string
     */
    public static function getCountDisciplinesGroup($groupId)
    {
        return GroupsDiscipline::find()->where(['group_id' => $groupId])->count();
    }

    /**
     * Функция находит всех родителей и потомков группы
     * @param $idGroup
     * @return array|null
     */
    public static function GetListGroups($idGroup)
    {
        $model = Groups::find()->where(['id' => $idGroup])->one();
        $array = Groups::ListGoDown($model->p_id);// добаляем родителей
        array_push($array, $model);                // добавляем текущий элемент
        $array = Groups::ListGoUp($model->id, $array); // добавляем потомков
        return $array;
    }

    /**
     * Ищем предшествующие группы
     * @param $p_id
     * @param null $array
     * @return array|null
     */
    public static function ListGoDown($p_id, $array = NULL)
    {
        if ($array == NULL) {
            $array = [];
        }
        if ($p_id != 0) {
            $model = Groups::find()->where(['id' => $p_id])->one();

            array_push($array, $model);
            Groups::ListGoDown($model->p_id, $array);
        }
        return $array;
    }

    /**
     * Ищем последующие группы
     * @param $id
     * @param $array
     * @return mixed
     */
    public static function ListGoUp($id, $array)
    {

        if ($id != NULL) {
            $model = Groups::find()->where(['p_id' => $id])->one();
            array_push($array, $model);
            Groups::ListGoUp($model->id, $array);

        }
        return $array;
    }

    /**
     * Устанавливается оследний год обучения группы
     * @param $id
     * @return array|null|static
     */
    public static function disableGroup($id)
    {
        $model = Groups::findOne(['id' => $id]);
        $model->active = Groups::GROUP_END;
        if ($model->save()) {
            return $model;
        } else {
            return $model->errors;
        }
    }

    /**
     * Метод ищет всех студентов в выбраннной группе
     * И переводит их
     * @param $fromGroupId
     * @param $toGroupId
     * @param $toYearId
     * @param null $semestr
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function TransferStudentToGroup($fromGroupId, $toGroupId, $toYearId, $semestr = null)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $flagTransaction = true;
        // Работа со старыми статусами
        $flagTransaction = $flagTransaction && StudentStatus::updateStudentStatusInGroup($fromGroupId, $semestr);


        // Работа со связями студентов и группы
        //TODO ошибка при сохранении
        $oldStudentsGroup = StudentGroup::findAll(['group_id' => $fromGroupId]);
        foreach ($oldStudentsGroup as $student) {
            $studentG = new StudentGroup();
            $studentG->student_id = $student->student_id;
            $studentG->group_id = $toGroupId;
            $studentG->year_id = $toYearId;
            if ($studentG->save()) {
                $flagTransaction = $flagTransaction && true;
                $model = StudentStatus::createStudentStatus(
                    StudentStatus::STATUS_ACTIVE,
                    'Создано автоматически при переводы из группы',
                    $semestr,
                    date("Y-m-d H:i:s"),
                    $studentG->id
                );
                ($model instanceof StudentStatus) ? $flagTransaction = $flagTransaction && true : $flagTransaction = $flagTransaction && false;
            } else {
                \Yii::$app->session->setFlash('error', 'Ошибка при переносе студентов (backend/models/Groups/ TransferStudentToGroup');
                $flagTransaction = $flagTransaction && false;
            }
        }
        if ($flagTransaction == true) {
            $transaction->commit();
            return true;
        } else {
            $transaction->rollback();
            return false;
        }


    }
}
