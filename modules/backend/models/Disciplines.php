<?php

namespace app\modules\backend\models;

use Yii;

/**
 * This is the model class for table "disciplines".
 *
 * @property integer $id
 * @property string $name
 * @property string $create_at
 * @property integer $create_by
 *
 * @property GroupsDiscipline[] $groupsDisciplines
 */
class Disciplines extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'disciplines';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_at'], 'safe'],
            [['create_by'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('evaluation', 'ID'),
            'name' => Yii::t('evaluation', 'Name discipline'),
            'create_at' => Yii::t('evaluation', 'Create At'),
            'create_by' => Yii::t('evaluation', 'Create By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupsDisciplines()
    {
        return $this->hasMany(GroupsDiscipline::className(), ['discipline_id' => 'id']);
    }
    public static function getDiscipline($id){

        return GroupsDiscipline::find()->with('discipline')->where(['groups_discipline.id'=>$id])->one();
    }

}
