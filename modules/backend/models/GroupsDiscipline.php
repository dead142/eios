<?php

namespace app\modules\backend\models;

use app\models\Profile;
use app\modules\backend\models\Disciplines;
use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "groups_discipline".
 *
 * @property integer $id
 * @property integer $group_id
 * @property string $semestr_id
 * @property string $discipline_teacher_id
 *
 * @property Formgrade[] $formgrades
 * @property Grades[] $grades
 * @property Groups $group
 * @property DisciplinesTeachers $disciplineTeacher
 */
class GroupsDiscipline extends \yii\db\ActiveRecord
{



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups_discipline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'semestr_id'], 'required'],
            [['group_id', 'discipline_teacher_id'], 'integer'],
            [['semestr_id'], 'string'],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['discipline_teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => DisciplinesTeachers::className(), 'targetAttribute' => ['discipline_teacher_id' => 'id']],


        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('evaluation', 'ID'),
            'group_id' => Yii::t('evaluation', 'Name group'),
            'semestr_id' => Yii::t('evaluation', 'Semestr'),
            'discipline_teacher_id' => Yii::t('backend', 'Discipline Teacher ID'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplineTeacher()
    {
        return $this->hasOne(DisciplinesTeachers::className(), ['id' => 'discipline_teacher_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormgrades()
    {
        return $this->hasMany(Formgrade::className(), ['groups_discipline_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrades()
    {
        return $this->hasMany(Grades::className(), ['discipline_id' => 'id']);
    }

    /**
     * @return \app\modules\backend\models\Disciplines
     */
    public function getDiscipline()
    {
        return $this->disciplineTeacher->discipline;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    /**
     * @return Profile
     */
    public function getTeacher()
    {
        return $this->disciplineTeacher->teacher;
    }

    public function getTeacherName()
    {
        return $this->disciplineTeacher->name;
    }



    /**
     * функция ищет все связи группы и дициалин, а так жевсе формы контроля этой группы
     * @param $group_id  int
     * @return Groups[]|GroupsDiscipline[]|StudentGroup[]|Year[]|array|\yii\db\ActiveRecord[]
     */
    public static function getDisciplinesWithFormgradesByGroupId($group_id){
        return self::find()
                   ->joinWith('formgrades')
                   ->where(['group_id' => $group_id])
                   ->all();

    }

}
