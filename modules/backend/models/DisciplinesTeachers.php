<?php

namespace app\modules\backend\models;

use app\models\Profile;
use Yii;

/**
 * This is the model class for table "disciplines_teachers".
 *
 * @property int $id
 * @property int $teacher_id
 * @property int $discipline_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_bt
 *
 * @property Disciplines $discipline
 * @property Profile $teacher
 */
class DisciplinesTeachers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'disciplines_teachers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['teacher_id', 'discipline_id'], 'required'],
            [['teacher_id', 'discipline_id', 'created_bt'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['discipline_id'], 'exist', 'skipOnError' => true, 'targetClass' => Disciplines::className(), 'targetAttribute' => ['discipline_id' => 'id']],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['teacher_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'teacher_id' => Yii::t('backend', 'Teacher ID'),
            'discipline_id' => Yii::t('backend', 'Discipline ID'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_bt' => Yii::t('backend', 'Created Bt'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipline()
    {
        return $this->hasOne(Disciplines::className(), ['id' => 'discipline_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'teacher_id']);
    }

    public function getName(){
        return $this->discipline->name.' ('.$this->teacher->fullName.' )';
    }
}
