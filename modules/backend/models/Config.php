<?php

namespace app\modules\backend\models;

use Yii;

/**
 * This is the model class for table "config".
 *
 * @property integer $id
 * @property string $option_name
 * @property integer $option_value
 */
class Config extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_name', 'option_value'], 'required'],
            [['option_value'], 'integer'],
            [['option_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/main', 'ID'),
            'option_name' => Yii::t('backend/main', 'Option Name'),
            'option_value' => Yii::t('backend/main', 'Option Value'),
        ];
    }
}
