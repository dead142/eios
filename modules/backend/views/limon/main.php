<?php
use yii\helpers\Html;

$context = $this->context;
?>

<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
<div class="row fileupload-buttonbar" style="padding: 50px;">

    <!--            Photo display here           -->
    <div class="col-md-4">
        <?=
        Html::img(Yii::getAlias('@web') . '/' . $context->model->photo_url, [
            'width' => '150px',
            'height' => '150px',
            'class' => 'center-block img-rounded',
            "id" => "photo"
        ]) ?>
    </div>

    <div class="col-md-8">
        <!-- The fileinput-button span is used to style the file input field as button -->
        <div class="text-center">
        <span class="btn btn-success fileinput-button">
            <i class="glyphicon glyphicon-plus"></i>
            <span><?= Yii::t('backend/user', 'Upload photo') ?></span>

            <?php
            $name = $context->model instanceof \yii\base\Model && $context->attribute !== null ? Html::getInputName($context->model, $context->attribute) : $context->name;
            $value = $context->model instanceof \yii\base\Model && $context->attribute !== null ? Html::getAttributeValue($context->model, $context->attribute) : $context->value;
            echo Html::hiddenInput($name, $value) . Html::fileInput($name, $value, $context->options);
            ?>

        </span>
        </div>

        <!-- The global progress bar -->
        <div id="progress" class="progress" style="margin-top: 20px;">
            <div class="progress-bar progress-bar-success"></div>
        </div>
        <!-- The container for the uploaded files -->
        <div id="files" class="files text-center"></div>
    </div>
</div>


        