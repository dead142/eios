<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use limion\jqueryfileupload\JQueryFileUpload;
/**
 * @var yii\web\View $this
 * @var dektrium\user\models\User $user
 * @var dektrium\user\models\Profile $profile
 */
?>

<?php $this->beginContent('@dektrium/user/views/admin/update.php', ['user' => $user]) ?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-9',
        ],
    ],
]); ?>
<!------------------------------------------user photo --------------------------------------------------------------------->


<?=
# Guide https://yiigist.com/package/limion/yii2-jquery-fileupload-widget#!?tab=readme

JQueryFileUpload::widget([
    'model' => $profile,
    'attribute' => 'photoImg',
    'url' =>  ['/backend/ajax/image-upload'], // your route for saving images, ['/backend/ajax/image-upload', 'someparam' => 'somevalue'],
    'appearance' => 'plus', // available values: 'ui','plus' or 'basic'
    'clientOptions' => [
        'maxFileSize' => 10000000 ,
        'acceptFileTypes' => new yii\web\JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
        'autoUpload' => true
    ],
    'formId' => $profile->user_id,
    'options' => [
        'accept' => 'image/*',
        'multiple' => false,
        'class' => 'text-center',
    ],
    'clientEvents' => [

        'done' => 'function (e, data) {
                                var obj = JSON.parse(data.result);
                                $( "#photo" ).attr("src","/web/"+obj.thumbnailUrl);
                                }',

        'progressall' => "function (e, data) {
                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                $('#progress .progress-bar').css(
                                'width',
                                progress + '%'
                                );
                                }"
    ]
]); ?>

<?= $form->field($profile, 'surname') ?>
<?= $form->field($profile, 'name') ?>
<?= $form->field($profile, 'middlename') ?>
<?= $form->field($profile, 'public_email') ?>
<?= $form->field($profile, 'website') ?>
<?= $form->field($profile, 'location') ?>
<?= $form->field($profile, 'gravatar_email') ?>
<?= $form->field($profile, 'bio')->textarea() ?>

<div class="form-group">
    <div class="col-lg-offset-3 col-lg-9">
        <?= Html::submitButton(Yii::t('user', 'Update'), ['class' => 'btn btn-block btn-success']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->endContent() ?>
