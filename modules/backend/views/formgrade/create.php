<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\Formgrade */

$this->title = Yii::t('evaluation', 'Create Formgrade');
$this->params['breadcrumbs'][] = ['label' => Yii::t('evaluation', 'Formgrades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="formgrade-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
