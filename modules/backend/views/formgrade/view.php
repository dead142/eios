<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\Formgrade */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('evaluation', 'Formgrades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="formgrade-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('evaluation', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('evaluation', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('evaluation', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //'type',
            'name',
            'date:date',
            'groupsDiscipline.group.name',
            'groupsDiscipline.discipline.name'
        ],
    ]) ?>

</div>
