<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\Formgrade */
/* @var $form yii\widgets\ActiveForm */

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
?>

<div class="formgrade-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList([ 'current control' => Yii::t('evaluation', 'current control'), 'intermediate control' => Yii::t('evaluation', 'intermediate control'), ], ['prompt' => '']) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::classname(), [
        'language' => 'ru',
        'options' => ['class' => 'form-control'],
         'dateFormat' => 'yyyy-MM-dd',
    ]) ?>
    <?= $form->field($model, 'groups_discipline_id')->widget(Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\modules\backend\models\GroupsDiscipline::find()->all(),'id','teacherName'),
        'options' => ['placeholder' => Yii::t('backend','Select teacher and discipline')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('evaluation', 'Create') : Yii::t('evaluation', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
