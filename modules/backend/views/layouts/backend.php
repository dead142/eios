<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '<em style="margin-left: 10px;"> EIEE__<span class="glyphicon glyphicon-pencil"></span></em> <small>Администрирование  </small>',
        'brandUrl' => Url::to(['/backend']),
        'options' => [
            'class' => 'navbar-inverse',
          //  'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],

        'items' => [
            [
                    'label' => Yii::t('backend/main', 'File manager'),
                'url' => Url::toRoute('/elfinder/manager'),
                'linkOptions' => ['target' => '_blank'],

            ],



            [
                'label' => Yii::t('frontend/main', 'Portfolio'),
                'items' => [
                //   ['label' => Yii::t('frontend/main', 'All activity'), 'url' => Url::toRoute(['/frontend/default/all','id'=>empty(Yii::$app->user->identity->id) ? NULL:Yii::$app->user->identity->id])],
                // '<li class="divider"></li>',
                 '<li class="dropdown-header"> </li>',
                    ['label' => Yii::t('backend/main', 'Area'), 'url' => Url::toRoute('/backend/area/index')],
                    ['label' => Yii::t('backend/main', 'Document'), 'url' => Url::toRoute('/backend/document/index')],
                    ['label' => Yii::t('backend/main', 'Organization'), 'url' => Url::toRoute('/backend/organization/index')],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">'.Yii::t('backend/main', 'Social, cultural, sports activities').'</li>',
                    ['label' => Yii::t('backend/main', 'Status'), 'url' => Url::toRoute('/backend/status/index')],
                    ['label' => Yii::t('backend/main', 'Type Events'), 'url' => Url::toRoute('/backend/event/index')],
                    ['label' => Yii::t('backend/main', 'Levels'), 'url' => Url::toRoute('/backend/level/index')],
                    ['label' => Yii::t('backend/main', 'Type Participations'), 'url' => Url::toRoute('/backend/participation/index')],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">'.Yii::t('backend/main', 'Conferences, schools, workshops, seminars').'</li>',
                    ['label' => Yii::t('backend/main', 'Kind Involvements'), 'url' => Url::toRoute('/backend/kindinvolvement/index')],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">'.Yii::t('backend/main', 'Monographs, collections , articles').'</li>',
                    ['label' => Yii::t('backend/main', 'Type Editions'), 'url' => Url::toRoute('/backend/typeedition/index')],
                    ['label' => Yii::t('backend/main', 'Langs'), 'url' => Url::toRoute('/backend/lang/index')],
                    ['label' => Yii::t('backend/main', 'Countries'), 'url' => Url::toRoute('/backend/country/index')],
                    ['label' => Yii::t('backend/main', 'Indexes'), 'url' => Url::toRoute('/backend/indexes/index')],
                     ],
            ],
            ['label' => Yii::t('materials', 'Material'), 'url' => Url::toRoute('/backend/materials/index')],
            ['label' => Yii::t('backend/main', 'Config'), 'url' => Url::toRoute('/backend/config/index')],

            Yii::$app->user->isGuest ?
                ['label' => 'Sign in', 'url' => ['/user/security/login']] :
                ['label' => 'Sign out (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/user/security/logout'],
                    'linkOptions' => ['data-method' => 'post']],
            ['label' => 'Register', 'url' => ['/user/registration/register'], 'visible' => Yii::$app->user->isGuest]
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= \app\widgets\Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">

    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
