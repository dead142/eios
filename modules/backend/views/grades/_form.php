<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\modules\backend\models\Formgrade;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\Grades */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grades-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'student_id')->textInput() ?>

    <?= $form->field($model, 'grade')->dropDownList([ 2 => '2', 3 => '3', 4 => '4', 5 => '5', 'Çà÷åò' => 'Çà÷åò', 'Íåçà÷åò' => 'Íåçà÷åò', 'Îòëè÷íî' => 'Îòëè÷íî', 'Õîðîøî' => 'Õîðîøî', 'Óäîâëåòâîðèòåëüíî' => 'Óäîâëåòâîðèòåëüíî', 'Íåóäîâëåòâîðèòåëüíî' => 'Íåóäîâëåòâîðèòåëüíî', 'Íåÿâêà' => 'Íåÿâêà', 'Áîëüíè÷íûé' => 'Áîëüíè÷íûé', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'formgrade_id')->dropDownList(ArrayHelper::map(Formgrade::find()->all(),'id','name')) ?>

    <?= $form->field($model, 'discipline_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('evaluation', 'Create') : Yii::t('evaluation', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
