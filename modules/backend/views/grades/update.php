<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\Grades */

$this->title = Yii::t('evaluation', 'Update {modelClass}: ', [
    'modelClass' => 'Grades',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('evaluation', 'Grades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('evaluation', 'Update');
?>
<div class="grades-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
