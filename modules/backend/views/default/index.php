<?php

use yii\helpers\Url;

/* @var $this yii\web\View */


$this->params['breadcrumbs'][] = $this->title;
?>
<? if (  Yii::$app->user->can('admins')) : ?>
<div class="index">

    <h2> Действия в начале года</h2>
    <!--==================================== Первая секция =============================================-->
    <div class="row">

        <div class="col-md-3">
            <a href="<?= Url::toRoute('/backend/year/create') ?>">
                <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                <?= Yii::t('backend/main', 'Add new year') ?>
            </a>

        </div>

        <div class="col-md-1">
            <span style="font-size: 50px; color: #337ab7;" class="glyphicon glyphicon-triangle-right"
                  aria-hidden="true"></span>
        </div>

        <div class="col-md-3">
            <a href="<?= Url::toRoute('/backend/group-transfer/index') ?>">
            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Перевод групп на
            следующий год обучения
            </a>
        </div>

        <div class="col-md-1">
            <span style="font-size: 50px; color: #337ab7;" class="glyphicon glyphicon-triangle-right"
                  aria-hidden="true"></span>
        </div>

        <div class="col-md-3">
            <a href="<?= Url::toRoute('/backend/groups/create') ?>">
                <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                <?= Yii::t('backend/main', 'Add new groups current year') ?>
            </a> <br>

        </div>
        <div class="col-md-1">
            <span style="font-size: 50px; color: #337ab7;" class="glyphicon glyphicon-triangle-bottom"
                  aria-hidden="true"></span>
        </div>
    </div>
    <hr>

    <!--==================================== Вторая секция =============================================-->
    <div class="row">
        <div class="col-md-3">
        </div>

        <div class="col-md-1">
        </div>

        <div class="col-md-3">
            <a href="<?= Url::toRoute('/backend/groups-discipline/index') ?>"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Назначить дисциплины
                группам</a> <br>
        </div>

        <div class="col-md-1">
            <span style="font-size: 50px; color: #337ab7;" class="glyphicon glyphicon-triangle-left"
                  aria-hidden="true"></span>
        </div>

        <div class="col-md-3">
            Добавить поступивших абитуриентов в
            группу<br>
            <a href="<?= Url::toRoute('/backend/registration/index') ?>">
                <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                <?= Yii::t('backend/main', 'Generate login/password for groups') ?>
            </a>
            <br>
            <a href="<?= Url::toRoute('/backend/registration/import-user') ?>">
                <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                <?= Yii::t('backend/main', 'Import users from file xls, xlsx') ?>
            </a>
        </div>

        <div class="col-md-1">
            <span style="font-size: 50px; color: #337ab7;" class="glyphicon glyphicon-triangle-left"
                  aria-hidden="true"></span>
        </div>
    </div>

</div>
<? endif; ?>
<!--==================================== третья секция =============================================-->
<h2> Действия в течении года</h2>
<div class="row">
    <? if (  Yii::$app->user->can('admins')) : ?>
    <div class="col-md-3">
        <h3>Пользователи</h3>
        <a href="<?= Url::toRoute('/backend/student-status/index') ?>"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Отчислить/ восстановить
            студента</a> <br>

        <a href="<?= Url::toRoute('/user/admin/create') ?>">
            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            <?= Yii::t('backend/main', 'Add teachers') ?>
        </a><br>

        <a href="<?= Url::toRoute('/backend/registration/index') ?>">
            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            <?= Yii::t('backend/main', 'Generate login/password for groups') ?>
        </a>
        <br>
        <a href="<?= Url::toRoute('/backend/registration/import-user') ?>">
            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            <?= Yii::t('backend/main', 'Import users from file xls, xlsx') ?>
        </a>    <br>
        <a href="<?= Url::toRoute('/user/admin/index') ?>">
            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
           Управление ролями и пользователями
        </a>  <br>
        <a href="<?= Url::toRoute('/backend/student-to-group/student-assign-group') ?>">
            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            Привязать пользователя к группе
        </a><sup style="color: red;">new</sup>

    </div>
    <? endif; ?>
    <div class="col-md-3">
        <h3>Дисциплины</h3>
        <a href="<?= Url::toRoute('/backend/groups/index') ?>"> <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Выставить
            оценки по группам </a> <br>
        <a href="<?= Url::toRoute('/backend/disciplines/disciplines-by-teacher') ?>"> <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Выставить
            оценки (для преподавателей)</a> <br>
        <? if (  Yii::$app->user->can('admins')) : ?>
        <a href="<?= Url::toRoute('/backend/disciplines/index') ?>"> <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Добавить
            Дисциплины</a> <br>
        <? endif; ?>
      <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Посмотреть список
            дисциплин <br>


    </div>
    <? if (  Yii::$app->user->can('admins')) : ?>
    <div class="col-md-3">
        <h3>Группы</h3>
        <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Посмотреть дисциплины
            групп  <br>
        <a href="<?= Url::toRoute('/backend/groups/index') ?>">
            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            <?= Yii::t('backend/main', 'See all groups') ?>
        </a> <br>
        <a href="<?= Url::toRoute('/backend/groups-discipline/index') ?>"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Назначить дисциплины
            группам</a> <br>
    </div>

    <div class="col-md-3">
        <h3>Другое</h3>
        <a href="<?= Url::toRoute('/backend/year/index') ?>">
            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
            <?= Yii::t('backend/main', 'See all years') ?></a>
          <br>
        <a href="<?= Url::toRoute('/backend/questions/index') ?>">
            <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
          Модерация раздела вопросы и ответы</a>

    </div>
        <? endif; ?>
</div>
