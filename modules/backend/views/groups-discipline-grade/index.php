<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\backend\models\GroupsDisciplineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->params['breadcrumbs'][] = Yii::t('evaluation', 'Current study');
$this->params['breadcrumbs'][] =  ['label' => Yii::t('evaluation', 'Disciplines of groups').' '.$group->name, 'url' => ['/backend/disciplines/discipline-by-group', 'id' => $group->id]];
$this->params['breadcrumbs'][] =


$this->params['breadcrumbs'][] = $this->title;
?>

<div class="groups-discipline-index" xmlns="http://www.w3.org/1999/html">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('evaluation', 'Create control'), ['/backend/formgrade/create','groupDiscipline_id'=>$groupDiscipline_id,'group_id'=>$group->id], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
        $result = $dataProvider->query->all();
        $array=[];
   $control=array();
    foreach ($result as $item){
                $array[$item->student->surname.' '.$item->student->name.' '.$item->student->middlename][$item->formgrade_id] =
                    [
                        'formgrade_id'=> $item->formgrade_id,
                         'grade' => $item->grade,
                         'id_grade' => $item->id

                        ];
                $control[$item->formgrade_id]  = ['name'=>$item->formgrade->name];
                 }
    ?>
     <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => ['/backend/grades/save-grades'],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton( Yii::t('evaluation', 'Save grades'), ['class' =>'btn btn-primary']) ?>
    </div>


    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th ><?= Yii::t('evaluation', 'FIO') ?></th>
                <?php foreach ($control as $key1 => $k): ?>
                    <th class="vertical" >


                        <div style="writing-mode: vertical-lr;">
                            <?= $k['name'] ?>
                            <?= Html::a('<span class="glyphicon glyphicon-search"></span>',Url::to(['/backend/formgrade/view','id'=>$key1])) ?>
                        </div>

                    </th>
                    <?php endforeach; ?>
            </tr>
    </thead>
        <? if ($control == NULL) : ?>
            <?php
           // \yii\helpers\VarDumper::dump($usersGroup,[10],true);
                    foreach ($usersGroup as $value){
                        echo '   <tr><td>';
                            echo $value->surname.' ' , $value->name.' ', $value->middlename;
                        echo '  </td>  </tr>';
                    }
            ?>
        <?php endif; ?>

            <?php

            foreach ($array as $key => $value): ?>
            <tr>
                <td>
                    <?   echo   $key;?>
                </td>
                    <?php foreach ($array[$key] as  $val): ?>
                <td>
                    <?= Html::dropDownList('grade_id['.$val['id_grade'].']', null,
                        [ '0' =>'--', '2' => '2', '3' => '3', '4' => '4', 5 => '5', 'pass' => 'Зачет', 'fail' => 'Незачет', 'Excellent' => 'Отлично', 'Good' => 'Хорошо', 'Satisfactorily' => 'Удовлетворительно', 'Unsatisfactorily' => 'Неудовлетворительно', 'Failure to appear' => 'Неявка', 'Hospital' => 'Больничный', ],
                        ['options' => [$val['grade'] => ['Selected'=>'selected']]]) ?>
                </td>
                    <?php endforeach; ?>
            <tr>
             <?php endforeach; ?>

    </table>

    <? Url::remember();?>
    <?php ActiveForm::end(); ?>
</div>