<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use \yii\helpers\ArrayHelper;
use \app\modules\backend\models\Groups;
use app\modules\backend\models\Year;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\Year */
/* @var $form yii\widgets\ActiveForm */
?>
<?
$url = Yii::$app->request->baseUrl. '/backend/student-to-group/add-to-group';
$cfr = Yii::$app->request->getCsrfToken()
?>
<? $script = <<< JS
    $("span").click(function(){
 var id = $(this).attr("id");
 var selectValue = $("[param = "+id+"]").val();
console.log(selectValue);

      $.ajax({
             url: "$url",
            type: "post",
            data: {
                id: id,
                groupSelected: selectValue,

                 _csrf : "$cfr"


            },
            success: function (data) {

                  console.log(data);
            }
        });
          });
JS;
 $this->registerJs($script);
  ?>

<div class="year-form">


 <? // Html::DropDownList('ss',null, \yii\helpers\ArrayHelper::map($groups, 'id','name')) ?>
<?php \yii\widgets\Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        //  'ID',
        'name',

        [
            'label' => Yii::t('groups', 'Surname'),
            'attribute' => 'surname'
        ],

        [  'label' => Yii::t('groups', 'Middlename'),
            'attribute' => 'middlename',],
       /* [
            'label'=>Yii::t('groups', 'year start'),
            'attribute' => 'year',
            'format' => 'raw',
            'filter'=>ArrayHelper::map(Year::find()->all(),'year_start','year_start'),
            'value' =>'group.idYear.year_start'

        ],*/
        [
            'label'=>Yii::t('groups', 'Group'),
            'attribute' => 'group',
            'format' => 'raw',
            'filter'=> ArrayHelper::map(Groups::find()->all(),'name','name'),
           // 'value' => 'studentGroups.StudentGroup'
            'value' => function($data){
             //  return \yii\helpers\VarDumper::dump($data->user_id,[10],true);
                if (\app\modules\backend\models\StudentGroup::StudentGroup($data->user_id)){
                    return  \app\modules\backend\models\StudentGroup::StudentGroup($data->user_id)->group->name;
                }
                    }

        ],

        [
            'attribute' => 'Select group',
            'format' => 'raw',

            'value' => function ($data) {
                return $this->render('setGroup.php', ['model' => $data]);

            },
        ],
        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
<?php \yii\widgets\Pjax::end(); ?>