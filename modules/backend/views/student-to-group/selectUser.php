<?php

use kartik\select2\Select2;
use yii\helpers\Html;

/**
 * @var $studentWithGroup
 *
 */


//$array[$i]=> [
//              'user_id' => '32'
//            'name' => 'Пользователь1 Незнакомец  '
//            'public_email' => ''
//            'gravatar_email' => ''
//            'gravatar_id' => 'd41d8cd98f00b204e9800998ecf8427e'
//            'location' => ''
//            'website' => ''
//            'bio' => ''
//            'timezone' => null
//            'surname' => 'Незнакомец '
//            'middlename' => ''
//            'photo_url' => null
//        ]
$provider = new \yii\data\ArrayDataProvider([
    'allModels' => $studentWithGroup,
    'pagination' => [
        'pageSize' => 10,
    ],
    'sort' => [
        'attributes' => [
            'id',
            'name',
        ],
    ],
]);


$script = <<< JS
    $(document).ready(function(){
      
            $('[data-toggle="tooltip"]').tooltip({
               html: true
            });
        });
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_READY);
?>


    <h1><?= Yii::t('backend', 'Set user to group') ?></h1>

<?php
$groups = \yii\helpers\ArrayHelper::map(\app\modules\backend\models\Groups::find()->all(), 'id', 'fullNameGroup');
//\yii\helpers\VarDumper::dump( \app\modules\backend\models\Groups::find()->all(), 10, true);
echo \yii\grid\GridView::widget([
    'dataProvider' => $provider,


    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],


        [
            'attribute' => 'name',
            'value' => function ($model) {
                $rows = '<tr>
                            <td>Группа</td>
                            <td>Курс</td>
                            <td>Год</td>
                        </tr>';
                foreach ($model->groups as $group) {
                    $rows .= '<tr>
                                <td>' . $group->name . '</td>
                                <td>' . $group->course . '</td>
                                <td>' . $group->idYear->name . '</td>
                            </tr>';
                }
                $table = '<table border="1" >' . $rows . '</table>';
                return Html::a($model->fullName, null, ['data-toggle' => 'tooltip', 'title' => $table]);
            },
            'format' => 'raw',
            'label' => Yii::t('backend', 'FIO user'),
        ],

        [
            'headerOptions' => ['style' => 'width:50%'],
            'label' => Yii::t('backend', 'Group'),
            'format' => 'raw',
            'value' => function ($array) use ($groups) {
                return Select2::widget([
                    'name' => 'group',
                    'value' => '',

                    'data' => $groups,
                    'pluginEvents' => [
                        "change" => 'function() { 
                         var data_id = $(this).val();
                        //alert(data_id);
    }',
                    ],
                    'options' => [
                        'multiple' => false,
                        'placeholder' => Yii::t('backend', 'Select a group'),
                        'id' => 'group' . $array['user_id'],

                    ],
                    'addon' => [
                        'append' => [
                            'content' => Html::a(Yii::t('backend', 'Connect'), null, [
                                'class' => 'btn btn-danger',
                                'title' => 'Mark on map',
                                'data-toggle' => 'tooltip',

                                'onclick' => " 
     $.ajax({
    type     :'POST',
    cache    : false,
     data: {
                 user_id: " . $array['user_id'] . " , 
                 group_id: $('#group" . $array['user_id'] . "').val(), 
             },
        url  : '" . \Yii::$app->request->baseUrl . '/backend/student-to-group/student-assign-group' . "',
    success  : function(response) {
        console.log(response);
        console.log($('#id_of_your_select2_widget').val());
    }
    });return false;",

                            ]),
                            'asButton' => true,
                        ],
                    ],
                ]);
                //   return $array['user_id'];

            },
        ],


    ],
]);

