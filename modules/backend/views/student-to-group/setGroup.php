<?php
use yii\helpers\Html;
use yii\bootstrap\Button;

?>

<?= Html::dropDownList('group','',\yii\helpers\ArrayHelper::map(\app\modules\backend\models\Groups::find()->all(),'id','name'),['prompt' => ' -- Select Group --', 'param'=>$model->user_id]) ?>

<?= \yii\helpers\Html::a( '<span id="'.$model->user_id.'" class="glyphicon glyphicon-floppy-saved"></span>','' ,
    ['title' => Yii::t('yii', 'Save'), 'data-pjax' => '0']); ?>