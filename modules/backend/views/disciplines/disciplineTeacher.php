 <?
 use yii\widgets\Pjax;
 use yii\grid\GridView;
 use yii\helpers\Html;

?>
<?php Pjax::begin(); ?>
 <?= GridView::widget([
    'dataProvider' => $dataProvider,
     'columns' => [
         ['class' => 'yii\grid\SerialColumn'],

       //  'id',
         [
             'label'=>Yii::t('evaluation', 'year_name'),
         'value'=>'group.YearGroup',
             ],
        // 'group.name',
         'semestr_id',
         [
             'label'=>Yii::t('evaluation', 'Discipline'),
             'attribute' => 'discipline',
             'format' => 'raw',
             'value'=> function ($data) {
                 return Html::a(Html::encode($data->discipline->name),['/backend/grades/grades-disciplines/',
                     'groupDiscipline_id' =>$data->id,
                     'group_id'=>$data->group_id
                 ]);
             }

         ],

     //['class' => 'yii\grid\ActionColumn' ,'template'=>'',],
        ],
    ]);
 ?>
 <?php Pjax::end(); ?>