<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\Disciplines */

$this->title = Yii::t('evaluation', 'Update {modelClass}: ', [
    'modelClass' => 'Disciplines',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('evaluation', 'Disciplines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('evaluation', 'Update');
?>
<div class="disciplines-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
