<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\backend\models\DisciplinesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('evaluation', 'Disciplines of groups') . ' ' . $groupName;
$this->params['breadcrumbs'][] = Yii::t('evaluation', 'Current study');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disciplines-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('evaluation', 'Add discipline to group'), ['/backend/groups-discipline/create', 'group_id' => $groupId], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => Yii::t('evaluation', 'Discipline'),
                'attribute' => 'discipline',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a(Html::encode($data->discipline->name), ['/backend/grades/grades-disciplines/',
                        'groupDiscipline_id' => $data->id,
                        'group_id' => $data->group_id
                    ]);
                }

            ],
            [
                'label' => Yii::t('evaluation', 'Teacher'),
                'attribute' => 'teacher',
                'value' => 'teacher.FullName'
            ],
            [
                'label' => Yii::t('evaluation', 'Semestr'),
                'attribute' => 'semestr_id',
                'value' => 'semestr_id'
            ],


            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {view}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-edit"></span>',['/backend/disciplines/update-discipline-group','id' =>$model->id], [
                            'title' => 'Обновить',
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-remove"></span>',['/backend/disciplines/delete-discipline-group','id' =>$model->id], [
                            'title' => 'Удалить',
                            'data-confirm' => 'Вы действительно хотите удалить запись?',
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
