<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\GroupsDiscipline */
/* @var $disciplineModel app\modules\backend\models\Disciplines */
/* @var $form yii\widgets\ActiveForm */


$this->params['breadcrumbs'][] = ['label' => 'Группы', 'url' => ['/backend/groups/index']];
$this->params['breadcrumbs'][] = ['label' => 'Дисциплины группы: '.$model->group->name, 'url' => ['/backend/disciplines/discipline-by-group','id'=>$model->group_id]];
$this->params['breadcrumbs'][] = ['label' => 'Дисциплина '.$disciplineModel->name];
?>

<div class="disciplines-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($disciplineModel, 'name')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'semestr_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group_id')->textInput(['maxlength' => true, 'disabled' => true, 'value' => $model->group->name]) ?>



    <?= $form->field($model, 'teacher_id')->widget(\kartik\select2\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Profile::findAllByRole('teacher'), 'user_id', 'fullName'),
        'options' => ['placeholder' => 'Выберите преподавателя'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('evaluation', 'Create') : Yii::t('evaluation', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>