<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\organization */

$this->title = Yii::t('portfolio', 'Create Organization');
$this->params['breadcrumbs'][] = ['label' => Yii::t('portfolio', 'Organizations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organization-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
