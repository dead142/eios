<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\frontend\models\organization */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="organization-form">

    <?php $form = ActiveForm::begin(['id'=>$model->formName()]); ?>



    <?= $form->field($model, 'NAME')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('Bbuttons', 'Create') : Yii::t('Bbuttons', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS

$('form#{$model->formName()}').on('beforeSubmit', function(e)
{
   var \$form = $(this);
    $.post(
        \$form.attr("action"), // serialize Yii2 form
        \$form.serialize()
    )
        .done(function(result) {
        if(result == 1)
        {
        console.log('fsdfdsf');
            $(\$form).trigger("reset");
          //  $.pjax.reload({container:'#branchesGrid'});
            $('#modal').modal('hide');
        }else
        {
            $("#message").html(result);
        }
        }).fail(function()
        {
            console.log("server error");
        });
    return false;
});

JS;
$this->registerJs($script);
?>