<?php
/**
 * @var $model \app\modules\backend\models\TransferOneUserForm
 */


use app\modules\backend\models\Year;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<h1>Перевод одного студента</h1>
<? $form = \yii\widgets\ActiveForm::begin() ?>
<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'old_year_id')->dropDownList(ArrayHelper::map(Year::find()->all(), 'id', 'name'),
            [
                'id'     => 'old-transfer-year',
                'prompt' => 'Выберите год',
            ]) ?>

        <?= $form->field($model, 'old_group_id')->widget(DepDrop::classname(), [
            'model'         => $model,
            'attribute'     => 'old_group_id',
            'options'       => ['id' => 'old_transfer-group', 'placeholder' => 'Select ...'],
            'pluginOptions' => [
                'url'         => Url::to(['/backend/groups/groups-list-by-year-ajax']),
                'depends'     => ['old-transfer-year'],
                'placeholder' => 'Выберите группу',
            ],

        ]) ?>

        <?= $form->field($model, 'student_id')->widget(DepDrop::classname(), [
            'model'         => $model,
            'attribute'     => 'old_group_id',
            'pluginOptions' => [
                'url'         => Url::to(['/backend/student/students-list-by-year-ajax']),
                'depends'     => ['old_transfer-group'],
                'placeholder' => 'Выберите студента',
            ],
        ]) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'new_year_id')->dropDownList(ArrayHelper::map(Year::find()->all(), 'id', 'name'),
            [
                'id'     => 'new-transfer-year',
                'prompt' => 'Выберите год',
            ]) ?>

        <?= $form->field($model, 'new_group_id')->widget(DepDrop::classname(), [
            'model'         => $model,
            'attribute'     => 'new_group_id',
            'options'       => ['id' => 'new_transfer-group', 'placeholder' => 'Select ...'],
            'pluginOptions' => [
                'url'         => Url::to(['/backend/groups/groups-list-by-year-ajax']),
                'depends'     => ['new-transfer-year'],
                'placeholder' => 'Выберите группу',
            ],

        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'isSaveGrades')->checkbox([], false)->label('Сохранить старые 
        оценки') ?>
    </div>
</div>


<?= Html::submitButton('Перевести') ?>
<? \yii\widgets\ActiveForm::end() ?>
