<?php
/**
 * @version  1.0.1
 * @var $this yii\web\View
 * @var $this yii\web\View
 * @var $models \app\modules\backend\models\Groups
 * @var $yearToId  \app\modules\backend\controllers\GroupTransferController
 */

use app\modules\backend\models\Year;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = Yii::t('backend/main', 'Transfers group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/main', 'Groups')];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/backend/group-transfer/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/main', 'Step 2')];


$script = <<< JS
    $(document).ready(function(){
      
            $('[data-toggle="tooltip"]').tooltip({
               html: true
            });
        });
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_READY);
?>
    <script>
        function check() {
            var check = document.getElementsByClassName('isTransfer');
            for (var i = 0; i < check.length; i++) {
                if (check[i].type == 'checkbox') {
                    check[i].checked = true;
                }
            }
        }

        function uncheck() {
            var uncheck = document.getElementsByClassName('isTransfer');
            for (var i = 0; i < uncheck.length; i++) {
                if (uncheck[i].type == 'checkbox') {
                    uncheck[i].checked = false;
                }
            }
        }
    </script>

    <h1><?= $this->title ?></h1>
<?php $form = \yii\bootstrap\ActiveForm::begin(); ?>


    <table class="table table-bordered">
        <tr>
            <th><?= Yii::t('backend/main', 'Name previous group') ?></th>
            <th><?= Yii::t('backend/main', 'Year previous group') ?></th>
            <th><?= Yii::t('backend/main', 'Course old group') ?></th>
            <th><?= Yii::t('backend/main', 'New group') ?></th>
            <th><?= Yii::t('backend/main', 'Name new group') ?></th>
            <th><?= Yii::t('backend/main', 'Course new group') ?></th>
            <th>Группа продолжает обучение(перевести)?</th>
            <th><?= Yii::t('backend/main', 'Is transfer') ?>
                <br><a href="#" value="Check All" onclick="check();"><?= Yii::t('backend/main', 'Select all') ?></a> /
                <br><a href="#" value="Check All" onclick="uncheck();"><?= Yii::t('backend/main', 'Unselect all') ?></a>
            </th>


        </tr>
        <? foreach ($models as $index => $model) : ?>


            <tr>
                <!-- ===================================Old group data ======================================================-->
                <td>
                    <? foreach ($model->ParentChildInfo as $key => $item): ?>
                        <!-- обходим массив всех групп всех годов  -->
                        <!-- если id группы совпадает с текущей то жирным  -->
                        <? if ($item->id_year == $model->id_year): ?>
                            <!-- если $key=0 добавляем стрелочку =) -->
                            <?= $key == 0 ? '' : '->'; ?>
                            <?= '<b>' . $item->name . '</b> <sup>' . $item->idYear->name . ' </sup> ' ?>
                            <!-- если id группы НЕ совпадает с текущей то обычным -->
                        <? else: ?>
                            <?= $item->name . '<sup>' . $item->idYear->name . ' </sup> ' ?>
                        <? endif; ?>
                    <? endforeach; ?>
                </td>
                <td>
                    <?= $model->idYear->name ?>
                </td>
                <td>
                    <?= $model->course ?>
                </td>

                <!-- ===================================New group data ======================================================-->
                <td>

                    <?= Select2::widget([

                        'name' => 'Groups[' . $model->id . '][id_year]',

                        'value' => $yearToId ? $yearToId : null,
                        'data' => ArrayHelper::map(Year::find()->asArray()->all(), 'id', 'name'),
                        'options' => [
                            'id' => $model->id,
                            'placeholder' => 'Select a year ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </td>
                <?= $form->field($model, '[' . $model->id . ']p_id')->hiddenInput(['value' => $model->id])->label(false) ?>
                <td>
                    <?= $form->field($model, '[' . $model->id . ']name')->label(false) ?>
                </td>
                <td>
                    <?= $form->field($model, '[' . $model->id . ']course')->label(false) ?>
                </td>
                <td>
                    <?= $form->field($model, '[' . $model->id . ']active')
                        ->checkbox()
                        ->label('да', ['data-toggle' => 'tooltip', 'title' => 'Если галочка не установлена, то группа будет помечена как "выпускная",
                                т.е. студенты группы выпустились. Если галочка установлена, то студенты будут переведены
                                в указанную группу.'])
                    ?>
                </td>
                <td>
                    <?= $form->field($model, '[' . $model->id . ']isTransfer')->checkbox(['class' => 'isTransfer'])->label(Yii::t('backend/main', 'Is transfer')) ?>


                </td>

            </tr>
        <? endforeach; ?>

    </table>


    <div class="row">
        <div class="col-md-12">
            <?= Html::submitButton("Перевести!", ['class' => 'btn  btn-danger']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>