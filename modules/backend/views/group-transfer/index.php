<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;


use app\modules\backend\models\Year;

$this->title = Yii::t('backend/main', 'Transfers group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/main', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label'=>$this->title,'url' =>['/backend/group-transfer/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/main', 'Step 1')];
?>

<h1><?= $this->title ?></h1>

<?= Html::beginForm(['/backend/group-transfer/transfer'], 'post', ['data-pjax' => '0']); ?>
<div class="form-group">
    <div class="row">
        <div class="col-md-12">
            <?= Html::label(Yii::t('backend/main', 'From year'), 'year') ?>
            <?= Select2::widget([
                'name' => 'year_from',
//            'value' => $year ? $year : null,
                'data' => ArrayHelper::map(Year::find()->asArray()->all(), 'id', 'name'),
                'options' => [
                        'id' => 'parent_year',
                        'placeholder' => Yii::t('backend/main', 'Select a year ...')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= Html::label(Yii::t('backend/main', 'To year'), 'year_to') ?>
            <?= \kartik\depdrop\DepDrop::widget([
                    'name'=>'year_to',
                'pluginOptions' => [
                    'depends' => ['parent_year'],
                    'placeholder' => 'Выберите год из списка',
                    'url' => \yii\helpers\Url::to(['/api/years/childs-year-ajax'])
            ]
            ]) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= Html::submitButton(Yii::t('backend/main', 'Find groups'),
                [
                    'class' => 'btn  btn-primary',
                    'style' => 'margin-top: 30px;'
                ]) ?>
        </div>

    </div>

    <?= Html::endForm() ?>
</div>
