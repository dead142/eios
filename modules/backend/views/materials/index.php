<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\common\models\MaterialsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('materials', 'Materials');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="materials-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('materials', 'Create Materials'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

          //  'ID',
            'NAME',
          //  'FULLTEXT:html',
            'DESC:html',
            'DATE_CREATE',
            'ORDER',
            // 'DATE_UPDATE',
            // 'CREATE_BY',
       //      'STATUS',
            [
                                'label' => Yii::t('backend/main', 'Link for material'),
                                'value' => function($model)  {
                                            $url = \yii\helpers\Url::toRoute(['/frontend/default/view','id'=>$model->ID]);
                                           return '<input type="text" value="'.$url.'" />';
                                },
               'format' =>'raw',
],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
