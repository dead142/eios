<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use dosamigos\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use mihaildev\ckeditor\CKEditor;


/* @var $this yii\web\View */
/* @var $model app\modules\common\models\Materials */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="materials-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?=  Html::activeHiddenInput($model, 'VERSION') ?>

    <?= $form->field($model, 'NAME')->textInput(['maxlength' => true]) ?>
 <?php //http://razmik.ru/news/ustanovka-ckeditor-i-kcfinder-v-yii2 ?>
<!--    --><?//= $form->field($model, 'FULLTEXT')->widget(CKEditor::className(), [
//        'options' => ['rows' => 6],
//        'preset' => 'full',
//        'clientOptions' => [
//            'filebrowserUploadUrl' => \yii\helpers\Url::toRoute('/site/url')
//        ]
//
//    ])  ?>


    <?=
    $form->field($model, 'DESC')->widget(CKEditor::className(), [
      //  'options' => ['rows' => 6],
      ///  'preset' => 'basic',
        'editorOptions' => ElFinder::ckeditorOptions(['elfinder'],[
            'preset' => 'basic', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
        ]),


    ])
    ?>

    <?=
    $form->field($model, 'FULLTEXT')->widget(CKEditor::className(), [

    'editorOptions' => ElFinder::ckeditorOptions(['elfinder'],[/* Some CKEditor Options */]),

])
    ?>


    <?= $form->field($model, 'STATUS')->textInput()->dropDownList([1=>'yes',0=>'no']) ?>

    <?= $form->field($model, 'ORDER')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('Bbuttons', 'Create') : Yii::t('Bbuttons', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
