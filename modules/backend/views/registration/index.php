<?php
/**
 * @var $years array Years ['id','name']
 * @var $model \yii\base\DynamicModel
 * @see \app\api\controllers\GroupsController actionIndex
 * @see \app\api\controllers\GroupsController for Ajax DepDrop
 */

use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('backend\main', 'Add users in groups');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<? $form = ActiveForm::begin(['action' => ['form-users']]); ?>

<?= $form->field($model, 'year_id')->widget(Select2::classname(), [
    'name' => 'year_id',
    'data' => $years,
    'options' => ['placeholder' => 'Выберите год', 'id' => 'year_id'],
    'pluginOptions' => [
        'allowClear' => true,
        'id' => 'year_id'
    ],
])->label('Год'); ?>

<?= $form->field($model, 'group_id')->widget(\kartik\depdrop\DepDrop::classname(), [
    'pluginOptions' => [
        'depends' => ['year_id'],
        'placeholder' => 'Выберите группу из списка',
        'url' => \yii\helpers\Url::to(['/api/groups/list-by-year-ajax'])
    ]
])->label('Группа'); ?>


<?= $form->field($model, 'count')->textInput()->label('Количество'); ?>


<div class="form-group">
    <?= Html::submitButton(Yii::t('backend\main', 'Go to add users'), ['class' => 'btn btn-success']);

    ActiveForm::end();
    ?>
</div>