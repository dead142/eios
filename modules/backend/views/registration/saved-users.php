<?php
use yii\helpers\Html;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\main', 'Add users in groups'), 'url' => ['index']];
$this->title = Yii::t('backend\main', 'Generated data');
$this->params['breadcrumbs'][] = $this->title;
?>


<h1><?= Html::encode($this->title) ?></h1>
<!-------------------------------------------- Show error messages ----------------------------------------------->

<div class="alert alert-danger" role="alert"> <?= Yii::t('backend\main', 'Save table!') ?></div>
<table class="table table-hover">
    <tr>
        <th><?= Yii::t('backend\main', 'FIO') ?></th>
        <th><?= Yii::t('backend\main', 'Login') ?></th>
        <th><?= Yii::t('backend\main', 'Password') ?></th>
    </tr>

<? foreach($usersModels as $data): ?>
<? $exportCsv[] = [
        'user' => $data['profile']['surname'].' '.$data['profile']['name'].' '.$data['profile']['middlename'],
        'password' => $data['user']['password'],
        'login' => $data['user']['username'],
    ] ?>
<tr>
    <td><?= $data['profile']['surname'].' '.$data['profile']['name'].' '.$data['profile']['middlename']?></td>
    <td><?= $data['user']['username'] ?></td>
    <td><?= $data['user']['password'] ?></td>


</tr>
<? endforeach ?>
</table>


<?php $form = \yii\widgets\ActiveForm::begin(['action'=>'download-excel']); ?>
<?= Html::hiddenInput('data', serialize($exportCsv)); ?>

<?= Html::submitButton(  Yii::t('Bbuttons', 'Export to excel'),  ['class' => 'btn btn-primary']) ?>


<?php \yii\widgets\ActiveForm::end(); ?>
