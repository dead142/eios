<?
/**
 * @var $groupName \app\modules\backend\models\Groups fullGroupName
 * @var $groupId \app\modules\backend\models\Groups id
 * @var $usersCount int
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\main', 'Add users in groups'), 'url' => ['index']];
$this->title = Yii::t('backend\main', 'Form add users in groups');
$this->params['breadcrumbs'][] = $this->title;
?>

    <h2> <?= 'Группа: ' . $groupName . '' . Yii::t('backend\main', 'count of users is') . '' . $usersCount ?></h2>
<? $form = ActiveForm::begin(['action' => ['save-users']]); ?>


<?= Html::hiddenInput('groupId', $groupId) ?>
<?= Html::hiddenInput('usersCount', $usersCount) ?>


    <div class="row">
        <?php for ($i = 0; $i < $usersCount; $i++): ?>
            <div class=" col-sm-1 ">
                <h3><?= $i + 1 ?></h3>
            </div>
            <div class=" col-sm-4 ">
                <?= $form->field($model, "surname[$i]")->label(Yii::t('backend/user', 'surname')) ?>
            </div>
            <div class=" col-sm-3">
                <?= $form->field($model, "name[$i]")->label(Yii::t('backend/user', 'name')) ?>
            </div>
            <div class=" col-sm-4 ">
                <?= $form->field($model, "middlename[$i]")->label(Yii::t('backend/user', 'middlename')) ?>
            </div>
        <?php endfor ?>
    </div>
<?
echo Html::submitButton(Yii::t('backend\main', 'Add users'), ['class' => 'btn btn-success']);

ActiveForm::end();
?>