<?php
/**
 * @View for backend/registration Controller
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>
<h1> Загрузка данных о пользователях из файла</h1>
<!-------------------------------------------- Show error messages ----------------------------------------------->
<div id="row">
    <? if (Yii::$app->session->hasFlash('error')) : ?>
        <div id="message" class="alert alert-danger">
            <?= Yii::$app->session->getFlash('error'); ?>
        </div>
    <? endif; ?>
</div>
<!----------------------------------------- Form for file -------------------------------------------------------->
<div id="row">

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'options' => ['enctype' => 'multipart/form-data']]);
    ?>
    <div class="col-md-4">
        <?= \kartik\select2\Select2::widget([
            'name' => 'group',
            'attribute' => 'state_2',
            'data' => \yii\helpers\ArrayHelper::map(\app\modules\backend\models\StudentGroup::find()->all(), 'id', 'group.fullNameGroup'),
            'options' => ['placeholder' => 'Select a group ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>
    <div class="col-md-4">
        <?= Html::fileInput('file', 'file', ['class' => 'btn btn-default']) ?>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <?= Html::submitButton('Upload', ['class' => 'btn btn-default']) ?>
        </div>

        <?php ActiveForm::end() ?>
    </div>
</div>


