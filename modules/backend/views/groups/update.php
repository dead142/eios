<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\Groups */

$this->title = Yii::t('evaluation', 'Update {modelClass}: ', [
    'modelClass' => 'Groups',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('evaluation', 'Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('evaluation', 'Update');
?>
<div class="groups-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
