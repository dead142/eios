<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\backend\models\GroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('evaluation', 'Groups');
$this->params['breadcrumbs'][] = Yii::t('evaluation', 'Current study');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="domain-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
   <? // \yii\helpers\VarDumper::dump(ArrayHelper::map($dataProvider->query->all(), 'id','name', 'course'),[10],true)?>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_itemGroups',
        'summary' => ''


    ]); ?>

</div>
