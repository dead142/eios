<?php
/**
 * @var $searchModel app\modules\common\models\search\StudentGroupSearch
 * @var $dataProvider app\modules\backend\models\StudentGroup
 */

use yii\grid\GridView;
use yii\helpers\Html;

?>
<?php
$this->params['breadcrumbs'][] = ['label' => 'Группы', 'url' => ['/backend/groups/index']];
$this->title = Yii::t('groups', 'Groups and students');
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?></h1>
<?php //  TODO echo $this->render('_search', ['model' => $searchModel]); ?>

<?php \yii\widgets\Pjax::begin(); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'fullStudentName',
            'label' => Yii::t('groups', 'Full Student Name'),
            // 'value' => 'student.fullName',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a($model->student->fullName,
                    [
                        '/backend/student/profile',
                        'userId' => $model->student_id
                    ]);
            }
        ],

        [
            'format' => 'raw',
            'label' => Yii::t('frontend/main', 'Grade user'),
            'value' => function ($data) {

                return Html::a(Yii::t('frontend/main', 'See'), ['/frontend/grade/grade-user', 'id' => $data['student_id']]);
            }
        ],

        [
            'format' => 'raw',
            'label' => 'Удалить пользователя из группы',
            'value' => function ($model) {
                return Html::a('<span class="glyphicon glyphicon-remove text-danger"></span> <span class="text-danger">Удалить пользователя из группы</span>',
                    [
                        '/backend/groups/delete-user-from-group',
                        'userGroupId' => $model->id
                    ]

                    , [
                        'title' => 'Удалить',
                        'data-confirm' => 'Вы действительно хотите удалить запись? Все оценки студента и его история обучения в группе будет удалена!',
                        'data-method' => 'post',
                        'data-pjax' => '0',
                    ]);
            },

        ],

    ],
]); ?>
<?php \yii\widgets\Pjax::end(); ?>