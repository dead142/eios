<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\GroupsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="groups-search">

    <?php $form = ActiveForm::begin([
        'action' => ['groups'],
        'method' => 'get',
    ]); ?>
    <div id="row">
        <div class="col-sm-6 col-md-6" >
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="col-sm-6 col-md-6" >
            <?= $form->field($model, 'course') ?>
        </div>
    </div>

    <div class="form-group" >
        <?= Html::submitButton(Yii::t('evaluation', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('evaluation', 'Reset'), ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('evaluation', 'Create and edit groups'), ['index'], ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
