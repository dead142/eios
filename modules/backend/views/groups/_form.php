<?php

use app\modules\backend\models\Year;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\Groups */
/* @var $form yii\widgets\ActiveForm */


$this->registerCssFile("@web/css/libs/checkbox.css", []);

$yearList = ArrayHelper::map(Year::find()->all(), 'id', 'name');
$groupList = ArrayHelper::map(\app\modules\backend\models\Groups::find()->all(), 'id', 'yearGroup');
?>

<div class="groups-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'p_id')->widget(\kartik\select2\Select2::classname(), [
                'model' => $model,
                'attribute' => 'p_id',
                'data' => $groupList,



                'pluginOptions' => [

                    'placeholder' => 'Выберите группу',

                ],
            ]) ?>
        </div>
        <div class="col-md-2">

            <label for="groups-active">
                <?= $form->field($model, 'active')->checkbox()->label(false) ?>
                <span></span>
            </label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">

            <?= $form->field($model, 'id_year')->dropDownList($yearList)->hint('Текущий год обучения группы') ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'year_start_id')
                ->dropDownList($yearList, ['maxlength' => true, 'value' => $model->isNewRecord ? null : $model->id_year])
                ->hint('Год начала обучения для группы') ?>
        </div>
    </div>
    <? // $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'course')->dropDownList(
        [
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
            6 => '6',
            7 => '7',
            8 => '8',
            9 => '9',
        ]
    );
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('evaluation', 'Create') : Yii::t('evaluation', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
