<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\modules\backend\models\Year;
use app\modules\backend\models\Groups;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\backend\models\GroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('evaluation', 'Groups');
$this->params['breadcrumbs'][] = $this->title;

$years =  ArrayHelper::map(Year::find()->all(), 'id', 'name');
?>
<div class="groups-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('evaluation', 'Create Groups'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'headerOptions' => ['style' => 'width:20%'],
                'label' => 'История группы',
                'value' => function ($model) {
                    $str = '';
                    $array = \app\modules\backend\models\Groups::GetListGroups($model->id);
                    foreach ($array as $item){
                        if (!empty($item)){
                            if ($item->id == $model->id){
                                $str .= '<b><small>'.$item->course.' курс: ' .$item->year .'</small></b><br>';
                            }
                            else {
                                $str .= '<small>'.$item->course.' курс: ' .$item->year .'</small> <br> ';
                            }

                        }


                    }
                    return $str;
                },
                'format' => 'html',
            ],
            [
                'headerOptions' => ['style' => 'width:15%'],
                'attribute' =>   'name',
            ],


            'course',
            [
                'headerOptions' => ['style' => 'width:15%'],
                'label' => 'Год обучения',
                'attribute' => 'id_year',
                'value' => 'idYear.name',
                'filter' => $years

            ],
            [
                 'headerOptions' => ['style' => 'width:15%'],
                'label' => 'Год поступления',
                'attribute' => 'year_start_id',
                'value' => 'idYear.name',
                'filter' => $years

            ],
            [
                'attribute' => 'active',
                'value' => function ($model) {
                    if ($model->active == false) {
                        if (is_null($model->p_id)){
                            return '<p class="text-danger">Группа выпустилась в '.$model->idYear->name.'</p>';
                        } else {
                            return '<p class="text-danger">Группа перевелась в группу <br>'.Groups::getFullNameById($model->p_id).'</p>';
                        }

                    } else if ($model->name == true) {
                        return '<p class="text-success">Группа обучается </p>';
                    }
                },
                'format' => 'html',
                'filter' => [true=>'Да',false => 'Нет']


            ],

            //'created_at:datetime',
            [
                'headerOptions' => ['style' => 'width:20%'],
                'value' => function ($model) {
                    $out =  Html::a('Список студентов <span class="badge pull-right"> '.\app\modules\backend\models\Groups::getCountStudentsGroup($model->id).'</span>',
                        [
                            '/backend/groups/students-list',
                            'groupId' => $model->id
                        ],
                        [
                            'class' => 'btn btn-primary  btn-block'
                        ]
                    );
                    $out .= Html::a('Дисциплины  <span class="badge pull-right"> '.\app\modules\backend\models\Groups::getCountDisciplinesGroup($model->id).'</span>',
                        [
                            '/backend/disciplines/discipline-by-group',
                            'id' => $model->id
                        ],
                        [
                            'class' => 'btn btn-success btn-block'
                        ]
                    );
                    return $out;

                },
                'format' => 'html',
            ],



            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
   </div>
