<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\common\models\TypeParticipation */

$this->title = Yii::t('portfolio', 'Create Type Participation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('portfolio', 'Type Participations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-participation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
