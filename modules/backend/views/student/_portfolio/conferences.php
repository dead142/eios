<?php
/**
 * @var $conferences \app\modules\frontend\models\Conferences[]
 */
?>
<ol>
    <? foreach ($conferences as $conference) : ?>
        <li>
            <?= isset($conference['FILE']) ? '<a href="'.$conference['FILE'].'">' : null ?>

            <?= isset($conference['sTATUS']['NAME']) ?  $conference['sTATUS']['NAME'].'. '  : null ?>
            <?= isset($conference['NAME']) ?  $conference['NAME'].'. '  : null ?>
            <?= isset($conference['DATE_START']) ?  ' c '. date("d-m-Y",strtotime($conference['DATE_START'])): null ?>
            <?= isset($conference['DATE_END']) ?  ' по '. date("d-m-Y",strtotime($conference['DATE_END'])).'. '  : null ?>
            <?= isset($conference['LOCATION']) ? 'Место проведения: '.$conference['LOCATION'].'. '   : null ?>
            <?= isset($conference['kINDINVOVLVEMENT']['NAME']) ?  $conference['kINDINVOVLVEMENT']['NAME'].'. '  : null ?>
            <?= isset($conference['FILE']) ? '</a>' : null ?>
        </li>
    <? endforeach; ?>
</ol>
