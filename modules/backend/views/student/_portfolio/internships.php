<?php
/**
 * @var $internships \app\modules\frontend\models\Internship[]
 * @var $internship \app\modules\frontend\models\Internship
 */
?>
<ol>
    <? foreach ($internships as $internship) : ?>
        <li>
            <?= isset($internship['FILE']) ? '<a href="'.$degree ['FILE'].'">' : null ?>


            <?= isset($internship['NAME']) ?  $internship['NAME'].'. '  : null ?>
            <?= isset($internship['DATE_START']) ?  ' c '. date("d-m-Y",strtotime($internship['DATE_START'])) : null ?>
            <?= isset($internship['DATE_END']) ?  ' по '. date("d-m-Y",strtotime($internship['DATE_END'])).'. '  : null ?>

            <?= isset($internship['FILE']) ? '</a>' : null ?>
        </li>
    <? endforeach; ?>
</ol>
