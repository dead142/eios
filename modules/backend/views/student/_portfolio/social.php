<?php
/**
 * @var $socialActivities \app\modules\frontend\models\socialActivities[]
 * @var $socialActivity \app\modules\frontend\models\socialActivities
 */
?>
<ol>
    <? foreach ($socialActivities as $socialActivity) : ?>
        <li>
            <?= isset($socialActivity['FILE']) ? '<a href="'.$socialActivity ['FILE'].'">' : null ?>


            <?= isset($socialActivity['NAME']) ?  $socialActivity['NAME'].'. '  : null ?>
            <?= isset($socialActivity['ROLE']) ?  $socialActivity['ROLE'].'. '  : null ?>
        

            <?= isset($socialActivity['sTATUS']['NAME']) ? ' '.$socialActivity['sTATUS']['NAME'].'. '   : null ?>
            <?= isset($socialActivity['tYPEEVENT']['NAME']) ? ' '.$socialActivity['tYPEEVENT']['NAME'].'. '   : null ?>
            <?= isset($socialActivity['lEVEL']['NAME']) ? ' '.$socialActivity['lEVEL']['NAME'].'. '   : null ?>
            <?= isset($socialActivity['tYPEPARTICIPATION']['NAME']) ? ' '.$socialActivity['tYPEPARTICIPATION']['NAME'].'. '   : null ?>

            <?= isset($socialActivity['DATE_START']) ?  ' c '. date("d-m-Y",strtotime($socialActivity['DATE_START'])) : null ?>
            <?= isset($socialActivity['DATE_END']) ?  ' по '. date("d-m-Y",strtotime($socialActivity['DATE_END'])).'. '  : null ?>

            <?= isset($socialActivity['FILE']) ? '</a>' : null ?>

            <i><?= isset($socialActivity['DESC']) ?  $socialActivity['DESC'] : null ?></i>
        </li>
    <? endforeach; ?>
</ol>
