<?php
/**
 * @var $awards \app\modules\frontend\models\Awards[]
 */
?>
<ol>
<? foreach ($awards as $award) : ?>
<li>
    <?= isset($award['FILE']) ? '<a href="'.$award['FILE'].'">' : null ?>
    <?= isset($award['NAME']) ? 'Наименование документа: '.$award['NAME'].'. '  : null ?>
    <?= isset($award['dOCUMENT']['NAME']) ? 'Тип документа: '.$award['dOCUMENT']['NAME'].'. '   : null ?>
    <?= isset($award['oRGANIZATION']['NAME']) ? 'Название организации: '.$award['oRGANIZATION']['NAME'].'. '   : null ?>
     <?= isset($award['FILE']) ? '</a>' : null ?>
</li>
<? endforeach; ?>
</ol>
