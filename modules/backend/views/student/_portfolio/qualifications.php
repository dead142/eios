<?php
/**
 * @var $qualifications \app\modules\frontend\models\Qualification[]
 * @var $qualification \app\modules\frontend\models\Qualification
 */
?>
<ol>
    <? foreach ($qualifications as $qualification) : ?>
        <li>
            <?= isset($qualification['FILE']) ? '<a href="'.$project ['FILE'].'">' : null ?>


            <?= isset($qualification['NAME']) ?  $qualification['NAME'].'. '  : null ?>
            <?= isset($qualification['LOCATION']) ?  $qualification['LOCATION'].'. '  : null ?>
            <?= isset($qualification['HOURS']) ?  $qualification['HOURS'].' часов. '  : null ?>
            <?= isset($qualification['oRGANIZATION']['NAME']) ? 'Название организации: '.$qualification['oRGANIZATION']['NAME'].'. '   : null ?>


            <?= isset($qualification['DATE_START']) ?  ' c '. date("d-m-Y",strtotime($qualification['DATE_START'])) : null ?>
            <?= isset($qualification['DATE_END']) ?  ' по '. date("d-m-Y",strtotime($qualification['DATE_END'])).'. '  : null ?>
            <?= isset($qualification['DATE_ISSUE']) ?  ' Дата выдачи: '. date("d-m-Y",strtotime($qualification['DATE_ISSUE'])).'. '  : null ?>

            <?= isset($qualification['FILE']) ? '</a>' : null ?>
        </li>
    <? endforeach; ?>
</ol>
