<?php
/**
 * @var $degrees \app\modules\frontend\models\Degree[]
 * @var $degree \app\modules\frontend\models\Degree
 */
?>
<ol>
    <? foreach ($degrees as $degree) : ?>
        <li>
            <?= isset($degree['FILE']) ? '<a href="'.$degree ['FILE'].'">' : null ?>


            <?= isset($degree['NAME']) ?  $degree['NAME'].'. '  : null ?>
            <?= isset($degree['dOCUMENT']['NAME']) ? 'Тип документа: '.$degree['dOCUMENT']['NAME'].'. '   : null ?>

            <?= isset($degree['DOCUMENT_NUMBER']) ? ' '.$degree['DOCUMENT_NUMBER'].'. '   : null ?>
            <?= isset($degree['oRGANIZATION']['NAME']) ? 'Название организации: '.$degree['oRGANIZATION']['NAME'].'. '   : null ?>

            <?= isset($degree['FILE']) ? '</a>' : null ?>
        </li>
    <? endforeach; ?>
</ol>
