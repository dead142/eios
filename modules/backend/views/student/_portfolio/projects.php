<?php
/**
 * @var $projects \app\modules\frontend\models\Projects[]
 * @var $project \app\modules\frontend\models\Projects
 */
?>
<ol>
    <? foreach ($projects as $project) : ?>
        <li>
            <?= isset($project['FILE']) ? '<a href="'.$project ['FILE'].'">' : null ?>


            <?= isset($project['NAME']) ?  $project['NAME'].'. '  : null ?>
            <?= isset($project['ROLE']) ?  $project['ROLE'].'. '  : null ?>
            <?= isset($project['TITLE_COMPETENTION']) ?  $project['TITLE_COMPETENTION'].'. '  : null ?>
            <?= isset($project['TITLE_SECTION']) ?  $project['TITLE_SECTION'].'. '  : null ?>
            <?= isset($project['TITLE_SECTION']) ?  $project['TITLE_SECTION'].'. '  : null ?>
            <?= isset($project['oRGANIZATION']['NAME']) ? 'Название организации: '.$project['oRGANIZATION']['NAME'].'. '   : null ?>


            <?= isset($project['DATE_START']) ?  ' c '. date("d-m-Y",strtotime($project['DATE_START'])) : null ?>
            <?= isset($project['DATE_END']) ?  ' по '. date("d-m-Y",strtotime($project['DATE_END'])).'. '  : null ?>

            <?= isset($project['FILE']) ? '</a>' : null ?>
        </li>
    <? endforeach; ?>
</ol>
