<?php
/**
 * @var $intellectuals \app\modules\frontend\models\Intellectual[]
 * @var $intellectual \app\modules\frontend\models\Intellectual
 */
?>
<ol>
    <? foreach ($intellectuals as $intellectual) : ?>
        <li>
            <?= isset($intellectual['FILE']) ? '<a href="'.$intellectual ['FILE'].'">' : null ?>


            <?= isset($intellectual['NAME']) ?  $intellectual['NAME'].'. '  : null ?>
            <?= isset($intellectual['COPYRIGHT']) ?  $intellectual['COPYRIGHT'].'. '  : null ?>
            <?= isset($intellectual['oRGANIZATION']['NAME']) ? 'Название организации: '.$intellectual['oRGANIZATION']['NAME'].'. '   : null ?>

            <?= isset($intellectual['DATE_ISSUE']) ?  ' Дата выдвчи '. date("d-m-Y",strtotime($intellectual['DATE_ISSUE'])) : null ?>


            <?= isset($intellectual['FILE']) ? '</a>' : null ?>
        </li>
    <? endforeach; ?>
</ol>
