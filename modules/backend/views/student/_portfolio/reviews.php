<?php
/**
 * @var $reviews \app\modules\frontend\models\Reviews[]
 * @var $review \app\modules\frontend\models\Reviews
 */
?>
<ol>
    <? foreach ($reviews as $review) : ?>
        <li>
            <?= isset($review['FILE']) ? '<a href="'.$review ['FILE'].'">' : null ?>

            <?= isset($review['NAME']) ?  $review['NAME'].'. '  : null ?>
            <?= isset($review['AUTHOR']) ?  $review['AUTHOR'].'. '  : null ?>
            <?= isset($review['oRGANIZATION']['NAME']) ? 'Название организации: '.$review['oRGANIZATION']['NAME'].'. '   : null ?>

            <?= isset($review['FILE']) ? '</a>' : null ?>
        </li>
    <? endforeach; ?>
</ol>
