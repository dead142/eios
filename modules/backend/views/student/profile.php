<?php
/**
 * @var $student \app\models\Profile
 * @var $studentGroups \app\modules\backend\models\StudentGroup
 * @var $studentGroupYear \app\modules\backend\models\Year
 * @var $discipline \app\modules\backend\models\GroupsDiscipline
 */

$this->params['breadcrumbs'][] = ['label' => 'Группы', 'url' => ['/backend/groups/index']];
$this->params['breadcrumbs'][] = 'Группы и студенты';
$this->params['breadcrumbs'][] = 'Портфолио студента';
?>
<div class="row">
    <div class="col-md-6">
        <?= \yii\helpers\Html::a( '<span class="glyphicon glyphicon-fast-backward"> Назад</span>', \Yii::$app->request->referrer,['class'=>'btn btn-primary btn-block'] ) ?>
    </div>
    <div class="col-md-6">
    </div>
</div>

    <h1><?= $student->fullName ?></h1>
    <table class="table">
        <tr>
            <? foreach ($studentGroups as $studentGroupYear) : ?>
                <td>
                    <?= $studentGroupYear['group']['idYear']['name'] ?>

                </td>
            <? endforeach; ?>

        </tr>
        <tr>
            <? foreach ($studentGroups

            as $studentGroup) : ?>

            <td>
                <?= $studentGroup['group']['name']; ?>
                (<span>Курс </span> <?= $studentGroup['group']['course']; ?>)
                <?= (is_null($studentGroup['group']['active']) || $studentGroup['group']['active'] == 0) ? '<span class="label label-danger">Закончено</span>' : '<span class="label label-success">Текущее состояние</span>' ?>

                <ul>
                    <? foreach ($studentGroup['group']['groupsDisciplines'] as $discipline) : ?>
                        <li>
                            <a href="<?= \yii\helpers\Url::to(['/backend/grades/grades-disciplines','groupDiscipline_id'=>$discipline['id'],'group_id'=>$discipline['group_id']]) ?>">
                                <?= $discipline['discipline']['name']; ?>
                            </a>
                            <i>
                                (
                                <?= $discipline['teacher']['surname']; ?>
                                <?= $discipline['teacher']['name']; ?>
                                <?= $discipline['teacher']['middlename']; ?>
                                )
                            </i>
                        </li>

                    <? endforeach; ?>
                </ul>
                <? endforeach; ?>
            </td>
        </tr>
    </table>
    <h2>Достижения (портфолио) обучаемого</h2>
<?php
echo \yii\bootstrap\Tabs::widget([
    'encodeLabels' => false,
    'items' => [
        [
            'label' => 'Премии, награды, дипломы <span class="badge">' . count($studentGroup['student']['awards']) . '</span>',
            'content' => $this->render('_portfolio/awards',['awards'=>$studentGroup['student']['awards']]),
            'active' => true
        ],
        [
            'label' => 'Конференции, школы, симпозиумы, семинары <span class="badge">' . count($studentGroup['student']['conferences']) . '</span>',
            'content' => $this->render('_portfolio/conferences',['conferences'=>$studentGroup['student']['conferences']]),
            'active' => false
        ],
        [
            'label' => 'Степени, звания <span class="badge">' . count($studentGroup['student']['degrees']) . '</span>',
            'content' => $this->render('_portfolio/degrees',['degrees'=>$studentGroup['student']['degrees']]),
            'active' => false
        ],
        [
            'label' => 'Прохождение практик, стажировок <span class="badge">' . count($studentGroup['student']['internships']) . '</span>',
            'content' => $this->render('_portfolio/internships',['internships'=>$studentGroup['student']['internships']]),
            'active' => false
        ],
        [
            'label' => 'Проекты, гранты <span class="badge">' . count($studentGroup['student']['projects']) . '</span>',
            'content' => $this->render('_portfolio/projects',['projects'=>$studentGroup['student']['projects']]),
            'active' => false
        ],
        [
            'label' => 'Монографии, сборники, статьи <span class="badge">' . count($studentGroup['student']['publications']) . '</span>',
            'content' => $this->render('_portfolio/publications',['publications'=>$studentGroup['student']['publications']]),
            'active' => false
        ],
        [
            'label' => 'Повышение квалификации <span class="badge">' . count($studentGroup['student']['qualifications']) . '</span>',
            'content' => $this->render('_portfolio/qualifications',['qualifications'=>$studentGroup['student']['qualifications']]),
            'active' => false
        ],

        [
            'label' => 'Рецензии, отзывы, рекомендательные письма <span class="badge">' . count($studentGroup['student']['reviews']) . '</span>',
            'content' => $this->render('_portfolio/reviews',['reviews'=>$studentGroup['student']['reviews']]),
            'active' => false
        ],
        [
            'label' => 'Общественная, культурно-творческая, спортивная деятельность <span class="badge">' . count($studentGroup['student']['socialActivities']) . '</span>',
            'content' => $this->render('_portfolio/social',['socialActivities'=>$studentGroup['student']['socialActivities']]),
            'active' => false
        ],
        [
            'label' => 'Объекты интеллектуальной собственности <span class="badge">' . count($studentGroup['student']['intellectuals']) . '</span>',
            'content' => $this->render('_portfolio/intellectuals',['intellectuals'=>$studentGroup['student']['intellectuals']]),
            'active' => false
        ],


    ],
]);
?>