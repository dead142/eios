<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\backend\models\GroupsDisciplineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('evaluation', 'Groups Disciplines');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groups-discipline-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('evaluation', 'Create Groups Discipline'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            [
                'label'=>Yii::t('evaluation', 'Name group'),
                'attribute'=>'group_name',
                'value'=> 'group.name',
            ],
            [
                'label'=>Yii::t('evaluation', 'Discipline'),
                'attribute'=>'discipline',
                'value'=> 'discipline.name',
            ],
            [
                'label'=>Yii::t('evaluation', 'Teacher'),
                'attribute'=>'teacher',
                'value'=> 'teacher.FullName',
            ],

            'semestr_id',
            [
                'label'=>Yii::t('evaluation', 'year_name'),
                'attribute'=> 'year_name',
                'value' => 'group.idYear.name'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
