<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\GroupsDisciplineSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="groups-discipline-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'group_id') ?>

    <?= $form->field($model, 'discipline_id') ?>

    <?= $form->field($model, 'teacher_id') ?>

    <?= $form->field($model, 'semestr_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('evaluation', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('evaluation', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
