<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\GroupsDiscipline */

$this->title = Yii::t('evaluation', 'Create Groups Discipline');
$this->params['breadcrumbs'][] = ['label' => Yii::t('evaluation', 'Groups Disciplines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="groups-discipline-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
