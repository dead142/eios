<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\GroupsDiscipline */

$this->title = Yii::t('evaluation', 'Update {modelClass}: ', [
    'modelClass' => 'Groups Discipline',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('evaluation', 'Groups Disciplines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('evaluation', 'Update');
?>
<div class="groups-discipline-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
