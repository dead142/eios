<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Profile;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\GroupsDiscipline */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="groups-discipline-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(\app\modules\backend\models\Groups::find()->all(),'id','YearGroup')) ?>


    <?= $form->field($model, 'discipline_teacher_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\app\modules\backend\models\DisciplinesTeachers::find()->all(),'id','name'),
        'options' => ['placeholder' => Yii::t('backend','Select teacher and discipline')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>



    <?= $form->field($model, 'semestr_id')->dropDownList([
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        11 => '11',
        12 => '12',
        13 => '13',
        14 => '14',
        15 => '15',
        16 => '16',

    ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('evaluation', 'Create') : Yii::t('evaluation', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
