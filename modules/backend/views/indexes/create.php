<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\common\models\Indexes */

$this->title = Yii::t('portfolio', 'Create Indexes');
$this->params['breadcrumbs'][] = ['label' => Yii::t('portfolio', 'Indexes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indexes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
