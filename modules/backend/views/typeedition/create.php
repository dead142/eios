<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\common\models\TypeEdition */

$this->title = Yii::t('portfolio', 'Create Type Edition');
$this->params['breadcrumbs'][] = ['label' => Yii::t('portfolio', 'Type Editions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-edition-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
