<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\common\models\StudentStatus */

$this->title = Yii::t('student-status', 'Update status: ', [
    'modelClass' => 'Student Status',
]) . $model->studentGroup->student->fullName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('student-status', 'Student Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->studentGroup->student->fullName, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('student-status', 'Update');
?>
<div class="student-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
