<?php

use app\modules\backend\models\Year;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\modules\common\models\StudentStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-status-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'yearId')->dropDownList(ArrayHelper::map(Year::find()->all(), 'id', 'name'),
                [
                    'id' => 'student-status-year',
                    'prompt' => 'Выберите год',
                ]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'groupId')->widget(DepDrop::classname(), [
                'model' => $model,
                'attribute' => 'groupId',
                'options' => ['id' => 'student-status-group', 'placeholder' => 'Выберите группу'],
                'pluginOptions' => [
                    'url' => Url::to(['/api/groups/list-by-year-ajax']),
                    'depends' => ['student-status-year'],
                    'placeholder' => 'Выберите группу',
                ],

            ]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'studentId')->widget(DepDrop::classname(), [
                'model' => $model,
                'attribute' => 'studentId',
                'pluginOptions' => [
                    'url' => Url::to(['/api/student/list-by-year-ajax']),
                    'depends' => ['student-status-group'],
                    'placeholder' => 'Выберите студента',
                ],
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->dropDownList([
                'active' => Yii::t('student-status', 'active'),
                'inactive' => Yii::t('student-status', 'inactive'),
                'academy' => Yii::t('student-status', 'academy'),
            ],
                ['prompt' => ''])
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'date_out')->widget(DatePicker::classname(), [
                'options' => ['class' => 'form-control'],
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'date_in')->widget(DatePicker::classname(), [
                'options' => ['class' => 'form-control'],
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
            ]) ?>
        </div>
    </div>


    <?= $form->field($model, 'desc')->textarea(['rows' => 6]) ?>


    <!--    --><? //= $form->field($model, 'group_id')->widget(DepDrop::classname(), [
    //        'options' => ['id' => 'group-id'],
    //       'data' =>  [$model->studentGroup->id => $model->studentGroup->group->name],
    //        'type' => DepDrop::TYPE_SELECT2,
    //        'pluginOptions' => [
    //                //'initialize ' =>true,
    //            'depends' => ['year-id'],
    //            'placeholder' => Yii::t('student-status', 'Select a group ...'),
    //            'url' => Url::to(['/backend/student-status/get-json-group'])
    //        ]
    //    ]);
    //    ?>
    <!---->
    <!--    --><? //= $form->field($model, 'profile_id')->widget(DepDrop::classname(), [
    //        'options' => ['id' => 'profile-id'],
    //        'data' => [ $model->profile->user_id  =>$model->profile->fullName],
    //        'type' => DepDrop::TYPE_SELECT2,
    //        'pluginOptions' => [
    //            'depends' => ['year-id', 'group-id'],
    //            'placeholder' => Yii::t('student-status', 'Select a student ...'),
    //            'url' => Url::to(['/backend/student-status/get-json-profiles'])
    //        ]
    //    ]); ?>

    <?= $form->field($model, 'semestr')->dropDownList([
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',

    ], ['prompt' => '']) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('buttons', 'Create') : Yii::t('buttons', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
