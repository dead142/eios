<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\common\models\search\StudentStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('student-status', 'Student Statuses');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-status-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('student-status', 'Create Student Status'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'attribute' => 'fullName',
                'value' => 'studentGroup.student.fullName',

            ],
//            [
//                'attribute' => 'group',
//                'value' => function ($model) {
//                    return \app\modules\backend\models\StudentGroup::StudentGroup($model->profile_id)->group->name;
//                }
//
//            ],

            // 'desc:ntext',

            'semestr',
            'create_at',
            // 'update_at',
            // 'create_by',
            // 'date_out',
            // 'date_in',
            [
                'attribute' => 'name',
                'value' => function ($model) {
                    if ($model->name == 'inactive' || $model->name == 'academy') {
                        return '<span class="label label-danger">' . Yii::t('student-status', Yii::t('student-status', $model->name)) . '</span>';
                    } else if ($model->name === 'active') {
                        return '<span class="label label-success">' . Yii::t('student-status', Yii::t('student-status', $model->name)) . '</span>';
                    }
                },
                'format' => 'html',
                'filter' => [
                    'active' => Yii::t('student-status', 'Active'),
                    'inactive' => Yii::t('student-status', 'Inactive'),
                    'academy' => Yii::t('student-status', 'Academy'),
                ]
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
