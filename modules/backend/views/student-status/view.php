<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\common\models\StudentStatus */

$this->title = $model->studentGroup->student->fullName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('student-status', 'Student Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-status-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('buttons', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('student-status', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'name',
                'value' => function ($model) {
                    if ($model->name == 'inactive' || $model->name == 'academy') {
                        return '<span class="label label-danger">' . Yii::t('student-status', Yii::t('student-status', $model->name)) . '</span>';
                    } else if ($model->name === 'active') {
                        return '<span class="label label-success">' . Yii::t('student-status', Yii::t('student-status', $model->name)) . '</span>';
                    }
                },
                'format' => 'html'
            ],
            'date_out',
            'date_in',
            'desc:ntext',
            'semestr',


            'studentGroup.group.name',
            [
                'attribute' => 'create_by',
                'value' => function ($model) {
                    return $model->profile->fullName;
                },
            ],
            'create_at',
            'update_at',
        ],
    ]) ?>

</div>
