<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\common\models\KindInvolvement */

$this->title = Yii::t('portfolio', 'Create Kind Involvement');
$this->params['breadcrumbs'][] = ['label' => Yii::t('portfolio', 'Kind Involvements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kind-involvement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
