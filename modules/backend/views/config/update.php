<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\backend\models\Config */


$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\main', 'Config'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('backend\main', 'Update');
?>
<div class="config-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
