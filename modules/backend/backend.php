<?php

namespace app\modules\backend;

class backend extends \yii\base\Module
{

    public $layout = 'backend';
    public $controllerNamespace = 'app\modules\backend\controllers';


    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
