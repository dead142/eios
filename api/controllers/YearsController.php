<?php

namespace app\api\controllers;
use app\modules\backend\models\Year;
use yii\helpers\Json;
use app\modules\backend\models\StudentGroup;
use yii\web\Controller;


class YearsController extends Controller
{
    /**
     * Метод возврашает студетов  группы
     * ['id' => id, 'name' => FIO]
     */
    public function actionChildsYearAjax()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $year_id = $parents[0];
                $out     = Year::find()
                    ->select(
                        [
                            'name',
                            'id',
                        ])->where(['>','id', $year_id])
                    ->asArray()
                    ->all();
                return ['output' => $out, 'selected' => ''];
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }
}