<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 08.06.2018
 * Time: 1:03
 */

namespace app\api\controllers;
use yii\helpers\Json;
use app\modules\backend\models\StudentGroup;
use yii\web\Controller;


class StudentController extends Controller
{
    /**
     * Метод возврашает студетов  группы
     * ['id' => id, 'name' => FIO]
     */
    public function actionListByYearAjax()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $group_id = $parents[0];
                $out     = StudentGroup::find()->leftJoin('profile','profile.user_id=student_group.student_id')
                    ->select(
                        [
                            'profile.user_id AS id',
                            'CONCAT(profile.name," " , profile.surname, " ", profile.middlename) AS name',
                        ])->where(['student_group.group_id' => $group_id])
                    ->asArray()
                    ->all();
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }
}