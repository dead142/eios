<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 08.06.2018
 * Time: 1:00
 */

namespace app\api\controllers;
use yii\helpers\Json;
use app\modules\backend\models\Groups;


use yii\web\Controller;

class GroupsController extends Controller
{
    /**
     * Метод возврашает список групп года
     * ['id' => id, 'name' => Year Name]
     */
    public function actionListByYearAjax()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $id_year = $parents[0];
                $out     = Groups::find()->select(['id', "CONCAT (name, \" курс: \", course) as name"])->where(['id_year' => $id_year])->asArray()->all();
                return ['output' => $out, 'selected' => ''];

            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }
}