/**
 * Created by Администратор on 24.03.2016.
 */
$( document ).ready(function() {
    console.log(window.location.href);
    $('#isu-comments').append(
        '<div class="row">'+
            '<div class="col-sm-10">'+
                '<div class="input-group input-group-lg">'+
                    '<span class="input-group-addon" id="sizing-addon1"><span class="glyphicon glyphicon-user"></span> </span>'+
                    '<input type="text" class="form-control" placeholder="Оставить рецензию или комментарий" aria-describedby="sizing-addon" id="comment-field">'+
                '</div>'+
            '</div>'+
            '<div class="col-sm-2">'+
                '<button type="button" class="btn btn-primary btn-lg" id="send-comment">Комментировать</button>'+
            '</div>'+
        '</div>'
    );


    $.ajax({
        type: "POST",
        data: {entity: window.location.href},
        url: "/web/frontend/comment/get",
        success: function (comments)
        {
            $('#isu-comments').append(
                '<div id="comments_content">'+
                '</div>'
            );

            $.each(comments, function(index, comment) {

                var deleteIcon = '';
                if (comment.CREATED_BY_ME)
                {
                    deleteIcon = '<button type="button" class="btn btn-default trash-button" value="'+comment.ID+'">'+
                    '<span class="glyphicon glyphicon-trash trash"></span>'+
                    '</button>';
                }

                $('#comments_content').append(
                    '<div class="row comments-content">'+
                        '<div class="col-sm-2">'+
                            '<span class="glyphicon glyphicon-user picture"></span>'+
                        '</div>'+
                        '<div class="col-sm-9">'+
                            '<a href="/frontend/default/all?id='+comment.CREATED_BY+'">'+
                                '<div class="row name">'+
                                    comment.SURNAME + ' ' + comment.NAME + ' ' + comment.MIDDLENAME +
                                '</div>'+
                            '</a>'+
                            '<div class="row">'+
                            'Прокомментировал: '+comment.TEXT+
                            '</div>'+
                            '<div class="row date">'+
                            comment.CREATED_AT +
                            '</div>'+
                        '</div>'+

                        '<div class="col-sm-1">'+
                            deleteIcon+
                        '</div>'+
                    '</div>'
                )
            });
        }
    });

    $(document).on ("click", "#send-comment", function () {
        var comment = $('#comment-field').val();
        if (comment)
        {
            $.ajax({
                type: "POST",
                data: {text: comment, entity: window.location.href},
                url: "/web/frontend/comment/create",
                success: function (msg)
                {
                    location.reload();
                },
                error: function (error)
                {
                    console.log('error');
                }
            })
        }
    });

    $(document).on ("click", ".trash-button", function () {

        $.ajax({
            type: "POST",
            data: {comment_id: $(this).attr("value")},
            url: "/web/frontend/comment/delete",
            success: function (msg)
            {
                console.log(msg);
                location.reload();
            },
            error: function (error)
            {
                console.log('error');
            }
        })
    });
});