Электронная информационно-образовательная среда (ЭИОС) вуза
============================
 
После обновления от 13.08.2017 выполнить!
-------------------
php yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      messages/           contains i18n
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      modules/  
         backend/
         frontend/
         common/          contains common models
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.
1. `syntax error, unexpected '::' (T_PAAMAYIM_NEKUDOTAYIM)  `
        
        Причина: версия php < 7
  
**Решение**
 В файле `dektrium/yii2-user/models/UserSearch.php`

```
#!php

-        $table_name = $query->modelClass::tableName();
+        $model = $query->modelClass;
+        $table_name = $model::tableName();
```
 2. `Getting unknown property: ::last_login_at  `
 
    Причина: ??? что - то с кешем базы данных
**Решение** 
 

```
#!php

Yii::$app->db->schema->refresh();
 ```