<?php
namespace tests\codeception\unit\models;

use Yii;
use yii\codeception\TestCase;
use app\models\LoginForm;
use Codeception\Specify;

class MainHelperTest extends TestCase
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }
    /**
     * @dataProvider providerVote
     */
    public function testVote($a,$b)
    {
        $publication = new \app\helpers\MainHelper();
       // $this->assertEquals(2,$publication->setRating('Publications',5));
        $this->assertEquals($a,$publication->num($b));
    }

    /**
     *@dataProvider
     */
    public function providerVote()
    {
        return array(
            array(0, 0,),
            array(0, 1,),
            array(1,  1),
            array(3,  3)
        );
    }

}