29.06.2018 версия 1.9

**Назначение:** у каждой группы может быть только один родитель, так как группы переходят линейно

1. Добавлен индекс p_id_IDX в таблицу groups
2. SQL для выполнения добавления (tested) 
```sql
INSERT INTO migration (version, apply_time) VALUES ('m180629_185445_set_p_id_uniq_in_groups', UNIX_TIMESTAMP());
CREATE UNIQUE INDEX p_id_IDX ON groups (p_id)
```
2. SQL для выполнения отмены изменений (tested) 
```sql
DELETE FROM migration WHERE version = 'm180629_185445_set_p_id_uniq_in_groups'
DROP INDEX p_id_IDX ON groups;
```

