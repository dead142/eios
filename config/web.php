<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log' ],
    'defaultRoute' => 'frontend/default',
    // set target language to be Russian
    'language' => 'ru-RU',
    'charset'=>'utf-8',
    // set source language to be English
    'sourceLanguage' => 'en-EN',
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            'root' => [
                'path' => 'uploads',
                'name' => 'Files'
            ],
            'watermark' => [
                'source' => __DIR__ . '/logo.png', // Path to Water mark image
                'marginRight' => 5,          // Margin right pixel
                'marginBottom' => 5,          // Margin bottom pixel
                'quality' => 95,         // JPEG image save quality
                'transparency' => 70,         // Water mark image transparency ( other than PNG )
                'targetType' => IMG_GIF | IMG_JPG | IMG_PNG | IMG_WBMP, // Target image formats ( bit-field )
                'targetMinPixel' => 200         // Target image minimum pixel size
            ]
        ]
    ],
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/modules/backend/views/user',
                    '@limion/jqueryfileupload/views' => '@app/modules/backend/views/limon' //Upload file in settings/profile
                ],
            ],
        ],
        'session' => [
            'class' => 'app\components\Session',
        ],
        'formatter' => [
            'dateFormat' => 'd-M-Y',
            'datetimeFormat' => 'd-M-Y H:i:s',
            'timeFormat' => 'H:i:s',



            'nullDisplay' => '&nbsp;',
        ],
        'i18n' => [
            'translations' => [

                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => 'app\modules\university\message',
                    'sourceLanguage' => 'en-EN',
                    'fileMap' => [
                        'frontend/main' => 'frontend/main.php',
                        'backend/main' => 'backend/main.php',
                        'degrees' => 'frontend/degrees.php',
                        'projects' => 'frontend/projects.php',
                        'awards' => 'frontend/awards.php',
                        'social' => 'frontend/social.php',
                        'qualification' => 'frontend/qualification.php',
                        'intellectual' => 'frontend/intellectual.php',
                        'conferences' => 'frontend/conferences.php',
                        'publication' => 'frontend/publication.php',
                        'organization' => 'frontend/organization.php',
                        'internship' => 'frontend/internship.php',
                        'reviews' => 'frontend/reviews.php',
                        'groups' => 'frontend/groups.php',
                        'buttons' => 'frontend/buttons.php',
                        'Bbuttons' => 'backend/buttons.php',
                        'portfolio' => 'backend/portfolio.php',
                        'materials' => 'backend/materials.php',
                        'evaluation' => 'backend/evaluation.php',

                        'exception' => 'exception.php',
                        'B-User' => 'backend/user.php',
                        'backend' => 'backend.php',
                        # common models
                        'student-status'=>'common/student-status.php',

                        //'default' => 'university/default.php',
                        // 'app/error' => 'error.php',
                    ],

                ],
            ],
        ],
        'authClientCollection' => [
            'class' => \yii\authclient\Collection::className(),
            'clients' => [
                'google' => [
                    'class' => 'dektrium\user\clients\Google',
                    //'clientId' => '804019764065-mlhjune1klq0f0jjgn9bnmsbv00t5lvg.apps.googleusercontent.com',
                    // 'clientSecret' => 'SJFvhOwU_E-1bJkGeI_iLGKJ',
                    /* http://st.eeip.ru/ */
                    'clientId' => '804019764065-3e09m1oq2jacdaj1tj76ghfqqnbeib19.apps.googleusercontent.com',
                    'clientSecret' => 'IJdD52xz26yrlDRYo0WM5tEe',
                ],
            ],],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'sada',
            'enableCsrfValidation' => false,

        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,

            //  'rules' => [
            //       '<controller>/<action>' => '<controller>/<action>',
            //  ]

        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'modules' => [
        'modules' => [
            'simplechat' => [
                'class' => '\bubasuma\simplechat\Module',
            ],
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',

        ],

        'social' => [
            // the module class
            'class' => 'kartik\social\Module',

            // the global settings for the disqus widget
            'disqus' => [
                'settings' => ['shortname' => 'isusys'] // default settings
            ],

        ],
        # dektrium/yii2-user
        # work only with php 7!
        'user' => [
            'class' => 'dektrium\user\Module',
            'modelMap' => [
                'User' => 'app\models\User',
                'Profile' => 'app\models\Profile',
                'UserSearch' => 'app\models\UserSearch',
            ],

            'components' => [
                'manager' => [
                    'class' => 'common\models\User',
                ],
            ],

            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['admin']
        ],
        # dektrium/yii2-rbac
        # https://github.com/dektrium/yii2-rbac/tree/master/docs
        'rbac' => [
            'class' => 'dektrium\rbac\RbacWebModule',
        ],

        'frontend' => [
            'class' => 'app\modules\frontend\frontend',
        ],
        'backend' => [
            'class' => 'app\modules\backend\backend',
            //'defaultRoute' => 'materials/index'
        ],
        'api' => [
            'class' => 'app\api\Api',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
     //configuration adjustments for 'dev' environment
     $config['bootstrap'][] = 'debug';
     $config['modules']['debug'] = [
          'class' => 'yii\debug\Module',
     ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
