<?php
namespace app\helpers;
use app\modules\frontend\controllers\PublicationController;
use yii\db\ActiveRecord;
use yii\db\ActiveRecordInterface;
use yii\web\NotFoundHttpException;

/**
 * Class MainHelper
 * @package app\helpers
 * @param $classname ActiveRecord
 */
class MainHelper {
    /**
     * String to translit
     * @param $string
     * @return mixed|string
     */
    public static function translit($string) {
        $string = (string) $string; // преобразуем в строковое значение
        $string = strip_tags($string); // убираем HTML-теги
        $string = str_replace(array("\n", "\r"), " ", $string); // убираем перевод каретки
        $string = preg_replace("/\s+/", ' ', $string); // удаляем повторяющие пробелы
        $string = trim($string); // убираем пробелы в начале и конце строки
        $string = function_exists('mb_strtolower') ? mb_strtolower($string) : strtolower($string); // переводим строку в нижний регистр (иногда надо задать локаль)
        $string = strtr($string, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $string = preg_replace("/[^0-9a-z-_ ]/i", "", $string); // очищаем строку от недопустимых символов
        $string = str_replace(" ", "-", $string); // заменяем пробелы знаком минус
        return $string; // возвращаем результат
    }

    /**
     * Генерирует случаную строку
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    /**
     * @param $classname
     * @param $id
     * @return float|int
     * @throws NotFoundHttpException
     */
    public function setRating($classname,$id){

        $model = $this->findModel($classname::classname(),$id);
        $model->rating =  ($model->rating+\Yii::$app->request->post('rating'))/2;
        $model->save();
        return $model->rating;
    }

    public function findModel($classname,$id){
        if (($model = $classname::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function num($num){
        return $num;
    }


}