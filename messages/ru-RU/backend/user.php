<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 14.08.2017
 * Time: 1:42
 */
return array(
    'name'   =>  'Имя',
    'surname'   =>  'Фамилия',
    'middlename'   =>  'Отчество',
    'Upload photo' => 'Загрузить фотографию',
    'User role' => 'Роль',
    'fullName' => 'ФИО',
    'teacher' => 'Преподаватель',
    'student' => 'Студент',
    'admins' => 'Администратор',

);