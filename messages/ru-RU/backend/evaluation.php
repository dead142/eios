<?php

return array(
    /* Model Year */
    'Year Start' => 'Год начала учебного года',
    'Year End' => 'Год окончания учебного года',
    'Name' => 'Краткое название',
    /* Model Group */
    'Name group' => 'Номер группы',
    'Groups' => 'Группы',
    'Course' => 'Курс',
    'Current study' => 'Обучение в текущем году',
    'Create and edit groups' => 'Создание и редактирование групп',
    /* Discipline */
    'Disciplines of groups' => 'Дисциплины группы',
    'Add discipline to group' => 'Добавить дисциплины группе',
    'Discipline' => 'Дисциплина',
    'Teacher' => 'Преподаватель',
    'Semestr' => 'Семестр',
    'Name discipline' => 'Название дисциплины',
    'Create At' => 'Добавлена',
    'Create By' => 'Создана ',
    'Create Disciplines' => 'Добавить дисциплины',
    'Disciplines' => 'Дисциплины',

    /* Group */
     'year_name' => 'Название года',
   // 'Name group' => 'Номер группы', it in model
    'Id Year' => 'Года обучения',
    'Created At' => 'Добавлен',
    'Create Groups' => 'Создать группу',
    'Update {modelClass}: ' => 'Обновить Группу: ',
   /* Discipline and groups*/
    'Select teacher' => 'Выберите преподавателя',
    'Groups Disciplines' => 'Добавление дисциплин к группам',
    'Create Groups Discipline' => 'Добавить дисциплину к группе',
    /* Year*/
    'Create Year' => 'Добавить год обучения',
    'Years' => 'Года обучения',
    /* Grades */
    'Create control' => 'Добавить форму контроля',
    'Save grades' => 'Сохранить оценки',
    'FIO' => 'ФИО',
    /* Ащкь Grades */
    'Type' => 'Тип контроля',
    'Date of control' => 'Дата проведения',
    'Groups Discipline ID' => 'Дисциплина',
    'current control' => 'Текущий контроль',
    'intermediate control' => 'Промежуточный контроль',
    'Create Formgrade' => 'Добавить форму контроля',
    'Formgrades' => 'Формы контроля',

    /* buttons */
    'Search' => 'Найти',
    'Reset' => 'Сбросить',
    'Update' => 'Обновить',
    'Delete' => 'Удалить',
    'Create' => 'Создать',
    'Active' => 'Группа обучается',
    'Year start' => 'Год начала обучения'

);