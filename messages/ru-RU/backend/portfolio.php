<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.11.2015
 * Time: 23:23
 */
return array(
    /* Area*/
    'Name' => 'Название',
    'Areas' => 'Области наук',
    'Create Area' => 'Добавить область наук',

    'Documents' => 'Виды документа',
    'Create Document' => 'Добавить вид документ',

    'Organizations' => 'Организации',
    'Create Organization' => 'Добавить организацию',

    'Statuses' => 'Статусы мероприятий',
    'Create Status' => 'Добавить статус',

    'Type Events' => 'Типы мероприятий',
    'Create Type Event' => 'Добавить типы мероприятий',

    'Levels' => 'Уровни мероприятий',
    'Create Level' => 'Добавить уровни мероприятий',

    'Type Participations' => 'Виды участия в общественной деятельности',
    'Create Type Participation' => 'Добавить вид участия',

    'Kind Involvements' => 'Виды участия в конференциях',
    'Create Kind Involvement' => 'Добавить вид участия',

    'Type Editions' => 'Типы изданий',
    'Create Type Edition' => 'Добавить типы изданий',

    'Langs' => 'Языки',
    'Create Lang' => 'Добавить языки',

    'Countries' => 'Страны',
    'Create Country' => 'Добавить страны',

    'Indexes' => 'Индексы цитирования',
    'Create Indexes' => 'Добавить индексы цитирования',

    /* main layout*/

);