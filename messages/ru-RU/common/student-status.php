<?php
return [
    #model
    'Name'=>'Статус',
    'Desc'=>'Комментарий',
    'Year'=>'Год обучения',
    'Semestr'=>'Семестр обучения',
    'Create At'=>'Запись создана',
    'Update At'=>'Изменена',
    'Create By'=>'Автор записи',
    'Date Out'=>'Дата отчисления/академического отпуска',
    'Date In'=>'Дата возобновления обучения',
    'Group'=>'Группа',
    'Profile'=>'ФИО студента',
    'fullName'=>'ФИО студента',
    'Course'=>'Курс', // см. Semestr

    #form
    'active' => 'Студент обучается',
    'inactive' => 'Студент отчислен',
    'academy' => 'Студент в академическом отпуске',
    'Student Statuses' => 'Статусы студентов',
    'Create Student Status' => 'Добавить статус',
    'Update status: ' => 'Обновить статус: ',
        #hints
    'Select a year ...'=>'Выберите год обучения',
    'Select a group ...'=>'Выберите группу',
    'Select a student ...'=>'Выберите студента',
];