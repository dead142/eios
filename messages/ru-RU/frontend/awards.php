<?php
return array(
    'Awards' => 'Премии, награды, дипломы',
    'Create Awards' => 'Добавить премию, награду или диплом',
    'Update : ' => 'Обновить: ',
    // Database
    'Name' => 'Название',
    'Organization  ID' => 'Название организации',
    'Document  ID' => 'Название документа',
    'Issue  Date' => 'Дата выдачи документа',
    'Desc' => 'Описание',
    'Date  Create' => 'Дата создания',
    'Date  Update' => 'Дата обновления',
    //form
    'Create' => 'Сохранить',
    'Update' => 'Обновить',
    'Delete' => 'Удалить',



);