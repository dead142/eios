<?php
return array(
    'Intellectuals' => 'Объекты интеллектуальной собственности',
    'Create Intellectual' => 'Добавить объект интеллектуальной собственности',
    'Update {modelClass}: ' => 'Обновить объект интеллектуальной собственности:',
    // Database
    'Name' => 'Название',
    'copyright' => 'Правообладатель',
    'Organization  ID' => 'Название организации',
    'Document  ID' => 'Название документа',
    'Document' => 'Номер документа',
    'Date  Issue' => 'Дата выдачи',
    'Desc' => 'Описание',
    'Date  Create' => 'Дата создания',
    'Date  Update' => 'Дата обновления',
    //form
    'Create' => 'Сохранить',
    'Update' => 'Обновить',
    'Delete' => 'Удалить',


);