<?php
return array(
    'Conferences' => 'Конференции, школы, симпозиумы, семинары',
    'Create Conferences' => 'Добавить конференции, школы, симпозиумы, семинары',
    'Update {modelClass}: ' => 'Обновить конференции, школы, симпозиумы, семинары:',
    // Database
    'Name' => 'Название',
    'Location' => 'Место проведения',
    'Date  Start' => 'Дата начала',
    'Date  End' => 'Дата окончания',
    'Status  ID' => 'Статус мероприятия',
    'Type of participation' => 'Вид участия',
    'Name  Report' => 'Название доклада (экспоната)',
    'Number  Nir' => 'Номер НИР',
    'Date  Create' => 'Дата создания',
    'Date  Update' => 'Дата обновления',
    //form
    'Create' => 'Сохранить',
    'Update' => 'Обновить',
    'Delete' => 'Удалить',


);