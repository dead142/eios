<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 14.06.2016
 * Time: 23:43
 * Translation
 */
return array(
    'Internships' => 'Прохождение практик, стажировок',
    'Create Internship' => 'Добавить информацию о стажировках, прохождении практик',
    'Update {modelClass}: ' => 'Обновить информацию о стажировках, прохождении практик:',
    // Model
    'ID' => 'Уникальный идентификатор',
    'Name' => 'Название',
    'Date  Start' => 'Дата начала',
    'Date  End' => 'Дата окончания',
    'Date  Create' => 'Дата создания',
    'User  ID' => 'Автор',
    'Country  ID' => 'Страна',
    'FILE' => 'Файл',



);