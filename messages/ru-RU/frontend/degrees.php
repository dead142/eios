<?php

return array(
    'Degrees' => 'Степени, звания',
    'Create Degree' => 'Добавить степень, звание',
    'Update Degree' => 'Обновить запись',
    'Update {modelClass}: ' => 'Обновить запись:',
    'Delete Degree' => 'Удалить запись',

    'Name' => 'Название',
    'Area of science'=> 'Область науки',
    'The name of the organization'=> 'Название организации',
    'The document TITLE'=> 'Название документа ',
    'Date of issue'=> 'Дата выдачи',
    'The document number'=> 'Вид документа',
    'Date  Create'=> 'Дата создания записи',
    'DATE_UPDATE'=> 'Дата обновления записи',
    'Add organization to Database'=> 'Добавить организацию в БД',
    'DOCUMENT_NAME'=> 'Название документа ',

    'Create' => 'Создать',
    'Save' => 'Сохранить',
    'Update' => 'Обновить',

);