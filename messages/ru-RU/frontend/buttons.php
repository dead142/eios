<?php

return array(
    'Search' => 'Найти',
    'Reset' => 'Сбросить',
    'Create question' => 'Задать вопрос',
    'Create' => 'Создать',
    'Update' => 'Обновить',
    'Delete' => 'Удалить',
    /*  Placeholders*/
    'Select country' => 'Выберите страну',

);