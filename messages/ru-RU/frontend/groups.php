<?php

return array(
    'Group' => 'Группа',
    'Groups and students' => 'Группы и студенты',
    'My group' => 'Моя группа',
    'Name' => 'Имя ',
    'Surname' => 'Фамилия',
    'Middlename' => 'Отчество',
    'Full Student Name' => 'ФИО студента',
    'Group name' => 'Номер группы',
    'Show students group' => 'Список студентов группы',
    'Year name' => 'Год',
    'Disciplines group' => 'Дисциплины группы',
);