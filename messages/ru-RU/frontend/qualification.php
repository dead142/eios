<?php
return array(
    'Qualifications' => 'Повышение квалификации',
    'Create Qualification' => 'Добавить повышение квалификации',
    'Update {modelClass}: ' => 'Обновить повышение квалификации:',
    // Database
    'Name' => 'Название',
    'Location' => 'Место проведения',
    'Date  Start' => 'Дата начала',
    'Date  End' => 'Дата окончания',
    'Organization  ID' => 'Название организации',
    'Document  ID' => 'Название документа',
    'Date  Issue' => 'Дата выдачи',
    'Hours' => 'Объем в часах',
    'Date  Create' => 'Дата создания',
    'Date  Update' => 'Дата обновления',
    //form
    'Create' => 'Сохранить',
    'Update' => 'Обновить',
    'Delete' => 'Удалить',


);