<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.11.2015
 * Time: 23:23
 */
return array(
    /*index controller */
    'Chat' => 'Чат ',
    /* main layout*/
    'Groups' => 'Группы',
    'MyGroup' => 'Моя группа',
    'Find student' => 'Найти студента',
    'Portfolio' => 'Мое портфолио',
    'Profile' => 'Мой профиль',
    'backend' => 'Администрирование',
    'All activity'=>'Посмотреть все достижения',
    'Progress'=>'Результаты промежуточной аттестации<br> и освоения ООП',
    'Sign in'=>'Войти в систему',
    'Sign out'=>'Выйти из системы',
    /*  Degrees */
    'Degrees and titles' => 'Степени и звания',
    'Add Degrees and titles'=> 'Добавить звание или степень',
    'See my degrees and titles' => 'Посмотреть свои степени и звания',
    /*  Projects */
    'Projects and grants' => 'Проекты и гранты',
    'Add projects and grants' => 'Добавить проект или грант',
    'See my projects and grants' => 'Посмотреть свои проекты и гранты',
    /*  Awards */
    'Prizes, awards, diplomas' => 'Премии, награды, дипломы',
    'Add prizes, awards, diplomas' => 'Добавить премию, награду или диплом',
    'See my prizes, awards, diplomas' => 'Посмотреть свои премии, награды, дипломы',
    /*  Active */
    'Social, cultural, sports activities' => 'Общественная, культурно-творческая, спортивная деятельность',
     'Add social, cultural, sports activities' => 'Добавление общественной, культурно-творческой, спортивной деятельности',
    'See my social, cultural, sports activities' => 'Посмотреть свою общественную, культурно-творческую, спортивную деятельность',
    /*  Training */
    'Training' => 'Повышение квалификации',
    'Add training' => 'Добавление повышения квалификации',
    'See my training' => 'Посмотреть свои повышения квалификации',
    /*  Intellectual  */
    'Intellectual' => 'Объекты интеллектуальной собственности',
    'Add Intellectual' => 'Добавление объектов интеллектуальной собственности',
    'See my Intellectual' => 'Посмотреть свои объекты интеллектуальной собственности',
    /*  Conferences, schools, workshops, seminars*/
    'Conferences, schools, workshops, seminars' => 'Конференции, школы, симпозиумы, семинары',
    'Add conferences, schools, workshops, seminars' => 'Добавление конференции, школ, симпозиумов, семинаров',
    'See my conferences, schools, workshops, seminars' => 'Посмотреть свои конференции, школы, симпозиумы, семинары',
    /*  Monographs, collections , articles*/
    'Monographs, collections , articles' => 'Работы - курсовые, контрольные, монографии, сборники, статьи',
    'Add monographs, collections , articles' => 'Добавление кусровых, контрольных, монографий, сборников, стататей',
    'See my monographs, collections , articles' => 'Посмотреть свои курсовые, контрольные, монографии, сборники, статьи',
    'More...' => 'Перейти к разделу',
    /* Internship */
    'Internship' => 'Прохождение практик, стажировок',
    /*Reviews*/
    'Reviews' => 'Рецензии, отзывы, рекомендательные письма',
    /*  Grades Control*/
    'Main grades control' => 'Панель быстрого доступа',
    'Grades control' => 'Управление успеваемостью',
    'Student grades control' => 'Оценки студентов',
    'Grade types control' => 'Типы аттестаций',
    'Groups control' => 'Учебные группы',
    'Discipline groups control' => 'Учебные группы дисциплин',
    'Disciplines control' => 'Управление дисциплинами',
    'My disciplines control' => 'Мои дисциплины',
    'Discipline grades control' => 'Управление оценками студентов',
//    'GroupsControl' => 'Группы',
    'Portfolio user' =>'Портфолио пользователя',
    'Grade user' =>'Оценки пользователя',
    'See' =>'Поcмотреть',
    /* qoestions */
    'Question' =>'Вопрос',
    'Question short' =>'Заголовок вопроса',
    'Question Create At' =>'Добавлен',
    'Answer' =>'Ответ',
    'Questions' =>'Вопросы и ответы',
    'Create Questions' =>'Задать вопрос',
    'We get you answer. Wait a litle bit' =>'Ваш вопрос отправлен. Как только администратор ответит, вы сможете найти ответ в этом разделе',
    'pass' =>'Зачет',
    'fail' =>'Незачет',
    'Excellent' =>'Отлично',
    'Good' =>'Хорошо',
    'Satisfactorily' =>'Удовлетворительно',
    'Unsatisfactorily' =>'Неудовлетворительно',
    'Failure to appear' =>'Неявка',
    'Hospital' =>'Больничный',


);