<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 14.06.2016
 * Time: 23:43
 * Translation
 */
return array(
    'Reviews' => 'Рецензии, отзывы, рекомендательные письма',
    'Create Reviews' => 'Добавить информацию о стажировках, прохождении практик',
    'Update {modelClass}: ' => 'Обновить информацию о стажировках, прохождении практик:',
    // Model
    'ID' => 'Уникальный идентификатор',
    'Name' => 'Название',
    'Author' => 'Автор отзыва, рецензии',
    'Organization  ID' => 'Организация',
    'Create  Date' => 'Дата создания',
    'User  ID' => 'Автор',



);