<?php
return array(
    'Projects' => 'Проекты, гранты',
    'Create Projects' => 'Добавить проект или грант',
    'Delete' => 'Удалить',
    'Update {modelClass}: ' => 'Обновить :',
    // Database
    'Name' => 'Название',
    'Role' => 'Роль',
    'The title of the competition' => 'Название конкурса',
    'Title  Section' => 'Название раздела конкурса',
    'Code system project contest' => 'Код проекта по системе конкурса',
    'The winner of the contest' => 'Победитель конкурса',
    'Organization  ID' => 'Название организации',
    'Document  ID' => 'Название документа',
    'document number' => 'Номер документа',
    'Date  Issue' => 'Дата выдачи документа',
    'Date  Start' => 'Дата начала работы',
    'Date  End' => 'Дата окончания работы',
    'Funding' => 'Объем финансирования',
    'currency unit' => 'Денежная единица',
    'Date  Create' => 'Дата создания',
    'Date  Update' => 'Дата обновления',
    // form
    'No' => 'Нет',
    'Yes' => 'Да',
    'Create' => 'Сохранить',
    'Update' => 'Обновить',



);