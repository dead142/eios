<?php
return [
    'Set user to group' =>'Привязать пользователя к группе',
    'FIO user' =>'ФИО пользователя',
    'Group' =>'Группа',
    'Select a group' =>'Выберите группу',
    'Connect' =>'Соединить',
];