<?php
namespace app\components;

use Yii;

class Session extends \yii\web\Session {

    public function getAllFlashesNormalized() {
        $flashes = [];
        foreach (Yii::$app->session->getAllFlashes() as $key => $flash) {
            if (is_array($flash))
                foreach ($flash AS $message)
                    $flashes[] = ['key' => $key, 'message' => $message];
            else
                $flashes[] = ['key' => $key, 'message' => $flash];
        }

        return $flashes;
    }
}