<?php
/**
 * Created by PhpStorm.
 * User: Andrei
 * Date: 07.06.2018
 * Time: 23:18
 */

namespace app\commands;


use app\modules\backend\models\StudentGroup;
use app\modules\common\models\StudentStatus;
use yii\console\Controller;
use yii\helpers\VarDumper;

class StatusStudentController extends Controller
{
    public $message;

    public function options($actionID)
    {
        return ['message'];
    }

//    public function optionAliases()
//    {
//        return ['m' => 'message'];
//    }
    /**
     * Метод используется для создания начальных статусов всех студентов
     * use yii status-student
     */
    public function actionIndex()
    {
        $models = StudentGroup::find()->all();
        foreach ($models as $model) {
            if (StudentStatus::findOne(['student_group_id' => $model->id]) == NULL) {
                $status = new StudentStatus();
                $status->name = 'active';
                $status->desc = 'Создано автоматически при инициализации системы';
                $status->semestr = NULL;
                $status->date_in = $model->year->year_start;
                $status->student_group_id = $model->id;
                if ($status->save()) {
                    echo $status->id . " StudentStatus saved \n";
                }

            }
        }

    }

}