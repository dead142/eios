<?php
namespace app\commands;

use app\rbac\AuthorRule;
use Yii;
use yii\console\Controller;
use yii\rbac\Rule;


class RbacrController extends Controller
{
    //http://www.yiiframework.com/doc-2.0/guide-security-authorization.html
    /*
     * Admins наследует все права teacher
     * Example:
     * Teacher CAN 'Contact' action = > Admins CAN 'Contact' action
     * Admins CAN 'About' action
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        //command are here
        //after run yii rbacr/init
        // Создание разрешения
      //  $permission = $auth->createPermission('about');
      //  $permission->description = 'See a about';
      //    $auth->add($permission);
        //Если есть разрешение то присваеиваем его пользователю
      //  $contact = $auth->getPermission('about');
      $student = $auth->getRole('student');  // Получаем роль admins (teacher)
        // add "createPost" permission

        $rule = new \app\rbac\AuthorRule(); ;
        $auth->add($rule);

// add the "updateOwnPost" permission and associate the rule with it.
        $updateOwnPost = $auth->createPermission('updateOwnPost');
        $updateOwnPost->description = 'Update own post';
        $updateOwnPost->ruleName = $rule->name;
        $auth->add($updateOwnPost);

// "updateOwnPost" will be used from "updatePost"
        $auth->addChild($updateOwnPost, 'updatePost');

// allow "author" to update their own posts
        $auth->addChild($student, $updateOwnPost);

    }
}
