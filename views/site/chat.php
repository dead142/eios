<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('frontend/main', 'Chat');
$this->params['breadcrumbs'][] = $this->title;
?>

    <h1><?= Html::encode($this->title) ?></h1>


        <?php

        echo \sintret\chat\ChatRoom::widget([
                'url' => \yii\helpers\Url::to(['site/send-chat']),
                'userModel'=>  \app\models\User::className(),
                'userField'=>'avatarImage'
            ]
        );

        ?>
