﻿<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\helpers\URL;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

$session = Yii::$app->session;
AppAsset::register($this);

$roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
$is_admins = !empty($roles['admins']);
$is_student = !empty($roles['student']);
$is_teacher = !empty($roles['teacher']);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
    $js = <<< 'SCRIPT'
/* To initialize BS3 tooltips set this below */
$(function () {
    $("[data-toggle='tooltip']").tooltip();
});;
/* To initialize BS3 popovers set this below */
$(function () {
    $("[data-toggle='popover']").popover();
});
  function chCSS(id) {

    };
SCRIPT;
    // Register tooltip/popover initialization javascript
    $this->registerJs($js);
    ?>

    <? Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>


    <link id="CSSsource" type="text/css" href="" rel="stylesheet"/>


</head>
<body class="middle">
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '<em style="margin-left: 10px;"> EIEE__<span class="glyphicon glyphicon-pencil"></span></em> <small>Electronic Information  Educational Environment </small>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar  ',
            //'class' => 'nav nav-pills',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],

        'items' => [

            ['label' => '<span class="glyphicon glyphicon-user"></span>  ' . Yii::t('frontend/main', 'Profile'), 'url' => Url::toRoute(['/user/settings/profile'])],

            [
                'label' => '<span class="glyphicon glyphicon-asterisk"></span>  ' . Yii::t('frontend/main', 'backend'),
                'visible' => $is_admins || $is_teacher,
                'url' => ['/backend']
            ],

            [
                'label' => '<span class="glyphicon glyphicon-list"></span>  ' . Yii::t('frontend/main', 'Groups'),
                'visible' => !Yii::$app->user->isGuest,
                'items' => [
                    [
                        'label' => Yii::t('frontend/main', 'MyGroup'), 'url' => Url::toRoute(['/frontend/groups/users-my-group']),
                        'visible' => $is_student
                    ],
                    '<li class="divider"></li>',
                    '<li class="dropdown-header">Find</li>',
                    ['label' => Yii::t('frontend/main', 'Find student'), 'url' => Url::toRoute('/frontend/groups/list-user-group')],

                ],
            ],


            [
                'label' => '<span class="glyphicon glyphicon-calendar"></span>  ' . Yii::t('frontend/main', 'Progress'), 'url' => Url::toRoute(['/frontend/grade/grade-user', 'id' => empty(Yii::$app->user->identity->id) ? NULL : Yii::$app->user->identity->id]),
                'visible' => $is_student,
            ],

            [
                'label' => '<span class="glyphicon glyphicon-book"></span>  ' . Yii::t('frontend/main', 'Portfolio'),
                'items' => [
                    ['label' => Yii::t('frontend/main', 'All activity'), 'url' => Url::toRoute(['/frontend/default/all', 'id' => empty(Yii::$app->user->identity->id) ? NULL : Yii::$app->user->identity->id])],
                    '<li class="divider"></li>',

                    ['label' => Yii::t('frontend/main', 'Monographs, collections , articles'), 'url' => Url::toRoute('/frontend/publication/index')],
                    ['label' => Yii::t('frontend/main', 'Conferences, schools, workshops, seminars'), 'url' => Url::toRoute('/frontend/conferences/index')],
                    ['label' => Yii::t('frontend/main', 'Training'), 'url' => Url::toRoute('/frontend/qualification/index')],
                    ['label' => Yii::t('frontend/main', 'Social, cultural, sports activities'), 'url' => Url::toRoute('/frontend/social/index')],

                    ['label' => Yii::t('frontend/main', 'Degrees and titles'), 'url' => Url::toRoute('/frontend/degree/index')],
                    ['label' => Yii::t('frontend/main', 'Projects and grants'), 'url' => Url::toRoute('/frontend/projects/index')],
                    ['label' => Yii::t('frontend/main', 'Prizes, awards, diplomas'), 'url' => Url::toRoute('/frontend/awards/index')],
                    ['label' => Yii::t('frontend/main', 'Intellectual'), 'url' => Url::toRoute('/frontend/intellectual/index')],
                    ['label' => Yii::t('frontend/main', 'Internship'), 'url' => Url::toRoute('/frontend/internship/index')],
                    ['label' => Yii::t('frontend/main', 'Reviews'), 'url' => Url::toRoute('/frontend/reviews/index')],
                ],
            ],

            // ['label' => 'About', 'url' => ['/site/about']],
            ['label' => '<span class="glyphicon glyphicon-envelope"></span>   ' . Yii::t('frontend/main', 'Chat'), 'url' => ['/site/chat']],
            ['label' => '<span class="glyphicon glyphicon-envelope"></span>   ' . Yii::t('frontend/main', 'Questions'), 'url' => ['/frontend/questions']],
            Yii::$app->user->isGuest ?
                ['label' => Yii::t('frontend/main', 'Sign in'), 'url' => ['/user/security/login']] :
                ['label' => Yii::t('frontend/main', 'Sign out') . '(' . Yii::$app->user->identity->username . ')',
                    'url' => ['/user/security/logout'],
                    'linkOptions' => ['data-method' => 'post']],


        ],
        'encodeLabels' => false,
    ]);
    NavBar::end();
    ?>

    <? $js2 = <<< 'SCRIPT'
         var body = $("body");
        body.removeClass("small").removeClass("middle").removeClass("large").addClass($.cookie("test"));
        if($.cookie("bad") == 0){
            $('#CSSsource').attr('href', '/css/bad.css');
            $('#bad').text('Обычная версия');
        }
        else {
                $('#CSSsource').attr('href', '');

        }


        $(".font-small").click(function(){
            body.removeClass("small").removeClass("middle").removeClass("large").addClass("small");
            $.cookie("test", "small", { expires : 10 });
            return false;
        });
        $(".font-middle").click(function(){
            body.removeClass("small").removeClass("middle").removeClass("large").addClass("middle");
            $.cookie("test", "middle", { expires : 10 });
            return false;
        });
        $(".font-large").click(function(){
            body.removeClass("small").removeClass("middle").removeClass("large").addClass("large");
              $.cookie("test", "large", { expires : 10 });
            return false;
        });

        $('.set-view.font-size a').click(function(){
            $.cookie('font-size', $(this).attr('class').replace('font-', ''), {expires: 1, path: '/'});
        });



        $('#bad').click(function(){
         if($.cookie("bad") == 0){
            $.cookie('bad', "1", {expires: 10, path: '/'});
         }
         else {
             $.cookie('bad', "0", {expires: 10, path: '/'});
         }
         window.location.reload();
            console.log($.cookie("bad"));
        });

        console.log($.cookie("bad"));



SCRIPT;
    // Register tooltip/popover initialization javascript
    $this->registerJs($js2);
    ?>

    <div class="container">
        <div class="set-view font-size">
            <span>Размер шрифта:</span>
            <a href="#" class="font-small"><span class="glyphicon glyphicon-font"></span></a>
            <a href="#" class="font-middle""><span class="glyphicon glyphicon-font"></span></a>
            <a href="#" class="font-large"><span class="glyphicon glyphicon-font"></span></a>

            <a href="#" id="bad" class="font-middle">Версия для слабовидящих</a>
        </div>



    <div>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>

    </div>
    </div>
    <footer class="footer">
        <div class="container">
            <!--      <p class="pull-left"><a href="http://berezhkov.info">@Berezhkov</a> <?= date('Y') ?></p>

        <p class="pull-right">--><? //= Yii::powered() ?><!--</p>-->
        </div>
    </footer>

    <?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
