<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\jui\DatePicker;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;


use app\modules\backend\models\Year;
use app\modules\backend\models\Groups;

/* @var $this yii\web\View */
/* @var $model app\modules\common\models\StudentStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-status-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->dropDownList([
                'active' => Yii::t('student-status','active'),
                'inactive' => Yii::t('student-status','inactive'),
                'academy' => Yii::t('student-status','academy'),
            ],
                ['prompt' => ''])
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'date_out')->widget(DatePicker::classname(), [
                'options' => ['class' => 'form-control'],
                'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
            ]) ?>
        </div>
        <div class="col-md-4">
        <?= $form->field($model, 'date_in')->widget(DatePicker::classname(), [
            'options' => ['class' => 'form-control'],
            'language' => 'ru',
            'dateFormat' => 'yyyy-MM-dd',
        ]) ?>
        </div>
    </div>


    <?= $form->field($model, 'desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'year_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Year::find()->all(), 'id', 'name'),
            'options' => [
                'placeholder' => Yii::t('student-status', 'Select a year ...'),
                'id' => 'year-id'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]
    )
    ?>

    <?= $form->field($model, 'group_id')->widget(DepDrop::classname(), [
        'options' => ['id' => 'group-id'],
        'type' => DepDrop::TYPE_SELECT2,
        'pluginOptions' => [
            'depends' => ['year-id'],
            'placeholder' => Yii::t('student-status', 'Select a group ...'),
            'url' => Url::to(['/backend/student-status//get-json-group'])
        ]
    ]);
    ?>

    <?= $form->field($model, 'profile_id')->widget(DepDrop::classname(), [
        'options' => ['id' => 'profile-id'],
        'type' => DepDrop::TYPE_SELECT2,
        'pluginOptions' => [
            'depends' => ['year-id', 'group-id'],
            'placeholder' => Yii::t('student-status', 'Select a student ...'),
            'url' => Url::to(['/backend/student-status//get-json-profiles'])
        ]
    ]); ?>

    <?= $form->field($model, 'semestr')->dropDownList([
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',

    ], ['prompt' => '']) ?>







    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('buttons', 'Create') : Yii::t('buttons', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
